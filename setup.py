#! /usr/bin/python
# -*- coding: utf-8 -*-

from typing import List
from setuptools import setup, find_packages

PACKAGE_VERSION = '4.0.0'


def parse_requirements(path: str = 'requirements.txt') -> List[str]:
    with open(path) as f:
        return f.readlines()


def write_version_py(filename='src/main/python/version.py'):
    cnt = """
# THIS FILE IS GENERATED FROM SETUP.PY
version = '{}'
"""
    with open(filename, 'w') as f:
        f.write(cnt.format(PACKAGE_VERSION))


write_version_py()

setup(
    name="mobigen",
    version=PACKAGE_VERSION,
    description='',  # TODO
    url='https://gitlab.com/intelliseq/mobigen/',
    author='@wojciech-galan',
    install_requires=parse_requirements(),
    packages=find_packages(),
    classifiers=[
        'Operating System :: POSIX :: Linux',
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6'
    ],
    entry_points = {
        'console_scripts':[
            'mobigen = src.main.python.main:main',
        ]
    },
    include_package_data=True
)
