#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
tag=1.4.1
if [[ -d ${tempdir} ]]; then
    rm -rf $tempdir/*
else
    mkdir $tempdir
fi

source venv/bin/activate
python src/main/python/gnomad_based_allele_frequency.py
deactivate

mkdir $tempdir/data
mkdir -p $tempdir/src/main/resources/vitalleo_traits
mkdir -p $tempdir/src/main/resources/vitalleo_actionable
mkdir -p $tempdir/src/main/resources/pgs_traits
mkdir -p $tempdir/src/main/resources/pgs_data
mkdir -p $tempdir/src/main/resources/allele_frequencies
mkdir -p $tempdir/src/main/python

ln src/main/resources/vitalleo_traits/*.py $tempdir/src/main/resources/vitalleo_traits
ln src/main/resources/vitalleo_actionable/*.py $tempdir/src/main/resources/vitalleo_actionable
ln src/main/resources/pgs_traits/*.py $tempdir/src/main/resources/pgs_traits
ln src/main/resources/pgs_data/* $tempdir/src/main/resources/pgs_data
ln src/main/resources/allele_frequencies/* $tempdir/src/main/resources/allele_frequencies
ln requirements.txt $tempdir/
rsync -av --progress src/main/python/ $tempdir/src/main/python --exclude notebooks
ln data/Axiom_PMDA.na36.r6.a8.annot.csv $tempdir/data/

docker build --no-cache -t intelliseqarray/desc_language:$tag $tempdir -f src/main/docker/desc_language/$tag/Dockerfile
docker push intelliseqarray/desc_language:$tag

rm -rf $tempdir