#!/usr/bin/env bash

docker build --no-cache -t intelliseqarray/desc_language:1.2.1 . -f src/main/docker/desc_language/1.2.1/Dockerfile