#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -rf $tempdir/*
else
    mkdir $tempdir
fi

mkdir -p $tempdir/src/main/
mkdir -p $tempdir/src/main/resources

ln requirements.txt $tempdir/
cp -r src/main/python $tempdir/src/main/
cp -r src/main/resources/vitalleo_traits $tempdir/src/main/resources
cp -r src/main/resources/vitalleo_actionable $tempdir/src/main/resources
ls $tempdir

docker build --no-cache -t intelliseqarray/desc_language:1.3-thin $tempdir -f src/main/docker/desc_language/1.3-thin/Dockerfile
docker push intelliseqarray/desc_language:1.3-thin

rm -rf $tempdir