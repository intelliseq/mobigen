#!/usr/bin/env bash

docker build --no-cache -t intelliseqarray/gnomad_allele_frequencies:1.1.1 . -f src/main/docker/gnomad_allele_frequencies/1.1.1/Dockerfile