import logging
import pathlib
from typing import Dict
from typing import List
from typing import Union
from src.main.python.lib import mobigen_utils
from src.main.python.lib.data_access.dto import SnpData
from src.main.python.lib.data_access.dto import SnpDataManySamples

logger = logging.getLogger('description_language.' + __name__)


class VcfAccessor(object):
    def __init__(self, gzipped_vcf_paths: List[str]):
        super().__init__()
        self.sample_names = mobigen_utils.get_sample_names(gzipped_vcf_paths)
        self.files = {path_to_fname_stem(path):path for path in gzipped_vcf_paths}
        self.__data: Dict[str, Dict[str:SnpData]] = {}  # dictionary rsid:{sample_name:ModelSnpData}

    def __get_data_for_given_rsid(self, rsid) -> Dict[str, SnpData]:
        line = get_vcf_line_for_rsid(rsid, self.files)
        if not line:
            logger.debug(f'No line for rsid {rsid} found')
            raise DataNotPresentError
        data = mobigen_utils.get_genotypes(line, self.sample_names)
        self.__data[rsid] = {sample_name: SnpData(data.ref, data.alts, genotype) for sample_name, genotype in data.genotypes.items()}
        return self.__data[rsid]

    def get_data_for_sample(self, sample_name:str, rsid:str) -> SnpData:
        try:
            return self.__data[rsid][sample_name]
        except KeyError:
            try:
                return self.__get_data_for_given_rsid(rsid)[sample_name]
            except DataNotPresentError:
                return None


class DataNotPresentError(RuntimeError):
    pass


def get_vcf_line_for_rsid(rs_id:str, files_dict:Dict[str, str]) -> Union[None, str]:
    rsid_end = rs_id[-3:]
    try:
        f_path = files_dict[rsid_end]
    except KeyError:
        raise DataNotPresentError
    id_ = rs_id[2:]
    line = mobigen_utils.extract_line_containing_rs_id(f_path, rsid_end, id_)
    return line if not line.startswith('#') else None


def path_to_fname_stem(path:str) -> str:
    return pathlib.PurePath(path).name.split('.')[0]