#!/bin/bash
#biobank_id=$1
#pgs and biobank data directory on anakin: $2

while read line
do
    rsid=$( echo $line | cut -f1 -d ' ' )
    ref=$( echo $line | cut -f2 -d ' ' )
    alt=$( echo $line | cut -f3 -d ' ' )
    weight=$( echo $line | cut -f4 -d ' ' )
    line1=$( zq $2/minimal_hg19.tab.gz $rsid  | awk -v ref=$ref -v alt=$alt  '{ $4 == ref && split($5,a,","); for ( i in a ) if (a[i] == alt ) print $3, $4, a[i]};' )
    if [[ ! $line1 =~ rs  ]]
          then
              error=$( zq $2/minimal_hg19.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" )
              if [[ $error =~ "MY ERROR" ]]
              then
                   line1=$( zgrep "$rsid" $2/minimal_hg19.tab.gz  | awk -v rsid=$rsid -v ref=$ref -v alt=$alt  '{$3 == rsid && $4 == ref && split($5,a,","); for ( i in a ) if (a[i] == alt ) print $3, $4, a[i]};' )
              fi
    fi
    if [[ $line1 =~ rs ]]
    then
          ref19=$( echo $line1 | cut -f2 -d ' ' )
          alt19=$( echo $line1 | cut -f3 -d ' ' )
    else
          rsid=none
    fi
    if [ $rsid != none ]
    then
          result=$( zq $2/minimal_hg38.tab.gz $rsid | awk -v ref19=$ref19 -v alt19=$alt19 '{$4 == ref19 && split($5,a,","); for ( i in a ) if ( a[i] == alt19 ) {print "ok"} }'; )
          if [[ -n "$result" ]]
          then
              echo $rsid $alt $weight >> $1.ready_to_freq
          else
                  error=$( zq $2/minimal_hg38.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" )
                  if [[ $error =~ "MY ERROR" ]]
                  then
                      result=$( zgrep "$rsid" $2/minimal_hg38.tab.gz  | awk -v ref19=$ref19 -v alt19=$alt19 -v rsid=$rsid '{$3 == rsid && $4 == ref19 && split($5,a,","); for ( i in a ) if ( a[i] == alt19 ) {print "ok"} }'; )
                  fi
                  if  [[ -n "$result" ]]
                  then
                     echo $rsid $alt $weight >> $1.ready_to_freq
                  fi
          fi
    fi

done <$1.to_dbsnp

