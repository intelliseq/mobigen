#!/bin/bash
SOURCE_DIR=$(dirname ${BASH_SOURCE[0]})
PLINK=$3 ## "/opt/tools/plink"

read  -p 'Is high score associated with higher risk? (Enter y for yes, n for no) ' hh

FILE=$1
DATA_DIR=$2 ## /data/public/intelliseqngs/mobigen/pgs_traits
biobank_id=$( ls $FILE | cut -f1 -d '.' )

if [[ ! -e biobank_traits ]]
then
    mkdir biobank_traits
fi

if [[ ! -e biobank_data ]]
then
   mkdir biobank_data
fi


## Add rsid and some useful data ########################################################################################
paste <( zcat $DATA_DIR/variants.tsv.bgz | cut -f2-6 ) <( zcat $FILE ) | gzip > "$biobank_id".all-gwas-data.tsv.gz

## Remove low confidence variants, keep only significantly associated snps ##############################################
zcat  "$biobank_id".all-gwas-data.tsv.gz | awk -F "\t" '$1=="chr" || $10 =="false" && $17 < 5e-08' > "$biobank_id".sig-data.tsv

## GWAS analyses are for hg19 genome, alt allele is always the effect allele
## Check consistency of ref and alt alleles in dbSNP hg19 and hg38
awk -F "\t" 'BEGIN{OFS="\t"}; NR==1 { for (i=1; i<=NF; i++) { f[$i] = i }} NR!=1 { print $(f["rsid"]),$(f["ref"]),$(f["alt"]),$(f["beta"])} ' "$biobank_id".sig-data.tsv | cut -f 1-5  > "$biobank_id".to_dbsnp
$SOURCE_DIR/get_rsid_biobank.sh $biobank_id $DATA_DIR

## Prepare json with gnomAD data
split -l 1000  --additional-suffix=-splitted  "$biobank_id".ready_to_freq
for f in *-splitted
do
  rsids=$( grep -o -e 'rs[0-9][0-9]*' $f | tr "\n" "," | rev | cut -c 2- | rev )
  docker run --user $(id -u):$(id -g) --rm intelliseqarray/gnomad_allele_frequencies:1.0.1  --rsid $rsids > $f.json
done
jq -s 'add' *-splitted.json > "$biobank_id"_freq.json
rm *-splitted*

### Adding freq data (in python => easier for me to match records properly) #############################################

for pop in nfe #eas afr amr asj fin overall
do
    python3 $SOURCE_DIR/adding_freq_from_gnomad.py -s $biobank_id -p $pop

## Prepare file for variants clumping: rsid and p-values, but only for variants that passed all steps ###################
    cut -f1 "$biobank_id"_"$pop".ready_to_simulations -d ','  > "$biobank_id"_"$pop".good_rs
    head -1 "$biobank_id".sig-data.tsv | cut -f5,17 >  "$biobank_id"_"$pop".ready_for_clumping
    grep -w -f "$biobank_id"_"$pop".good_rs "$biobank_id".sig-data.tsv | cut -f5,17 >> "$biobank_id"_"$pop".ready_for_clumping

## clump data ###########################################################################################################
    $PLINK --clump "$biobank_id"_"$pop".ready_for_clumping --clump-p1 5e-08 --clump-r2 0.1 --clump-kb 1000 --clump-snp-field rsid --clump-field pval --vcf $DATA_DIR/eur.phase3.biobank.set.vcf.gz  --allow-extra-chr
    sed -e 's/ \+/\t/g' plink.clumped | sed 's/^\t//' | cut -f3 | grep -o 'rs[0-9]*' > "$biobank_id"_"$pop".index.rsid

## Get to simulations only index snps
   grep -w -f "$biobank_id"_"$pop".index.rsid  "$biobank_id"_"$pop".ready_to_simulations > "$biobank_id"_"$pop"_clumped.ready_to_simulations

## Run simulations ######################################################################################################
    python3 $SOURCE_DIR/pgs_simulations.py $SOURCE_DIR/"$biobank_id"_"$pop"_clumped.ready_to_simulations  beta

###  writing model ######################################################################################################

    upper=$( grep 'upper' stats_"$biobank_id"_"$pop"_clumped.tsv  | cut -f2 -d ' ' )
    lower=$( grep 'lower' stats_"$biobank_id"_"$pop"_clumped.tsv  | cut -f2 -d ' ' )
    pop1=$( echo $pop | sed  's/overall//' )
    pop2=_$pop
    pop3=$( echo $pop2 | sed  's/_overall//' )

    python3 $SOURCE_DIR/write_coefficients.py -i "$biobank_id"_"$pop"_clumped.ready_to_simulations -t beta
    if [ $hh = y ]
        then
            cat <( head -35 beta_model_template_hh.txt | sed "s/LOWER/$lower/g" | sed "s/UPPER/$upper/g" | sed "s/BIOBANK_ID/$biobank_id/" | sed "s/POPULATION/$pop1/" ) <( cat formatted_"$biobank_id"_"$pop"_clumped.ready_to_simulations ) <( tail -n +37  beta_model_template_hh.txt ) > biobank_traits/"$biobank_id""$pop3"_model.py
        elif [ $hh = n ]
        then
            cat <( head -35 beta_model_template_hl.txt | sed "s/LOWER/$lower/g" | sed "s/UPPER/$upper/g" | sed "s/BIOBANK_ID/$biobank_id/" | sed "s/POPULATION/$pop1/" ) <( cat formatted_"$biobank_id"_"$pop"_clumped.ready_to_simulations ) <( tail -n +37 beta_model_template_hl.txt ) > biobank_traits/"$biobank_id""$pop3"_model.py
    fi

    #rm "$PGS_id"_"$pop".ready_to_simulations formatted_"$PGS_id"_"$pop".ready_to_simulations
    mv "$biobank_id"_"$pop"_clumped_data.json biobank_data/"$biobank_id""$pop3"_data.json
done

#rm  "$PGS_id".ready_to_freq "$PGS_id"_freq.json stats_"$PGS_id"*







