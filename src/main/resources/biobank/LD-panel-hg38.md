### Prepare panel data for LD analysis
   
1) Download 1000G GRCh38 data:  
```bash
for i in {1..22} X
do
  wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.chr"$i".shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz
  wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.chr"$i".shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz.tbi
done


wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/integrated_call_samples_v3.20130502.ALL.panel
```

2) Subtract genotypes for EUR non FIN population (404 samples): 

```bash 
grep 'EUR' integrated_call_samples_v3.20130502.ALL.panel | grep -v 'FIN' | cut -f1 > eur.samples

for i in  ALL.chr*.vcf.gz
do
   chr=$( ls $i | cut -f2 -d '.' )
   bcftools view --samples-file eur.samples $i -o eur."$chr".phase3.vcf.gz -O z
   tabix -p vcf eur."$chr".phase3.vcf.gz;
done
```
3) Get dbSNP vcf file
   
```bash
wget https://ftp.ncbi.nih.gov/snp/latest_release/VCF/GCF_000001405.38.gz
wget https://ftp.ncbi.nih.gov/snp/latest_release/VCF/GCF_000001405.38.gz.tbi
```

4) Annotate vcfs with rsIDs   
  * contigs names from: https://www.ncbi.nlm.nih.gov/grc/human/data?asm=GRCh38.p12 (saved as `dbsnp-contigs.txt`),
* create chromosom-wise dbsnp files
* prepare reference fasta for indel normalisation
* normalise indel in dbSNP vcfs
* add rsID to 1000G vcfs
   
```bash
## change chromosome names (in dbSNP vcf)
while read f
do
  old=$( echo $f | cut -f2 -d ' ')
  new=$( echo $f | cut -f1 -d ' ')
  tabix -h GCF_000001405.38.gz "$old" | sed "s/$old/$new/" | bgzip > chr"$new".dbsnp.vcf.gz
  tabix -p vcf chr"$new".dbsnp.vcf.gz
done<dbsnp-contigs.txt
rm GCF_000001405.38.gz

## prepare reference fasta for indel normalisation
ref_file="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.gz"
for i in {1..22} X Y M; do samtools faidx $ref_file chr"$i" | sed 's/>chr/>/' | bgzip >> grch38-ref.fa.gz; done
samtools faidx grch38-ref.fa.gz

## realign indels in the dbSNP vcfs (indels in the 1000g data are realigned)
for i in {1..22} X
do
  bcftools norm -c x -f grch38-ref.fa.gz chr"$i".dbsnp.vcf.gz -o chr"$i".dbsnp.norm.vcf.gz -O z
  tabix -p vcf chr"$i".dbsnp.norm.vcf.gz
done

## annotate with rsIDs
for i in {1..22} X
do
     bcftools annotate eur.chr"$i".phase3.vcf.gz -c "ID" -a  chr"$i".dbsnp.norm.vcf.gz --collapse none -o chr"$i".eur.rsid.vcf.gz -O z 
     tabix -p vcf chr"$i".eur.rsid.vcf.gz
done
#rm chr*.dbsnp.*vcf.gz*
```

5) Merge biallelic lines into multiallelic lines and remove duplicated lines (plink fails when duplicated rsids are present)   
```bash
for i in  chr*.eur.rsid.vcf.gz 
do
  chr=$( ls $i | cut -f2 -d '.' )
  bcftools norm -m +any $i | bcftools norm -d none -o eur."$chr".rsid.filtered.vcf.gz -O z
  tabix -p vcf eur."$chr".rsid.filtered.vcf.gz
done
```
  
6) After this treatment some duplicated rsids  may still remain (it was problem for the original b37 1000G data) - identify them and remove (if necessary)  
```bash
for i in eur.chr*.rsid.filtered.vcf.gz ; do zcat $i | grep -v '^#'|  cut -f3  | tr ';' "\n" >> all.rsid; done
sort all.rsid | uniq -d > duplicated.rsid

for i in eur.chr*.rsid.filtered.vcf.gz
do
  chr=$( ls "$i" | cut -f2 -d '.' )
  bcftools filter -e 'ID = @duplicated.rsid' $i -o eur."$chr".rsid.filtered.uniquersid.vcf.gz -O z
  tabix -p vcf eur."$chr".rsid.filtered.uniquersid.vcf.gz
done
```

7) Merge output files - merged vcf can be used for any plink clumping analysis and is located in `/data/public/intelliseqngs/mobigen/pgs_data`  
```bash
files1=$( ls eur.chr[1-9].phase3.filtered.uniquersid.vcf.gz )
files2=$( ls eur.chr[1-2][0-9].phase3.filtered.uniquersid.vcf.gz )
files3=$( ls eur.chrX.phase3.filtered.uniquersid.vcf.gz )
bcftools concat $files1 $files2 $files3 -o /data/public/intelliseqngs/mobigen/pgs_data/eur.phase3.hg38.vcf.gz -O z 
```
8) For Biobank analysis: I think that it should be sufficient to subtract lines with rsids from Biobank panel and use this smaller file in plink analysis   
```bash
zcat /data/public/intelliseqngs/mobigen/pgs_data/variants.tsv.bgz | tail -n +2 | cut -f6 > gwas.biobank.rsid   
bcftools filter -i 'ID = @gwas.biobank.rsid' /data/public/intelliseqngs/mobigen/pgs_data/eur.phase3.hg38.vcf.gz \
                  -o /data/public/intelliseqngs/mobigen/pgs_data/eur.phase3.hg38.biobank.set.vcf.gz -O z
tabix -p vcf /data/public/intelliseqngs/mobigen/pgs_data/eur.phase3.hg38.biobank.set.vcf.gz
```


