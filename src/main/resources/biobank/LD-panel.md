### Prepare panel data for LD analysis
   
1) Download 1000G phase3 data:  
```bash
for i in {1..22}
do
  wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/ALL.chr"$i".phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
  wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/ALL.chr"$i".phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz.tbi
done

wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/ALL.chrX.phase3_shapeit2_mvncall_integrated_v5b.20130502.genotypes.vcf.gz
wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/ALL.chrX.phase3_shapeit2_mvncall_integrated_v5b.20130502.genotypes.vcf.gz.tbi
wget  ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/integrated_call_samples_v3.20130502.ALL.panel
```

2) Subtract genotypes for EUR non FIN population: 
```bash 
grep 'EUR' integrated_call_samples_v3.20130502.ALL.panel | grep -v 'FIN' | cut -f1 > eur.samples

for i in  ALL.chr*.vcf.gz
do
   chr=$( ls $i | cut -f2 -d '.' )
   bcftools view --samples-file eur.samples $i -o eur."$chr".phase3.vcf.gz -O z
   tabix -p vcf eur."$chr".phase3.vcf.gz;
done
```

3) Join biallelic lines into multiallelic lines and remove duplicated lines (plink fails when duplicated rsids are present)   
```bash
for i in  eur.chr*.phase3.vcf.gz 
do
  chr=$( ls $i | cut -f2 -d '.' )
  bcftools norm -m +any $i | bcftools norm -d none -o eur."$chr".phase3.filtered.vcf.gz -O z
  tabix -p vcf eur."$chr".phase3.filtered.vcf.gz
done
```
4) After this treatment some duplicated rsids still remain - identify them and remove  
```bash
for i in eur.chr*.phase3.filtered.vcf.gz ; do zcat $i | grep -v '^#'|  cut -f3  >> all.rsid; done
sort all.rsid | uniq -d > duplicated.rsid

for i in eur.chr*.phase3.filtered.vcf.gz
do
  chr=$( ls "$i" | cut -f2 -d '.' )
  bcftools filter -e 'ID = @duplicated.rsid' $i -o eur."$chr".phase3.filtered.uniquersid.vcf.gz -O z
  tabix -p vcf eur."$chr".phase3.filtered.uniquersid.vcf.gz
done
```
5) Merge output files - merged vcf can be used for any plink clumping analysis and is located in `/data/public/intelliseqngs/mobigen/pgs_traits`  
```bash
files1=$( ls eur.chr[1-9].phase3.filtered.uniquersid.vcf.gz )
files2=$( ls eur.chr[1-2][0-9].phase3.filtered.uniquersid.vcf.gz )
files3=$( ls eur.chrX.phase3.filtered.uniquersid.vcf.gz )
bcftools concat $files1 $files2 $files3 -o /data/public/intelliseqngs/mobigen/pgs_traits/eur.phase3.vcf.gz -O z 
```
6) For Biobank analysis: I think that it should be sufficient to subtract lines with rsids from Biobank panel and use this smaller file in plink analysis   
```bash
zcat /data/public/intelliseqngs/mobigen/pgs_traits/variants.tsv.bgz | tail -n +2 | cut -f6 > gwas.biobank.rsid   
bcftools filter -i 'ID = @gwas.biobank.rsid' /data/public/intelliseqngs/mobigen/pgs_traits/eur.phase3.vcf.gz \
                  -o /data/public/intelliseqngs/mobigen/pgs_traits/eur.phase3.biobank.set.vcf.gz -O z
tabix -p vcf /data/public/intelliseqngs/mobigen/pgs_traits/eur.phase3.biobank.set.vcf.gz
```
