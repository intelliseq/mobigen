### Preparing polygenic trait model from Biobank GWAS data  

List of traits and files that can be downloaded can be found [here](https://docs.google.com/spreadsheets/d/1kvPoupSzsSFBNSztMzl04xMoSC3Kcx3CrjVf4yBmESU/edit?ts=5b5f17db#gid=227859291) (sheet: `Manifest 201807`).  
The same file contains also methods description, (sheet `README`).  
Download command example:  
```bash
wget https://broad-ukb-sumstats-us-east-1.s3.amazonaws.com/round2/additive-tsvs/22126.gwas.imputed_v3.both_sexes.tsv.bgz  \
-O 22126.gwas.imputed_v3.both_sexes.tsv.bgz
```   
where `22126` denotes Biobank UK phenotype code.   
Mapping from phenotype code to phenotype description can be found in the above file (sheet `Description Lookup`).  
Detailed trait description can be obtained from `https://biobank.ctsu.ox.ac.uk/crystal/field.cgi?id=22126` (after substituting `id=22126` with the appropriate phenotype code)  
Information about trait SNP heritability can be obtained from `https://nealelab.github.io/UKBB_ldsc/h2_summary_22126.html` (after trait code substitution)

Important: GWAS was analysed on hg19 reference genome, alt allele was always used as an effect allele, all result files have the same set of markers used      
To prepare model and plot data files:  
1) Get all necessary scripts and empty model templates:  
```bash
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/biobank/biobank-get-data.sh
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/biobank/get_rsid_biobank.sh
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/PGS/adding_freq_from_gnomad.py #PGS dir
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/PGS/pgs_simulations.py  #PGS dir
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/PGS/write_coefficients.py #pgs dir
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/biobank/beta_model_template_hh.txt
wget https://gitlab.com/intelliseq/mobigen/-/raw/develop/src/main/resources/biobank/beta_model_template_hl.txt
```
You will also need plink1.9 (path on anakin: `/opt/tools/plink`)   
and huge 1000G and dbSNP resource files (path on anakin: `/data/public/intelliseqngs/mobigen/pgs_data`)  

2) Download Biobank GWAS file to the same directory  
3) Run `biobank-get-data.sh` script with input `Biobank Gwas file` as first positional argument and paths to resource dir and plink as second and third positional arguments   
```bash
chmod +x biobank-get-data.sh
chmod +x get_rsid_biobank.sh
./biobank-get-data.sh Biobank-GWAS-file /data/public/intelliseqngs/mobigen/pgs_data /opt/tools/plink

```
 
Note, that at the beginning script will ask you:   
 `Is high score associated with higher risk? (Enter y for yes, n for no)` - and you have to decide. 

After analysis two directories will be created in the working directory: `biobank_data` (with plot data) and `biobank_traits` (with models)   
and lots of other files that can be now removed (that is, if result looks ok)  