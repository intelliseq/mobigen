from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_value
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

# zmieniony schemat opisow
# POLA DLA ISEQ I NOTATKI (SKAD DOKLADNIE DANE, CO USUNELAM, LICZEBNOSC KOHORT) I TAKIE TAM

trait_title = "Warfarin effective dose"
trait_pmids = ["19300499"] #Table2; combined results
trait_heritability = None  # as fraction
trait_snp_heritability = None  # as fraction
trait_explained = 0.41  
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
#The mean warfarin dose [mg/week]given to a patient during series of three INR measurements between 2 and 3;
#INR: the ratio of the time required for a patient's blood to coagulate relative to that of a reference sample (desired INR ranges from 2 to3)
#Thus the mean dose may be considered as the effective dose of warfarin for a given patient; low warfarin metabolism => low values
#GWAS statistics (and model) from Table2 – combined results
#Association was performed on squared roots of warfarin doses, but table 2 description says that beta reflects effect that variants have on dose
#Population average [sum of 2*EAF*beta] = -0.84, where EAF - frequencies of the effect alleles in gnomad controls database for nfe
#N=1,053 Swedish patient
#Values returned by the model reflects proposed adjustments to the worfarine dose due to patients genotype 
#rs1057910(C)=CYP2C9*3, Ile359Leu; rs1799853(T)=CYP2C9*3,Arg144Cys; rs2108622(T)=CYP4F2, Val433Met


# MODEL
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_value(
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs9923231', effect_allele='T', beta_value=-0.97),
                        beta(rs_id='rs1057910', effect_allele='C', beta_value=-1.11),
                        beta(rs_id='rs1799853', effect_allele='T', beta_value=-0.54),
                        beta(rs_id='rs2108622', effect_allele='T', beta_value=0.21)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

genes = ["VKORC1","CYP2C9","CYP4F2"] 
