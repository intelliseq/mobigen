#!/bin/python3

import sys
f=open(sys.argv[1])
build = sys.argv[2]

if ("GRCh37" in build) or ("hg19" in build):
    assembly = "GCF_000001405.25"
elif  ("GRCh38" in build) or ("hg38" in build) :
    assembly = "GCF_000001405.38"
elif ("NCBI36" in build) or ("hg18" in build):
    assembly = "GCF_000001405.12"
elif ("NCBI35" in build) or ("hg17" in build):
    assembly = "GCF_000001405.11"
elif ("NCBI34" in build) or ("hg16" in build):
    assembly = "CF_000001405.10"

f2=open("dbsnp_api.sh", "w")
f2.write("#!/bin/bash\n")

for line in f:
    l = line.strip().split()
    chr = l[0]
    pos = l[1]
    ref = l[2]
    alt =l[3]
    effect = l[4]
    text = chr+"\t"+pos+"\t.\t"+ref+"\t"+alt+"\t"+effect
    text1 = chr+"\t"+pos+"\t.\t"+alt+"\t"+ref+"\t"+effect
    command = '''curl -X POST "https://api.ncbi.nlm.nih.gov/variation/v0/vcf/file/set_rsids?assembly='''+assembly+'''" -H  "accept: text/plain; charset=utf-8" -H  "Content-Type: text/plain; charset=utf-8" -d "'''+text+'''" | cut -f 3,5- >>  "$1".not_filtered\n'''
    command1 = '''curl -X POST "https://api.ncbi.nlm.nih.gov/variation/v0/vcf/file/set_rsids?assembly='''+assembly+'''" -H  "accept: text/plain; charset=utf-8" -H  "Content-Type: text/plain; charset=utf-8 " -d "'''+text1+'''" | cut -f 3,4,6- >>  "$1"_altered_alleles.not_filtered\n'''
    f2.write(command)
    f2.write(command1)
f2.close()
print("curl commands ready!")
