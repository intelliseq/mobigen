#!/bin/bash
#PGS_id=$1
#pattern=$2
#pgs_data directory on anakin: $3

while read line
do
    chr=$( echo $line | cut -f1 -d ' ' )
    pos=$( echo $line | cut -f2 -d ' ' )
    other=$( echo $line | cut -f3 -d ' ' )
    eff=$( echo $line | cut -f4 -d ' ' )
    weight=$( echo $line | cut -f5 -d ' ' )
    case $2 in
       hg19_no_rsid)
          line1=$( tabix $3/minimal_hg19.tab.gz $chr:$pos-$pos  | awk -v other=$other -v eff=$eff -v pos=$pos '{$2 == pos && $4 == other && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3, $4, a[i]};' )
          line2=$( tabix $3/minimal_hg19.tab.gz $chr:$pos-$pos | awk -v eff=$eff -v other=$other -v pos=$pos '{$2 == pos && $4 == eff && split($5,a,","); for ( i in a ) if ( a[i] == other ) print $3, $4, a[i]};' )
          if [[ $line1 =~ rs ]] && [[ ! $line2 =~ rs ]]
          then
              ref19=$( echo $line1  | cut -f2 -d ' ' )
              alt19=$( echo $line1 | cut -f3 -d ' ' )
              rsid=$( echo $line1 | cut -f1 -d ' ' )
          elif [[ ! $line1 =~ rs ]] && [[ $line2 =~ rs ]]
          then
              ref19=$( echo $line2  | cut -f2 -d ' ' )
              alt19=$( echo $line2 | cut -f3 -d ' ' )
              rsid=$( echo $line2 | cut -f1 -d ' ' )
          else
              rsid=none
          fi
          if [ $rsid != none ]
          then
              result=$( zq $3/minimal_hg38.tab.gz $rsid | awk -v ref19=$ref19 -v alt19=$alt19 '{$4 == ref19 && split($5,a,","); for ( i in a ) if ( a[i] == alt19 ) print "ok"}'; )
              if [[ -n "$result" ]]
              then 
                  echo $rsid $eff $weight >> $1.ready_to_freq
              else
                  error=$( zq $3/minimal_hg38.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" )
                  if [[  $error =~ "MY  ERROR" ]]
                  then
                      result=$( zgrep  $3/minimal_hg19.tab.gz | awk -v rsid=$rsid -v other=$other -v eff=$eff  '{$3 == rsid && $4 == other && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3, $4, a[i]};' )
                      if  [[ -n "$result" ]]
                      then
                          echo $rsid $eff $weight >> $1.ready_to_freq
                      fi 
                  fi
              fi
          fi
       ;;
       hg19_with_rsid) 
          rsid=$pos
          line1=$( zq $3/minimal_hg19.tab.gz $rsid  | awk -v other=$other -v eff=$eff  '{ $4 == other && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3, $4, a[i]};' )
          line2=$( zq $3/minimal_hg19.tab.gz $rsid  | awk -v other=$other -v eff=$eff  '{ $4 == eff && split($5,a,","); for ( i in a ) if (a[i] == other ) print $3, $4, a[i]};' )
          
          if [[ ! $line1 =~ rs  ]] &&  [[ ! $line2 =~ rs  ]]
          then
              error=$( zq $3/minimal_hg19.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" ) 
              if [[ $error =~ "MY ERROR" ]]
              then
                   line1=$( zgrep "$rsid" $3/minimal_hg19.tab.gz  | awk -v rsid=$rsid -v other=$other -v eff=$eff  '{$3 == rsid && $4 == other && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3, $4, a[i]};' )
                   line2=$( zgrep "$rsid"  $3/minimal_hg19.tab.gz | awk -v rsid=$rsid -v other=$other -v eff=$eff  '{$3 ==rsid && $4 == eff && split($5,a,","); for ( i in a ) if (a[i] == other ) print $3, $4, a[i]};' )
              fi         
          fi
          if [[ $line1 =~ rs ]] && [[ ! $line2 =~ rs ]]
          then
              ref19=$( echo $line1  | cut -f2 -d ' ' )
              alt19=$( echo $line1 | cut -f3 -d ' ' )
          elif [[ ! $line1 =~ rs ]] && [[ $line2 =~ rs ]]
          then
              ref19=$( echo $line2  | cut -f2 -d ' ' )
              alt19=$( echo $line2 | cut -f3 -d ' ' )
          else
              rsid=none
          fi
          if [ $rsid != none ]
          then
              result=$( zq $3/minimal_hg38.tab.gz $rsid | awk -v ref19=$ref19 -v alt19=$alt19 '{$4 == ref19 && split($5,a,","); for ( i in a ) if ( a[i] == alt19 ) {print "ok"} }'; )
              if [[ -n "$result" ]]
              then 
                  echo $rsid $eff $weight >> $1.ready_to_freq
              else
                  error=$( zq $3/minimal_hg38.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" )
                  if [[ $error =~ "MY ERROR" ]]
                  then
                      result=$( zgrep "$rsid" $3/minimal_hg38.tab.gz  | awk -v ref19=$ref19 -v alt19=$alt19 -v rsid=$rsid '{$3 == rsid && $4 == ref19 && split($5,a,","); for ( i in a ) if ( a[i] == alt19 ) {print "ok"} }'; )
                  fi
                  if  [[ -n "$result" ]]
                  then
                     echo $rsid $eff $weight >> $1.ready_to_freq
                  fi
              fi
          fi
       ;; 
       hg38_no_rsid)
          line1=$( tabix $3/minimal_hg38.tab.gz $chr:$pos-$pos  | awk -v other=$other -v eff=$eff -v pos=$pos '{$2 == pos && $4 == other && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3, $4, a[i]};' )
          line2=$( tabix $3/minimal_hg38.tab.gz $chr:$pos-$pos | awk -v eff=$eff -v other=$other -v pos=$pos '{$2 == pos && $4 == eff && split($5,a,","); for ( i in a ) if ( a[i] == other ) print $3, $4, a[i]};' )
          if [[ ! $line1 =~ rs  ]] &&  [[ ! $line2 =~ rs  ]]
          then
              error=$( zq $3/minimal_hg38.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" ) 
              if [[ $error =~ "MY ERROR" ]]
              then 
                   line1=$( zgrep "$rsid" $3/minimal_hg19.tab.gz  | awk -v rsid=$rsid -v other=$other -v eff=$eff  '{$3 == rsid && $4 == other && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3, $4, a[i]};' )
                   line2=$( zgrep "$rsid"  $3/minimal_hg19.tab.gz | awk -v rsid=$rsid -v other=$other -v eff=$eff  '{$3 ==rsid && $4 == eff && split($5,a,","); for ( i in a ) if (a[i] == other ) print $3, $4, a[i]};' )
              fi         
          fi

          if [[ $line1 =~ rs ]] && [[ ! $line2 =~ rs ]]
          then
              rsid=$( echo $line1 | cut -f1 -d ' ' )
          elif [[ ! $line1 =~ rs ]] && [[ $line2 =~ rs ]]
          then
              rsid=$( echo $line2 | cut -f1 -d ' ' )
          else
              rsid=none
          fi
          if [ $rsid != none ]
          then 
              echo $rsid $eff $weight >> $1.ready_to_freq
          fi
      ;; # tu jestem
      hg38_with_rsid)
          rsid=$pos
          line1=$( zq $3/minimal_hg38.tab.gz $rsid  | awk  -v eff=$eff  ' $4 == eff { print $3 }' )
          line2=$( zq $3/minimal_hg38.tab.gz $rsid  | awk  -v eff=$eff  '{ split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3};' )
          if [[ $line1 =~ rs ]] && [[ $line2 =~ rs ]]
          then
              rsid=none
          elif [[ ! $line1 =~ rs ]] && [[ ! $line2 =~ rs ]]
          then
              error=$( zq $3/minimal_hg38.tab.gz $rsid 2>/dev/null; STATUS=$(echo $?); $([ $STATUS == "1" ]) && echo "MY ERROR" )
              if [[ $error =~ "MY ERROR" ]]
              then
                 line1=$( zgrep $3/minimal_hg38.tab.gz $rsid  | awk -v rsid=$rsid  -v eff=$eff  '$3 == rsid && $4 == eff { print $3 }' )
                 line2=$( zq $3/minimal_hg38.tab.gz $rsid  | awk  -v eff=$eff  -v rsid=$rsid '{ $3 == rsid && split($5,a,","); for ( i in a ) if (a[i] == eff ) print $3};' )
                 if [[ $line1 =~ rs ]] && [[ $line2 =~ rs ]]
                 then
                      rsid=none
                 elif [[ ! $line1 =~ rs ]] && [[ ! $line2 =~ rs ]]
                 then
                      rsid=none
                 fi
              fi
          fi
          if [ $rsid != none ]
          then 
              echo $rsid $eff $weight >> $1.ready_to_freq
          fi
      ;;
    esac   
done <$1.to_dbsnp

