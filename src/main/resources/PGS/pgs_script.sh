#!/bin/bash

### meta-data compilation #########################################################################################################

genome_build=$( grep 'Original Genome Build' $1 | cut -f 2 -d '=' | sed 's/ //g' ) #needed for rsid matching
PGS_id=$( grep 'PGS ID' $1 | cut -f 2 -d '=' | sed 's/ //g' )  # needed ;)
trait_title=$( grep 'Reported Trait' $1 |  cut -f 2 -d '=' | sed 's/ //' | sed 's/ /_/g' )  #needed for plot 
trait_title_low=$(  echo $trait_title | tr A-Z a-z )
#echo $trait_title_low
trait_doi=$( grep 'Citation ' $1 | cut -f2 -d ':' )
trait_pmid=$( wget --quiet -O - "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?ids="$trait_doi"&versions=no&format=json" | jq .records[].pmid )
#echo $trait_pmid
rsID_check=$( grep -c 'rsID' $1 )
beta_check=$( grep -c 'effect_weight' $1 )      #needed for simulations and final model
ref_check=$( grep -c 'reference_allele' $1 )
effect_check=$( grep -c 'effect_allele' $1)

### PGS data check ################################################################################################################

if  [ $effect_check -eq 0 ]
then
    echo Effect alleles are not given.

elif  [ $rsID_check -lt 1 ] && [ $genome_build = NR ]
then
    echo Impossible to get RSIDs.

elif [ $rsID_check -lt 1 ] && [ $ref_check -lt 1 ]
then
    echo Impossible to get RSIDs.

elif [ $rsID_check -lt 1 ] && [[ ! GRCh37 =~ $genome_build ]] && [[ ! hg19 =~ $genome_build ]] && [[ ! GRCh38 =~ $genome_build ]] && [[ ! hg38 =~ $genome_build ]]
then
       echo Unknown genome build, impossible to get RSIDs.
    
else

###  valid PGS data ##################################################################################################################

### checking effect type ##############################################################################################################

    if  [ $beta_check -eq 1 ]  #needed for simulations
    then
        effect_type=beta
    else
       effect_type=or
    fi
 
    echo $rsID_check
    echo $genome_build
    echo $effect_type
    echo $trait_title
    read  -p 'Is high score associated with higher risk? (Enter y for yes, n for no) ' hh

    ### removing info lines and interaction terms #####################################################################################
 
    grep -v '^#' $1  | grep -v '_x_'  > $PGS_id.no_info

    ### preparing files for frequency check plus for simulations ######################################################################
    
    if [ $rsID_check -eq 1 ]
    then
        awk -F "\t" 'BEGIN{OFS="\t"}; NR==1 { for (i=1; i<=NF; i++) { f[$i] = i }} NR!=1 { print $(f["rsID"]),$(f["effect_allele"]), $(f["effect_weight"]),$(f["OR"])} ' $PGS_id.no_info |  cut -f 1-3  >  $PGS_id.ready_to_freq
    elif [ $rsID_check -eq 0 ] 
    then 
        awk -F "\t" 'BEGIN{OFS="\t"}; NR==1 { for (i=1; i<=NF; i++) { f[$i] = i }} NR!=1 { print $(f["chr_name"]),$(f["chr_position"]),$(f["reference_allele"]),$(f["effect_allele"]),$(f["effect_weight"]),$(f["OR"])} ' $PGS_id.no_info | cut -f 1-5  > $PGS_id.to_dbsnp
        #python3  write_curl_commands.py $PGS_id.to_dbsnp $genome_build
        #chmod 755 dbsnp_api.sh
        #./dbsnp_api.sh $PGS_id 
        #grep  '^rs[0-9]' $PGS_id.not_filtered  >  $PGS_id.ready_to_freq 
        #grep  '^rs[0-9]' "$PGS_id"_altered_alleles.not_filtered  >>  $PGS_id.ready_to_freq
        #rm dbsnp_api.sh $PGS_id.to_dbsnp "$PGS_id"_altered_alleles.not_filtered $PGS_id.not_filtered
        ./get_rsid.sh $PGS_id $genome_build
        rm $PGS_id.to_dbsnp
    fi
    rm $PGS_id.no_info  

    ### creating jsons with gnomad frequencies ######################################################################################

    rsids=$( grep -o -e 'rs[0-9][0-9]*' $PGS_id.ready_to_freq | tr "\n" "," | rev | cut -c 2- | rev )
    docker run --user $(id -u):$(id -g) --rm intelliseqarray/gnomad_allele_frequencies:1.0.1  --rsid $rsids > "$PGS_id"_freq.json

    ### adding freq data (in python => easier for me to match records properly) ######################################################

    for pop in nfe eas afr amr asj fin overall
    do
        python3 adding_freq_from_gnomad.py -s $PGS_id -p $pop
         
    ### running simulations, creating files needed for plot ##########################################################################
    
       python3 pgs_simulations.py "$PGS_id"_"$pop".ready_to_simulations  $effect_type
    
    ###  plotting, just for me #######################################################################################################
    
        upper=$( grep 'upper' stats_"$PGS_id"_"$pop".tsv  | cut -f2 -d ' ' )
        lower=$( grep 'lower' stats_"$PGS_id"_"$pop".tsv  | cut -f2 -d ' ' )
        #Rscript R_commands.txt "$PGS_id"_"$pop"_data.tsv "$PGS_id"_"$pop".pdf $trait_title $lower $upper $effect_type 1.5
        #Rscript R_commands.txt "$PGS_id"_"$pop"_data.tsv "$PGS_id"_"$pop".pdf $trait_title $lower $upper $effect_type 1.5 $hh

    #### writing model ###############################################################################################################
        
        pop1=$( echo $pop | sed  's/overall//' )
        pop2=_$pop
        pop3=$( echo $pop2 | sed  's/_overall//' )

        if [ $effect_type = beta ]
        then
            python3 write_coefficients.py -i "$PGS_id"_"$pop".ready_to_simulations -t $effect_type
            if [ $hh = y ]
            then
                cat <( head -35 beta_model_template_hh.txt | sed "s/PMID/$trait_pmid/" | sed "s/TITLE/$trait_title/" | sed "s/PGS_ID/$PGS_id/" | sed "s/LOWER/$lower/g" | sed "s/UPPER/$upper/g" | sed "s/POPULATION/$pop1/" ) <( cat formatted_"$PGS_id"_"$pop".ready_to_simulations ) <( tail -n +37  beta_model_template_hh.txt ) > ../pgs_traits/"$trait_title_low"_"$PGS_id""$pop3"_model.py
            elif [ $hh = n ]
            then
                cat <( head -35 beta_model_template_hl.txt | sed "s/PMID/$trait_pmid/" | sed "s/TITLE/$trait_title/" | sed "s/PGS_ID/$PGS_id/" | sed "s/LOWER/$lower/g" | sed "s/UPPER/$upper/g" | sed "s/POPULATION/$pop1/" ) <( cat formatted_"$PGS_id"_"$pop".ready_to_simulations ) <( tail -n +37  beta_model_template_hl.txt ) > ../pgs_traits/"$trait_title_low"_"$PGS_id""$pop3"_model.py
            fi
        elif [ $effect_type = or ]
        then
            python3 write_coefficients.py -i "$PGS_id"_"$pop".ready_to_simulations -t $effect_type
            if [ $hh = y ]
            then
                cat <( head -35 odds_ratio_model_template_hh.txt | sed "s/PMID/$trait_pmid/" | sed "s/TITLE/$trait_title/" | sed "s/PGS_ID/$PGS_id/" | sed "s/LOWER/$lower/g" | sed "s/UPPER/$upper/g" | sed "s/POPULATION/$pop1/" ) <( cat formatted_"$PGS_id"_"$pop".ready_to_simulations ) <( tail -n +37  odds_ratio_model_template_hh.txt ) > ../pgs_traits/"$trait_title_low"_"$PGS_id""$pop3"_model.py
            elif [$hh = n ]
            then
                cat <( head -35 odds_ratio_model_template_hl.txt | sed "s/PMID/$trait_pmid/" | sed "s/TITLE/$trait_title/" | sed "s/PGS_ID/$PGS_id/" | sed "s/LOWER/$lower/g" | sed "s/UPPER/$upper/g" | sed "s/POPULATION/$pop1/" ) <( cat formatted_"$PGS_id"_"$pop".ready_to_simulations ) <( tail -n +37  odds_ratio_model_template_hl.txt ) > ../pgs_traits/"$trait_title_low"_"$PGS_id""$pop3"_model.py
            fi
        fi
        #rm "$PGS_id"_"$pop".ready_to_simulations formatted_"$PGS_id"_"$pop".ready_to_simulations
        mv "$PGS_id"_"$pop"_data.json ../pgs_data/"$trait_title_low"_"$PGS_id""$pop3"_data.json
    done
rm  "$PGS_id".ready_to_freq "$PGS_id"_freq.json
fi

