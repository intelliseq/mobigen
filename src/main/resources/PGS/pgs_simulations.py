import random
import sys
import numpy as np
import json
import os
##sys.argv[1]=name of the input_file where:
##1st_column=rs_id, 2nd_column=effect_allele(EA), 3rd_column=EA_freq, 4th_coulunn=effect size;
##sys.argv[2]=beta/or
##data needed to plot distribution of aggregated risk scores in simulated populations ==> input_file_name_data.tsv
##statistics needed for model ==> file stats_imput_file_neme.tsv
## usage(command line):
## python pge_simulations.py imput_file beta (for regression model)
## python pge_simulations.py imput_file or (for odds ratio)

random.seed(1979)

def prepare_simulation_data(f=sys.argv[1]):
    """reads file given as first command line argument
    calculates frequencies of genotypes for each locus, uses H-W law;
    returns data needed for simulations: for each locus:
    frequency of ea homozygotes, sum of frequencies of heterozygotes and ea homozygotes, effect (or/beta)"""
    simulation_data = []
    for i in open(f):
        line = i.strip().split(",")
        ea_fq=float(line[2])
        freq_hom_ea = ea_fq**2
        freq_het = 2*ea_fq*(1-ea_fq)
        simulation_data_line = (freq_hom_ea, freq_hom_ea +freq_het, float(line[3]))
        simulation_data.append(simulation_data_line)
    return simulation_data

def calculate_random_phenotype_score_for_beta(simulation_data):
    """calculates aggregated score (across all model loci) for one randomly chosen genotype;
     used for regression models (beta)"""
    score = 0.0
    for item in simulation_data:
        genotype_choice = random.random()
        if genotype_choice < item[0]:
            score += 2*item[2]
        elif genotype_choice < item[1]:
            score += item[2]
        else:
            score += 0.0
    return score

def calculate_random_phenotype_score_for_or(simulation_data):
    """calculates aggregated (across all model loci_ score for one randomly chosen genotype;
    used for odds ratio models (or)"""
    score = 1.0
    for item in simulation_data:
        genotype_choice = random.random()
        if genotype_choice < item[0]:
            score = score*item[2]
        elif genotype_choice < item[1]:
            score = score
        else:
            score = score*(1/item[2])
    return score

def get_distribution(simulation_data, model_type):
    """repeats score calculations for 1000000 'random individuals"""
    distribution = []
    if model_type == "beta":
        for i in range(100000):
            distribution.append(calculate_random_phenotype_score_for_beta(simulation_data))
    if model_type == "or":
        for i in range(100000):
            distribution.append(calculate_random_phenotype_score_for_or(simulation_data))
    return distribution

def prepare_bins(sorted_distribution_data,rsid_number):
    """prepares plot data from sorted distribution data"""
    l = len(sorted_distribution_data)
    MIN = sorted_distribution_data[0]
    #print(min)
    MAX = sorted_distribution_data[-1]
    #print(max)
    number_of_bins = min(rsid_number*3, 100)
    interval_length = (MAX - MIN)/number_of_bins
    #print(interval_length)
    start = MIN
    end = start + interval_length
    plot_x_data = []
    plot_y_data = []
    while end < (MAX + interval_length/20) :
        x = (start + 0.5*interval_length)
        #print(x)
        subdata = [ i for i in sorted_distribution_data if i >= start and i < end ]
        y  = len(subdata)/l
        #point = [x,y]
        plot_x_data.append(x)
        plot_y_data.append(y)
        start += interval_length
        end += interval_length
    plot_dict = {
        "x": plot_x_data,
        "y": plot_y_data
    }
    return plot_dict

     
 
def main():
    path = sys.argv[1]
    simulation_data = prepare_simulation_data(path)
    rsid_number = len(simulation_data)
    directory, fname = path.rsplit(os.sep, 1)
    name = fname.split(".")[0]
    model_type = sys.argv[2]
    distribution = get_distribution(simulation_data, model_type)
    distribution.sort()
    if  model_type == "or":
        log_dist = [ np.log(i) for i in distribution ]
        plot_data = prepare_bins(log_dist,rsid_number)
    elif model_type == "beta":
        plot_data = prepare_bins(distribution,rsid_number)
    #np.savetxt(name+"_data.tsv", plot_data, fmt='%.4e')
    f1 = open(name+"_data.json", "w")
    text_to_json = json.dumps(plot_data)
    f1.write(text_to_json)
    f1.close()
    with open(os.path.join(directory, "stats_"+name+".tsv"), "w") as f2:
        names=["min: ","lower: ","median: ","upper: ","max: "]
        numb=[distribution[0],distribution[9999],distribution[49999],distribution[89999],distribution[99999]]
        numb_formatted = [format(i, '.4f') for i in numb]
        for i in range(len(names)):
            t=names[i]+str(numb_formatted[i])+"\n"
            f2.write(t)
    print(sum(distribution)/float(len(distribution)))
    print(np.std(distribution))

if __name__=='__main__': main()
