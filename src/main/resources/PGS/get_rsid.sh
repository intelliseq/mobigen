#!/bin/bash
#PGS_id=$1
#build=$2

if [[ GRCh37 =~ $2 ]] || [[ hg19 =~ $2 ]]
then
    database=minimal_hg19.gz

elif [[ GRCh38 =~ $2 ]] || [[ hg38 =~ $2 ]]
then
    database=minimal_hg38.gz
fi    

while read line
do
    chr=$( echo $line | cut -f1 -d ' ' )
    pos=$( echo $line | cut -f2 -d ' ' )
    ref=$( echo $line | cut -f3 -d ' ' )
    eff=$( echo $line | cut -f4 -d ' ' )
    weight=$( echo $line | cut -f5 -d ' ' )
    rsid=$( tabix $database $chr:$pos-$pos  | awk -v ref=$ref -v eff=$eff -v pos=$pos '$2 == pos && $4 == ref && $5 == eff {print $3}' | head -1 )
    if [[ $rsid =~ rs ]]
    then
        echo  $rsid $eff $weight >> $1.not_filtered
    else 
       rsid1=$( tabix $database $chr:$pos-$pos  | awk -v eff=$eff -v ref=$ref -v pos=$pos '$2 == pos && $4 == eff && $5 == ref {print $3}' | head -1 )
       echo $rsid1 $eff $weight >>  $1.not_filtered
    fi
done <$1.to_dbsnp

###tabix $database $chr:$pos-$pos | awk -v alt=$alt -v ref=$ref '($4 == ref && $5 == alt) {print $1"\t"$3}' | head -1

grep '^rs[0-9]' $1.not_filtered  | sed 's/ /\t/g' > $1.ready_to_freq
rm $1.not_filtered
