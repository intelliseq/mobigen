#!/usr/bin/python3

import argparse
import sys
import json

parser = argparse.ArgumentParser(description='adds frequency data for given rsid')
parser.add_argument('--pgs_id', '-s', type=str, required=True, help='PGS file id, for example PGS000001')
parser.add_argument('--pop_id', '-p', type=str, required=True, help='gnomAD population abbreviation;\npossible values: nfe, eas, afr, amr, asj, fin, oth, overall' )
args = parser.parse_args()

f1 = open(args.pgs_id+".ready_to_freq")

f2 = open(args.pgs_id+"_freq.json")
frequency_dict = json.load(f2)
f2.close()

def check_pop_arg(arg):
    print(arg)
    if  arg not in 'nfe,eas,afr,amr,asj,fin,oth,overall':
        pop, AF_pop = arg, "AF"
        print("Warning: invalid population name, calculations performed for the overall allele frequencies.")
    elif  arg == 'overall':
        pop, AF_pop  = "overall","AF"
    else: 
        pop, AF_pop = arg,'AF_'+arg
    return pop, AF_pop
#output_file = open(args.pgs_id+"_"+pop+".ready_to_simulations", "w") 

#print(frequency_dict["rs10069690"])
def main():
    f1 = open(args.pgs_id+".ready_to_freq")
    pop, AF_pop = check_pop_arg(args.pop_id)
    output_file = open(args.pgs_id+"_"+pop+".ready_to_simulations", "w")
    for line in f1:
        rsid, effect_allele, effect = line.strip().split()
        try:
            frequency_dict[rsid]['ref']
        except KeyError:
            continue
        else:
            ref = frequency_dict[rsid]['ref']
            alt = frequency_dict[rsid]['alt']
            try:
                freq = float(frequency_dict[rsid][AF_pop])
            except TypeError:
                freq = None
            if alt == effect_allele and freq is not None:
                final_freq = freq
            elif ref == effect_allele and freq is not None:
                final_freq = 1 - freq
            else:
                final_freq = None
        if final_freq is None:
            output_line = ""
        else:
            output_line = rsid+","+effect_allele+","+str(final_freq)+","+effect+"\n"
        output_file.write(output_line)
    f1.close()
    output_file.close()
        

if __name__=='__main__': main()

