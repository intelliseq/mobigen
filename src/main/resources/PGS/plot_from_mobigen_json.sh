#!/bin/bash

SOURCE_DIR=$(dirname ${BASH_SOURCE[0]})
#$1 mobigen_output_json
#$2 model path, for example src.main.resources.pgs_traits
#plot saved in example breast_cancer_PGS000001_nfe_model.pdf file
path=$( echo $2 | sed 's/\./\\./g' )
models=$( grep "$path" $1 | cut -f1  -d ':' | rev | cut -f1 -d '.' | rev | sed 's/\"//' )
for model in $models
do
    echo $model
    paste <( sed 's/src.main.resources.pgs_traits.//' $1 | jq .$model.plot_data.x | grep -v '\[\|\]' | sed 's/,//' ) <( sed 's/src.main.resources.pgs_traits.//' $1 | jq .$model.plot_data.y | grep -v '\[\|\]' | sed 's/,//' ) > plot_data_file.txt
    plot_title=$( sed 's/src.main.resources.pgs_traits.//' $1 | jq .$model.trait_title | sed 's/\"//g' )
    lower_boundary=$( sed 's/src.main.resources.pgs_traits.//' $1 | sed 's/ risk/_risk/' | jq .$model.category_boundaries.Average_risk.from )
    upper_boundary=$( sed 's/src.main.resources.pgs_traits.//' $1 |  sed 's/ risk/_risk/' | jq .$model.category_boundaries.Average_risk.to )
    patient_score=$( sed 's/src.main.resources.pgs_traits.//' $1 | jq .$model.numerical_output )
    left_plot_category=$( sed 's/src.main.resources.pgs_traits.//' $1 | sed 's/ risk/_risk/' | jq .$model.category_on_the_left_side_of_plot | sed 's/\"//g' )
    Rscript $SOURCE_DIR/R_commands.txt plot_data_file.txt $model.svg $plot_title $lower_boundary $upper_boundary $patient_score $left_plot_category
    rm plot_data_file.txt
done


