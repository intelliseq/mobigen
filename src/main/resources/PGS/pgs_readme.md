## **This file describes how to make model from the PGS files**  
PGS files can be downloaded from:   
ftp://ftp.ebi.ac.uk/pub/databases/spot/pgs/ScoringFiles_formatted/   
    
Download and unpack chosen PGS file abbreviated below as PGS.txt.gz (for example PGS000001.txt.gz):      
```
wget ftp://ftp.ebi.ac.uk/pub/databases/spot/pgs/ScoringFiles_formatted/PGS.txt.gz   
zcat PGS.txt.gz > PGS.txt   
```

Download scripts:  

Commands to download all files:  
```bash
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/pgs_script_new.sh
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/get_rsid_new.sh
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/adding_freq_from_gnomad.py
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/pgs_simulations.py
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/write_coefficients.py
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/beta_model_template_hh.txt
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/beta_model_template_hl.txt
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/odds_ratio_model_template_hh.txt
wget https://gitlab.com/intelliseq/mobigen/-/raw/biobank-gwas/src/main/resources/PGS/odds_ratio_model_template_hl.txt
```

Prepare model:   
```
chmod +x pgs_script_new.sh
chmod +x get_rsid_new.sh
./pgs_script_new.sh PGS.txt path_to_ankin_data_directory   
```
where **path_to_anakin_data_directory** shows the directory (on anakin) which contains the necessary "big files":   
* minimal_hg19.tab.gz   
* minimal_hg38.tab.gz   
* minimal_hg19.tab.gz.tbi   
* minimal_hg38.tab.gz.tbi   
* minimal_hg19.tab.gz.zindex   
* minimal_hg38.tab.gz.zindex   
   
At the moment **path_to_anakin_data_directory** is:   
`/data/public/intelliseqngs/mobigen/pgs_data`   
   
Note:   
1) Not all PGS files contain enough data to prepare model      
(it is evaluated by the *pgs_script_new.sh* which in case of missing data prints info and quits)      
2) After start *pgs_script_new.sh*  prints trait name and asks user if high polygenic score is associated with higher risk   
Think, decide and enter `y` if yes or `n` if no 
   
Prepared model files are automatically located in **pgs_traits** directory    
Prepared plot data files (added later to the final output json) are located in the **pgs_data** directory   
Both directories (if not present) are created in the working directory by the *pgs_script_new.sh*   
   

   
  
