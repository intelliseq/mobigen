import argparse

parser = argparse.ArgumentParser(description='writes coefficients_and_snps_to be_consideret part of models')
parser.add_argument('--input', '-i', type=str, required=True, help='input file, comma deliminated:rsid,effect allele, allele frequency, effect')
parser.add_argument('--type', '-t', type=str, required=True, help='model type, possible values: beta, or')
args = parser.parse_args() 

def write_model_coefficients(f, type):
    coefficients =[]
    for line in open(f):
        l = line.strip().split(',')
        rsid, effect_allele, effect = l[0], l[1], l[3]
        if type == "beta":
            coefficient="                        beta(rs_id='"+rsid+"', effect_allele='"+effect_allele+"', beta_value="+effect+")"
        elif type == "or":
            coefficient="                        odds_ratio(rs_id='"+rsid+"', effect_allele='"+effect_allele+"', odds_ratio_value="+effect+")"
        coefficients.append(coefficient)
    text = ",\n".join(coefficients)+"\n"
    return text

def main():
    f = args.input
    text = write_model_coefficients(f,args.type)
    f2 = open("formatted_"+args.input, "w")
    f2.write(text)
    f2.close()
if __name__=='__main__': main()

