
#!/bin/bash

SOURCE_DIR=$(dirname ${BASH_SOURCE[0]})
#$1 mobigen_output_json
#plot saved in example breast_cancer_PGS000001_nfe_model.pdf file
number_of_models=$( cat $1 | jq .[].module | wc -w )
i=0
while [ $i -lt $number_of_models ]
do
    model=$( cat $1 | jq .[$i].module | rev | cut -f1 -d '.' | rev | sed 's/\"//' )
    paste <( cat $1 | jq .[$i].plot_data.x | grep -v '\[\|\]' | sed 's/,//' ) <( cat $1 | jq .[$i].plot_data.y | grep -v '\[\|\]' | sed 's/,//' ) > plot_data_file.txt
    plot_title=$( cat $1 | jq .[$i].trait_title | sed 's/\"//g' )
    lower_boundary=$(  sed 's/ risk/_risk/' $1 | jq .[$i].category_boundaries.Average_risk.from )
    upper_boundary=$(  sed 's/ risk/_risk/' $1 | jq .[$i].category_boundaries.Average_risk.to )
    patient_score=$( cat $1 | jq .[$i].numerical_output )
    left_plot_category=$( sed 's/ risk/_risk/' $1 | jq .[$i].category_on_the_left_side_of_plot | sed 's/\"//g' )
    patient_category=$( sed 's/ risk/_risk/' $1 | jq .[$i].categorical_output | sed 's/\"//g' )
    Rscript $SOURCE_DIR/R_commands.txt plot_data_file.txt $model.svg $plot_title $lower_boundary $upper_boundary $patient_score $left_plot_category $patient_category
    rm plot_data_file.txt
    ((i++))
done


