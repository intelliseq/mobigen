#!/usr/bin/env python3
import os
import re
import sys
import json
import shutil
import argparse
import tempfile
import importlib
from typing import Any
from typing import Set
from typing import Dict
from typing import Pattern
from typing import NamedTuple

__version__ = '0.0.1'

MODEL_RE:Pattern = re.compile('^model\s*=\s*Choice\s*\(\s*.+}\s*\)', re.DOTALL | re.MULTILINE)
CATEGORIES_RE:Pattern = re.compile('categories\s*=\s*\[.+?\]', re.DOTALL)
SNIPS_RE:Pattern = re.compile('coefficients_and_snps_to_be_considered\s*=\s*\[.*\]', re.DOTALL)
FUNCTION_RE:Pattern = re.compile('''function_to_apply\s*=\s*['"](.*)['"]''')
SNIP_RE:Pattern = re.compile('(beta|odds_ratio)\s*\(\s*(.+)\s*\)')
RSID_RE:Pattern = re.compile('''rs_id\s*=\s*['"](rs\d+)['"]''')
EFFECT_ALLELE_RE:Pattern = re.compile('''effect_allele\s*=\s*['"](.+)['"]''')
COEFF_RE:Pattern = re.compile('(beta_value|odds_ratio_value)\s*=\s*([-\d.]+)')
POPULATION_RE:Pattern = re.compile('^trait_was_prepared_for_population.+', re.MULTILINE)
VARIANRS_RE:Pattern = re.compile('variants\s*=\s*\[.+^\]', re.DOTALL | re.MULTILINE)
CATEGORY_NAME_RE = re.compile('''category_name\s*=\s*['"](.+)['"]''')

FUNCTION_TO_MODEL_TYPE_DICT = {
    'multiply':'odds_ratio',
    'add':'beta'
}


class ModelElementsStrings(NamedTuple):
    categories_str:str
    snips_str:str
    function_str:str


class SnipData(NamedTuple):
    rs_id:str
    effect_allele:str
    coeff_value:str

NEW_MODEL_STRING_TEMPLATE = \
'''
model = PolygenicRiskScore\
        (
            {},
            {},
            model_type='{}'
        )
'''

MODEL_FILE_TEMPLATE = \
'''from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

{}
{}
'''


def get_model_and_pop_string(model_path:str) -> str:
    with open(model_path) as f:
        content = f.read()
        model_found = MODEL_RE.search(content)
        pop_found = POPULATION_RE.search(content)
        return content, model_found.group(), pop_found.group()


def parse_model_string(model_str:str) -> ModelElementsStrings:
    categories_match = CATEGORIES_RE.search(model_str)
    snips_and_coeffs_match = SNIPS_RE.search(model_string)
    function_match = FUNCTION_RE.search(model_string)
    return ModelElementsStrings(categories_match.group(), snips_and_coeffs_match.group(), function_match.group(1))


def get_category_names(categories_string:str) -> Set[str]:
    return set(CATEGORY_NAME_RE.findall(categories_string))


def old_to_new_model(model_elements:ModelElementsStrings, template:str=NEW_MODEL_STRING_TEMPLATE) -> str:
    return template.format(
        model_elements.categories_str,
        snips_section_conversion(model_elements.snips_str),
        FUNCTION_TO_MODEL_TYPE_DICT[model_elements.function_str]
    )


def snips_section_conversion(snip_section_str:str) -> str:
    new_lines = []
    found = SNIP_RE.findall(snip_section_str)
    for _, element in found:
        line = parse_snip_line(element)
        new_line = convert_snip_data_to_new_line(line)
        new_lines.append(new_line)
    return 'snips_and_coefficients={{\n{}\n}}'.format(',\n'.join(new_lines))


def parse_snip_line(line:str) ->SnipData:
    rs_id = RSID_RE.search(line).group(1)
    eff_allele = EFFECT_ALLELE_RE.search(line).group(1)
    coeff = COEFF_RE.search(line).group(2)
    return SnipData(rs_id,eff_allele,coeff)


def convert_snip_data_to_new_line(snip_data:SnipData) -> str:
    return f"'{snip_data.rs_id}': ModelData(effect_allele='{snip_data.effect_allele}', coeff_value={snip_data.coeff_value})"


def create_new_model_file_content(pop_str:str, new_model_str:str, template:str=MODEL_FILE_TEMPLATE):
    return template.format(pop_str, new_model_str)


def get_description_content(content:str, model_str:str, pop_str:str) -> Dict[str, str]:
    new_content = remove_import_variant_genotypegetter_lines(content).replace(model_str, '').replace(pop_str, '')
    with tempfile.NamedTemporaryFile('w+t', delete=False) as f_out:
        f_out.write(new_content)
    name = f'{f_out.name}.py'
    shutil.move(f_out.name, name)
    directory, _ = name.rsplit(os.path.sep, 1)
    _, import_name = f_out.name.rsplit(os.path.sep, 1)
    sys.path.insert(0, directory)
    module = importlib.import_module(import_name)
    os.remove(name)
    fields = (x for x in dir(module) if not x.startswith('__'))
    ret = {}
    for field in sorted(fields):
        ret[field] = eval(f'module.{field}')
    return ret


def remove_import_variant_genotypegetter_lines(content:str) -> str:
    out_lines = []
    for line in content.split(os.linesep):
        if 'import ' in line:
            continue
        elif 'GenotypeGetter' in line:
            continue
        else:
            out_lines.append(line)
    return os.linesep.join(out_lines).replace(get_variant_lines(content), '')


def get_variant_lines(content:str) -> str:
    match = VARIANRS_RE.search(content)
    return match.group() if match else ''


def correct_fieldnames_with_categories(input:Dict[str, Any], categories:Set[str]) -> Dict[str, Any]:
    output={}
    for k, v in input.items():
        if isinstance(v, dict) and v and all([x in categories for x in v.keys()]):
            output[f'{k}_choice'] = v
        else:
            output[k] = v
    return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Converts old-style model to new-style one and a description file')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, required=True, help='old model path')
    parser.add_argument('-o', '--output', type=str, required=True, help='new model path')
    parser.add_argument('-d', '--description', type=str, required=True, help='description file path')
    arguments = parser.parse_args()

    content, model_string, pop_string = get_model_and_pop_string(os.path.expanduser(arguments.input))
    model_elements = parse_model_string(model_string)
    categories = get_category_names(model_elements.categories_str)
    new_model_string = old_to_new_model(model_elements)
    new_model_file_content = create_new_model_file_content(pop_string, new_model_string)
    description_content_initial = get_description_content(content, model_string, pop_string)
    description_content = correct_fieldnames_with_categories(description_content_initial, categories)
    with open(os.path.expanduser(arguments.output), 'w') as f:
        f.write(new_model_file_content)
    with open(os.path.expanduser(arguments.description), 'w') as f:
        json.dump(description_content, f, indent=4)