# Intro: This file contains information how to map fields from the output json file to the wireframe fields

*name of field in json file => result  page section*


## Genotype independent sections

**trait_short_description**: full trait name  
&nbsp;&nbsp;**=>** displayed as the **main title**

**trait_description**: trait description  
&nbsp;&nbsp;**=>** displayed **below the title, above the graph** 

**trait_pmids**:  list of PMIDs of articles used during trait model preparation  
&nbsp;&nbsp;**=>** can be used to fill the "**References**" field  
To get details about the article use:

```bash
curl -X GET --header 'Accept: application/json' 'https://www.ebi.ac.uk/europepmc/webservices/rest/MED/28832619/references?&format=json'
```

where 28832619 is the PMID.  
The obtained json can be piped to `jq` to obtain desired fields:

```bash
| jq '.referenceList.reference[0].title' #title
| jq '.referenceList.reference[0].authorString' #authors
| jq '.referenceList.reference[0].journalAbbreviation' #journal
| jq '.referenceList.reference[0].issue' #issue
| jq '.referenceList.reference[0].volume' #volume
| jq '.referenceList.reference[0].pubYear' #pulication year
```


**trait_science**: describes number of SNPs analyzed, heritability, variance explained, and in case of a single locus test, analyzed gene name  
&nbsp;&nbsp;**=>**  "**About the test**"/"**The science behind the test**"/ "**Information test**" section for a polygenic trait  
&nbsp;&nbsp;**=>**  "**About the test**"/"**The science behind the test**"/"**Test interpretation**"/ "**Genes analyzed**" section for a single SNP test

**test_type**: two values possible "Polygenic risk score" or "Single SNP analysis"  
&nbsp;&nbsp;**=>**  "**About the test**" section for polygenic traits only(?)

**trait_test_details**: short description how polygenic risk score was obtained  
&nbsp;&nbsp;**=>**  "**About the test**"/"**The science behind the test**"/"**Details**" section for polygenic traits only

**trait_genes**: list of genes; for polygenic traits only  
&nbsp;&nbsp;**=>** "**About the test**"/"**The science behind the test**"/"**Genes analyzed**" section for polygenic traits only

**other_factors**: description how other (environmental)  factors influence the trait [prepared by Intelliger]  
&nbsp;&nbsp;**=>**  "**Other factors**" section


## Genotype dependent sections

possible values for the **trait_class** for each trait are listed in **trait_classes**  

**description[trait_class]**: description of the genetic risk  
&nbsp;&nbsp;**=>**  "**Details about your results**" section

**description_recommendation[trait_class]**: recommendations  
&nbsp;&nbsp;**=>** "**What can I do about it?**" section <!-- u nas czasami ta część jest pusta dla "korzystnych genotypów" --> 
[empty string in our models mean that this genotype is beneficial so there is no need to recomend anything and no need to present `What can I do about it?` field to the user] 

**description_title[trait_class]**: title of the risk category  
&nbsp;&nbsp;**=>** **Graph title**

**description_genotype[trait_class]**: genotype for single locus trait  
&nbsp;&nbsp;**=>**  "**About the test**"/"**The science behind the test**" /**Table** (1st column)

**description_risk[trait_class]**: genotype for single locus trait  
&nbsp;&nbsp;**=>**  "**About the test**"/"**The science behind the test**" /**Table** (2nd column)

Polygenic trait:
![image](Result_page_L_mapped.png)

Polygenic trait:
![image](Result_page_L2_mapped.png)

Single SNP analysis:
![image](Result_page_L3_mapped.png)
