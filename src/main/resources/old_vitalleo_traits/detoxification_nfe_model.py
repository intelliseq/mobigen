from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

# POLA DLA ISEQ I NOTATKI (SKAD DOKLADNIE DANE, CO USUNELAM, LICZEBNOSC KOHORT) I TAKIE TAM
trait_title = "Detoxification:_glutathione_S-transferases_blood_level"
trait_pmids = ["29875488"]
trait_heritability = None  # heritability of analyzed protein not given, but according to PMID:25652787 the mean heritability of plazma protein concentration is 13.6 (estimatd for sample of 342 proteins)
trait_snp_heritability = None  # as fraction
trait_explained = None  # as fraction
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip, nie uzupelnione przez Intelliger
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "We used result of the genome wide association study (GWAS), in which associations between genotype and concentration of 3,622 plasma proteins were analyzed. This stydy included 3,301 individuals of European ancestry [1]. From this dataset we choose six variants, which were significantly associated with blood concentration of the detoxification enzymes: glutatione S-transferases (encoded in genes GSTA1, GSTO1, GSTP1, GSTA4, GSTM1). The average heritability of plasma protein concentration is about 14% [2].\nOur analyses use genetic data (genotypes) for those six SNPs to calculate the polygenic score."
genes = ['GSTA1', 'GSTO1', 'GSTP1', 'KRT18P55', 'AC011481.4', 'NLRP12']

# Quantification of 3,622 plasma proteins in 3,301 healthy participants from the INTERVAL study (a genomic bioresource of 50,000 blood donors in England).
# Protein concentrations were measured with aptamers (SOMAmer reagents) and given in relative fluorescence units
# Before GWAS relative protein abundances were first natural log-transformed
# Log-transformed protein levels were adjusted in a linear regression for age, sex, duration between blood draw and processing  and the first three principal components of ancestry
# The protein residuals from this linear regression were then rank-inverse normalized and used as phenotypes for association testing.
# Thus, coefficients obtained for different enzymes should be comparable and their sum should reflect the overall transferases activity
# I took GWAS statistics from SupplementaryTable4 (meta-analysis part)
# I took coefficients for the following glutathione S-transferases:A1, omega-1, P, A4, Mu1; encoded in genes GSTA1, GSTO1, GSTP1, GSTA4, GSTM1

# MODEL
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-1.071, category_name='High risk'),
                        QuantitativeCategory(from_=-1.071, to=0.931, category_name='Average risk'),
                        QuantitativeCategory(from_=0.931, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs2290758', effect_allele='A', beta_value=0.42),
                        beta(rs_id='rs2282326', effect_allele='C', beta_value=-0.91),
                        beta(rs_id='rs1695', effect_allele='G', beta_value=-0.18),
                        beta(rs_id='rs241775', effect_allele='T', beta_value=-0.18),
                        beta(rs_id='rs483082', effect_allele='T', beta_value=0.27),
                        beta(rs_id='rs62143206', effect_allele='T', beta_value=0.54)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Detoxification'

about = ('There are several genes that are key to the detoxification process in the '
         'body.  These are enzymes related to the function of the body’s own cellular '
         'detoxifying agent glutathione along with allowing the body to eliminate '
         'toxins through sweat, urine, and the bowel.')

how_your_result_was_calculated = ('We analyzed six variants that influence blood level of the following '
                                  'glutatione S-transferases: A1, omega-1, P, A4, Mu1; encoded in genes GSTA1, '
                                  'GSTO1, GSTP1, GSTA4, GSTM1 [PMID: 29875488].  The mean heritability of '
                                  'plazma protein concentration is about 13.6 [PMID:25652787]')

other_factors_statement = ('Unwanted foreign, xenobiotic compounds, chemicals, and matter from outside '
                           'the body and even metabolic byproducts from inside the body can cause '
                           'toxicity.')

other_factors = [{'content': 'Cruciferous and allium vegetables help increase the activity of '
                             'your detoxification system, which aids the removal of harmful '
                             'substances from your body.',
                  'title': 'Diet'},
                 {'content': 'Supplements that enhance glutathione levels, provide additional '
                             'fiber and prebiotics, and help to safely capture and eliminate '
                             'toxins from the system are considered extremely valuable in our '
                             'current, abundantly toxic environment.',
                  'title': 'Supplements'},
                 {'content': 'According to the EPA, our indoor environment is two to five '
                             'times more toxic than our outdoor environment. Personal care '
                             'products like soap, makeup, shampoos, nail polish, hair sprays, '
                             'mold and mildew, poor ventilation, cleaning products, air '
                             'fresheners, dish and laundry detergent, teflon cookware, '
                             'plastics, medications, and others need to be taken into '
                             'consideration when looking to reduce exposure.',
                  'title': 'Lifestyle'},
                 {'content': 'Glutathione is the main antioxidant and a key component of '
                             'cellular detoxification in the body. The production and '
                             'utilization of this important compound is greatly affected by '
                             'certain key genetically based enzymes.  Our bodies naturally '
                             'produce glutathione, but toxins, stress, trauma, and sickness '
                             'cause deficiency. This also causes a burden on the liver, which '
                             'is your body’s detox super organ.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your toxicity levels and other factors, you may '
                                           'have an average risk for problems or health issues such as: '
                                           'inflammation, certain types of cancers, vitamin '
                                           'deficiencies, chronic pain, fatigue, weight loss resistance, '
                                           'hormonal imbalance, body odor, bad breath, and skin '
                                           'problems.',
                           'High risk': 'Depending on your toxicity levels and other factors, you may '
                                        'have a higher risk for problems or health issues such as: '
                                        'inflammation, certain types of cancers, vitamin deficiencies, '
                                        'chronic pain, fatigue, weight loss resistance, hormonal '
                                        'imbalance, body odor, bad breath, and skin problems.',
                           'Low risk': 'Depending on your toxicity levels and other factors, you may '
                                       'have an low risk for problems or health issues such as: '
                                       'inflammation, certain types of cancers, vitamin deficiencies, '
                                       'chronic pain, fatigue, weight loss resistance, hormonal '
                                       'imbalance, body odor, bad breath, and skin problems.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you may likely to have '
                                    'issues detoxifying from unwanted, foreign and internally '
                                    'generated toxins.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have issues detoxifying from unwanted, foreign and internally '
                                 'generated toxins.',
                    'Low risk': 'Based on the genetic factors tested, you may likely to have '
                                'issues detoxifying from unwanted, foreign and internally '
                                'generated toxins.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to help prevent '
                                             'detoxification and to stay healthy. Certain blood tests can '
                                             'also check your risk factors for health issues related to '
                                             'environmental toxins. Talk to your practitioner about what '
                                             'is right for you.',
                             'High risk': 'There are things you can do to try to help prevent '
                                          'detoxification and to stay healthy. Certain blood tests can '
                                          'also check your risk factors for health issues related to '
                                          'environmental toxins. Talk to your practitioner about what is '
                                          'right for you.',
                             'Low risk': 'There are things you can do to try to help prevent '
                                         'detoxification and to stay healthy. Certain blood tests can also '
                                         'check your risk factors for health issues related to '
                                         'environmental toxins. Talk to your practitioner about what is '
                                         'right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Get help to identify the main sources of toxins '
                                                'in your life, minimize exposures, and determine '
                                                'the need for toxicity testing, and/or '
                                                'detoxification.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Seeking out a more organic and natural diet to '
                                                'minimize exposure to toxins.  A good resource '
                                                'to explore each year is also the “Dirty dozen” '
                                                'and the “Clean fifteen” that address '
                                                'chemical-based farming practices dangerous to '
                                                'human health. Eating cruciferous and allium '
                                                'vegetables as part of a plant-based high fiber '
                                                'diet assists in raising glutathione levels and '
                                                'helps with elimination.  Additionally, focus on '
                                                'drinking enough filtered water and green juices '
                                                'to help in expelling toxins from the body.',
                                     'title': 'Diet'},
                                    {'content': 'Glutathione is your body’s major detoxifying '
                                                'agent and is diminished through our exposure to '
                                                'toxins.  To build it back up in the system, '
                                                'requires the intake of the 3 amino acids that '
                                                'make up glutathione.  Look for supplements that '
                                                'contain, cysteine, glutamate, and glycine.  '
                                                'Additionally, glutathione requires certain '
                                                'other nutrients in order to be processed and '
                                                'utilized effectively in the body that are '
                                                'additionally important to supplement which '
                                                'include; milk thistle, Vitamin C, Vitamin E, B '
                                                'vitamins, selenium, magnesium, zinc, and alpha '
                                                'lipoic acid.',
                                     'title': 'Supplements'},
                                    {'content': 'Exercise helps to mobilize toxins, particularly '
                                                'when we sweat.  Pay attention to the chemical '
                                                'environment around you, looking to minimize '
                                                'exposure to food additives, preservatives, '
                                                'commercial meat and dairy, farm-raised fish, '
                                                'GMO foods, glyphosates, smoke, drugs and '
                                                'vaccines, chemical smells in the car, at home, '
                                                'or in other buildings, plastic bottles, the '
                                                'chemicals in new fabrics, paints, furniture, '
                                                'and floorings, pesticides, tap water, and '
                                                'personal care products.  Use green or chemical '
                                                'free resources for all of these areas wherever '
                                                'possible.',
                                     'title': 'Lifestyle'}],
                   'High risk': [{'content': 'Get help to identify the main sources of toxins in '
                                             'your life, minimize exposures, and determine the '
                                             'need for toxicity testing, and/or detoxification.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Seeking out a more organic and natural diet to '
                                             'minimize exposure to toxins.  A good resource to '
                                             'explore each year is also the “Dirty dozen” and '
                                             'the “Clean fifteen” that address chemical-based '
                                             'farming practices dangerous to human health. '
                                             'Eating cruciferous and allium vegetables as part '
                                             'of a plant-based high fiber diet assists in '
                                             'raising glutathione levels and helps with '
                                             'elimination.  Additionally, focus on drinking '
                                             'enough filtered water and green juices to help in '
                                             'expelling toxins from the body.',
                                  'title': 'Diet'},
                                 {'content': 'Glutathione is your body’s major detoxifying agent '
                                             'and is diminished through our exposure to toxins.  '
                                             'To build it back up in the system, requires the '
                                             'intake of the 3 amino acids that make up '
                                             'glutathione.  Look for supplements that contain, '
                                             'cysteine, glutamate, and glycine.  Additionally, '
                                             'glutathione requires certain other nutrients in '
                                             'order to be processed and utilized effectively in '
                                             'the body that are additionally important to '
                                             'supplement which include; milk thistle, Vitamin C, '
                                             'Vitamin E, B vitamins, selenium, magnesium, zinc, '
                                             'and alpha lipoic acid.',
                                  'title': 'Supplements'},
                                 {'content': 'Exercise helps to mobilize toxins, particularly '
                                             'when we sweat.  Pay attention to the chemical '
                                             'environment around you, looking to minimize '
                                             'exposure to food additives, preservatives, '
                                             'commercial meat and dairy, farm-raised fish, GMO '
                                             'foods, glyphosates, smoke, drugs and vaccines, '
                                             'chemical smells in the car, at home, or in other '
                                             'buildings, plastic bottles, the chemicals in new '
                                             'fabrics, paints, furniture, and floorings, '
                                             'pesticides, tap water, and personal care '
                                             'products.  Use green or chemical free resources '
                                             'for all of these areas wherever possible.',
                                  'title': 'Lifestyle'}],
                   'Low risk': [{'content': 'Get help to identify the main sources of toxins in '
                                            'your life, minimize exposures, and determine the '
                                            'need for toxicity testing, and/or detoxification.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Seeking out a more organic and natural diet to '
                                            'minimize exposure to toxins.  A good resource to '
                                            'explore each year is also the “Dirty dozen” and the '
                                            '“Clean fifteen” that address chemical-based farming '
                                            'practices dangerous to human health. Eating '
                                            'cruciferous and allium vegetables as part of a '
                                            'plant-based high fiber diet assists in raising '
                                            'glutathione levels and helps with elimination.  '
                                            'Additionally, focus on drinking enough filtered '
                                            'water and green juices to help in expelling toxins '
                                            'from the body.',
                                 'title': 'Diet'},
                                {'content': 'Glutathione is your body’s major detoxifying agent '
                                            'and is diminished through our exposure to toxins.  '
                                            'To build it back up in the system, requires the '
                                            'intake of the 3 amino acids that make up '
                                            'glutathione.  Look for supplements that contain, '
                                            'cysteine, glutamate, and glycine.  Additionally, '
                                            'glutathione requires certain other nutrients in '
                                            'order to be processed and utilized effectively in '
                                            'the body that are additionally important to '
                                            'supplement which include; milk thistle, Vitamin C, '
                                            'Vitamin E, B vitamins, selenium, magnesium, zinc, '
                                            'and alpha lipoic acid.',
                                 'title': 'Supplements'},
                                {'content': 'Exercise helps to mobilize toxins, particularly '
                                            'when we sweat.  Pay attention to the chemical '
                                            'environment around you, looking to minimize '
                                            'exposure to food additives, preservatives, '
                                            'commercial meat and dairy, farm-raised fish, GMO '
                                            'foods, glyphosates, smoke, drugs and vaccines, '
                                            'chemical smells in the car, at home, or in other '
                                            'buildings, plastic bottles, the chemicals in new '
                                            'fabrics, paints, furniture, and floorings, '
                                            'pesticides, tap water, and personal care products.  '
                                            'Use green or chemical free resources for all of '
                                            'these areas wherever possible.',
                                 'title': 'Lifestyle'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = [
    {
        "trait": "Less Effective Detoxification",
        "gene": "GSTP1",
        "rsid": "rs1695",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "The GSTP1 (glutathione S-transferase P1) enzyme influences the impact that free-radicals have on the development and recovery of diseases related to oxidative stress. Enzymes like GSTP1 take activated toxins and make them water-soluble so they can be metabolized by the body. GSTP1 is part of the GST family participating in the detoxification process related to xenobiotics and the utilization of glutathione to detoxify the cell.",
        "risk_allele":'G',
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with the GG genotype are more likely to have less activity of the GSTP1 enzyme. Cruciferous and allium vegetables help to enhance GST activity along with the addition of glutathione enhancing supplements."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with the AG genotype are somewhat more likely to have less activity of the GSTP1 enzyme. Cruciferous and allium vegetables help to enhance GST activity along with the addition of glutathione enhancing supplements."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with the AA genotype are less likely to have less activity of the GSTP1 enzyme."
            }
        ]
    },
    {
        "trait": "Detoxification",
        "gene": "CYP1A1",
        "rsid": "rs4646903",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": "19531241"
        },
        "intro": "CYP1A1 is one of the most important detoxification enzymes due to its broad substrate specificity and wide distribution throughout the body. On the other hand, CYP1A1 can also produce highly carcinogenic intermediate metabolites through oxidation of polycyclic aromatic hydrocarbons.",
        "risk_allele":'G',
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Higher risk",
                "description": "The GG genotype related to CYP1A1 can cause an high risk of disease due to environmental toxins. It\u2019s important to detox and reduce exposure to environmental procarcinogens such as polycyclic aromatic hydrocarbons (PAHs) \u2013 from burning coal, oil, gas, wood, and tobacco, and smoking of any kind, and other volatile organic compounds (VOCs) from chemicals used in manufacturing. Detoxification and managing exposures are both important."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "The AG genotype related to CYP1A1 can cause an somewhat higher risk of disease due to environmental toxins. It\u2019s important to detox and reduce exposure to environmental procarcinogens such as polycyclic aromatic hydrocarbons (PAHs) \u2013 from burning coal, oil, gas, wood, and tobacco, and smoking of any kind, and other volatile organic compounds (VOCs) from chemicals used in manufacturing. Detoxification and managing exposures are both important."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Lower risk",
                "description": "The AA genotype related to CYP1A1 can cause a lower risk of disesase due to environmental toxins."
            }
        ]
    },
    {
        "trait": "Detoxification",
        "gene": "CYP1A1",
        "rsid": "rs1048943",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": "19531241"
        },
        "intro": "CYP1A1 is one of the most important detoxification enzymes due to its broad substrate specificity and wide distribution throughout the body. On the other hand, CYP1A1 can also produce highly carcinogenic intermediate metabolites through oxidation of polycyclic aromatic hydrocarbons.",
        "risk_allele":'C',
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher risk",
                "description": "The CC genotype related to CYP1A1 can cause a higher risk of disease due to environmental toxins. It\u2019s important for those with a variant to this SNP to detox and reduce exposure to environmental procarcinogens such as polycyclic aromatic hydrocarbons (PAHs) \u2013 from burning coal, oil, gas, wood, and tobacco, and smoking of any kind, and other volatile organic compounds (VOCs) from chemicals used in manufacturing. Detoxification and managing exposures are both important"
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "The CT genotype related to CYP1A1 can cause an somewhat higher risk of disease due to environmental toxins. It\u2019s important for those with a variant to this SNP to detox and reduce exposure to environmental procarcinogens such as polycyclic aromatic hydrocarbons (PAHs) \u2013 from burning coal, oil, gas, wood, and tobacco, and smoking of any kind, and other volatile organic compounds (VOCs) from chemicals used in manufacturing. Detoxification and managing exposures are both important"
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Lower risk",
                "description": "T/T\u00a0 No variant was detected."
            }
        ]
    },
    {
        "trait": "Less Effective Detoxification",
        "gene": "NQ01",
        "rsid": "rs1800566",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "The NQO1 gene encodes the NAD(P)H quinone dehydrogenase 1. NQ01 has an antioxidant function to protect cells from dangerous free radicals and is part of the detoxification process for potential cancer causing factors that come from exposure to tobacco smoke, diet, and estrogen metabolism.",
        "risk_allele":'A',
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Individuals with the AA genotype may have a higher risk of being prone to the mutagenic and carcinogenic effects of smoke and other environmental toxins or volatile organic compounds (VOCs).\nSolutions include increasing glutathione and supporting liver functions. Milk thistle, green tea extracts, sulforaphane from cruciferous vegetables (Broccoli, Bok choy, cabbage, kale, Brussels, sprouts, and cauliflower), and antioxidants like Vitamin E from supplements are all helpful."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Individuals with the AG genotype may have a somewhat higher risk of being prone to the mutagenic and carcinogenic effects of smoke and other environmental toxins or volatile organic compounds (VOCs).\nSolutions include increasing glutathione and supporting liver functions. Milk thistle, green tea extracts, sulforaphane from cruciferous vegetables (Broccoli, Bok choy, cabbage, kale, Brussels, sprouts, and cauliflower), and antioxidants like Vitamin E from supplements are all helpful."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Individuals with the GG genotype may have a lower risk of being prone to the mutagenic and carcinogenic effects of smoke and other environmental toxins or volatile organic compounds (VOCs)."
            }
        ]
    }
]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
