from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Vitamin_D"
trait_pmids = ["29343764"]  # 29343764 table 1
trait_heritability = 0.5
trait_snp_heritability = 0.075
trait_explained = 0.03
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test =""
genes = ["AL109628.1", "AMDHD1", "CYP24A1", "CYP2R1", "GC", "NADSYN1"]
science_behind_the_test = "Vitamin D blood level is moderately heritable, with h"+chr(0x00b2)+" estimate of about 50% [1]. Our test is based on results of the genome wide association study (GWAS) [1], which included over 79,000 individuals of European ancestry. The study identified 6 variants significantly associated with the vitamin D concentration. Together these SNPs explain about 3% of between people variance in the this vitamin level [1].\nOur analyses use genetic data (genotypes) for these SNPs to calculate the polygenic score."
######### vitamin_D
# na podstawie: doi: 10.1038/s41467-017-02662-2
# poziom witaminy D (a dokładniej 25-hydroxywitaminy D - nie wiem, czy to jedyna forma) w surowicy dla 79366 osób pochodzenia europejskiego;
# dane o rs: z Tabeli1 (główny artykuł, dane dla "discovery cohort"), effekt allelu podany jako współczynnik regresji
# to tylko 6 wariantów
# efekty były podane, jako współczynniki beta z regresji liniowej (po transformacji logarytmicznej poziomu witaminy)
# odziedziczalnosc: około 50-80 %, zmiennosc wyjaśniona przez wszystkie SNPy- 7.5%; przez te istotne statystycznie - około 3%
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_vitD.txt i stats_vitD.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji gnomAD controls;


model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.0441, category_name='High risk'),
                        QuantitativeCategory(from_=-0.0441, to=0.1331, category_name='Average risk'),
                        QuantitativeCategory(from_=0.1331, category_name='Low risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs3755967', effect_allele='T', beta_value=-0.089),
                        beta(rs_id='rs12785878', effect_allele='T', beta_value=0.036),
                        beta(rs_id='rs10741657', effect_allele='A', beta_value=0.031),
                        beta(rs_id='rs17216707', effect_allele='T', beta_value=0.026),
                        beta(rs_id='rs10745742', effect_allele='T', beta_value=0.017),
                        beta(rs_id='rs8018720', effect_allele='C', beta_value=-0.017)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Vitamin D'

about = ('Vitamin D plays a important role in immunity, muscle function, calcium '
         'absorption and bone health, and the cardiovascular system. Low vitamin D '
         'levels may increase the risk of cardiovascular disease, depression, cancer, '
         'diabetes, and autoimmune disease. The body can make vitamin D, but it '
         'requires the skin to be exposed to sunlight (UVB rays). Vitamin D '
         'supplements and sun exposure can help raise vitamin D levels if they are low '
         '-- it can be difficult to get enough vitamin D through diet alone.')

how_your_result_was_calculated = ('Your result is a risk based on 6 genetic variants, specifically called SNPs, '
                                  'found in genes associated with vitamin D level. Overall, this explains about '
                                  '3% of the genetic variation in blood vitamin D level in the human '
                                  'population, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact vitamin D levels, including diet, lifestyle, skin tone, '
                           'and genetics. Although several genes are associated with our vitamin D '
                           'level, genetics has a moderate overall impact on this when you consider all '
                           'the other factors.')

other_factors = [{'content': 'Vitamin D supplements can help raise vitamin D levels within '
                             'your body.  Speak with your practitioner about the recommended '
                             'daily dosing.',
                  'title': 'Supplements'},
                 {'content': 'Limited availability in foods like fatty fish.', 'title': 'Diet'},
                 {'content': 'Regular sun exposure.  However, using sunscreen can limit and '
                             'reduce the exposure.',
                  'title': 'Lifestyle'}]

what_your_result_means = {'Average risk': 'Vitamin D plays a critical role in immunity, muscle '
                                           'function, calcium absorption and bone health, the '
                                           'cardiovascular system - for a healthy heart and circulation, '
                                           'respiratory system –for healthy lungs and airways, brain '
                                           'development, and with anti-cancer effects.',
                           'High risk': 'Vitamin D plays a critical role in immunity, muscle function, '
                                        'calcium absorption and bone health, the cardiovascular system - '
                                        'for a healthy heart and circulation, respiratory system –for '
                                        'healthy lungs and airways, brain development, and with '
                                        'anti-cancer effects.',
                           'Low risk': 'Depending on your vitamin D level and other factors, you may '
                                       'have a lower risk for low bone density and other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average blood vitamin D level.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a lower blood vitamin D level.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a higher blood vitamin D level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent low vitamin D '
                                             'levels and stay healthy. Certain blood tests can also check '
                                             'your vitamin D level. Talk to your practitioner about what '
                                             'is right for you.',
                             'High risk': 'There are things you can do to try to prevent low vitamin D '
                                          'levels and stay healthy. Certain blood tests can also check '
                                          'your vitamin D level. Talk to your practitioner about what is '
                                          'right for you.',
                             'Low risk': 'There are things you can do to try to prevent low vitamin D '
                                         'levels and stay healthy. Certain blood tests can also check your '
                                         'vitamin D level. Talk to your practitioner about what is right '
                                         'for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a low vitamin D level. Consider '
                                                'talking to your practitioner about blood tests '
                                                'to check your vitamin D level, and possible '
                                                'next steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'The recommended amount of vitamin D per day for '
                                                'adults is 125mcg (5,000 IU), and you might need '
                                                'to take a vitamin D supplement to get to that '
                                                'amount. Vitamin D supplements are available '
                                                'over the counter, but talk to your practitioner '
                                                'about the right quality and dosage to take, '
                                                'which should consider your diet, sun exposure, '
                                                'and skin tone (darker skin can block out UVB '
                                                'rays).',
                                     'title': 'Supplements'},
                                    {'content': "Try to get regular sun exposure, but don't "
                                                'forget the sunscreen. If you have darker skin '
                                                'it can naturally block out UVB rays needed for '
                                                'the skin to make vitamin D, so the sun exposure '
                                                'may not be as effective.',
                                     'title': 'Lifestyle'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your vitamin D level. Talk to them about '
                                             'this and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'The recommended amount of vitamin D per day for '
                                             'adults is 125mcg (5,000 IU), and you might need to '
                                             'take a vitamin D supplement to get to that amount. '
                                             'Vitamin D supplements are available over the '
                                             'counter, but talk to your practitioner about the '
                                             'right quality and dosage to take, which should '
                                             'consider your diet, sun exposure, and skin tone '
                                             '(darker skin can block out UVB rays).',
                                  'title': 'Supplements'},
                                 {'content': "Try to get regular sun exposure, but don't forget "
                                             'the sunscreen. If you have darker skin it can '
                                             'naturally block out UVB rays needed for the skin '
                                             'to make vitamin D, so the sun exposure may not be '
                                             'as effective.',
                                  'title': 'Lifestyle'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a low vitamin D level. Consider talking to your '
                                            'practitioner about blood tests to check your '
                                            'vitamin D level, and possible next steps you can '
                                            'take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'The recommended amount of vitamin D per day for '
                                            'adults is 125mcg (5,000 IU), and you might need to '
                                            'take a vitamin D supplement to get to that amount. '
                                            'Vitamin D supplements are available over the '
                                            'counter, but talk to your practitioner about the '
                                            'right quality and dosage to take, which should '
                                            'consider your diet, sun exposure, and skin tone '
                                            '(darker skin can block out UVB rays).',
                                 'title': 'Supplements'},
                                {'content': "Try to get regular sun exposure, but don't forget "
                                            'the sunscreen. If you have darker skin it can '
                                            'naturally block out UVB rays needed for the skin to '
                                            'make vitamin D, so the sun exposure may not be as '
                                            'effective.',
                                 'title': 'Lifestyle'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 17, 2019',
                                     'links': 'https://ods.od.nih.gov/factsheets/VitaminD-Consumer/',
                                     'title': '"Vitamin D Fact Sheet for Consumers" from '
                                              'National Institutes of Health'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood concentration of vitamin D"
trait_science = "Blood concentration of vitamin D is moderately heritable and about 50% of differences observed among people has a genetic origin. Our analysis was based on 6 SNPs associated with this vitamin level. These variants jointly explain about 3% of the variation between people."
trait_test_details = "We determined your genotype at six genomic positions, which are known to influence vitamin D blood level. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
