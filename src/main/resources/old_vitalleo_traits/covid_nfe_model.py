from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Severe_Covid-19_with_respiratory_failure"
trait_pmids = ["32558485", "30405986" ] # (GWAS statistics from https://ikmb.shinyapps.io/COVID-19_GWAS_Browser/, effect allele= ALLELE2, results for META cohort); 30405986 - heritability
trait_heritability = None
trait_snp_heritability = None
trait_explained = None
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = ("Susceptibility to infectious disease is moderately heritable [1]."
                           "Our test is based on results from the recently published GWAS study [2], "
                           "which aimed to identify genetic factors involved in the development of severe Covid-19 with respiratory failure. "
                           "DNA sequences of over 1600 SARS-Cov2 infected patients from hospitals "
                           "in Italy and Spain were compared to genetic material isolated from the control group. "
                           "The latter included about 2100 people which were not infected by the virus "
                           "or were infected but had mild or no Covid-19 symptoms. "
                           "In total, the study analyzed 8,582,968 single-nucleotide polymorphisms.\n"
                           "Significant associations with two common variants (SNPs) were detected. "
                           "Our analyses use genetic data (genotypes) for those SNPs to calculate the polygenic score.")

genes = ["ABO", "LZTFL1"]

# I took only two “independent” SNPs  with p-value < 5e-08; I substituted the rs11385942 with SNP from the array (rs35731912 )
# Study design: meta-analysis of the patient and control groups from Italy and Spain 1980/2381  (cases/controls).
# Control group included healthy individuals and small number of infected patients with mild symptoms, cases included people with severe symptoms (hospitalised)
# Model was adjusted for age and sex
# Low risk category ~ 15%, high risk ~7% of individuals

model = Choice(
    {
        Priority(0):
            {
                'when': always_true,  # nie ma dodatkowych warunków, jak przy kolore włosów
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.423, category_name='Low risk'),
                        QuantitativeCategory(from_=0.423, to=1.01, category_name='Average risk'),
                        QuantitativeCategory(from_=1.01, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        odds_ratio(rs_id='rs35731912', effect_allele='C', odds_ratio_value=0.564000579501318),
                        odds_ratio(rs_id='rs657152', effect_allele='C', odds_ratio_value=1.32498349134725)
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

trait = ''

about = ""
how_your_result_was_calculated = ""

other_factors_statement = ""
other_factors = []

what_your_result_means = {'Average risk': '',
                    'High risk': '',
                    'Low risk': ''}

result_statement = {'Average risk': '',
                    'High risk': '',
                    'Low risk': ''}

what_you_can_do_statement = {'Average risk': '',
                    'High risk': '',
                    'Low risk': ''}

what_you_can_do = {'Average risk': '',
                    'High risk': '',
                    'Low risk': ''}
science_behind_your_result = ""

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': []
              }


