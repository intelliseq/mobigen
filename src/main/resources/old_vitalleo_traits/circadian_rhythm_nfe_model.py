from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Morning_person"
trait_pmids = ["30696823", "28364486"]  # 30696823 Suppl.Table2; 28364486 heritability estimation
trait_heritability = 0.5
trait_snp_heritability = 0.14
trait_explained = None  # 25 minutes ;)
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.07.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "Chronotype is a moderately heritable trait - about 50% of differences observed among people has a genetic origin [1]. Our test is based on results of the genome wide association studies (GWAS) [1], which included almost 700 000 individuals of European ancestry (Biobank and 23andMe). The study reports 394 variants significantly associated with the circadian rhythms in European population.\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["AJAP1", "PER3", "Z98884.1", "AL357873.1", "RSC1A1", "HTR6", "RNU7-200P", "AL590609.1", "MIR3116-2", "PDE4B",
         "PIGK", "ADH5P2", "AL357632.1", "BARHL2", "RNU6-210P", "LINC01761", "RN7SL831P", "DPYD-AS1", "AL355310.3",
         "RNU7-70P", "TRIM33", "MIR6878", "GOLPH3L", "RF00019", "RABGAP1L", "AXDND1", "RNASEL", "RGS16", "RF00019",
         "AL359976.1", "LINC01031", "AL353072.2", "MIR4735", "AL590682.1", "LINC01249", "AC009486.1", "SDHCP3", "ADCY3",
         "AL133245.1", "AC007378.1", "HNRNPA1P57", "AC019129.1", "CAMKMT", "CRIPT", "STON1-GTF2A1L", "MIR548BA",
         "AC009971.1", "AC068725.1", "AC069157.1", "LINC01793", "AC007381.1", "USP34", "MEIS1", "PCYOX1", "TACR1",
         "SUCLA2P2", "AC079117.1", "AC084149.1", "EIF2AK3", "AC016738.2", "MTND5P22", "R3HDM1", "AC079793.1", "EPC2",
         "AC009313.2", "MAP3K20-AS1", "CIR1", "AC080125.1", "AC006460.1", "AC020719.1", "AC007383.2", "AC012485.1",
         "HES6", "PER2", "AC012485.3", "TRAF3IP1", "RNU6-234P", "MIR2467", "HINT2P1", "GRM7-AS2", "RAD18", "LINC01267",
         "AC132807.2", "UBE2E2-AS1", "RN7SL216P", "LINC02033", "RBM6", "Z84492.1", "DNAH1", "SAMMSON", "FOXP1-AS1",
         "MIR1284", "RNU6-386P", "VDAC1P7", "AC129807.1", "LINC02008", "MIR5688", "ALCAM", "DRD3", "LSAMP-AS1", "ADCY5",
         "TMEM108-AS1", "MRAS", "RF02089", "P2RY1", "TIPARP-AS1", "AC108738.1", "GHSR", "NCEH1", "LINC01208",
         "AC084211.1", "LINC01995", "DGKG", "AC078852.2", "FAM193A", "AC093898.1", "IGBP1P5", "EPHA5-AS1", "MIR1269A",
         "HNRNPD", "MIR8066", "AC093879.1", "CAMK2D", "LINC02465", "AC073429.1", "NOCT", "MIR7849", "AC005150.1",
         "PRLR", "RNU6-294P", "CRHBP", "ALDH7A1P1", "RN7SL629P", "AC091826.1", "MEF2C-AS1", "AC099520.2", "LINC01950",
         "EFNA5", "SEMA6A-AS2", "KRT18P16", "LINC01470", "LSM1P2", "LINC01947", "AC113423.2", "AC138965.2", "PRR7",
         "TMEM170B", "RNU1-11P", "AL138720.1", "AL589647.1", "HIST1H3PS1", "MICD", "EHMT2", "MDGA1", "FOXP4", "ZNF318",
         "FTH1P5", "HCRTR2", "KHDRBS2", "RNU4-66P", "AL590727.1", "MIR548AI", "GPR6", "AL606845.1", "TUBB8P2", "SAMD5",
         "AL160162.1", "MTRF1L", "AL450345.1", "AL590302.1", "RPL6P21", "RNA5SP228", "PDE1C", "RP9P", "DDC", "AUTS2",
         "CALN1", "AC004808.1", "MARK2P10", "AC005072.1", "ORAI2", "POLR2J", "FAM185A", "RN7SKP198", "AC073626.2",
         "FEZF1", "PLXNA4", "EXOC4", "RF00017", "KCNH2", "RNA5SP251", "CSMD1", "AC103957.1", "TRIM35", "NRG1-IT1",
         "AF279873.2", "AC090740.1", "AC021915.1", "RNU4-50P", "COX6CP8", "KCNB2", "HNF4G", "RNA5SP273", "AC022695.1",
         "YWHAZ", "AF178030.1", "RF02211", "LINC01591", "PTPRD", "AL353811.1", "ZCCHC7", "RPL35AP21", "AL513124.1",
         "MTND2P9", "RPS6P12", "AL354733.1", "AL161910.1", "AL807761.4", "ZNF618", "AL445489.1", "NEK6", "NR5A1",
         "AL158151.1", "PMPCA", "EXD3", "AL157709.1", "AC051618.1", "BICC1", "ANK3", "AC067752.1", "RNU6-523P", "TET1",
         "AC073176.2", "UNC5B", "EIF4A1P8", "MARCKSL1P1", "PDZD8", "RF00019", "GPR26", "MIR4296", "NPS", "AL359508.1",
         "PPP2R2D", "ARNTL", "AKR1B1P3", "RN7SL188P", "LINC00678", "MIR8068", "MPPED2", "CD59", "AC087521.2",
         "AC018410.1", "AP001350.1", "CTSF", "CCDC90B", "MTNR1B", "AP003072.2", "AP001790.1", "RN7SKP53", "DRD2",
         "ZBTB16", "NXPE2", "MIR100", "RNU4-23P", "AP002833.1", "AC007406.2", "SLC15A5", "AC007529.2", "AC084819.1",
         "MIR920", "AK6P2", "ALG10B", "ARID2", "KMT2D", "SCN8A", "COPZ1", "R3HDM2", "AVPR1A", "AC138331.1",
         "AC009522.1", "RFX4", "RF00019", "AC073863.1", "RPL11P5", "ZCCHC8", "AL354821.1", "POLR3KP1", "MIR548X2",
         "LINC00383", "RF00440", "KLF5", "FBXL3", "MYCBP2", "GPC5", "HNRNPA1P29", "AL354741.1", "MYO16-AS2",
         "AL359649.1", "AL121821.1", "GCH1", "OTX2-AS1", "LINC02322", "AL390816.1", "AL161756.1", "AC006349.1",
         "AC026888.1", "AL163642.1", "AL163932.1", "AL845552.2", "AC044787.1", "WDR72", "HMGB1P51", "AC087477.2",
         "ASB7", "Z92544.1", "AC018767.2", "USP7", "USP31", "AC008938.1", "AC007861.1", "LINC02180", "CASC16", "FTO",
         "AC007495.2", "AC009102.2", "GNPATP", "RF00019", "AC009097.2", "PMFBP1", "VAMP2", "RASD1", "AC008133.1",
         "AC004134.1", "C1QL1", "CR936218.1", "COPZ2", "CALCOCO2", "CA10", "ANKFN1", "MSI2", "RF00012", "TUBD1",
         "AC005828.2", "MIR548D2", "AP005057.1", "AKAIN1", "RF00019", "AC104961.1", "RF00019", "AC022601.1",
         "AC009899.1", "AC100781.1", "RPL17P45", "SKOR2", "TCF4-AS1", "LINC01415", "SEC11C", "ZCCHC2", "RNU6-1037P",
         "AC116003.3", "AC006538.3", "DUS3L", "ILF3", "PGPEP1", "AC011504.1", "LINC01791", "CYP2A6", "POU2F2", "NOVA2",
         "AC011484.1", "MACROD2", "PPIAP17", "AL359511.1", "DYNLT3P1", "AL035454.1", "LINC01727", "AL034550.1",
         "PABPC1L", "SLC12A5", "MIR5739", "MCM5", "KCNJ4", "TNRC6B", "OGFRP1", "FAM118A"]

# GWAS statistics from Suppl.Table2 (Conditional analysis implemented in GCTA-COJO software, RefA = morning allele; beta: bj, p-value: pj)
# Meta-analysis: 697,828 UK Biobank and 23andMe participants (European ancestry)
# description_genotype = {} #for single SNP test
# description_genotype["low_trait"] = "hom" # for one snip test
# description_genotype["average_trait"] = "het" #for one snip test
# description_genotype["high_trait"] = "hom" # for one snip test

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.3739, category_name='High risk'),
                        QuantitativeCategory(from_=-0.3739, to=0.0583, category_name='Average risk'),
                        QuantitativeCategory(from_=0.0583, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs909757', effect_allele='T', beta_value=0.0101154),
                        beta(rs_id='rs228726', effect_allele='A', beta_value=-0.0246902),
                        beta(rs_id='rs150812083', effect_allele='C', beta_value=-0.139101),
                        beta(rs_id='rs61773390', effect_allele='T', beta_value=0.0222605),
                        beta(rs_id='rs12065331', effect_allele='T', beta_value=-0.0121075),
                        beta(rs_id='rs17448682', effect_allele='T', beta_value=0.0164241),
                        beta(rs_id='rs10917513', effect_allele='T', beta_value=-0.0140683),
                        beta(rs_id='rs10916892', effect_allele='T', beta_value=-0.0154329),
                        beta(rs_id='rs2506089', effect_allele='T', beta_value=0.00954965),
                        beta(rs_id='rs12140153', effect_allele='T', beta_value=-0.0263311),
                        beta(rs_id='rs11208844', effect_allele='A', beta_value=-0.014163),
                        beta(rs_id='rs12040629', effect_allele='A', beta_value=0.0325023),
                        beta(rs_id='rs11588913', effect_allele='A', beta_value=-0.0105534),
                        beta(rs_id='rs5016898', effect_allele='T', beta_value=-0.0105916),
                        beta(rs_id='rs72720396', effect_allele='A', beta_value=-0.0189914),
                        beta(rs_id='rs481214', effect_allele='A', beta_value=0.0096222),
                        beta(rs_id='rs6702412', effect_allele='T', beta_value=0.010238),
                        beta(rs_id='rs2256459', effect_allele='A', beta_value=0.0130103),
                        beta(rs_id='rs1760217', effect_allele='A', beta_value=0.0131328),
                        beta(rs_id='rs41279738', effect_allele='T', beta_value=0.0308801),
                        beta(rs_id='rs17575798', effect_allele='A', beta_value=-0.0172218),
                        beta(rs_id='rs6537747', effect_allele='T', beta_value=-0.0106379),
                        beta(rs_id='rs11102807', effect_allele='A', beta_value=-0.00976878),
                        beta(rs_id='rs9436119', effect_allele='A', beta_value=0.0202979),
                        beta(rs_id='rs7524620', effect_allele='A', beta_value=0.0116737),
                        beta(rs_id='rs6665637', effect_allele='A', beta_value=-0.0112026),
                        beta(rs_id='rs115073088', effect_allele='A', beta_value=-0.0380572),
                        beta(rs_id='rs975025', effect_allele='T', beta_value=-0.0242884),
                        beta(rs_id='rs34335333', effect_allele='T', beta_value=-0.011129),
                        beta(rs_id='rs1144566', effect_allele='T', beta_value=0.10868),
                        beta(rs_id='rs2378824', effect_allele='T', beta_value=0.00989845),
                        beta(rs_id='rs34580810', effect_allele='A', beta_value=-0.0112003),
                        beta(rs_id='rs1221502', effect_allele='A', beta_value=0.011033),
                        beta(rs_id='rs4657983', effect_allele='A', beta_value=-0.0116315),
                        beta(rs_id='rs10754194', effect_allele='C', beta_value=-0.0164202),
                        beta(rs_id='rs6429233', effect_allele='A', beta_value=0.00968303),
                        beta(rs_id='rs13011556', effect_allele='C', beta_value=-0.0147395),
                        beta(rs_id='rs62124718', effect_allele='A', beta_value=-0.0201599),
                        beta(rs_id='rs72796401', effect_allele='A', beta_value=0.0162399),
                        beta(rs_id='rs11676272', effect_allele='A', beta_value=0.0103349),
                        beta(rs_id='rs11678584', effect_allele='A', beta_value=-0.0144417),
                        beta(rs_id='rs848552', effect_allele='C', beta_value=-0.0128053),
                        beta(rs_id='rs6725031', effect_allele='T', beta_value=-0.00971863),
                        beta(rs_id='rs75120545', effect_allele='T', beta_value=0.044999),
                        beta(rs_id='rs12998046', effect_allele='A', beta_value=0.0128244),
                        beta(rs_id='rs6544906', effect_allele='A', beta_value=0.010563),
                        beta(rs_id='rs66888174', effect_allele='A', beta_value=0.0100569),
                        beta(rs_id='rs972557', effect_allele='T', beta_value=0.0103256),
                        beta(rs_id='rs10495976', effect_allele='A', beta_value=-0.0126166),
                        beta(rs_id='rs12470914', effect_allele='A', beta_value=0.0236277),
                        beta(rs_id='rs4672458', effect_allele='T', beta_value=-0.0107536),
                        beta(rs_id='rs10175975', effect_allele='T', beta_value=0.0132679),
                        beta(rs_id='rs812925', effect_allele='C', beta_value=-0.0143375),
                        beta(rs_id='rs113851554', effect_allele='T', beta_value=-0.026945),
                        beta(rs_id='rs2706762', effect_allele='T', beta_value=-0.0179937),
                        beta(rs_id='rs12464387', effect_allele='A', beta_value=-0.0104612),
                        beta(rs_id='rs6727752', effect_allele='A', beta_value=0.0106282),
                        beta(rs_id='rs10520176', effect_allele='T', beta_value=0.0167843),
                        beta(rs_id='rs4254526', effect_allele='T', beta_value=-0.0112643),
                        beta(rs_id='rs11681299', effect_allele='T', beta_value=0.0109267),
                        beta(rs_id='rs34509802', effect_allele='A', beta_value=0.015674),
                        beta(rs_id='rs76064513', effect_allele='T', beta_value=0.0137471),
                        beta(rs_id='rs77248969', effect_allele='A', beta_value=-0.0156898),
                        beta(rs_id='rs28380327', effect_allele='A', beta_value=0.0180917),
                        beta(rs_id='rs2166559', effect_allele='T', beta_value=-0.0147327),
                        beta(rs_id='rs747003', effect_allele='T', beta_value=0.00963493),
                        beta(rs_id='rs13004345', effect_allele='T', beta_value=-0.0105315),
                        beta(rs_id='rs6433478', effect_allele='T', beta_value=-0.0114645),
                        beta(rs_id='rs1593205', effect_allele='A', beta_value=0.0131349),
                        beta(rs_id='rs11677484', effect_allele='T', beta_value=0.0118579),
                        beta(rs_id='rs1064213', effect_allele='A', beta_value=0.0196147),
                        beta(rs_id='rs184033703', effect_allele='A', beta_value=0.0266266),
                        beta(rs_id='rs57690685', effect_allele='A', beta_value=-0.0454184),
                        beta(rs_id='rs960783', effect_allele='T', beta_value=-0.0464552),
                        beta(rs_id='rs80261926', effect_allele='T', beta_value=0.0612319),
                        beta(rs_id='rs58574366', effect_allele='A', beta_value=-0.0234733),
                        beta(rs_id='rs80271258', effect_allele='T', beta_value=-0.0420601),
                        beta(rs_id='rs188004381', effect_allele='A', beta_value=-0.0282685),
                        beta(rs_id='rs62182135', effect_allele='A', beta_value=-0.0124195),
                        beta(rs_id='rs35346733', effect_allele='A', beta_value=-0.0133308),
                        beta(rs_id='rs111261826', effect_allele='A', beta_value=-0.0120069),
                        beta(rs_id='rs149611468', effect_allele='T', beta_value=0.062582),
                        beta(rs_id='rs6794796', effect_allele='A', beta_value=0.0117625),
                        beta(rs_id='rs9817910', effect_allele='A', beta_value=-0.0108386),
                        beta(rs_id='rs73050286', effect_allele='T', beta_value=0.0136068),
                        beta(rs_id='rs13059636', effect_allele='A', beta_value=-0.00998411),
                        beta(rs_id='rs114848860', effect_allele='A', beta_value=-0.0392365),
                        beta(rs_id='rs12636669', effect_allele='T', beta_value=0.0243861),
                        beta(rs_id='rs2236950', effect_allele='A', beta_value=-0.0155516),
                        beta(rs_id='rs56357549', effect_allele='T', beta_value=0.0139181),
                        beta(rs_id='rs17007397', effect_allele='C', beta_value=0.00948751),
                        beta(rs_id='rs17655548', effect_allele='A', beta_value=-0.0117959),
                        beta(rs_id='rs7626335', effect_allele='A', beta_value=-0.0127112),
                        beta(rs_id='rs7429614', effect_allele='T', beta_value=0.0165186),
                        beta(rs_id='rs116291712', effect_allele='A', beta_value=-0.0433646),
                        beta(rs_id='rs4324483', effect_allele='T', beta_value=-0.00948386),
                        beta(rs_id='rs56242353', effect_allele='T', beta_value=0.0161316),
                        beta(rs_id='rs1449403', effect_allele='A', beta_value=0.018559),
                        beta(rs_id='rs34967119', effect_allele='A', beta_value=0.0097007),
                        beta(rs_id='rs1800828', effect_allele='C', beta_value=0.0110459),
                        beta(rs_id='rs79402305', effect_allele='A', beta_value=0.0203378),
                        beta(rs_id='rs72966564', effect_allele='T', beta_value=-0.0112593),
                        beta(rs_id='rs13065394', effect_allele='T', beta_value=-0.012342),
                        beta(rs_id='rs4550782', effect_allele='T', beta_value=0.0123847),
                        beta(rs_id='rs7649164', effect_allele='T', beta_value=0.00995918),
                        beta(rs_id='rs6440833', effect_allele='A', beta_value=0.0104435),
                        beta(rs_id='rs2102506', effect_allele='A', beta_value=-0.00963073),
                        beta(rs_id='rs1599374', effect_allele='A', beta_value=0.0133638),
                        beta(rs_id='rs482204', effect_allele='A', beta_value=-0.0120556),
                        beta(rs_id='rs3850174', effect_allele='A', beta_value=-0.0148881),
                        beta(rs_id='rs301218', effect_allele='A', beta_value=-0.0110594),
                        beta(rs_id='rs4859215', effect_allele='T', beta_value=0.0124646),
                        beta(rs_id='rs4484214', effect_allele='A', beta_value=0.0113038),
                        beta(rs_id='rs1468945', effect_allele='A', beta_value=-0.0160509),
                        beta(rs_id='rs73182221', effect_allele='A', beta_value=0.0176745),
                        beta(rs_id='rs3796618', effect_allele='A', beta_value=-0.00967013),
                        beta(rs_id='rs4690085', effect_allele='A', beta_value=-0.00946361),
                        beta(rs_id='rs4698678', effect_allele='C', beta_value=0.0127609),
                        beta(rs_id='rs1502249', effect_allele='A', beta_value=0.00938648),
                        beta(rs_id='rs6838677', effect_allele='A', beta_value=-0.0103554),
                        beta(rs_id='rs4860734', effect_allele='A', beta_value=0.010745),
                        beta(rs_id='rs6846730', effect_allele='T', beta_value=-0.0152052),
                        beta(rs_id='rs2850979', effect_allele='T', beta_value=-0.01174),
                        beta(rs_id='rs4834311', effect_allele='A', beta_value=-0.00986469),
                        beta(rs_id='rs7700110', effect_allele='A', beta_value=0.0114822),
                        beta(rs_id='rs17455138', effect_allele='T', beta_value=0.0128276),
                        beta(rs_id='rs4241964', effect_allele='T', beta_value=-0.0135514),
                        beta(rs_id='rs938836', effect_allele='A', beta_value=-0.0103171),
                        beta(rs_id='rs72729847', effect_allele='T', beta_value=-0.0135853),
                        beta(rs_id='rs1458146', effect_allele='A', beta_value=-0.0114226),
                        beta(rs_id='rs10058356', effect_allele='T', beta_value=-0.0110316),
                        beta(rs_id='rs7701529', effect_allele='A', beta_value=-0.0136948),
                        beta(rs_id='rs32897', effect_allele='T', beta_value=0.0127838),
                        beta(rs_id='rs7721608', effect_allele='T', beta_value=0.0105143),
                        beta(rs_id='rs388340', effect_allele='A', beta_value=-0.0106416),
                        beta(rs_id='rs4269995', effect_allele='T', beta_value=-0.0164489),
                        beta(rs_id='rs79864120', effect_allele='A', beta_value=-0.01576),
                        beta(rs_id='rs77960', effect_allele='A', beta_value=0.0112845),
                        beta(rs_id='rs62378355', effect_allele='A', beta_value=-0.00995875),
                        beta(rs_id='rs1559253', effect_allele='A', beta_value=0.0102669),
                        beta(rs_id='rs17140201', effect_allele='A', beta_value=-0.0128042),
                        beta(rs_id='rs13172141', effect_allele='A', beta_value=0.0102482),
                        beta(rs_id='rs67988891', effect_allele='C', beta_value=-0.0159592),
                        beta(rs_id='rs2901796', effect_allele='A', beta_value=0.010664),
                        beta(rs_id='rs72834074', effect_allele='A', beta_value=0.0157017),
                        beta(rs_id='rs42210', effect_allele='C', beta_value=-0.0121263),
                        beta(rs_id='rs12518401', effect_allele='A', beta_value=-0.0111451),
                        beta(rs_id='rs7735794', effect_allele='A', beta_value=0.0143718),
                        beta(rs_id='rs465670', effect_allele='T', beta_value=0.0108088),
                        beta(rs_id='rs9394154', effect_allele='C', beta_value=-0.0104616),
                        beta(rs_id='rs9381812', effect_allele='A', beta_value=-0.0224408),
                        beta(rs_id='rs1811899', effect_allele='T', beta_value=-0.0126842),
                        beta(rs_id='rs2742986', effect_allele='T', beta_value=-0.0104988),
                        beta(rs_id='rs766406', effect_allele='T', beta_value=-0.0114101),
                        beta(rs_id='rs56402621', effect_allele='T', beta_value=0.0368966),
                        beta(rs_id='rs486416', effect_allele='A', beta_value=-0.0117019),
                        beta(rs_id='rs13203140', effect_allele='T', beta_value=-0.0106944),
                        beta(rs_id='rs12206814', effect_allele='C', beta_value=0.0097765),
                        beta(rs_id='rs2396004', effect_allele='A', beta_value=0.0102025),
                        beta(rs_id='rs3857599', effect_allele='A', beta_value=0.0135439),
                        beta(rs_id='rs2653349', effect_allele='A', beta_value=0.0305985),
                        beta(rs_id='rs1931814', effect_allele='A', beta_value=0.0125437),
                        beta(rs_id='rs2881955', effect_allele='T', beta_value=0.0123663),
                        beta(rs_id='rs12195792', effect_allele='A', beta_value=0.0145652),
                        beta(rs_id='rs11154718', effect_allele='T', beta_value=-0.00995368),
                        beta(rs_id='rs60616179', effect_allele='A', beta_value=0.0232065),
                        beta(rs_id='rs4535583', effect_allele='T', beta_value=0.0101527),
                        beta(rs_id='rs9496623', effect_allele='A', beta_value=-0.0106843),
                        beta(rs_id='rs2050185', effect_allele='A', beta_value=0.0101178),
                        beta(rs_id='rs827749', effect_allele='T', beta_value=-0.0140395),
                        beta(rs_id='rs9479402', effect_allele='T', beta_value=-0.0741237),
                        beta(rs_id='rs62436127', effect_allele='C', beta_value=0.0146695),
                        beta(rs_id='rs9348050', effect_allele='T', beta_value=0.0110808),
                        beta(rs_id='rs4027217', effect_allele='A', beta_value=-0.0112381),
                        beta(rs_id='rs10237162', effect_allele='T', beta_value=0.0166309),
                        beta(rs_id='rs10951325', effect_allele='T', beta_value=0.0164952),
                        beta(rs_id='rs12701263', effect_allele='T', beta_value=-0.0127421),
                        beta(rs_id='rs6967481', effect_allele='T', beta_value=0.0158365),
                        beta(rs_id='rs4236237', effect_allele='A', beta_value=-0.011223),
                        beta(rs_id='rs2944831', effect_allele='A', beta_value=0.0121783),
                        beta(rs_id='rs3807651', effect_allele='A', beta_value=0.0097622),
                        beta(rs_id='rs10254050', effect_allele='C', beta_value=-0.0242622),
                        beta(rs_id='rs202157', effect_allele='T', beta_value=-0.0111009),
                        beta(rs_id='rs6960360', effect_allele='A', beta_value=-0.0153433),
                        beta(rs_id='rs1131359', effect_allele='T', beta_value=0.0381967),
                        beta(rs_id='rs4729854', effect_allele='A', beta_value=-0.0178367),
                        beta(rs_id='rs73406288', effect_allele='T', beta_value=0.0231847),
                        beta(rs_id='rs76187115', effect_allele='T', beta_value=-0.0453712),
                        beta(rs_id='rs148266530', effect_allele='T', beta_value=0.0355364),
                        beta(rs_id='rs2396719', effect_allele='A', beta_value=0.0140264),
                        beta(rs_id='rs6968240', effect_allele='A', beta_value=0.0107296),
                        beta(rs_id='rs62465218', effect_allele='A', beta_value=-0.0133833),
                        beta(rs_id='rs6958557', effect_allele='T', beta_value=0.0117939),
                        beta(rs_id='rs113161209', effect_allele='A', beta_value=0.0175983),
                        beta(rs_id='rs2072413', effect_allele='T', beta_value=-0.0105492),
                        beta(rs_id='rs62479736', effect_allele='T', beta_value=0.0117295),
                        beta(rs_id='rs35524253', effect_allele='A', beta_value=0.0155012),
                        beta(rs_id='rs2979139', effect_allele='A', beta_value=-0.0128509),
                        beta(rs_id='rs2322605', effect_allele='A', beta_value=-0.0101057),
                        beta(rs_id='rs71523448', effect_allele='C', beta_value=-0.0227513),
                        beta(rs_id='rs6993892', effect_allele='T', beta_value=-0.0156925),
                        beta(rs_id='rs6468316', effect_allele='T', beta_value=-0.0094642),
                        beta(rs_id='rs7845620', effect_allele='A', beta_value=-0.0201794),
                        beta(rs_id='rs10109566', effect_allele='A', beta_value=-0.00977336),
                        beta(rs_id='rs34054660', effect_allele='A', beta_value=0.0094188),
                        beta(rs_id='rs187028', effect_allele='A', beta_value=-0.0114695),
                        beta(rs_id='rs16939162', effect_allele='A', beta_value=0.0172581),
                        beta(rs_id='rs6988733', effect_allele='T', beta_value=0.00977671),
                        beta(rs_id='rs7006885', effect_allele='A', beta_value=0.0126362),
                        beta(rs_id='rs3100052', effect_allele='A', beta_value=0.0115158),
                        beta(rs_id='rs2737245', effect_allele='T', beta_value=0.0160406),
                        beta(rs_id='rs4909296', effect_allele='T', beta_value=-0.0093573),
                        beta(rs_id='rs1871729', effect_allele='A', beta_value=-0.0104921),
                        beta(rs_id='rs6477309', effect_allele='T', beta_value=0.014064),
                        beta(rs_id='rs2844016', effect_allele='T', beta_value=0.0110916),
                        beta(rs_id='rs308521', effect_allele='T', beta_value=0.0145771),
                        beta(rs_id='rs6560218', effect_allele='T', beta_value=-0.009648),
                        beta(rs_id='rs62553781', effect_allele='T', beta_value=-0.0338118),
                        beta(rs_id='rs12378543', effect_allele='T', beta_value=-0.0102277),
                        beta(rs_id='rs555784', effect_allele='A', beta_value=-0.0108891),
                        beta(rs_id='rs295268', effect_allele='T', beta_value=-0.0123003),
                        beta(rs_id='rs3138490', effect_allele='A', beta_value=0.0104211),
                        beta(rs_id='rs10759208', effect_allele='T', beta_value=-0.0108421),
                        beta(rs_id='rs11788633', effect_allele='C', beta_value=0.0102734),
                        beta(rs_id='rs10818834', effect_allele='T', beta_value=0.0128065),
                        beta(rs_id='rs3780204', effect_allele='C', beta_value=-0.0105886),
                        beta(rs_id='rs4838192', effect_allele='T', beta_value=0.00946749),
                        beta(rs_id='rs10988239', effect_allele='T', beta_value=-0.0113038),
                        beta(rs_id='rs12380242', effect_allele='T', beta_value=-0.0106076),
                        beta(rs_id='rs28458909', effect_allele='T', beta_value=-0.031742),
                        beta(rs_id='rs497338', effect_allele='T', beta_value=0.011902),
                        beta(rs_id='rs1630848', effect_allele='A', beta_value=0.009267),
                        beta(rs_id='rs9416744', effect_allele='A', beta_value=0.0162008),
                        beta(rs_id='rs11597421', effect_allele='A', beta_value=-0.00949538),
                        beta(rs_id='rs12249410', effect_allele='T', beta_value=-0.0161244),
                        beta(rs_id='rs17712705', effect_allele='A', beta_value=-0.00995687),
                        beta(rs_id='rs2298117', effect_allele='T', beta_value=-0.00941342),
                        beta(rs_id='rs10999667', effect_allele='A', beta_value=0.0100024),
                        beta(rs_id='rs10762434', effect_allele='C', beta_value=0.0120324),
                        beta(rs_id='rs61875203', effect_allele='T', beta_value=0.0113467),
                        beta(rs_id='rs1163238', effect_allele='A', beta_value=-0.00958099),
                        beta(rs_id='rs7900191', effect_allele='T', beta_value=-0.00940837),
                        beta(rs_id='rs2173244', effect_allele='T', beta_value=-0.0104275),
                        beta(rs_id='rs3808964', effect_allele='T', beta_value=0.00992183),
                        beta(rs_id='rs9664044', effect_allele='T', beta_value=-0.012019),
                        beta(rs_id='rs10830107', effect_allele='A', beta_value=0.0122577),
                        beta(rs_id='rs76518095', effect_allele='T', beta_value=0.0183421),
                        beta(rs_id='rs12771973', effect_allele='A', beta_value=-0.011014),
                        beta(rs_id='rs7949336', effect_allele='A', beta_value=-0.0110576),
                        beta(rs_id='rs297343', effect_allele='T', beta_value=-0.010304),
                        beta(rs_id='rs10832648', effect_allele='A', beta_value=-0.0152297),
                        beta(rs_id='rs10742179', effect_allele='A', beta_value=0.0146756),
                        beta(rs_id='rs4923541', effect_allele='T', beta_value=0.0101765),
                        beta(rs_id='rs621421', effect_allele='T', beta_value=-0.0130099),
                        beta(rs_id='rs11032362', effect_allele='A', beta_value=0.0316312),
                        beta(rs_id='rs7111582', effect_allele='A', beta_value=-0.0210319),
                        beta(rs_id='rs35233100', effect_allele='T', beta_value=-0.0284884),
                        beta(rs_id='rs10838687', effect_allele='T', beta_value=0.0175979),
                        beta(rs_id='rs12808544', effect_allele='A', beta_value=-0.015624),
                        beta(rs_id='rs662094', effect_allele='A', beta_value=0.0122234),
                        beta(rs_id='rs1278402', effect_allele='A', beta_value=0.0114397),
                        beta(rs_id='rs271043', effect_allele='A', beta_value=0.0101268),
                        beta(rs_id='rs1508608', effect_allele='A', beta_value=0.0124923),
                        beta(rs_id='rs4121878', effect_allele='C', beta_value=0.00979473),
                        beta(rs_id='rs17577073', effect_allele='A', beta_value=0.0108108),
                        beta(rs_id='rs2514214', effect_allele='A', beta_value=0.0109572),
                        beta(rs_id='rs4936290', effect_allele='A', beta_value=-0.0110364),
                        beta(rs_id='rs7108229', effect_allele='A', beta_value=0.00932796),
                        beta(rs_id='rs3867239', effect_allele='A', beta_value=0.01152),
                        beta(rs_id='rs74357745', effect_allele='A', beta_value=0.0159952),
                        beta(rs_id='rs7943634', effect_allele='T', beta_value=-0.0111804),
                        beta(rs_id='rs3782860', effect_allele='T', beta_value=0.011116),
                        beta(rs_id='rs1799464', effect_allele='A', beta_value=-0.0103614),
                        beta(rs_id='rs12298405', effect_allele='T', beta_value=-0.0114625),
                        beta(rs_id='rs2433634', effect_allele='A', beta_value=-0.011749),
                        beta(rs_id='rs11611435', effect_allele='T', beta_value=0.0110995),
                        beta(rs_id='rs4436614', effect_allele='A', beta_value=0.0120274),
                        beta(rs_id='rs1843888', effect_allele='A', beta_value=0.0229432),
                        beta(rs_id='rs247929', effect_allele='C', beta_value=0.0142529),
                        beta(rs_id='rs7975791', effect_allele='T', beta_value=0.0258352),
                        beta(rs_id='rs10735836', effect_allele='A', beta_value=-0.0155518),
                        beta(rs_id='rs7299922', effect_allele='A', beta_value=0.0114368),
                        beta(rs_id='rs487722', effect_allele='T', beta_value=0.0126619),
                        beta(rs_id='rs11174782', effect_allele='T', beta_value=-0.0160855),
                        beta(rs_id='rs10877962', effect_allele='T', beta_value=0.0112827),
                        beta(rs_id='rs711098', effect_allele='A', beta_value=0.0100561),
                        beta(rs_id='rs7959983', effect_allele='T', beta_value=-0.014009),
                        beta(rs_id='rs7304278', effect_allele='A', beta_value=-0.0144347),
                        beta(rs_id='rs7298532', effect_allele='T', beta_value=0.0120263),
                        beta(rs_id='rs3955311', effect_allele='T', beta_value=0.0131154),
                        beta(rs_id='rs80097534', effect_allele='T', beta_value=-0.0172005),
                        beta(rs_id='rs7964138', effect_allele='A', beta_value=0.0105707),
                        beta(rs_id='rs9597241', effect_allele='A', beta_value=0.0157582),
                        beta(rs_id='rs2321993', effect_allele='A', beta_value=-0.0116257),
                        beta(rs_id='rs9571526', effect_allele='T', beta_value=-0.0115617),
                        beta(rs_id='rs2593487', effect_allele='A', beta_value=-0.0125354),
                        beta(rs_id='rs495593', effect_allele='A', beta_value=0.0112209),
                        beta(rs_id='rs45597035', effect_allele='A', beta_value=-0.00980824),
                        beta(rs_id='rs9573980', effect_allele='A', beta_value=0.0589561),
                        beta(rs_id='rs61963491', effect_allele='A', beta_value=0.102414),
                        beta(rs_id='rs9523817', effect_allele='A', beta_value=-0.0106609),
                        beta(rs_id='rs1886205', effect_allele='A', beta_value=0.0144381),
                        beta(rs_id='rs9558942', effect_allele='T', beta_value=-0.00984319),
                        beta(rs_id='rs3815983', effect_allele='T', beta_value=-0.0109785),
                        beta(rs_id='rs1163628', effect_allele='A', beta_value=-0.0142635),
                        beta(rs_id='rs61990287', effect_allele='A', beta_value=0.0110353),
                        beta(rs_id='rs2878172', effect_allele='A', beta_value=-0.00998016),
                        beta(rs_id='rs962961', effect_allele='T', beta_value=-0.0119951),
                        beta(rs_id='rs6573308', effect_allele='T', beta_value=0.00982424),
                        beta(rs_id='rs7143933', effect_allele='T', beta_value=0.0110793),
                        beta(rs_id='rs2978382', effect_allele='T', beta_value=0.00997671),
                        beta(rs_id='rs4903203', effect_allele='A', beta_value=0.0115603),
                        beta(rs_id='rs12436039', effect_allele='T', beta_value=0.014497),
                        beta(rs_id='rs4550384', effect_allele='T', beta_value=0.0119551),
                        beta(rs_id='rs710284', effect_allele='T', beta_value=0.00966848),
                        beta(rs_id='rs11845599', effect_allele='A', beta_value=-0.0127102),
                        beta(rs_id='rs59986227', effect_allele='C', beta_value=-0.0132245),
                        beta(rs_id='rs12442008', effect_allele='T', beta_value=0.0127329),
                        beta(rs_id='rs4775086', effect_allele='A', beta_value=-0.0113763),
                        beta(rs_id='rs12594634', effect_allele='T', beta_value=-0.0107548),
                        beta(rs_id='rs12442674', effect_allele='A', beta_value=0.0120939),
                        beta(rs_id='rs1873958', effect_allele='A', beta_value=0.0136386),
                        beta(rs_id='rs72773411', effect_allele='A', beta_value=0.0131039),
                        beta(rs_id='rs12445235', effect_allele='C', beta_value=-0.0097861),
                        beta(rs_id='rs2304467', effect_allele='C', beta_value=-0.0100462),
                        beta(rs_id='rs11641239', effect_allele='T', beta_value=0.0109354),
                        beta(rs_id='rs7203707', effect_allele='A', beta_value=-0.0104966),
                        beta(rs_id='rs35888008', effect_allele='A', beta_value=-0.0122264),
                        beta(rs_id='rs1344490', effect_allele='T', beta_value=-0.00984493),
                        beta(rs_id='rs12927162', effect_allele='A', beta_value=0.0248274),
                        beta(rs_id='rs1421085', effect_allele='T', beta_value=-0.0202621),
                        beta(rs_id='rs6499821', effect_allele='A', beta_value=0.017563),
                        beta(rs_id='rs2550298', effect_allele='T', beta_value=-0.0179894),
                        beta(rs_id='rs8063159', effect_allele='A', beta_value=0.0144827),
                        beta(rs_id='rs72790386', effect_allele='T', beta_value=0.0269846),
                        beta(rs_id='rs11641811', effect_allele='A', beta_value=-0.0106683),
                        beta(rs_id='rs17604349', effect_allele='A', beta_value=-0.016523),
                        beta(rs_id='rs1061032', effect_allele='T', beta_value=0.0288978),
                        beta(rs_id='rs11545787', effect_allele='A', beta_value=-0.0181401),
                        beta(rs_id='rs2232831', effect_allele='A', beta_value=0.0124124),
                        beta(rs_id='rs12938235', effect_allele='T', beta_value=0.00943034),
                        beta(rs_id='rs2011528', effect_allele='T', beta_value=-0.0147613),
                        beta(rs_id='rs3760381', effect_allele='A', beta_value=0.013339),
                        beta(rs_id='rs7225002', effect_allele='A', beta_value=-0.0112885),
                        beta(rs_id='rs12051', effect_allele='A', beta_value=-0.0127524),
                        beta(rs_id='rs612720', effect_allele='A', beta_value=0.0093955),
                        beta(rs_id='rs55846845', effect_allele='A', beta_value=-0.0110217),
                        beta(rs_id='rs72829706', effect_allele='A', beta_value=0.0274752),
                        beta(rs_id='rs8072058', effect_allele='A', beta_value=-0.0116783),
                        beta(rs_id='rs412000', effect_allele='C', beta_value=-0.0104066),
                        beta(rs_id='rs58681483', effect_allele='A', beta_value=0.0180656),
                        beta(rs_id='rs72841368', effect_allele='A', beta_value=-0.0144235),
                        beta(rs_id='rs2916148', effect_allele='A', beta_value=0.0128878),
                        beta(rs_id='rs2580160', effect_allele='A', beta_value=0.0112206),
                        beta(rs_id='rs62082402', effect_allele='T', beta_value=0.0211412),
                        beta(rs_id='rs1788784', effect_allele='A', beta_value=-0.0112061),
                        beta(rs_id='rs1013987', effect_allele='T', beta_value=-0.0124637),
                        beta(rs_id='rs4419127', effect_allele='A', beta_value=0.0205434),
                        beta(rs_id='rs17643207', effect_allele='A', beta_value=0.0193778),
                        beta(rs_id='rs9950528', effect_allele='A', beta_value=-0.0103637),
                        beta(rs_id='rs75457814', effect_allele='A', beta_value=-0.0117882),
                        beta(rs_id='rs12969848', effect_allele='T', beta_value=0.0160081),
                        beta(rs_id='rs9956387', effect_allele='A', beta_value=-0.00934139),
                        beta(rs_id='rs28602385', effect_allele='A', beta_value=0.0106989),
                        beta(rs_id='rs4800998', effect_allele='A', beta_value=0.01621),
                        beta(rs_id='rs9964420', effect_allele='A', beta_value=-0.0235868),
                        beta(rs_id='rs11152350', effect_allele='A', beta_value=-0.0125657),
                        beta(rs_id='rs34329963', effect_allele='T', beta_value=-0.0154058),
                        beta(rs_id='rs1025601', effect_allele='T', beta_value=-0.00966197),
                        beta(rs_id='rs10402849', effect_allele='T', beta_value=0.012545),
                        beta(rs_id='rs36055559', effect_allele='A', beta_value=-0.0155822),
                        beta(rs_id='rs7248205', effect_allele='T', beta_value=0.0122863),
                        beta(rs_id='rs9636202', effect_allele='A', beta_value=-0.0126398),
                        beta(rs_id='rs73026775', effect_allele='A', beta_value=-0.0146423),
                        beta(rs_id='rs4804951', effect_allele='A', beta_value=0.0107465),
                        beta(rs_id='rs56113850', effect_allele='T', beta_value=-0.0106669),
                        beta(rs_id='rs58876439', effect_allele='A', beta_value=0.0220732),
                        beta(rs_id='rs1007204', effect_allele='A', beta_value=-0.0101447),
                        beta(rs_id='rs11670534', effect_allele='T', beta_value=-0.0140938),
                        beta(rs_id='rs2760545', effect_allele='T', beta_value=0.0108296),
                        beta(rs_id='rs6131805', effect_allele='T', beta_value=0.0114936),
                        beta(rs_id='rs947088', effect_allele='T', beta_value=0.010528),
                        beta(rs_id='rs6131942', effect_allele='A', beta_value=-0.0140132),
                        beta(rs_id='rs1474754', effect_allele='A', beta_value=-0.0105825),
                        beta(rs_id='rs6047481', effect_allele='A', beta_value=0.0104565),
                        beta(rs_id='rs1737893', effect_allele='T', beta_value=-0.0112572),
                        beta(rs_id='rs2072727', effect_allele='T', beta_value=0.013632),
                        beta(rs_id='rs57236847', effect_allele='C', beta_value=0.0104442),
                        beta(rs_id='rs695459', effect_allele='T', beta_value=-0.00984758),
                        beta(rs_id='rs28459838', effect_allele='T', beta_value=0.0121112),
                        beta(rs_id='rs118047999', effect_allele='C', beta_value=0.0112614),
                        beta(rs_id='rs139911', effect_allele='T', beta_value=-0.0141776),
                        beta(rs_id='rs762995', effect_allele='A', beta_value=-0.0101045),
                        beta(rs_id='rs6007594', effect_allele='A', beta_value=-0.0119836)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Circadian rhythms'

about = ('Everybody has natural waking and sleeping patterns, which are called '
         'circadian rhythms. These may shift as we move from childhood to adulthood, '
         'but we all have certain times of day when we naturally feel energized or '
         'sleepy. Disrupted circadian rhythms can be a risk factor for poor sleep '
         "quality, weight gain and diabetes. Our body's circadian rhythms determine "
         'our tendency to being a "morning" or "evening" person, and this is partly '
         'due to genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 396 genetic variants, specifically called '
                                  'SNPs, in genes associated with circadian rhythms.')

other_factors_statement = ("Many factors impact a person's circadian rhythms, including, age, lifestyle, "
                           'diet, and genetics. Genetics has a moderate overall impact on circadian '
                           'rhythms when you consider all the other factors.')

other_factors = [{'content': "Regularly traveling across time zones can disrupt the body's "
                             'circadian rhythms. Not keeping a regular bedtime, having '
                             'late-night computer, TV, and cell phone screen time, and othe '
                             'forms of late-night light exposure can also disrupt circadian '
                             'rhythms. All of these make getting good sleep a challenge.',
                  'title': 'Lifestyle'},
                 {'content': 'Consuming stimulants like caffeine later in the day (such as '
                             "after noon) can disrupt the body's circadian rhythms and cause "
                             'extra wakefulness periods, particularly if there is a '
                             'sensitivity to caffeine.',
                  'title': 'Diet'}]

what_your_result_means = {'Average risk': 'Depending on your circadian rhythm and other factors, you '
                                           'may have an average risk for sleep disturbances, and '
                                           'diabetes or weight gain related to that.',
                           'High risk': 'Depending on your circadian rhythm and other factors, you may '
                                        'have a higher risk for sleep disturbances, and diabetes or '
                                        'weight gain related to that.',
                           'Low risk': 'Depending on your circadian rhythm and other factors, you may '
                                       'have a lower risk for sleep disturbances, and diabetes or weight '
                                       'gain related to that.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average circadian rhythm.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have an eveningness circadian rhythm and be an "evening '
                                 'person."',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a morningness circadian rhythm and be a "morning person."'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to maintain your '
                                             'circadian rhythms and stay healthy. Certain tests can also '
                                             'check your risk factors for related health issues. Talk to '
                                             'your practitioner about what is right for you.',
                             'High risk': 'There are things you can do to try to maintain your circadian '
                                          'rhythms and stay healthy. Certain tests can also check your '
                                          'risk factors for related health issues. Talk to your '
                                          'practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to maintain your circadian '
                                         'rhythms and stay healthy. Certain tests can also check your risk '
                                         'factors for related health issues. Talk to your practitioner '
                                         'about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for health issues related to disrupted '
                                                'circadian rhythms. Consider talking to your '
                                                'practitioner about ordering certain tests, such '
                                                'as sleep tests if getting good sleep is hard '
                                                'for you. They can tell you more about this and '
                                                'possible next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'A regular bedtime is helpful for maintaining '
                                                'regular circadian rhythms. Consider limiting '
                                                'the amount of time you spend in front of a '
                                                'screen or exposed to light in the evenings, as '
                                                'it can disrupt sleep. If you regularly have '
                                                'poor quality sleep, or less sleep than you '
                                                'need, this can increase the risk for diabetes '
                                                'or weight gain over time. Keeping a sleep diary '
                                                'can help you keep track of the amount and '
                                                "quality of sleep you're getting.",
                                     'title': 'Lifestyle'},
                                    {'content': "It's always a good idea to be mindful about "
                                                'consuming items with caffeine later in the day, '
                                                'since they can keep you awake and disrupt your '
                                                'sleep. This includes coffee, green/black tea, '
                                                'caffeinated soft drinks (like Mountain Dew), '
                                                'energy drinks, and chocolate or '
                                                'chocolate-containing foods.',
                                     'title': 'Diet'}],
                   'High risk': [{'content': 'Since you tend to prefer evenings, consider '
                                             'limiting the amount of time you spend in front of '
                                             'a screen or exposed to light late at night. If you '
                                             'regularly have poor quality sleep, or less sleep '
                                             'than you need, this can increase the risk for '
                                             'diabetes or weight gain over time. Keeping a sleep '
                                             'diary can help you keep track of the amount and '
                                             "quality of sleep you're getting.",
                                  'title': 'Lifestyle'},
                                 {'content': 'Try to cut back on consuming items with caffeine '
                                             'later in the day, since they can keep you up '
                                             'longer than usual. This includes coffee, '
                                             'green/black tea, caffeinated soft drinks (like '
                                             'Mountain Dew), energy drinks, and chocolate or '
                                             'chocolate-containing foods.',
                                  'title': 'Diet'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for health issues related to disrupted circadian '
                                            'rhythms. Consider talking to your practitioner '
                                            'about ordering certain tests, such as sleep tests '
                                            'if getting good sleep is hard for you. They can '
                                            'tell you more about this and possible next steps '
                                            'that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'A regular bedtime is helpful for maintaining '
                                            'regular circadian rhythms. If you are up later than '
                                            'usual, consider limiting the amount of time you '
                                            'spend in front of a screen or exposed to light, as '
                                            'it can disrupt sleep. If you regularly have poor '
                                            'quality sleep, or less sleep than you need, this '
                                            'can increase the risk for diabetes or weight gain '
                                            'over time. Keeping a sleep diary can help you keep '
                                            "track of the amount and quality of sleep you're "
                                            'getting.',
                                 'title': 'Lifestyle'},
                                {'content': "It's always a good idea to be mindful about "
                                            'consuming items with caffeine later in the day, '
                                            'since they can keep you awake and disrupt your '
                                            'sleep. This includes coffee, green/black tea, '
                                            'caffeinated soft drinks (like Mountain Dew), energy '
                                            'drinks, and chocolate or chocolate-containing '
                                            'foods.',
                                 'title': 'Diet'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_short_description = "Circadian rhythm"
trait_science = "Chronotype is a moderately heritable trait - about 50% of differences observed among people has a genetic origin. Our analysis was based on 396 SNPs associated with people's preferences for being a 'morning person'. The average sleep time of the 5% of individuals carrying the most morningness alleles is 25 min earlier than the 5% carrying the least alleles."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the chronotype. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the light level."
