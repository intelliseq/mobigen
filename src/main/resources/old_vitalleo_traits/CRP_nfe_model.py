from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "C-reactive_protein"
trait_pmids = ["30388399", "14617617"]  # 30388399 Table1, Suppl.TableS3, Suppl.TableS7
trait_heritability = 0.4  # PMID:14617617
trait_snp_heritability = None  # as fraction
trait_explained = 0.07
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Serum concentration of C-reactive protein (CRP) is moderately heritable - about 40% of differences observed among people has a genetic origin [1]. Our test is based on the meta-analysis study, which combined results of almost 130 previously published genome wide association studies (GWAS) [2]. Altogether, these studies included almost 350 000 individuals of European ancestry and 85 variants significantly associated with CRP blood level in European population. These SNPs explain about 7% of variance in blood CRP concentration [2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score. "

genes = ["RPL5P6", "TMEM18", "FABP1", "IL1R1-AS1", "LINC01953", "TRAIP", "AC022217.3", "HLA-DQA1", "TPI1P3", "MIR588",
         "L3MBTL3", "AC073072.1", "AC083864.2", "RNA5SP276", "AC084083.1", "ABO", "LIPA", "RN7SKP151", "AC018410.1",
         "MIR6503", "MIR4692", "AC018475.1", "RGS6", "DMXL2", "FTO", "RN7SL442P", "CD300LF", "AC090340.1", "AC090771.1",
         "AL121845.3", "AF064858.1", "AL021707.2", "ZDHHC18", "RNU1-70P", "SERPINA1", "WDR72", "PABPC4-AS1", "LEPR",
         "IL6R", "CRP", "OR2B11", "GCKR", "IL1F10", "BCL7B", "AC021736.1", "AC068643.2", "HNF1A", "AC009560.1", "SALL1",
         "PTPN2", "APOC1", "MIR3646", "AL606517.2", "LEPR", "AC097063.1", "LEPR", "IL6R", "ACKR1", "OR10J3", "OR10J6P",
         "CRPP1", "CRP", "AL445528.1", "AL592295.3", "NLRP3", "OR2B11", "RNU6-1151P", "AC021736.1", "AC069185.1",
         "OASL", "AC027688.2", "SALL1", "AC007344.1", "LINC01571", "NDUFB8P2", "CD300LF", "APOE", "APOC1"]

# Meta-analysis of  HapMap (204,402 individuals from 78 studies) and 1KG (148,164 individuals  from 49 studies) imputed-genotype GWASs; individuals of European ancestry; details in Suppl.TableS1
# I combined the following variants (in all analyses effect given as beta coefficients representing a 1-unit change in the natural-log-transformed CRP (mg/L) per copy increment in coded allele):
# 1) Table1: Newly Identified Loci Associated with CRP (Loci Found in the HapMap GWAS+Additional Loci Found in the 1KG GWAS)
# 2) Suppl.TableS3:  Previously identified loci for CRP levels significant in the current HapMap GWAS
# 3) Suppl.TableS7 Additional joint/conditional variants in 1KG GWAS (I used joint p-value,beta,sd => values after conditioning on other loci); one CRP variant not in gnomAD database -removed
# Coded allele==effect allele: I checked a few variants in gwas catalog and coded alleles are shown as effect(risk) alleles, but:
# frequencies in Table1 are labeled as Frequency of coded allele, but are frequencies of the minor allele
# description_genotype = {} #for single SNP test
# description_genotype["low_trait"] = "hom" # for one snip test
# description_genotype["average_trait"] = "het" #for one snip test
# description_genotype["high_trait"] = "hom" # for one snip test

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=1.7525, category_name='Low risk'),
                        QuantitativeCategory(from_=1.7525, to=2.8835, category_name='Average risk'),
                        QuantitativeCategory(from_=2.8835, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs469772', effect_allele='T', beta_value=-0.031),
                        beta(rs_id='rs12995480', effect_allele='T', beta_value=-0.031),
                        beta(rs_id='rs4246598', effect_allele='A', beta_value=0.022),
                        beta(rs_id='rs9284725', effect_allele='C', beta_value=0.027),
                        beta(rs_id='rs1441169', effect_allele='G', beta_value=-0.025),
                        beta(rs_id='rs2352975', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs17658229', effect_allele='C', beta_value=0.056),
                        beta(rs_id='rs9271608', effect_allele='G', beta_value=0.042),
                        beta(rs_id='rs12202641', effect_allele='T', beta_value=-0.023),
                        beta(rs_id='rs1490384', effect_allele='T', beta_value=-0.025),
                        beta(rs_id='rs9385532', effect_allele='T', beta_value=-0.026),
                        beta(rs_id='rs1880241', effect_allele='G', beta_value=-0.028),
                        beta(rs_id='rs2710804', effect_allele='C', beta_value=0.021),
                        beta(rs_id='rs2064009', effect_allele='C', beta_value=-0.027),
                        beta(rs_id='rs2891677', effect_allele='C', beta_value=-0.02),
                        beta(rs_id='rs1051338', effect_allele='G', beta_value=0.024),
                        beta(rs_id='rs10832027', effect_allele='G', beta_value=-0.026),
                        beta(rs_id='rs10838687', effect_allele='G', beta_value=-0.031),
                        beta(rs_id='rs1582763', effect_allele='A', beta_value=-0.022),
                        beta(rs_id='rs7121935', effect_allele='A', beta_value=-0.022),
                        beta(rs_id='rs11108056', effect_allele='G', beta_value=-0.028),
                        beta(rs_id='rs2239222', effect_allele='G', beta_value=0.035),
                        beta(rs_id='rs4774590', effect_allele='A', beta_value=-0.022),
                        beta(rs_id='rs1558902', effect_allele='A', beta_value=0.034),
                        beta(rs_id='rs178810', effect_allele='T', beta_value=0.02),
                        beta(rs_id='rs10512597', effect_allele='T', beta_value=-0.037),
                        beta(rs_id='rs4092465', effect_allele='A', beta_value=-0.027),
                        beta(rs_id='rs12960928', effect_allele='C', beta_value=0.024),
                        beta(rs_id='rs2315008', effect_allele='T', beta_value=-0.023),
                        beta(rs_id='rs2836878', effect_allele='G', beta_value=0.043),
                        beta(rs_id='rs6001193', effect_allele='G', beta_value=-0.028),
                        beta(rs_id='rs75460349', effect_allele='A', beta_value=0.086),
                        beta(rs_id='rs1514895', effect_allele='A', beta_value=-0.027),
                        beta(rs_id='rs112635299', effect_allele='T', beta_value=-0.107),
                        beta(rs_id='rs1189402', effect_allele='A', beta_value=0.025),
                        beta(rs_id='rs2293476', effect_allele='C', beta_value=0.03),
                        beta(rs_id='rs1805096', effect_allele='G', beta_value=0.104),
                        beta(rs_id='rs4129267', effect_allele='C', beta_value=0.088),
                        beta(rs_id='rs2794520', effect_allele='C', beta_value=0.182),
                        beta(rs_id='rs10925027', effect_allele='T', beta_value=0.036),
                        beta(rs_id='rs1260326', effect_allele='T', beta_value=0.073),
                        beta(rs_id='rs13409371', effect_allele='A', beta_value=0.048),
                        beta(rs_id='rs13233571', effect_allele='C', beta_value=0.057),
                        beta(rs_id='rs4841132', effect_allele='G', beta_value=0.065),
                        beta(rs_id='rs10778215', effect_allele='T', beta_value=0.033),
                        beta(rs_id='rs7310409', effect_allele='G', beta_value=0.137),
                        beta(rs_id='rs340005', effect_allele='A', beta_value=0.03),
                        beta(rs_id='rs10521222', effect_allele='C', beta_value=0.104),
                        beta(rs_id='rs2852151', effect_allele='A', beta_value=0.025),
                        beta(rs_id='rs4420638', effect_allele='A', beta_value=0.229),
                        beta(rs_id='rs1800961', effect_allele='C', beta_value=0.112),
                        beta(rs_id='rs7539178', effect_allele='A', beta_value=-0.036),
                        beta(rs_id='rs67129560', effect_allele='A', beta_value=-0.034),
                        beta(rs_id='rs72683129', effect_allele='A', beta_value=-0.073),
                        beta(rs_id='rs13375019', effect_allele='C', beta_value=-0.083),
                        beta(rs_id='rs12083537', effect_allele='A', beta_value=0.05),
                        beta(rs_id='rs61812598', effect_allele='A', beta_value=-0.093),
                        beta(rs_id='rs3027012', effect_allele='T', beta_value=-0.032),
                        beta(rs_id='rs56288844', effect_allele='A', beta_value=-0.185),
                        beta(rs_id='rs6695494', effect_allele='T', beta_value=0.033),
                        beta(rs_id='rs149520992', effect_allele='T', beta_value=-0.272),
                        beta(rs_id='rs72698571', effect_allele='T', beta_value=-0.101),
                        beta(rs_id='rs12029262', effect_allele='C', beta_value=-0.114),
                        beta(rs_id='rs1800947', effect_allele='C', beta_value=0.15),
                        beta(rs_id='rs3091244', effect_allele='A', beta_value=0.298),
                        beta(rs_id='rs2246469', effect_allele='A', beta_value=0.208),
                        beta(rs_id='rs11265263', effect_allele='A', beta_value=-0.141),
                        beta(rs_id='rs4131568', effect_allele='T', beta_value=-0.187),
                        beta(rs_id='rs9427392', effect_allele='A', beta_value=0.039),
                        beta(rs_id='rs67090117', effect_allele='T', beta_value=-0.041),
                        beta(rs_id='rs9988571', effect_allele='T', beta_value=-0.03),
                        beta(rs_id='rs7012637', effect_allele='A', beta_value=0.037),
                        beta(rs_id='rs4841133', effect_allele='A', beta_value=-0.054),
                        beta(rs_id='rs1616534', effect_allele='T', beta_value=0.028),
                        beta(rs_id='rs7310409', effect_allele='A', beta_value=-0.136),
                        beta(rs_id='rs2259883', effect_allele='C', beta_value=-0.036),
                        beta(rs_id='rs1874467', effect_allele='C', beta_value=0.034),
                        beta(rs_id='rs116971887', effect_allele='T', beta_value=-0.11),
                        beta(rs_id='rs1965024', effect_allele='A', beta_value=-0.034),
                        beta(rs_id='rs7200325', effect_allele='T', beta_value=0.027),
                        beta(rs_id='rs79376728', effect_allele='T', beta_value=-0.026),
                        beta(rs_id='rs1292056', effect_allele='T', beta_value=-0.023),
                        beta(rs_id='rs2384955', effect_allele='T', beta_value=-0.038),
                        beta(rs_id='rs429358', effect_allele='T', beta_value=0.23),
                        beta(rs_id='rs157595', effect_allele='A', beta_value=0.05)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'C-reactive protein level'

about = ('C-reactive protein is normally found in the blood at certain levels. This '
         'level can rise when the body has inflammation, which plays a role in many '
         'complex diseases. Inflammation can be a response to infection or other '
         'medical conditions. High C-reactive protein levels may also be an indicator '
         'of infection or inflammatory diseases like lupus, inflammatory bowel '
         'disease, or rheumatoid arthritis. Chronically high C-reactive protein levels '
         'are a risk factor for serious health issues such as coronary artery disease, '
         'stroke, cancer, depression, autoimmune disease, arthritis, injury, skin '
         'conditions, and Type II Diabetes.')

how_your_result_was_calculated = ('Your result is a risk based on 86 genetic variants, specifically called '
                                  'SNPs, in genes associated with blood C-reactive protein level. Overall, this '
                                  'explains about 7% of the genetic variation in blood C-reactive protein level '
                                  'in the human population, based on current scientific knowledge.')

other_factors_statement = ("Many factors impact a person's blood pressure, including, neurological "
                           'health, spinal subluxation, age, biological sex, ethnicity, lifestyle, other '
                           'health issues, and genetics. Genetics has a moderate overall impact on blood '
                           'pressure when you consider all the other factors.')

other_factors = [{'content': 'People of African descent tend to have higher C-reactive protein '
                             'levels.',
                  'title': 'Ethnic Background'},
                 {'content': 'Having another health issue, such as diabetes, chronic kidney '
                             'disease, thyroid problems, or sleep apnea, are risk factors for '
                             'high blood pressure. Upper cervical spinal misalignment has been '
                             'proven as a cause of high blood pressure and should later be evaluated ',
                  'title': 'Other Health Issues'},
                 {'content': 'A diet high in carbohydrates, particularly refined carbohydrates '
                             'and sugar, and bad fats increases inflammation. Risk factors for '
                             'diabetes and heart disease can impact C-reactive protein levels. '
                             'These include having a less active lifestyle, being overweight, '
                             'smoking, and a diet high in bad fats. Certain genotypes create '
                             'additional problems when consuming saturated fats and have more '
                             'difficulty managing carb-related inflammation.',
                  'title': 'Lifestyle'},
                 {'content': 'Nutrient deficiencies such as Vitamin D, B-vitamins, and '
                             'antioxidants can lead to inflammation. The current, standard '
                             'diet is high in inflammatory Omega 6 and low in '
                             'anti-inflammatory Omega 3. This Omega 6:3 imbalance can be a '
                             'direct cause of inflammatory related illness in the brain, '
                             'heart, pulmonary system, and immune system.',
                  'title': 'Diet'}]

what_your_result_means = {'Average risk': 'Depending on your C-reactive protein level and other '
                                           'factors, you may have an average risk for inflammatory '
                                           'medical conditions, heart disease, or diabetes.',
                           'High risk': 'Depending on your C-reactive protein level and other factors, '
                                        'you may have a higher risk for inflammatory medical conditions, '
                                        'heart disease, or diabetes.',
                           'Low risk': 'Depending on your C-reactive protein level and other factors, '
                                       'you may have a lower risk for inflammatory medical conditions, '
                                       'heart disease, or diabetes.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you may be more likely '
                                    'to have an average blood C-reactive protein level.',
                    'High risk': 'Based on the genetic factors tested, you may be more likely to '
                                 'have a higher blood C-reactive protein level.',
                    'Low risk': 'Based on the genetic factors tested, you may be more likely to '
                                'have a lower blood C-reactive protein level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent heart disease '
                                             'and diabetes to stay healthy. Certain blood tests can also '
                                             'check your risk factors for a high C-reactive protein level. '
                                             'Talk to your practitioner about what is right for you.',
                             'High risk': 'There are things you can do to try to prevent heart disease and '
                                          'diabetes to stay healthy. Certain blood tests can also check '
                                          'your risk factors for health issues related to a high '
                                          'C-reactive protein level. Talk to your practitioner about what '
                                          'is right for you.',
                             'Low risk': 'There are things you can do to try to prevent heart disease and '
                                         'diabetes to stay healthy. Certain blood tests can also check '
                                         'your risk factors for a high C-reactive protein level. Talk to '
                                         'your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk factors for heart disease, diabetes, or '
                                                'other health issues. Talk to them about this '
                                                'and possible next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Carbohydrates, particularly processed carbs, '
                                                'flours, and sugars can increase inflammation. '
                                                'Even complex carbohydrates such as oats, rice, '
                                                'and whole grain breads can still turn into '
                                                'sugar and fat, and create inflammation. A diet '
                                                'rich in high fiber plant products such as '
                                                'vegetables and low-glycemic fruits like '
                                                'berries, good fats, and quality, lean proteins '
                                                'will help you to lower your risk. Also consider '
                                                'adding more omega-3 fats into your diet, while '
                                                'lowering your intake of omega 6 fats. Talk to '
                                                'your practitioner to determine whether a '
                                                'Mediterranean plan, Modified Mediterranean, or '
                                                'a low-carb/keto plan is right for you.',
                                     'title': 'Diet'},
                                    {'content': 'If you smoke, now is a good time to quit -- '
                                                'smoking increases the risk for heart disease. '
                                                "As well, try to get your body moving. It's "
                                                'recommended that adults get 2.5 hours of '
                                                'moderate physical activity, like brisk walking '
                                                'or biking, every week. Not only can this lower '
                                                'your risk of heart disease and diabetes, but it '
                                                'can also help you maintain a healthy weight.',
                                     'title': 'Lifestyle'}],
                   'High risk': [{'content': 'Your practitioner may be able to order an '
                                             'Inflammation and Vitamin D blood test or others to '
                                             'check your risk factors for heart disease, '
                                             'diabetes, or other health issues. Talk to them '
                                             'about this and possible next steps that you can '
                                             'take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Carbohydrates, particularly processed carbs, '
                                             'flours, and sugars can increase inflammation. Even '
                                             'complex carbohydrates such as oats, rice, and '
                                             'whole grain breads can still turn into sugar and '
                                             'fat, and create inflammation.  A diet rich in high '
                                             'fiber plant products such as vegetables and '
                                             'low-glycemic fruits like berries, good fats, and '
                                             'quality, lean proteins will help you to lower your '
                                             'risk. Also consider adding more omega-3 fats into '
                                             'your diet, while lowering your intake of omega 6 '
                                             'fats. Talk to your practitioner to determine '
                                             'whether a Mediterranean plan, Modified '
                                             'Mediterranean, or a low-carb/keto plan is right '
                                             'for you.',
                                  'title': 'Diet'},
                                 {'content': 'If you smoke, now is a good time to quit -- '
                                             'smoking increases the risk for heart disease. As '
                                             "well, try to get your body moving. It's "
                                             'recommended that adults get 2.5 hours of moderate '
                                             'physical activity, like brisk walking or biking, '
                                             'every week. Not only can this lower your risk of '
                                             'heart disease and diabetes, but it can also help '
                                             'you maintain a healthy weight.',
                                  'title': 'Lifestyle'},
                                 {'content': 'Talk to your practitioner about '
                                             'supplements such as omega-3 fatty acids, turmeric '
                                             'and boswellia. An omega-3 blood spot status test '
                                             'may be helpful in determining your levels.',
                                  'title': 'Supplements'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'factors for heart disease, diabetes, or other '
                                            'health issues. Talk to them about this and possible '
                                            'next steps that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Carbohydrates, particularly processed carbs, '
                                            'flours, and sugars can increase inflammation. Even '
                                            'complex carbohydrates such as oats, rice, and whole '
                                            'grain breads can still turn into sugar and fat, and '
                                            'create inflammation. A diet rich in high fiber '
                                            'plant products such as vegetables and low-glycemic '
                                            'fruits like berries, good fats, and quality, lean '
                                            'proteins will help you to lower your risk. Also '
                                            'consider adding more omega-3 fats into your diet, '
                                            'while lowering your intake of omega 6 fats. Talk to '
                                            'your practitioner to determine whether a '
                                            'Mediterranean plan, Modified Mediterranean, or a '
                                            'low-carb/keto plan is right for you.',
                                 'title': 'Diet'},
                                {'content': 'If you smoke, now is a good time to quit -- smoking '
                                            'increases the risk for heart disease. As well, try '
                                            "to get your body moving. It's recommended that "
                                            'adults get 2.5 hours of moderate physical activity, '
                                            'like brisk walking or biking, every week. Not only '
                                            'can this lower your risk of heart disease and '
                                            'diabetes, but it can also help you maintain a '
                                            'healthy weight.',
                                 'title': 'Lifestyle'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_short_description = "C-reactive protein"
trait_science = "Serum concentration of C-reactive protein(CRP) is moderately heritable - about 40% of differences observed among people has a genetic origin. Our analysis was based on 86 SNPs associated with CRP blood levels. These variants jointly explain about 7% of the variation in plasma amounts of this protein between people."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the circulating C-reactive protein level. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
