from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Telomere_length"
trait_pmids = ["23535734"]  # 23535734  table 1
trait_heritability = 0.44
trait_snp_heritability = None
trait_explained = 0.012
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "Telomere length is highly heritable, and it is estimated that about 44-80% of differences observed among people at the same age has a genetic origin [1]. Our test is based on results of the genome wide association studies (GWAS) [1], which included over 47,000 individuals of European ancestry. The study identified 7 variants significantly associated with the telomere length. Together these SNPs explain about 1.2% of between people variance in telomere length [2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["AC003973.1", "MIR4454", "MYNN", "STN1", "TERT", "TSPYL6", "ZBTB46-AS1"]

######### telomere_length
# na podstawie: doi: doi:10.1038/ng.2528
# długość telomerów w leukocytach dla 37. tys osób pochodzenia europejskiego, dodatkowo potwierdzanie wariantów dla dalszych 10 tys.;
# długość telomerów w leukocytach jest silnie skorelowana z długością w innych tkankach
# dane o rs: z Tabeli1 (główny artykuł, warianty o p-value <5e-08), effekt allelu podany jako współczynnik regresji (ale też przeliczony na pz)
# tylko 7 wariantów
# odziedziczalnosc: około 44-80 %, zmiennosc wyjaśniona przez istotne SNPy- 1.23% (Tabela1)
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_tel.txt i stats_tel.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.7001, category_name='High risk'),
                        QuantitativeCategory(from_=-0.7001, to=-0.4219, category_name='Average risk'),
                        QuantitativeCategory(from_=-0.4219, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs10936599', effect_allele='T', beta_value=-0.097),
                        beta(rs_id='rs2736100', effect_allele='A', beta_value=-0.078),
                        beta(rs_id='rs7675998', effect_allele='A', beta_value=-0.074),
                        beta(rs_id='rs9420907', effect_allele='A', beta_value=-0.069),
                        beta(rs_id='rs8105767', effect_allele='A', beta_value=-0.048),
                        beta(rs_id='rs755017', effect_allele='A', beta_value=-0.062),
                        beta(rs_id='rs11125529', effect_allele='C', beta_value=-0.056)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Telomere Length'

about = ('Telomeres are areas of DNA found at the ends of chromosomes (structure that '
         'contain genes and DNA). Telomeres give stability to chromosomes and protect '
         'them from damage. When most cells in the body divide, which happens often, '
         "the telomeres shorten a little. Over time, the cell's telomeres get too "
         'short and that cell may not be able to divide anymore or may die. This can '
         'naturally happen with age or for other reasons. Having a lower than average '
         'telomere length (when compared to others of the same age and biological sex) '
         'is a potential risk factor for health issues related to aging, like some '
         'forms of heart disease and cancer. However, the exact impact of telomere '
         'length on these health risks is still unclear. Several things impact a '
         "person's telomere length, including genetics.")

how_your_result_was_calculated = ('Your result is a risk based on 7 genetic variants, specifically called SNPs, '
                                  'found in genes associated with telomere length. Overall, this explains about '
                                  '1.2% of the genetic variation in telomere length between people, based on '
                                  'current scientific knowledge.')

other_factors_statement = ("Many factors impact a person's telomere length, including lifestyle, "
                           'biological sex, race/ethnicity, environment, other health issues, and '
                           'genetics.')

other_factors = [{'content': '', 'title': 'Lifestyle'},
                 {'content': 'Having diabetes or a high blood sugar level contributes to '
                             'telomere length.',
                  'title': 'Other Health Issues'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have short telomere lengths. These types of risk '
                             'factors, which are usually due to rare genetic variants, are not '
                             'included in this test. If you have a family history of a genetic '
                             'condition that causes people to have a short telomere length, '
                             'talk to your Practitioner or a genetic counselor.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your telomere length and other factors, you may '
                                           'have an average risk for some forms of heart disease, '
                                           'cancer, or other health issues.',
                           'High risk': 'Depending on your telomere length and other factors, you may '
                                        'have a higher risk for some forms of heart disease, cancer, or '
                                        'other health issues. The exact risks related to telomere length '
                                        'are still unclear.',
                           'Low risk': 'Depending on your telomere length and other factors, you may '
                                       'have a lower risk for some forms of heart disease, cancer, or '
                                       'other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average telomere length.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a shorter telomere length.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a longer than average telomere length.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to maintain telomere '
                                             'length and stay healthy. Certain blood tests can also check '
                                             'your risk factors for heart disease or other health issues. '
                                             'Talk to your practitioner about what is right for you.',
                             'High risk': 'There are things you can do to try to maintain telomere length '
                                          'and stay healthy. Certain blood tests can also check your risk '
                                          'factors for heart disease or other health issues. Talk to your '
                                          'practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to maintain telomere length '
                                         'and stay healthy. Certain blood tests can also check your risk '
                                         'factors for heart disease or other health issues. Talk to your '
                                         'practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a shorter telomere length. Consider '
                                                'talking to your practitioner about ordering '
                                                'tests to check your risk factors of related '
                                                'health issues, and possible next steps that you '
                                                'can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'High emotional stress, low physical activity, '
                                                'low amounts of sleep, and being overweight are '
                                                'risk factors for a shorter telomere length. Try '
                                                'to find ways to address these if they apply to '
                                                'you, since it is good for overall wellness. '
                                                'Talking to friends, family, a mental health '
                                                'specialist or meditating can help to manage '
                                                'stress. Adults should get 2.5 hours of moderate '
                                                'physical activity, like brisk walking or '
                                                'biking, every week. Not only can this lower '
                                                'your risk of heart disease, but it can also '
                                                'help you maintain a healthy weight and get '
                                                'better sleep.',
                                     'title': 'Lifestyle'},
                                    {'content': 'Short telomere length can run in families, and '
                                                'these risk factors are not included in this '
                                                'test. If you have a close relative (parent, '
                                                'child, sibling) with a genetic condition that '
                                                'causes people to have a short telomere length, '
                                                'speak to your practitioner about your family '
                                                'history. If you have blood tests to look for '
                                                'related health issues, information about your '
                                                'family history will help with understanding '
                                                'your blood test results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your risk factors for heart disease or other '
                                             'health issues related to a shorter telomere '
                                             'length. Talk to them about this and possible next '
                                             'steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'High emotional stress, low physical activity, low '
                                             'amounts of sleep, and being overweight are risk '
                                             'factors for a shorter telomere length. Try to find '
                                             'ways to address these if they apply to you. '
                                             'Talking to friends, family, a mental health '
                                             'specialist or meditating can help to manage '
                                             'stress. Adults should get 2.5 hours of moderate '
                                             'physical activity, like brisk walking or biking, '
                                             'every week. Not only can this lower your risk of '
                                             'heart disease, but it can also help you maintain a '
                                             'healthy weight and get better sleep. Talk to your '
                                             'practitioner if you need resources or support with '
                                             'these.',
                                  'title': 'Lifestyle'},
                                 {'content': 'If you have a high blood sugar level or diabetes '
                                             'you may have a risk for shorter telomere length. '
                                             'Speak to your practitioner about how this factors '
                                             'into things, if your telomere length is found to '
                                             'be low.',
                                  'title': 'Other Health Issues'},
                                 {'content': 'Short telomere length can run in families, and '
                                             'these risk factors are not included in this test. '
                                             'If you have a close relative (parent, child, '
                                             'sibling) with a genetic condition that causes '
                                             'people to have a short telomere length, speak to '
                                             'your practitioner about your family history. If '
                                             'you have blood tests to look for related health '
                                             'issues, information about your family history will '
                                             'help with understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a shorter telomere length. Consider talking to '
                                            'your practitioner about ordering tests to check '
                                            'your risk factors of related health issues, and '
                                            'possible next steps that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'High emotional stress, low physical activity, low '
                                            'amounts of sleep, and being overweight are risk '
                                            'factors for a shorter telomere length. Try to find '
                                            'ways to address these if they apply to you, since '
                                            'it is good for overall wellness. Talking to '
                                            'friends, family, a mental health specialist or '
                                            'meditating can help to manage stress. Adults should '
                                            'get 2.5 hours of moderate physical activity, like '
                                            'brisk walking or biking, every week. Not only can '
                                            'this lower your risk of heart disease, but it can '
                                            'also help you maintain a healthy weight and get '
                                            'better sleep.',
                                 'title': 'Lifestyle'},
                                {'content': 'Short telomere length can run in families, and '
                                            'these risk factors are not included in this test. '
                                            'If you have a close relative (parent, child, '
                                            'sibling) with a genetic condition that causes '
                                            'people to have a short telomere length, speak to '
                                            'your practitioner about your family history. If you '
                                            'have blood tests to look for related health issues, '
                                            'information about your family history will help '
                                            'with understanding your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://blogs.cdc.gov/genomics/2011/06/09/tell-me-more-about-telomeres/',
                                     'title': '"Tell Me More about Telomeres" from Centers '
                                              'for Disease Control and Prevention'},
                                    {'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.ncbi.nlm.nih.gov/pubmed/23535734',
                                     'title': 'Codd V, et al. Nature Genetics. 2014'},
                                    {'accessed': 'Accessed July 1, 2019. \n',
                                     'links': 'https://www.ncbi.nlm.nih.gov/pubmed/28322101',
                                     'title': 'Wang J, et al. Journal of International '
                                              'Medical Research. 2016'},
                                    {'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.ncbi.nlm.nih.gov/pubmed/24335912',
                                     'title': 'Starkweather AR, et al. Nursing Research. '
                                              '2014'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Telomere length"
trait_science = "Telomere length is moderately heritable as about 44% of differences observed among people at the same age has a genetic origin. Our analysis was based on 7 SNPs associated with telomere length. These variants jointly explain only about 1.2% of the variation in telomere length between people."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the telomere length. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
