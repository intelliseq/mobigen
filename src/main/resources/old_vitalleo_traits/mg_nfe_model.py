from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Blood_magnesium"
trait_pmids = ["20700443"]  # 20700443 table 2: effect allele=coded allele
trait_heritability = 0.3
trait_snp_heritability = None
trait_explained = 0.016
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "Blood magnessium concentration moderately heritable, with heritability estimate of about 30% [1]. Our test is based on results of the genome wide association studies (GWAS) [1], which included over 15,000 individuals of European descent. The study identified 5 variants significantly associated with the blood magnesiumlevel in European population. Together these SNPs explain less than 2% of between people variance in magnessium level [2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["AC074033.1", "AC112249.1", "BRWD1P2", "DCDC1", "MUC1", "TRPM6"]

# 15366 individuals of European descent

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.0579, category_name='High risk'),
                        QuantitativeCategory(from_=-0.0579, to=-0.0315, category_name='Average risk'),
                        QuantitativeCategory(from_=-0.0315, category_name='Low risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs4072037', effect_allele='C', beta_value=-0.01),
                        beta(rs_id='rs13146355', effect_allele='G', beta_value=-0.005),
                        beta(rs_id='rs11144134', effect_allele='T', beta_value=-0.011),
                        beta(rs_id='rs3925584', effect_allele='C', beta_value=-0.006),
                        beta(rs_id='rs448378', effect_allele='G', beta_value=-0.004)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Magnesium'

about = ('Magnesium is a mineral central to the functionality of our cells. It’s an '
         'important part of our nervous system, enabling it to send signals to all of '
         'our muscles, including our heart.\n'
         'Magnesium is also a key element for strengthening and regenerating both bone '
         'and muscle structure, which makes it crucial for athletes to have a steady '
         'supply of it coursing through their system.\n'
         'Finally, in addition to easing tense muscles, magnesium also provides you '
         'with a better night’s sleep.')

how_your_result_was_calculated = ('Your result is a risk based on 6 genetic variants, specifically called SNPs, '
                                  'found in genes associated with blood magnesium level. Overall, this explains '
                                  'less than 2% of the the genetic variation in magnesium level in the human '
                                  'population, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact magnesium levels, including diet, lifestyle, biological '
                           'sex, age, other health issues, and genetics. Genetics has a moderate overall '
                           'impact on magnesium level when you consider all the other factors.')

other_factors = [{'content': 'Green leafy vegetables, fatty fish, kidney beans, oatmeal, brown '
                             'rice, pumpkin seeds, avocado, raw nuts, and spinach all contain '
                             'magnesium. Most people are deficient and require '
                             'supplementation.',
                  'title': 'Diet'},
                 {'content': 'People with type 2 diabetes or some digestive conditions (such '
                             "as Crohn's disease and celiac disease) may have lower magnesium "
                             'levels.',
                  'title': 'Other Health Issues'},
                 {'content': 'Magnesium supplements can help raise levels to recommended daily '
                             'levels. They are available in a multivitamin or as a separate '
                             'dietary supplement.',
                  'title': 'Supplements'}]

what_your_result_means = {'Average risk': 'Magnesium is important for many parts of the body, such as '
                                           'the nerves, muscles (including the heart), and bones. It is '
                                           'also important for controlling blood sugar, blood pressure, '
                                           'as well as making protein and DNA. Low magnesium levels may '
                                           'be a risk factor for lower bone density, type 2 diabetes, '
                                           'and other health problems. Magesium is  key element for '
                                           'strengthening and regenerating bones and muscles, which '
                                           'makes it crucial for athletes to have a steady supply of it '
                                           'coursing through their system. It can also assist in a '
                                           "better night's sleep.",
                           'High risk': 'Magnesium is important for many parts of the body, such as the '
                                        'nerves, muscles (including the heart), and bones. It is also '
                                        'important for controlling blood sugar, blood pressure, as well '
                                        'as making protein and DNA. Low magnesium levels may be a risk '
                                        'factor for lower bone density, type 2 diabetes, and other '
                                        'health problems. Magesium is  key element for strengthening and '
                                        'regenerating bones and muscles, which makes it crucial for '
                                        'athletes to have a steady supply of it coursing through their '
                                        "system. It can also assist in a better night's sleep.",
                           'Low risk': 'Magnesium is important for many parts of the body, such as the '
                                       'nerves, muscles (including the heart), and bones. It is also '
                                       'important for controlling blood sugar, blood pressure, as well '
                                       'as making protein and DNA. Low magnesium levels may be a risk '
                                       'factor for lower bone density, type 2 diabetes, and other health '
                                       'problems. Magesium is  key element for strengthening and '
                                       'regenerating bones and muscles, which makes it crucial for '
                                       'athletes to have a steady supply of it coursing through their '
                                       "system. It can also assist in a better night's sleep."}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average blood magnesium level.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a lower blood magnesium level.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a higher blood magnesium level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent low magnesium '
                                             'levels and stay healthy. Certain tests can also check your '
                                             'magnesium level. Talk to your practitioner about what is '
                                             'right for you.',
                             'High risk': 'There are things you can do to try to prevent low magnesium '
                                          'levels and stay healthy. Certain tests can also check your '
                                          'magnesium level. Talk to your practitioner about what is right '
                                          'for you.',
                             'Low risk': 'There are things you can do to try to prevent low magnesium '
                                         'levels and stay healthy. Certain tests can also check your '
                                         'magnesium level. Talk to your practitioner about what is right '
                                         'for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a low magnesium level. Consider '
                                                'talking to your practitioner about blood tests '
                                                'or other forms of tests to check your magnesium '
                                                'level, and possible next steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'A balanced and healthy diet is always a good '
                                                'idea. Many foods naturally contain magnesium, '
                                                'such as nuts, legumes (like chickpeas and '
                                                'lentils), seeds, whole grains, and leafy green '
                                                'vegetables (like spinach). Dairy products like '
                                                'milk and yogurt also contain magnesium.',
                                     'title': 'Diet'},
                                    {'content': 'If you have certain digestive conditions (such '
                                                "as Crohn's disease and celiac disease) or type "
                                                '2 diabetes you may have lower magnesium levels. '
                                                'Speak to your practitioner about how this '
                                                'factors into things, if your magnesium levels '
                                                'are found to be low from other tests.',
                                     'title': 'Other Health Issues'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your magnesium level. Talk to them about '
                                             'this and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'A balanced and healthy diet is always a good idea. '
                                             'Many foods naturally contain magnesium, such as '
                                             'nuts, legumes (like chickpeas and lentils), seeds, '
                                             'whole grains, and leafy green vegetables (like '
                                             'spinach). Dairy products like milk and yogurt also '
                                             'contain magnesium.',
                                  'title': 'Diet'},
                                 {'content': 'If you have certain digestive conditions (such as '
                                             "Crohn's disease and celiac disease) or type 2 "
                                             'diabetes you may have lower magnesium levels. '
                                             'Speak to your practitioner about how this factors '
                                             'into things, if your magnesium levels are found to '
                                             'be low from other tests.',
                                  'title': 'Other Health Issues'},
                                 {'content': 'The recommended amount of magnesium per day for '
                                             'adult men is 400-420mg, and it is 310-320mg for '
                                             'adult women. This amount is often in a '
                                             'multivitamin mineral supplement or other dietary '
                                             'supplements. Magnesium may also be in some '
                                             'laxatives or over-the-counter medicines for '
                                             'heartburn and indigestion. Talk to your '
                                             'practitioner about the dosage to take. Too much '
                                             'magnesium can be harmful.',
                                  'title': 'Supplements'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a low magnesium level. Consider talking to your '
                                            'practitioner about blood tests to check your '
                                            'magnesium level, and possible next steps you can '
                                            'take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'A balanced and healthy diet is always a good idea. '
                                            'Many foods naturally contain magnesium, such as '
                                            'nuts, legumes (like chickpeas and lentils), seeds, '
                                            'whole grains, and leafy green vegetables (like '
                                            'spinach). Dairy products like milk and yogurt also '
                                            'contain magnesium.',
                                 'title': 'Diet'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 23, 2019',
                                     'links': 'https://ods.od.nih.gov/factsheets/Magnesium-Consumer/',
                                     'title': '"Magnesium Fact Sheet for Consumers" from '
                                              'National Institutes of Health'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood magnesium level"
trait_science = "Blood magnesium level is moderately heritable as only about 30% of differences observed among people has a genetic origin. Our analysis was based on six SNPs associated with concentration of Mg in the blood. These variants jointly explain less than 2% of the variation between people."
trait_test_details = "We determined your genotype at six genomic positions, which are known to influence the blood magnesium level. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
