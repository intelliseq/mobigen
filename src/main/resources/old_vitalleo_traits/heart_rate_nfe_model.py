from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Heart_rate_after_physical_exercise"
trait_pmids = ["29497042"]  # 29497042 table 1
trait_heritability = 0.6
trait_snp_heritability = 0.2
trait_explained = None
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Heart rate after exercise is partially controlled by genetic factors and about 60% of differences observed among people has a genetic origin. Our test is based on results of the genome wide association studies (GWAS) [1], which included over 59,000 individuals of European ancestry (BioBank UK). The study reports 25 variants significantly associated with the after exercise heart hate [1].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["AC004947.2", "AC009387.1", "AC022101.1", "AC087633.2", "ACTG1P18", "AP000593.1", "CAV1", "CNTN3", "GDI2P2",
         "GNG11", "KNOP1P1", "KRT8P51", "MIR5584", "MIR7843", "NDUFA11", "PAX2", "POP4", "PRDM6", "RNA5SP94",
         "RNU7-104P", "SATB1-AS1", "SERINC2", "SRRT", "SYT10"]

######### heart_rate_increase_in_response_to_exercise
# na podstawie: doi: 10.1038/s41467-018-03395-6
# N=około 59 tys. osób pochodzenia europejskiego (UK BioBank);
# Te badania dotyczyły częstości bicia serca podczas wysiłku, oraz w odstępach 10s po wysiłku (10, 20, 30, 40, 50s)==> źle jeśli częstości duża
# Uzyskane wartości pomiarów były ze sobą skorelowane (fenotypowo - r od 0.44 do 0.92; oraz genotypowo - r od 0.6 do 0.99)
# Dlatego do obliczenia ryzyka  połączyłam warianty, które okazały się istotne dla którejkolwiek z cech
# dane o rs: z Tabeli1 (główny artykuł), wzięłam wszystkie, bo ich niezależność była potwierdzona poprzez “conditional analysis”
# w sumie 25 wariantów
# efekty były podane, jako współczynniki beta, ale dane wcześniej transformowane (log2, potem "rank-based inverse normal transformation")
# odziedziczalnosc: około 60 %, zmiennosc wyjaśniona przez wszystkie SNPy- około 20 %; przez istotne statystycznie - nie podano
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_heart_rate.txt i stats_heart_rate.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=1.2941, category_name='Low risk'),
                        QuantitativeCategory(from_=1.2941, to=1.7179, category_name='Average risk'),
                        QuantitativeCategory(from_=1.7179, category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs11589125', effect_allele='T', beta_value=0.075),
                        beta(rs_id='rs272564', effect_allele='A', beta_value=0.046),
                        beta(rs_id='rs61765646', effect_allele='A', beta_value=0.056),
                        beta(rs_id='rs1899492', effect_allele='T', beta_value=0.04),
                        beta(rs_id='rs17362588', effect_allele='G', beta_value=0.062),
                        beta(rs_id='rs35596070', effect_allele='C', beta_value=0.06),
                        beta(rs_id='rs73043051', effect_allele='C', beta_value=0.041),
                        beta(rs_id='rs34310778', effect_allele='C', beta_value=0.036),
                        beta(rs_id='rs4836027', effect_allele='T', beta_value=0.05),
                        beta(rs_id='rs151283', effect_allele='C', beta_value=0.042),
                        beta(rs_id='rs2224202', effect_allele='A', beta_value=0.043),
                        beta(rs_id='rs2158712', effect_allele='A', beta_value=0.045),
                        beta(rs_id='rs180238', effect_allele='T', beta_value=0.043),
                        beta(rs_id='rs3757868', effect_allele='G', beta_value=0.077),
                        beta(rs_id='rs1997571', effect_allele='A', beta_value=0.042),
                        beta(rs_id='rs17168815', effect_allele='G', beta_value=0.062),
                        beta(rs_id='rs7072737', effect_allele='A', beta_value=0.079),
                        beta(rs_id='rs7130652', effect_allele='T', beta_value=0.076),
                        beta(rs_id='rs4963772', effect_allele='A', beta_value=0.09),
                        beta(rs_id='rs6488162', effect_allele='C', beta_value=0.103),
                        beta(rs_id='rs61928421', effect_allele='C', beta_value=0.09),
                        beta(rs_id='rs17180489', effect_allele='C', beta_value=0.055),
                        beta(rs_id='rs12906962', effect_allele='T', beta_value=0.048),
                        beta(rs_id='rs12974440', effect_allele='G', beta_value=0.067),
                        beta(rs_id='rs12986417', effect_allele='G', beta_value=0.037)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Post-Exercise Heart Rate'

about = ('Heart rate goes up during exercise, then naturally recovers and comes back '
         'down. This is how the heart muscle works and strengthens during exercise. If '
         'the heart is working well, heart rate quickly recovers to normal after '
         'exercise. For some people, their heart rate stays high for too long after '
         'exercise. This could be a sign of a heart poorly working, or possibly a sign '
         'of a heart problem or other health problems. A longer post-exercise heart '
         'rate can also happen in people who are less physically fit than others their '
         'age. Several things impact post-exercise heart rate, including genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 25 genetic variants, specifically called '
                                  'SNPs, associated with heart rate recovery.')

other_factors_statement = ("Many factors impact a person's post-exercise heart rate, including "
                           'lifestyle, other health issues, and genetics. Genetics has a moderate '
                           'overall impact on post-exercise heart rate when you consider all the other '
                           'factors.')

other_factors = [{'content': '', 'title': 'Lifestyle'},
                 {'content': 'Some health issues, like high blood pressure or diabetes, can '
                             "affect someone's heart rate. Certain medicines, like some for "
                             'asthma, can have a similar effect.',
                  'title': 'Other Health Issues'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have a longer post-exercise heart rate. These '
                             'types of risk factors, which are usually due to rare genetic '
                             'variants, are not included in this test. If you have a family '
                             'history of a high post-exercise heart rate recovery time, talk '
                             'to your Practitioner or a genetic counselor.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your post-exercise heart rate and other '
                                           'factors, you may have an average risk for some forms of '
                                           'heart disease or other health issues.',
                           'High risk': 'Depending on your post-exercise heart rate and other factors, '
                                        'you may have a higher risk for some forms of heart disease or '
                                        'other health issues.',
                           'Low risk': 'Depending on your post-exercise heart rate and other factors, '
                                       'you may have a lower risk for some forms of heart disease or '
                                       'other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average post-exercise heart rate.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher post-exercise heart rate.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower post-exercise heart rate.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent a higher '
                                             'post-exercise heart rate and stay healthy. Certain blood '
                                             'tests can also check your risk factors for some forms of '
                                             'heart disease. Talk to your practitioner about what is right '
                                             'for you.',
                             'High risk': 'There are things you can do to try to prevent a higher '
                                          'post-exercise heart rate and stay healthy. Certain blood tests '
                                          'can also check your risk factors for some forms of heart '
                                          'disease. Talk to your practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent a higher '
                                         'post-exercise heart rate and stay healthy. Certain blood tests '
                                         'can also check your risk factors for some forms of heart '
                                         'disease. Talk to your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a higher post-exercise heart rate. '
                                                'Consider talking to your practitioner about '
                                                'ordering blood tests to check your risk factors '
                                                'for heart disease or other health issues, and '
                                                'possible next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Being physically active is a good workout for '
                                                'your heart and keeps it strong, especially as '
                                                'your heart rate increases and decreases. '
                                                'Regular physical exercise can "condition" your '
                                                'heart and keep your post-exercise heart rate in '
                                                "the normal range. It's recommended that adults "
                                                'get 2.5 hours of moderate physical activity, '
                                                'like brisk walking or biking, every week. When '
                                                "exercise generates a high demand on the body's "
                                                'need for oxygen during the workout, it '
                                                "increases the body's need for oxygen after the "
                                                'workout. As a result, you can continue to burn '
                                                'fat and calories long after the workout is '
                                                'finished. To enhance these benefits, try '
                                                'incorporating High Intensity-Interval Training '
                                                '(HIIT) weight lifting exercises as part of a '
                                                'circuit.',
                                     'title': 'Lifestyle'},
                                    {'content': 'A high post-exercise heart rate can run in '
                                                'families, and these risk factors are not '
                                                'included in this test. If you have a close '
                                                'relative (parent, child, sibling) with a high '
                                                'post-exercise heart rate, speak to your '
                                                'practitioner about your family history. If you '
                                                'have blood tests to look for related health '
                                                'issues, information about your family history '
                                                'will help with understanding your blood test '
                                                'results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your risk factors for heart disease or '
                                             'other health issues. Talk to them about this and '
                                             'possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Being physically active is a good workout for your '
                                             'heart and keeps it strong, especially as your '
                                             'heart rate increases and decreases. Regular '
                                             'physical exercise can "condition" your heart and '
                                             "lower your post-exercise heart rate. It's "
                                             'recommended that adults get 2.5 hours of moderate '
                                             'physical activity, like brisk walking or biking, '
                                             'every week. Not only can this lower your risk of '
                                             'heart disease, but it can also help you maintain a '
                                             'healthy weight. When exercise generates a high '
                                             "demand on the body's need for oxygen during the "
                                             "workout, it increases the body's need for oxygen "
                                             'after the workout. As a result, you can continue '
                                             'to burn fat and calories long after the workout is '
                                             'finished. To enhance these benefits, try '
                                             'incorporating High Intensity-Interval Training '
                                             '(HIIT) weight lifting exercises as part of a '
                                             'circuit.',
                                  'title': 'Lifestyle'},
                                 {'content': 'If you have high blood pressure or diabetes, this '
                                             'can raise your heart rate. Some antibiotics, '
                                             'antidepressants and some medications for asthma, '
                                             'thyroid conditions, cough, colds, and allergies '
                                             'can have a similar effect. Speak to your '
                                             'practitioner about how this factors into things, '
                                             'if your post-exercise heart rate is found to be '
                                             'high.',
                                  'title': 'Other Health Issues'},
                                 {'content': 'A high post-exercise heart rate can run in '
                                             'families, and these risk factors are not included '
                                             'in this test. If you have a close relative '
                                             '(parent, child, sibling) with a high post-exercise '
                                             'heart rate, speak to your practitioner about your '
                                             'family history. If you have blood tests to look '
                                             'for related health issues, information about your '
                                             'family history will help with understanding your '
                                             'blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a higher post-exercise heart rate. Consider '
                                            'talking to your practitioner about ordering blood '
                                            'tests to check your risk factors for heart disease '
                                            'or other health issues, and possible next steps '
                                            'that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Being physically active is a good workout for your '
                                            'heart and keeps it strong, especially as your heart '
                                            'rate increases and decreases. Regular physical '
                                            'exercise can "condition" your heart and keep your '
                                            "post-exercise heart rate low. It's recommended that "
                                            'adults get 2.5 hours of moderate physical activity, '
                                            'like brisk walking or biking, every week. When '
                                            "exercise generates a high demand on the body's need "
                                            'for oxygen during the workout, it increases the '
                                            "body's need for oxygen after the workout. As a "
                                            'result, you can continue to burn fat and calories '
                                            'long after the workout is finished. To enhance '
                                            'these benefits, try incorporating High '
                                            'Intensity-Interval Training (HIIT) weight lifting '
                                            'exercises as part of a circuit.',
                                 'title': 'Lifestyle'},
                                {'content': 'A high post-exercise heart rate can run in '
                                            'families, and these risk factors are not included '
                                            'in this test. If you have a close relative (parent, '
                                            'child, sibling) with a high post-exercise heart '
                                            'rate, speak to your practitioner about your family '
                                            'history. If you have blood tests to look for '
                                            'related health issues, information about your '
                                            'family history will help with understanding your '
                                            'blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://my.clevelandclinic.org/health/diagnostics/17402-pulse--heart-rate',
                                     'title': '"Pulse and Heart Rate" from Cleveland Clinic'},
                                    {'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.heart.org/en/healthy-living/fitness/fitness-basics/target-heart-rates',
                                     'title': '"Know Your Target Heart Rates for Exercise, '
                                              'Losing Weight and Health" from American '
                                              'Heart Association'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Heart rate after exercise"
trait_science = "Heart rate after exercise is partially controlled by genetic factors and about 60% of differences observed among people has a genetic origin. Our analysis was based on 25 SNPs associated with heart rate recovery."
trait_test_details = "We determined your genotype at genomic positions, which influence the heart rate recovery. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the lifestyle."
