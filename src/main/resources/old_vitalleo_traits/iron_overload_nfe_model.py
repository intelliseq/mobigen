from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Iron_overload"
trait_pmids = ["11874997", "24729993", "11399207", "20301613", "23985001", "19444013"]
trait_heritability = None  # as fraction
trait_snp_heritability = None  # as fraction
trait_explained = None  # as fraction
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Heritable hemochromatosis is caused usually by variants localized within the HFE gene and impairing the function of protein encoded by this gene. Two such mutations are relatively frequent in human populations: rs1799945 (His63Asp) and rs1800562 (Cys282Tyr). These variants are found in 87% of patients with iron overload [1-6].\nOur analyses use genetic data (genotypes) for these SNPs to calculate the polygenic score."
genes = ["HFE"]

# trait_genotypes = ["hom", "het", "hom"] # for one snip analysis
# Analysis based on two variants localized within HFE gene
# 1) His63Asp; rs1799945 C=>G (freq gnomAD 0.143507)
# 2) Cys282Tyr; rs1800562 G=>A (freq gnomAD 0.0471698)
# Both variants are classified as pathogenic but with reduced penetrance (ClinVar)
# In homozygotes (also in compound heterozygotes) they lead to elevated transferin stauration and ferritin level => rarely to iron overload
# rs1799945 seems to be less damaging
# These variants are in linkage equilibrium in EUR population => thus: frequencies of genotypes independent [hwe]
# I assigned the beta values arbitrary (see below): rs1799945 H63D: C=0, G=0.6; rs1800562 C282Y G=0, A=1
#	H63D/C282Y; joint genotypes freq; risk_score; risk_category
#	hom_wt/hom_wt; 0.666; 0; low_risk
#	hom_wt/het; 0.066, 1.0; low_risk
#	het/wt_hom; 0.223; 0.6; low_risk
#	het/het; 0.022; 1.6; elevated_risk #high? but it can be in the same allele
#	het/hom_mut; 0.0005; 2.6; high_risk
#	hom_mut/het; 0.0019; 2.2; high_risk
#	hom_mut/hom_mut; 4.58e-05; 3.2; high_risk
#	hom_wt/hom_mut; 0.0016; 2.0; high_risk
#	hom_mut/hom_wt; 0.0187; 1.2; elevated_risk
# frequencies: low_risk: 0.955; elevated_risk: 0.041; high_risk:  0.04
# both variants are on array
# description_genotype["low_trait"] = "hom" # for one snip test
# description_genotype["average_trait"] = "het" #for one snip test
# description_genotype["high_trait"] = "hom" # for one snip test

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=1.1, category_name='Low risk'),
                        QuantitativeCategory(from_=1.1, to=1.9, category_name='Average risk'),
                        QuantitativeCategory(from_=1.9, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs1799945', effect_allele='G', beta_value=0.6),
                        beta(rs_id='rs1800562', effect_allele='A', beta_value=1.0)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Iron overload'

about = ('The body needs iron and gets it from sources like diet, but the right amount '
         'is critical. Too little iron is called iron deficiency, or anemia. Too much '
         'iron, called iron overload, is also a problem. If iron overload occurs for '
         "some time (perhaps years), iron can accumulate in the body's organs -- this "
         'may cause liver disease, heart disease, or diabetes if not treated. '
         'Treatment for iron overload includes phlebotomy (medical removal of blood '
         'from the body) or other measures, and in many cases this can prevent '
         'symptoms. Iron overload may be acquired or inherited. In the U.S., inherited '
         'iron overload is a very common genetic disease.')

how_your_result_was_calculated = ('Your result is a risk based on 2 genetic variants, specifically called SNPs, '
                                  'in 1 gene associated with iron overload. These variants are found in '
                                  'approximately 87% of individuals with iron overload.')

other_factors_statement = ("Many factors impact a person's risk for iron overload, including biological "
                           'sex, ethnicity, lifestyle, and genetics. Genetics has a relatively high '
                           'overall impact on iron overload risk when you consider all the other '
                           'factors, particularly in people of Northern European Caucasian ancestry.')

other_factors = [{'content': 'Genetics has a moderate overall impact on telomere length when '
                             'you consider all the other factors.',
                  'title': 'Biological Sex'},
                 {'content': 'People who are of Northern European Caucasian descent have a '
                             'higher risk for iron overload. It is less common in Africans, '
                             'Hispanics, Asians, and First Nations peoples.',
                  'title': 'Ethnic Background'},
                 {'content': 'Heavy drinking or alcoholism, which taxes the liver, is a risk '
                             'factor for iron overload.',
                  'title': 'Lifestyle'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have iron overload. These types of risk factors, '
                             'which include diabetes, heart attack, liver disease, arthritis, '
                             'and erectile dysfunction, are not included in this test. If you '
                             'have a family history of iron overload, talk to your '
                             'Practitioner.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your blood iron levels and other factors, you '
                                           'may have an average risk for liver disease, diabetes, or '
                                           'other health issues related to iron overload.',
                           'High risk': 'Depending on your blood iron levels and other factors, you may '
                                        'have a higher risk for liver disease, diabetes, or other health '
                                        'issues related to iron overload.',
                           'Low risk': 'Depending on your blood iron levels and other factors, you may '
                                       'have a lower risk for liver disease, diabetes, or other health '
                                       'issues related to iron overload.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average risk for iron overload.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher risk for iron overload.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower risk for iron overload.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent symptoms of '
                                             'iron overload and stay healthy. Certain blood tests can also '
                                             'check your risk factors for iron overload. Talk to your '
                                             'practitioner about what is right for you.',
                             'High risk': 'There are things you can do to try to prevent symptoms of iron '
                                          'overload and stay healthy. Certain blood tests can also check '
                                          'your risk factors for iron overload. Talk to your practitioner '
                                          'about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent symptoms of iron '
                                         'overload and stay healthy. Certain blood tests can also check '
                                         'your risk factors for iron overload. Talk to your practitioner '
                                         'about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk factors for iron overload or other health '
                                                'issues. Consider talking to your practitioner '
                                                'about ordering blood tests to check your iron '
                                                'levels or risks for other health issues, and '
                                                'possible next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Try to avoid regular heavy drinking (8 or more '
                                                'drinks per week for women, 15 or more drinks '
                                                'per week for men). Heavy drinking is a risk '
                                                'factor for iron overload. Additionally, being '
                                                'physically active -- such as 2.5 hours of '
                                                'moderate physical activity every week -- can '
                                                'lower your risk of heart disease and diabetes, '
                                                'which are risk factors for iron overload.',
                                     'title': 'Lifestyle'},
                                    {'content': 'Iron overload can run in families, and some of '
                                                'these risk factors are not included in this '
                                                'test. If you have a close relative (parent, '
                                                'child, sibling) with iron overload, speak to '
                                                'your practitioner about your family history. If '
                                                'you have blood tests to look for related health '
                                                'issues, information about your family history '
                                                'will help with understanding your blood test '
                                                'results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your risk factors for iron overload or '
                                             'other health issues. Treatment may be available if '
                                             'your iron levels are high. Talk to them about this '
                                             'and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Keep a food diary to track iron-rich items in your '
                                             'diet, then discuss with your practitioner before '
                                             'making dietary changes. Iron-rich foods include '
                                             'beef, dark meat turkey broccoli, spinach, tofu, '
                                             'organ meats (like kidney, brain, heart, liver), '
                                             'clams, legumes, quinoa, and dark chocolate. Avoid '
                                             'regular heavy drinking (8 or more drinks per week '
                                             'for women, 15 or more drinks per week for men). '
                                             'Heavy drinking is a risk factor for iron overload. '
                                             'Speak to your practitioner if you need help on '
                                             'ways to make lifestyle changes, including speaking '
                                             'to a registered dietician about your diet.',
                                  'title': 'Lifestyle'},
                                 {'content': 'Iron overload can run in families, and some of '
                                             'these risk factors are not included in this test. '
                                             'If you have a close relative (parent, child, '
                                             'sibling) with iron overload, speak to your '
                                             'practitioner about your family history. If you '
                                             'have blood tests to look for related health '
                                             'issues, information about your family history will '
                                             'help with understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'factors for iron overload or other health issues. '
                                            'Consider talking to your practitioner about '
                                            'ordering blood tests to check your iron levels or '
                                            'risks for other health issues, and possible next '
                                            'steps that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Try to avoid regular heavy drinking (8 or more '
                                            'drinks per week for women, 15 or more drinks per '
                                            'week for men). Heavy drinking is a risk factor for '
                                            'iron overload. Additionally, being physically '
                                            'active -- such as 2.5 hours of moderate physical '
                                            'activity every week -- can lower your risk of heart '
                                            'disease and diabetes, which are risk factors for '
                                            'iron overload.',
                                 'title': 'Lifestyle'},
                                {'content': 'Iron overload can run in families, and some of '
                                            'these risk factors are not included in this test. '
                                            'If you have a close relative (parent, child, '
                                            'sibling) with iron overload, speak to your '
                                            'practitioner about your family history. If you have '
                                            'blood tests to look for related health issues, '
                                            'information about your family history will help '
                                            'with understanding your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_short_description = "Iron overload"
trait_science = "Heritable hemochromatosis is caused usually by variants localized within the HFE gene and impairing the function of protein encoded by this gene. Two such mutations are relatively frequent in human populations: rs1799945 (His63Asp) and rs1800562 (Cys282Tyr). These variants are found in 87% of patients with iron overload."
trait_test_details = "We determined your genotype at two genomic positions, which are known to influence risk of iron overload. Your polygenic risk score was calculated by summing up the effects of these SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
