from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "BMD"
trait_pmids = ["28869591"]  # 28869591 supplementary table 2; 22 snps removed (not in gnomad database)
trait_heritability = 0.6
trait_snp_heritability = None
trait_explained = 0.12
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "Bone mineral density is moderately heritable - about 60% of differences observed among people has a genetic origin [1]. Our test is based on results of the genome wide association studies (GWAS) [1], which included over 140 000 individuals of European ancestry (BioBank UK). The study reports 269 variants significantly associated with the BMD in European population. Together these SNPs explain about 12% of between people variance in bone density [1].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["AC000123.3", "AC002059.3", "AC004080.5", "AC004805.1", "AC005105.1", "AC005144.1", "AC005165.2", "AC005414.1",
         "AC005550.2", "AC006600.1", "AC007317.1", "AC008957.2", "AC009154.1", "AC009522.1", "AC009743.1", "AC012568.1",
         "AC013480.1", "AC018445.4", "AC019103.1", "AC021785.1", "AC022784.4", "AC026120.1", "AC026427.2", "AC048383.1",
         "AC055813.1", "AC069549.1", "AC074281.2", "AC090023.1", "AC090617.7", "AC090791.1", "AC092139.3", "AC092164.1",
         "AC093110.1", "AC096736.3", "AC099342.1", "AC103794.1", "AC110285.4", "AC113386.1", "AC126178.1", "AF178030.1",
         "AHNAK", "AK4P2", "AL021707.1", "AL024497.2", "AL035414.1", "AL049649.1", "AL135937.1", "AL136313.1",
         "AL136962.1", "AL137005.1", "AL138920.1", "AL158042.1", "AL158212.2", "AL161729.2", "AL162386.2", "AL162497.1",
         "AL354760.1", "AL356311.1", "AL445253.1", "AL445470.1", "AL445483.1", "AL589702.1", "AL590490.1", "AL591122.1",
         "AL691426.1", "AL844908.2", "AP000317.2", "AP001977.1", "AP002967.1", "AP003063.2", "AP005209.1", "AQP1",
         "ARHGAP26-IT1", "ARID1A", "ATAD1", "ATXN7L1", "AXIN1", "B4GALNT3", "BARX1", "BBOX1-AS1", "BMP2", "BMP4",
         "BMP5", "BNC2-AS1", "BTBD16", "CADM1", "CARMN", "CASC19", "CASC20", "CD109", "CD68", "CHCHD4P2", "CRADD",
         "CRTC3", "CSNK1G3", "CTBP2P8", "DAB2", "DDN", "DLEU7", "DLX5", "DLX6-AS1", "DOT1L", "EMP1", "ERG", "ESR1",
         "EYA1", "EYA4", "FABP3P2", "FAM129B", "FAM133B", "FAM210A", "FAM234B", "FBXL2", "FGFRL1", "FOSB", "FRZB",
         "FTO", "FUBP3", "FZD4", "GAPDHP54", "GFRAL", "GMDS-DT", "GPC6", "GPC6-AS2", "GSTM3P2", "HDAC5", "HLA-W",
         "HMGCLL1", "HMGN1P28", "HNRNPUL1", "HOXA11", "ICA1L", "IDUA", "IGFBP7", "IGHMBP2", "INPP5A", "INSC",
         "ITCH-AS1", "JAG1", "JAZF1", "JUP", "KCNMA1", "KIAA2012", "KIF25-AS1", "KLF12", "KLF6", "KREMEN1", "L3MBTL3",
         "LEKR1", "LINC00880", "LINC01571", "LINC01700", "LINC01877", "LINC01956", "LINC02127", "LINC02341",
         "LINC02487", "LINC02513", "LINC02644", "LNCAROD", "LRP4", "LRP5", "MEPE", "MIR1908", "MIR3164", "MIR3173",
         "MIR3183", "MIR4451", "MIR4713", "MIR6716", "MIR6870", "MLPH", "MMP14", "MPP7", "MRPL28", "MS4A6A", "MTND5P34",
         "NAA38", "NCAM1-AS1", "NDP", "NGEF", "NME8", "NTN1", "NXNP1", "PCNX3", "PDE7B", "PHF2", "PKD2L1", "PLEKHG4",
         "PLPPR3", "PLXDC2", "PRRX1", "RAD23BP2", "RERE-AS1", "RF00006", "RF00012", "RF00019", "RF01161", "RF01210",
         "RF02219", "RGS9", "RHPN2", "RMND1", "RN7SKP43", "RN7SL531P", "RN7SL547P", "RN7SL79P", "RNA5SP133",
         "RNA5SP276", "RNU4-23P", "RNU6-105P", "RNU6-157P", "RNU6-440P", "RNU6-537P", "RNU6-813P", "RNU7-165P",
         "RPL10AP1", "RPL10P1", "RPL12P6", "RPL21P75", "RPL29P19", "RPL7L1P12", "RPS23P4", "RPS7P4", "RREB1", "RSPO3",
         "SAMD12-AS1", "SBNO2", "SEM1", "SEMA3F-AS1", "SEPT9", "SF3A3", "SFRP4", "SLC30A10", "SLC4A4", "SLX4IP",
         "SMARCAD1", "SPEN", "SPTBN1", "SUPT3H", "SYNE1-AS1", "TBX15", "TECRP1", "THORLNC", "TMEM18", "TMEM92",
         "TNFRSF11A", "UBE3D", "USP31", "USP3-AS1", "WNT1", "WNT16", "WNT5B", "Z93930.3", "ZNF827"]

# based on: doi:10.1038/ng.3949
# study sample: 142,487 individuals of European ancestry (BioBank)
# phenotypes: bone mineral density was estimated by quantitative ultrasound of the heel (“eBMD”)
# raw data was transformed to obtain normal distribution (rank based inverse normal transformation)
# variants: authors identified 307 conditionally independent SNPs attaining genome-wide significance at 203 loci (Suppl. Table2);
# 4 SNPs removed (no rs_id), one removed (not existing variant); several variants have 0 frequency in EUR population (1000G)  - I left them
# together: 302 variants
# effects: regression analysis performed on transformed data
# heritability: 60-80%, explained (by significant SNPs): 12%
# model na podstawie 100000 symulacji (plik symul.py), wyniki w pliku dist_bmd.txt i stats_bmd.txt (wyniki z sześciu powtorzen symulacji)
# frequencies for EA taken from (EUR, 1000Genomes http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)
# 255

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.6143, category_name='High risk'),
                        QuantitativeCategory(from_=0.6143, to=1.6382, category_name='Average risk'),
                        QuantitativeCategory(from_=1.6382, category_name='Low risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs139603701', effect_allele='A', beta_value=0.0963),
                        beta(rs_id='rs2708632', effect_allele='T', beta_value=-0.0289),
                        beta(rs_id='rs75077113', effect_allele='A', beta_value=0.0229),
                        beta(rs_id='rs6701290', effect_allele='G', beta_value=0.0425),
                        beta(rs_id='rs7519889', effect_allele='G', beta_value=0.047),
                        beta(rs_id='rs34963268', effect_allele='G', beta_value=-0.0642),
                        beta(rs_id='rs4589135', effect_allele='T', beta_value=0.023),
                        beta(rs_id='rs4360494', effect_allele='G', beta_value=-0.02),
                        beta(rs_id='rs4397637', effect_allele='G', beta_value=-0.0389),
                        beta(rs_id='rs11209240', effect_allele='A', beta_value=-0.0408),
                        beta(rs_id='rs3790608', effect_allele='G', beta_value=-0.0405),
                        beta(rs_id='rs6680737', effect_allele='G', beta_value=-0.0251),
                        beta(rs_id='rs1080789', effect_allele='A', beta_value=0.0226),
                        beta(rs_id='rs35363078', effect_allele='T', beta_value=-0.0214),
                        beta(rs_id='rs12041600', effect_allele='G', beta_value=0.0255),
                        beta(rs_id='rs7535122', effect_allele='A', beta_value=0.0233),
                        beta(rs_id='rs7516171', effect_allele='C', beta_value=-0.0294),
                        beta(rs_id='rs17514738', effect_allele='T', beta_value=0.0234),
                        beta(rs_id='rs201324539', effect_allele='G', beta_value=-0.0316),
                        beta(rs_id='rs2275707', effect_allele='C', beta_value=0.0335),
                        beta(rs_id='rs7527300', effect_allele='C', beta_value=0.0307),
                        beta(rs_id='rs1414660', effect_allele='C', beta_value=-0.0829),
                        beta(rs_id='rs12714415', effect_allele='T', beta_value=0.0295),
                        beta(rs_id='rs6547870', effect_allele='G', beta_value=0.0236),
                        beta(rs_id='rs10490046', effect_allele='A', beta_value=0.0327),
                        beta(rs_id='rs7576689', effect_allele='C', beta_value=-0.0522),
                        beta(rs_id='rs1550429', effect_allele='C', beta_value=0.0039),
                        beta(rs_id='rs6761129', effect_allele='C', beta_value=-0.0356),
                        beta(rs_id='rs4233949', effect_allele='C', beta_value=0.0746),
                        beta(rs_id='rs34138479', effect_allele='A', beta_value=-0.0518),
                        beta(rs_id='rs7578166', effect_allele='A', beta_value=-0.0224),
                        beta(rs_id='rs62164915', effect_allele='A', beta_value=-0.0227),
                        beta(rs_id='rs1878526', effect_allele='G', beta_value=-0.0645),
                        beta(rs_id='rs115242848', effect_allele='C', beta_value=-0.3794),
                        beta(rs_id='rs10199437', effect_allele='G', beta_value=0.0609),
                        beta(rs_id='rs55983207', effect_allele='T', beta_value=-0.0472),
                        beta(rs_id='rs62159864', effect_allele='T', beta_value=-0.052),
                        beta(rs_id='rs34588551', effect_allele='C', beta_value=-0.0206),
                        beta(rs_id='rs10206992', effect_allele='T', beta_value=0.0241),
                        beta(rs_id='rs4675694', effect_allele='C', beta_value=-0.0353),
                        beta(rs_id='rs11679645', effect_allele='T', beta_value=0.0293),
                        beta(rs_id='rs10931982', effect_allele='T', beta_value=-0.0548),
                        beta(rs_id='rs62195575', effect_allele='A', beta_value=-0.026),
                        beta(rs_id='rs183979857', effect_allele='T', beta_value=-0.1918),
                        beta(rs_id='rs56185026', effect_allele='T', beta_value=-0.0369),
                        beta(rs_id='rs2675952', effect_allele='T', beta_value=-0.0243),
                        beta(rs_id='rs58057291', effect_allele='T', beta_value=0.023),
                        beta(rs_id='rs1560633', effect_allele='T', beta_value=-0.0221),
                        beta(rs_id='rs142515962', effect_allele='A', beta_value=0.0984),
                        beta(rs_id='rs370387', effect_allele='G', beta_value=-0.0425),
                        beta(rs_id='rs2624847', effect_allele='G', beta_value=0.0244),
                        beta(rs_id='rs11915970', effect_allele='A', beta_value=-0.0384),
                        beta(rs_id='rs1352014', effect_allele='A', beta_value=-0.0263),
                        beta(rs_id='rs344083', effect_allele='A', beta_value=0.0385),
                        beta(rs_id='rs56082403', effect_allele='T', beta_value=0.0303),
                        beta(rs_id='rs6829296', effect_allele='C', beta_value=0.0162),
                        beta(rs_id='rs4505759', effect_allele='C', beta_value=-0.0526),
                        beta(rs_id='rs112069922', effect_allele='C', beta_value=0.0757),
                        beta(rs_id='rs1386625', effect_allele='A', beta_value=0.0479),
                        beta(rs_id='rs11133474', effect_allele='C', beta_value=-0.022),
                        beta(rs_id='rs11729023', effect_allele='C', beta_value=-0.0317),
                        beta(rs_id='rs72976751', effect_allele='A', beta_value=-0.0343),
                        beta(rs_id='rs72655796', effect_allele='A', beta_value=0.0299),
                        beta(rs_id='rs10013456', effect_allele='A', beta_value=-0.0284),
                        beta(rs_id='rs6839437', effect_allele='T', beta_value=-0.0365),
                        beta(rs_id='rs10022648', effect_allele='A', beta_value=0.0229),
                        beta(rs_id='rs34687052', effect_allele='A', beta_value=0.023),
                        beta(rs_id='rs6870556', effect_allele='G', beta_value=0.0211),
                        beta(rs_id='rs1428968', effect_allele='C', beta_value=-0.0338),
                        beta(rs_id='rs2542710', effect_allele='A', beta_value=0.0244),
                        beta(rs_id='rs28744551', effect_allele='C', beta_value=-0.039),
                        beta(rs_id='rs559181', effect_allele='A', beta_value=0.0251),
                        beta(rs_id='rs9327301', effect_allele='G', beta_value=0.0239),
                        beta(rs_id='rs6882422', effect_allele='G', beta_value=0.0328),
                        beta(rs_id='rs3776221', effect_allele='A', beta_value=0.0241),
                        beta(rs_id='rs368510', effect_allele='G', beta_value=-0.0273),
                        beta(rs_id='rs4959677', effect_allele='G', beta_value=0.0269),
                        beta(rs_id='rs525678', effect_allele='A', beta_value=0.078),
                        beta(rs_id='rs9379084', effect_allele='G', beta_value=0.0414),
                        beta(rs_id='rs74971894', effect_allele='A', beta_value=0.0421),
                        beta(rs_id='rs9260426', effect_allele='A', beta_value=0.0209),
                        beta(rs_id='rs113166754', effect_allele='C', beta_value=0.0996),
                        beta(rs_id='rs1502199', effect_allele='A', beta_value=0.0232),
                        beta(rs_id='rs76768932', effect_allele='G', beta_value=-0.0496),
                        beta(rs_id='rs72868839', effect_allele='A', beta_value=-0.05),
                        beta(rs_id='rs10943130', effect_allele='C', beta_value=0.0228),
                        beta(rs_id='rs6454314', effect_allele='G', beta_value=0.0257),
                        beta(rs_id='rs9491689', effect_allele='C', beta_value=-0.0541),
                        beta(rs_id='rs7741021', effect_allele='A', beta_value=-0.0818),
                        beta(rs_id='rs1415701', effect_allele='G', beta_value=-0.0285),
                        beta(rs_id='rs9402490', effect_allele='G', beta_value=0.0563),
                        beta(rs_id='rs547545', effect_allele='T', beta_value=-0.0338),
                        beta(rs_id='rs12529766', effect_allele='C', beta_value=0.0321),
                        beta(rs_id='rs55784958', effect_allele='C', beta_value=0.0144),
                        beta(rs_id='rs4869744', effect_allele='T', beta_value=0.0947),
                        beta(rs_id='rs12525051', effect_allele='G', beta_value=-0.0556),
                        beta(rs_id='rs2941741', effect_allele='G', beta_value=-0.0763),
                        beta(rs_id='rs2504069', effect_allele='C', beta_value=-0.0451),
                        beta(rs_id='rs633891', effect_allele='C', beta_value=0.0198),
                        beta(rs_id='rs73029263', effect_allele='A', beta_value=0.0293),
                        beta(rs_id='rs528738', effect_allele='G', beta_value=-0.0351),
                        beta(rs_id='rs4708620', effect_allele='T', beta_value=0.0191),
                        beta(rs_id='rs10249754', effect_allele='A', beta_value=-0.0299),
                        beta(rs_id='rs13328356', effect_allele='C', beta_value=0.0237),
                        beta(rs_id='rs3095208', effect_allele='T', beta_value=-0.0249),
                        beta(rs_id='rs85', effect_allele='T', beta_value=-0.0375),
                        beta(rs_id='rs62454420', effect_allele='A', beta_value=-0.0438),
                        beta(rs_id='rs17501090', effect_allele='C', beta_value=0.0756),
                        beta(rs_id='rs10244184', effect_allele='T', beta_value=0.0336),
                        beta(rs_id='rs757980', effect_allele='G', beta_value=0.0375),
                        beta(rs_id='rs10276670', effect_allele='A', beta_value=0.0471),
                        beta(rs_id='rs17236800', effect_allele='A', beta_value=-0.0499),
                        beta(rs_id='rs1717731', effect_allele='T', beta_value=-0.0698),
                        beta(rs_id='rs6956946', effect_allele='T', beta_value=-0.0529),
                        beta(rs_id='rs302101', effect_allele='G', beta_value=0.0217),
                        beta(rs_id='rs12154498', effect_allele='A', beta_value=-0.0325),
                        beta(rs_id='rs4440558', effect_allele='A', beta_value=-0.0483),
                        beta(rs_id='rs17598132', effect_allele='C', beta_value=0.0282),
                        beta(rs_id='rs1724298', effect_allele='T', beta_value=-0.0328),
                        beta(rs_id='rs212417', effect_allele='G', beta_value=0.0338),
                        beta(rs_id='rs2536195', effect_allele='A', beta_value=-0.1679),
                        beta(rs_id='rs10668066', effect_allele='G', beta_value=-0.1739),
                        beta(rs_id='rs62621812', effect_allele='G', beta_value=0.0887),
                        beta(rs_id='rs2929308', effect_allele='T', beta_value=0.0425),
                        beta(rs_id='rs6471752', effect_allele='C', beta_value=0.0292),
                        beta(rs_id='rs283324', effect_allele='G', beta_value=0.0244),
                        beta(rs_id='rs7003794', effect_allele='C', beta_value=0.029),
                        beta(rs_id='rs114847962', effect_allele='A', beta_value=0.0315),
                        beta(rs_id='rs2737252', effect_allele='G', beta_value=-0.0379),
                        beta(rs_id='rs1005502', effect_allele='C', beta_value=0.0253),
                        beta(rs_id='rs117108011', effect_allele='A', beta_value=-0.0979),
                        beta(rs_id='rs1487241', effect_allele='A', beta_value=0.0256),
                        beta(rs_id='rs12340775', effect_allele='G', beta_value=0.0492),
                        beta(rs_id='rs7036453', effect_allele='C', beta_value=0.0573),
                        beta(rs_id='rs10992867', effect_allele='G', beta_value=-0.0312),
                        beta(rs_id='rs62558340', effect_allele='C', beta_value=0.0324),
                        beta(rs_id='rs28550561', effect_allele='A', beta_value=0.0272),
                        beta(rs_id='rs2188092', effect_allele='G', beta_value=-0.0387),
                        beta(rs_id='rs72767980', effect_allele='C', beta_value=-0.032),
                        beta(rs_id='rs68192277', effect_allele='TAAGAG', beta_value=0.0203),
                        beta(rs_id='rs74119759', effect_allele='C', beta_value=0.0341),
                        beta(rs_id='rs17688827', effect_allele='A', beta_value=0.0229),
                        beta(rs_id='rs7074558', effect_allele='C', beta_value=0.0259),
                        beta(rs_id='rs12241932', effect_allele='T', beta_value=0.0336),
                        beta(rs_id='rs947091', effect_allele='G', beta_value=-0.036),
                        beta(rs_id='rs17662822', effect_allele='C', beta_value=0.0388),
                        beta(rs_id='rs1159798', effect_allele='A', beta_value=0.0687),
                        beta(rs_id='rs7099953', effect_allele='G', beta_value=0.1313),
                        beta(rs_id='rs7088220', effect_allele='C', beta_value=-0.0297),
                        beta(rs_id='rs1877998', effect_allele='G', beta_value=0.0343),
                        beta(rs_id='rs147048550', effect_allele='TGTAA', beta_value=-0.069),
                        beta(rs_id='rs603424', effect_allele='G', beta_value=0.031),
                        beta(rs_id='rs11196171', effect_allele='A', beta_value=0.0286),
                        beta(rs_id='rs10885447', effect_allele='G', beta_value=-0.0263),
                        beta(rs_id='rs10749436', effect_allele='G', beta_value=-0.0249),
                        beta(rs_id='rs7069328', effect_allele='G', beta_value=0.0194),
                        beta(rs_id='rs1133400', effect_allele='A', beta_value=-0.0346),
                        beta(rs_id='rs17507577', effect_allele='G', beta_value=-0.0463),
                        beta(rs_id='rs111506224', effect_allele='C', beta_value=-0.0571),
                        beta(rs_id='rs35498021', effect_allele='T', beta_value=0.0242),
                        beta(rs_id='rs11029901', effect_allele='A', beta_value=0.0453),
                        beta(rs_id='rs10835489', effect_allele='G', beta_value=0.028),
                        beta(rs_id='rs12806687', effect_allele='C', beta_value=0.0419),
                        beta(rs_id='rs17602572', effect_allele='C', beta_value=0.0304),
                        beta(rs_id='rs174574', effect_allele='A', beta_value=0.0203),
                        beta(rs_id='rs117111740', effect_allele='T', beta_value=0.1155),
                        beta(rs_id='rs66864335', effect_allele='G', beta_value=0.0326),
                        beta(rs_id='rs61887821', effect_allele='G', beta_value=0.1997),
                        beta(rs_id='rs11228219', effect_allele='C', beta_value=0.0347),
                        beta(rs_id='rs4988321', effect_allele='G', beta_value=0.0729),
                        beta(rs_id='rs557266652', effect_allele='A', beta_value=0.4354),
                        beta(rs_id='rs3829241', effect_allele='G', beta_value=-0.0306),
                        beta(rs_id='rs149504726', effect_allele='G', beta_value=-0.157),
                        beta(rs_id='rs649693', effect_allele='T', beta_value=0.0657),
                        beta(rs_id='rs6589301', effect_allele='G', beta_value=0.0547),
                        beta(rs_id='rs17595156', effect_allele='G', beta_value=-0.0337),
                        beta(rs_id='rs1048932', effect_allele='C', beta_value=-0.04),
                        beta(rs_id='rs2509353', effect_allele='C', beta_value=0.0226),
                        beta(rs_id='rs1622638', effect_allele='G', beta_value=-0.0224),
                        beta(rs_id='rs4505077', effect_allele='C', beta_value=0.0199),
                        beta(rs_id='rs6489548', effect_allele='A', beta_value=0.024),
                        beta(rs_id='rs7959604', effect_allele='C', beta_value=0.056),
                        beta(rs_id='rs7301013', effect_allele='A', beta_value=-0.0318),
                        beta(rs_id='rs76243438', effect_allele='T', beta_value=-0.0423),
                        beta(rs_id='rs117481343', effect_allele='C', beta_value=-0.1292),
                        beta(rs_id='rs118115924', effect_allele='G', beta_value=0.1865),
                        beta(rs_id='rs10875906', effect_allele='C', beta_value=-0.0308),
                        beta(rs_id='rs2279743', effect_allele='T', beta_value=0.0328),
                        beta(rs_id='rs11832031', effect_allele='T', beta_value=0.0275),
                        beta(rs_id='rs10858944', effect_allele='G', beta_value=-0.0567),
                        beta(rs_id='rs7969076', effect_allele='T', beta_value=-0.0215),
                        beta(rs_id='rs11067228', effect_allele='A', beta_value=-0.0225),
                        beta(rs_id='rs1323168', effect_allele='A', beta_value=-0.0308),
                        beta(rs_id='rs8002850', effect_allele='G', beta_value=0.03),
                        beta(rs_id='rs58973023', effect_allele='A', beta_value=0.0469),
                        beta(rs_id='rs2147160', effect_allele='C', beta_value=-0.001),
                        beta(rs_id='rs1149821', effect_allele='T', beta_value=0.022),
                        beta(rs_id='rs72635657', effect_allele='G', beta_value=0.0846),
                        beta(rs_id='rs2008411', effect_allele='C', beta_value=0.0389),
                        beta(rs_id='rs1078514', effect_allele='C', beta_value=0.0238),
                        beta(rs_id='rs1042704', effect_allele='G', beta_value=0.0325),
                        beta(rs_id='rs10145299', effect_allele='T', beta_value=-0.0241),
                        beta(rs_id='rs10130587', effect_allele='G', beta_value=-0.0629),
                        beta(rs_id='rs55874297', effect_allele='G', beta_value=0.0384),
                        beta(rs_id='rs35816040', effect_allele='TAACA', beta_value=0.0426),
                        beta(rs_id='rs11636403', effect_allele='C', beta_value=-0.0347),
                        beta(rs_id='rs8023466', effect_allele='G', beta_value=-0.0299),
                        beta(rs_id='rs28587205', effect_allele='A', beta_value=0.0207),
                        beta(rs_id='rs2668602', effect_allele='G', beta_value=-0.0277),
                        beta(rs_id='rs11637971', effect_allele='A', beta_value=-0.0269),
                        beta(rs_id='rs2301522', effect_allele='A', beta_value=0.0298),
                        beta(rs_id='rs71378512', effect_allele='G', beta_value=0.0946),
                        beta(rs_id='rs62028332', effect_allele='G', beta_value=-0.0427),
                        beta(rs_id='rs9932220', effect_allele='G', beta_value=-0.033),
                        beta(rs_id='rs11643929', effect_allele='C', beta_value=0.0171),
                        beta(rs_id='rs72805220', effect_allele='C', beta_value=0.0634),
                        beta(rs_id='rs9972653', effect_allele='G', beta_value=-0.0204),
                        beta(rs_id='rs17680862', effect_allele='G', beta_value=0.0882),
                        beta(rs_id='rs4888151', effect_allele='A', beta_value=0.0252),
                        beta(rs_id='rs71390846', effect_allele='G', beta_value=0.0391),
                        beta(rs_id='rs2376600', effect_allele='G', beta_value=0.028),
                        beta(rs_id='rs35401268', effect_allele='GA', beta_value=-0.0568),
                        beta(rs_id='rs78180894', effect_allele='G', beta_value=0.0378),
                        beta(rs_id='rs74439044', effect_allele='T', beta_value=0.0376),
                        beta(rs_id='rs56235417', effect_allele='G', beta_value=0.0296),
                        beta(rs_id='rs4239232', effect_allele='T', beta_value=0.0226),
                        beta(rs_id='rs143043662', effect_allele='C', beta_value=-0.0876),
                        beta(rs_id='rs7209826', effect_allele='A', beta_value=-0.0423),
                        beta(rs_id='rs188810925', effect_allele='G', beta_value=-0.0669),
                        beta(rs_id='rs170634', effect_allele='C', beta_value=0.0308),
                        beta(rs_id='rs2696264', effect_allele='G', beta_value=-0.0338),
                        beta(rs_id='rs72829754', effect_allele='G', beta_value=-0.0353),
                        beta(rs_id='rs1036902', effect_allele='T', beta_value=-0.0522),
                        beta(rs_id='rs16961974', effect_allele='T', beta_value=0.0263),
                        beta(rs_id='rs11869530', effect_allele='G', beta_value=-0.0265),
                        beta(rs_id='rs8069036', effect_allele='T', beta_value=-0.0228),
                        beta(rs_id='rs73997493', effect_allele='C', beta_value=0.0415),
                        beta(rs_id='rs34202212', effect_allele='GC', beta_value=0.022),
                        beta(rs_id='rs11875132', effect_allele='T', beta_value=0.0244),
                        beta(rs_id='rs1941749', effect_allele='A', beta_value=0.0481),
                        beta(rs_id='rs884205', effect_allele='A', beta_value=-0.0332),
                        beta(rs_id='rs78015143', effect_allele='G', beta_value=-0.0365),
                        beta(rs_id='rs8108787', effect_allele='T', beta_value=0.0324),
                        beta(rs_id='rs4807629', effect_allele='A', beta_value=0.0323),
                        beta(rs_id='rs4806832', effect_allele='C', beta_value=0.0208),
                        beta(rs_id='rs7255601', effect_allele='G', beta_value=-0.1065),
                        beta(rs_id='rs13345456', effect_allele='C', beta_value=-0.0255),
                        beta(rs_id='rs3170167', effect_allele='T', beta_value=0.0316),
                        beta(rs_id='rs62198536', effect_allele='A', beta_value=-0.0377),
                        beta(rs_id='rs201402819', effect_allele='CTGT', beta_value=-0.0768),
                        beta(rs_id='rs6117854', effect_allele='G', beta_value=0.0353),
                        beta(rs_id='rs6040006', effect_allele='C', beta_value=0.0935),
                        beta(rs_id='rs17457340', effect_allele='T', beta_value=0.0641),
                        beta(rs_id='rs6040068', effect_allele='C', beta_value=0.0354),
                        beta(rs_id='rs6134038', effect_allele='A', beta_value=0.0375),
                        beta(rs_id='rs1980854', effect_allele='G', beta_value=-0.0392),
                        beta(rs_id='rs34952318', effect_allele='G', beta_value=0.0742),
                        beta(rs_id='rs13044413', effect_allele='A', beta_value=-0.0262),
                        beta(rs_id='rs3092018', effect_allele='A', beta_value=-0.0321),
                        beta(rs_id='rs239677', effect_allele='C', beta_value=0.0283),
                        beta(rs_id='rs55787537', effect_allele='T', beta_value=0.0304),
                        beta(rs_id='rs2836613', effect_allele='G', beta_value=-0.0324),
                        beta(rs_id='rs11088458', effect_allele='A', beta_value=0.0432),
                        beta(rs_id='rs4818988', effect_allele='A', beta_value=0.0206),
                        beta(rs_id='rs133441', effect_allele='A', beta_value=-0.0241),
                        beta(rs_id='rs134639', effect_allele='A', beta_value=0.0429),
                        beta(rs_id='rs13056435', effect_allele='C', beta_value=0.0481),
                        beta(rs_id='rs5952416', effect_allele='T', beta_value=-0.0359),
                        beta(rs_id='rs17307280', effect_allele='C', beta_value=0.0733)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Bone Mineral Density'

about = ('Bone mineral density (BMD) is a measure of bone health. A BMD test looks at '
         'bone density and compares it to a healthy person of the same age and '
         'biological sex to give a measurement. Low BMD is a risk factor for '
         'osteoporosis and bone fractures. Vitamin D and calcium work together to '
         'maintain BMD, and taking dietary supplements of these may help maintain BMD '
         'in some people.')

how_your_result_was_calculated = ('Your result is a risk based on 280 genetic variants, specifically called '
                                  'SNPs, found in 255 genes associated with bone mineral density. Overall, this '
                                  'explains about 12% of the genetic variation in bone mineral density between '
                                  'people, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact bone mineral density, including diet, lifestyle, other '
                           'health issues, and genetics. Genetics has a moderate overall impact on bone '
                           'mineral density when you consider all the other factors.')

other_factors = [{'content': 'A balanced and healthy diet is always a good idea. Many foods and drinks are good for '
                             'bone health. These include milk, yogurt, cheese, fatty fish (like salmon and tuna), leafy'
                             ' green vegetables, tomato products, and certain fruits (like oranges, bananas, '
                             'and plantains).',
                  'title': 'Diet'},
                 {'content': 'Conservative amount of Calcium, as well as practitioner '
                             'recommended dosage of vitamin D and magesium supplements can '
                             'help maintain bone mineral density.',
                  'title': 'Supplements'},
                 {'content': 'Staying active -- and including weight-bearing exercise -- helps '
                             'maintain bone mineral density and strong bones.',
                  'title': 'Lifestyle'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have a low bone mineral density or have '
                             'osteoporosis. These types of risk factors, which are usually due '
                             'to rare genetic variants, are not included in this test. If you '
                             'have a family history of these issues, talk to your '
                             'Practitioner.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your bone mineral density and other factors, '
                                           'you may have an average risk for osteoporosis or other '
                                           'health issues.',
                           'High risk': 'Depending on your bone mineral density and other factors, you '
                                        'may have a higher risk for osteoporosis or other health issues.',
                           'Low risk': 'Depending on your bone mineral density and other factors, you '
                                       'may have a lower risk for osteoporosis or other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average bone mineral density (BMD).',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a lower bone mineral density (BMD).',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a higher bone mineral density (BMD).'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent low bone '
                                             'mineral density and stay healthy. Certain tests can also '
                                             'check your vitamin and mineral levels that are important for '
                                             'bone health. Talk to your practitioner about what is right '
                                             'for you.',
                             'High risk': 'There are things you can do to try to prevent low bone mineral '
                                          'density and stay healthy. Certain tests can also check your '
                                          'vitamin and mineral levels that are important for bone health. '
                                          'Talk to your practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent low bone mineral '
                                         'density and stay healthy. Certain tests can also check your '
                                         'vitamin and mineral levels that are important for bone health. '
                                         'Talk to your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a low bone mineral density. Consider '
                                                'talking to your practitioner about ordering '
                                                'tests to check your levels of vitamins and '
                                                'minerals that are important for bone health, '
                                                'and possible next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'A balanced and healthy diet is always a good '
                                                'idea. Many foods and drinks are good for bone '
                                                'health. These include milk, yogurt, cheese, '
                                                'fatty fish (like salmon and tuna), leafy green '
                                                'vegetables, tomato products, and certain fruits '
                                                '(like oranges, bananas, and plantains).',
                                     'title': 'Diet'},
                                    {'content': 'Different vitamins and minerals are important '
                                                'to maintain bone mineral density, vitamin D, '
                                                'and magnesium (limit calcium). These are '
                                                'available in a multivitamin or as separate '
                                                'dietary supplements.Talk to your practitioner '
                                                'about dosages if you want to take these, '
                                                'especially if you are taking other supplements '
                                                'or are on medications. The amounts you need '
                                                'also vary by biological sex and age.',
                                     'title': 'Supplements'},
                                    {'content': 'Weight-bearing exercise, such as climbing '
                                                'stairs, brisk walking, jogging, or dancing, can '
                                                'help keeps bone strong. At the same time, it '
                                                'increases flexibility and helps to maintain a '
                                                'healthy weight.',
                                     'title': 'Lifestyle'},
                                    {'content': 'Low bone mineral density can run in families, '
                                                'and these risk factors are not included in this '
                                                'test. If you have a close relative (parent, '
                                                'child, sibling) with low bone mineral density '
                                                'or bone fractures, speak to your practitioner '
                                                'about your family history. If you have blood '
                                                'tests to look at certain vitamins and minerals, '
                                                'information about your family history will help '
                                                'with understanding your blood test results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your levels of vitamins and minerals that '
                                             'are important for bone health, as well as other '
                                             'tests. Talk to them about this and possible next '
                                             'steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Many foods and drinks are good for bone health. '
                                             'These include milk, yogurt, cheese, fatty fish '
                                             '(like salmon and tuna), leafy green vegetables, '
                                             'tomato products, and certain fruits (like oranges, '
                                             'bananas, and plantains). .',
                                  'title': 'Diet'},
                                 {'content': 'Different vitamins and minerals are important to '
                                             'maintain bone mineral density, vitamin D, and '
                                             'magnesium (limit calcium).  These are available in '
                                             'a multivitamin or as separate dietary '
                                             'supplements.Talk to your practitioner about the '
                                             'dosages to take, especially if you are taking '
                                             'other supplements or are on medications. These '
                                             'amounts also vary by biological sex and age.',
                                  'title': 'Supplements'},
                                 {'content': 'Weight-bearing exercise, such as climbing stairs, '
                                             'brisk walking, jogging, or dancing, can help keeps '
                                             'bone strong. At the same time, it increases '
                                             'flexibility and helps to maintain a healthy '
                                             'weight.',
                                  'title': 'Lifestyle'},
                                 {'content': 'Low bone mineral density can run in families, and '
                                             'these risk factors are not included in this test. '
                                             'If you have a close relative (parent, child, '
                                             'sibling) with low bone mineral density or bone '
                                             'fractures, speak to your practitioner about your '
                                             'family history. If you have blood tests to look at '
                                             'certain vitamins and minerals, information about '
                                             'your family history will help with understanding '
                                             'your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a low bone mineral density. Consider talking to '
                                            'your practitioner about ordering tests to check '
                                            'your levels of vitamins and minerals that are '
                                            'important for bone health, and possible next steps '
                                            'that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'A balanced and healthy diet is always a good idea. '
                                            'Many foods and drinks are good for bone health. '
                                            'These include milk, yogurt, cheese, fatty fish '
                                            '(like salmon and tuna), leafy green vegetables, '
                                            'tomato products, and certain fruits (like oranges, '
                                            'bananas, and plantains).',
                                 'title': 'Diet'},
                                {'content': 'Different vitamins and minerals are important to '
                                            'maintain bone mineral density, vitamin D, and '
                                            'magnesium (limit calcium). These are available in a '
                                            'multivitamin or as separate dietary '
                                            'supplements.Talk to your practitioner about dosages '
                                            'if you want to take these, especially if you are '
                                            'taking other supplements or are on medications. The '
                                            'amounts you need also vary by biological sex and '
                                            'age.',
                                 'title': 'Supplements'},
                                {'content': 'Weight-bearing exercise, such as climbing stairs, '
                                            'brisk walking, jogging, or dancing, can help keeps '
                                            'bone strong. At the same time, it increases '
                                            'flexibility and helps to maintain a healthy weight.',
                                 'title': 'Lifestyle'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = [
    {
        "trait": "Bone Loss",
        "gene": "COL1A1",
        "rsid": "rs1800012",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "COL1A1 influences soft tissues (ligaments, tendons and connective tissue).  Additionally, it is involved with bone mineralization through involvement in the ratio of collagen alpha chains produced by bone cells.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Higher risk for bone loss",
                "description": "Individuals with the AA genotype are at higher risk for bone loss and inadequate bone mineralization. A poor ratio of collagen alpha chains produced by bone cells equates to less healthy bone mass, density, and strength. \nWomen in particular who posses the (AA) genotype have an substantially increased risk of bone loss. \nBone minerals and nutrients are insufficient in the diet. Supplement with collagen, bone broth, magnesium, and vitamin D along with eating green-leafy vegetables."
            },
            {
                "genotype": [
                    "A",
                    "C"
                ],
                "gene_impact_string": "Somewhat higher risk for bone loss",
                "description": "Individuals with the AC genotype are at somewhat higher risk for bone loss and inadequate bone mineralization. A poor ratio of collagen alpha chains produced by bone cells equates to less healthy bone mass, density, and strength.  \nBone minerals and nutrients are insufficient in the diet. Supplement with collagen, bone broth, magnesium, and vitamin D along with eating green-leafy vegetables."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Lower risk of bone loss",
                "description": "Individuals with the CC genotype are at a lower risk for bone loss and inadequate bone mineralization."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Vitamin D (Bone)",
        "gene": "VDR",
        "rsid": "rs731236",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Genetics plays a key role in bone mass and the many other vitamin-D related components of health.  The VDR gene is a key component of bone mineral assimilation, intestinal calcium absorption, and bone cell growth.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those with the GG genotype are at a higher risk of potential bone loss. \nMaintain healthy vitamin D levels through vitamin D testing. Ttry to maintain a healthy intestinal flora, eat green leafy vegetable consumption, and try to minimize grains and sugars. Keep safe levels of sun exposure, and supplementation."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk for bone loss",
                "description": "Those with the GG genotype are at a higher risk of potential bone loss. \nMaintain healthy vitamin D levels through vitamin D testing. Ttry to maintain a healthy intestinal flora, eat green leafy vegetable consumption, and try to minimize grains and sugars. Keep safe levels of sun exposure, and supplementation."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those with the AA genotype are at a lower risk of low vitamin D and potential bone loss."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Increased risk of low bone mineral density",
        "gene": "VDR",
        "rsid": "rs1544410",
        "scientific_ref": {
            "website": "",
            "pmid": ""
        },
        "intro": "Genetics plays a key role in bone mass and the many other vitamin-D related components of health.  The VDR gene is a key component of bone mineral assimilation, intestinal calcium absorption, and bone cell growth.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those with the TT genotype are at a higher risk of potential bone mineral loss. \nMaintain healthy vitamin D levels through vitamin D testing. Try to maintain a healthy intestinal flora, eat green leafy vegetable consumption, and try to minimize grains and sugars. Keep safe levels of sun exposure, and supplementation."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those with the CT genotype are at somewhat higher risk of potential bone mineral loss. \nTry to maintain a healthy intestinal flora, eat green leafy vegetable consumption, and try to minimize grains and sugars. Keep safe levels of sun exposure, and supplementation."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those with the CC genotype are at a lower risk of low vitamin D and potential bone loss."
            }
        ],
        "risk_allele": "T"
    }
]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 23, 2019.\n',
                                     'links': 'https://medlineplus.gov/bonedensity.html',
                                     'title': '"Bone Density" from Medline Plus at National '
                                              'Institutes of Health'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Bone mineral density"
trait_science = "Bone mineral density index is moderately heritable - about 60% of differences observed among people has a genetic origin. Our analysis was based on 280 SNPs associated with BMD. These genetic variants are located within 255 genes. Together, they explain about 12% of the variation in BMD between people."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence bone mineral density. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
