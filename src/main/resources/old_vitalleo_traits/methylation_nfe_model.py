from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Homocysteine_concentration"
trait_pmids = ["23824729", "16706975", "20489178"]  # 23824729 - snips, 16706975, 20489178 - heritability
trait_heritability = 0.44
trait_snp_heritability = None  # as fraction
trait_explained = 0.059  # "The combined risk score explained 4.6% (Rotterdam Study I) to 5.9% (Rotterdam Study II) of the variance in tHcy concentrations" - PMID23824729
trait_authors = ["Wojciech Gałan"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip, nie uzupelnione przez Intelliger
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Bone homocysteine level is moderately heritable - about 44% of differences observed among people has a genetic origin [1-2]. Our test is based on results of the genome wide association studies (GWAS) [1], which included over 44,000 individuals of European descent [3]. The study identified 18 variants significantly associated with blood homocysteine concentration in European population. Together these SNPs explain about 6% of between people variance in this trait [1].\nOur analyses use genetic data (genotypes) for those SNPs to calculate the polygenic score."
genes = ['MTHFR', 'MTR', 'CPS1', 'MMUT', 'NOX4', 'CHMP1A', 'CBS', 'PRDX1', 'HIST1H2APS2', 'AC002064.3', 'CUBN',
         'HNF1A-AS1', 'FGF21', 'AL953897.1', 'TRDMT1', 'NOX4', 'FANCA', 'CBS']

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.2546, category_name='Low risk'),
                        QuantitativeCategory(from_=-0.2546, to=0.2471, category_name='Average risk'),
                        QuantitativeCategory(from_=0.2471, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs1801133', effect_allele='A', beta_value=0.1583),
                        beta(rs_id='rs2275565', effect_allele='T', beta_value=0.0542),
                        beta(rs_id='rs1047891', effect_allele='A', beta_value=0.0864),
                        beta(rs_id='rs9369898', effect_allele='A', beta_value=0.0449),
                        beta(rs_id='rs7130284', effect_allele='T', beta_value=-0.1242),
                        beta(rs_id='rs154657', effect_allele='A', beta_value=0.0963),
                        beta(rs_id='rs234709', effect_allele='T', beta_value=-0.0718),
                        beta(rs_id='rs4660306', effect_allele='T', beta_value=0.0435),
                        beta(rs_id='rs548987', effect_allele='C', beta_value=0.0597),
                        beta(rs_id='rs42648', effect_allele='A', beta_value=-0.0395),
                        beta(rs_id='rs1801222', effect_allele='A', beta_value=0.0453),
                        beta(rs_id='rs2251468', effect_allele='A', beta_value=-0.0512),
                        beta(rs_id='rs838133', effect_allele='A', beta_value=0.0422),
                        beta(rs_id='rs12134663', effect_allele='A', beta_value=-0.101),
                        beta(rs_id='rs12780845', effect_allele='A', beta_value=0.0529),
                        beta(rs_id='rs957140', effect_allele='A', beta_value=-0.045),
                        beta(rs_id='rs12921383', effect_allele='T', beta_value=-0.09),
                        beta(rs_id='rs2851391', effect_allele='T', beta_value=0.056)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Methylation'

about = ('When optimal methylation occurs, it has a significant positive impact on '
         'many biochemical reactions in the body that regulate the activity of cells, '
         'hormone metabolism, immune reactions, the cardiovascular, neurological, '
         'reproductive, and detoxification systems.')

how_your_result_was_calculated = ('We analyzed 18 common variants which influence blood homocysteine level. '
                                  'These variants explain about 5% of variance in homocysteine concentration.')

other_factors_statement = ('Homocysteine is a requirement for human health.  It only becomes a problem '
                           'when levels are elevated. Elevated levels can cause a substantial rise in '
                           'cardiac risks along with other conditions. When functioning well, the body '
                           'converts homocysteine to methionine or cysteine, which are also necessary '
                           'nutrients and not as harmful to the body.')

other_factors = [{'content': 'Genetics have a moderate overall impact.  Many people possess '
                             'genetic mutations that makes it challenging for their bodies to '
                             'methylate.  If the DNA test shows, for example, a MTHFR '
                             '(Methylene tetrahydrofolate reductase) variant the methylation '
                             'process is hindered and homocysteine is elevated.  Other '
                             'methylation related genes include MTHFR, CBS, MTR, MTRR, and '
                             'COMT.',
                  'title': 'Family History'},
                 {'content': 'Methylation impacts a wide range of health functions and can be '
                             'at the foundation of many types of illnesses.  Talk to your '
                             'Practioner.  A practitioner may recommend '
                             'further testing and lifestyle recommendations to discover if '
                             'other health problems are present, if any nutrient deficiencies '
                             'need to be addressed, and what care is necessary optimize the '
                             'body’s ability to function.',
                  'title': 'Lifestyle'},
                 {'content': 'It is important to eat a natural diet made up of whole, '
                             'un-processed foods.  Specific foods to focus on include: '
                             'asparagus, broccoli, avocado, Brussels sprouts, green leafy '
                             'vegetables, and legumes such as peas, beans, and lentils.',
                  'title': 'Diet'},
                 {'content': 'Nutrients that are helpful and important if an individual has '
                             'the genetic mutation that slows down the methylation cycle '
                             'include; Folate (B9), Methylcobalamin (B12), B6, B2, Magnesium, '
                             'and Vitamin D.',
                  'title': 'Supplements'}]

what_your_result_means = {'Average risk': 'Depending on your ability to methylate, you may experience '
                                           'increased risk of high homocysteine, which elevates the '
                                           'chances of heart disease and increases the risk of '
                                           'experiencing depression, anxiety, hormone imbalance, poor '
                                           'detox capacity, fatigue, and low energy.',
                           'High risk': 'Depending on your ability to methylate, you may experience '
                                        'increased risk of high homocysteine, which elevates the chances '
                                        'of heart disease and increases the risk of experiencing '
                                        'depression, anxiety, hormone imbalance, poor detox capacity, '
                                        'fatigue, and low energy.',
                           'Low risk': 'Depending on your ability to methylate, you may experience '
                                       'increased risk of high homocysteine, which elevates the chances '
                                       'of heart disease and increases the risk of experiencing '
                                       'depression, anxiety, hormone imbalance, poor detox capacity, '
                                       'fatigue, and low energy.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you may be more likely '
                                    'to have a average risk for methylation issues.',
                    'High risk': 'Based on the genetic factors tested, you may be more likely to '
                                 'have a high risk for methylation issues.',
                    'Low risk': 'Based on the genetic factors tested, you may be more likely to '
                                'have a low risk for methylation issues.'}

what_you_can_do_statement = {'Average risk': 'When methylation action is hindered; it can lead '
                                             'homocysteine accumulation.  The action of methylation is '
                                             'directly tied to the function of the MTHFR gene and several '
                                             'other associated genes.',
                             'High risk': 'When methylation action is hindered; it can lead homocysteine '
                                          'accumulation.  The action of methylation is directly tied to '
                                          'the function of the MTHFR gene and several other associated '
                                          'genes.',
                             'Low risk': 'When methylation action is hindered; it can lead homocysteine '
                                         'accumulation.  The action of methylation is directly tied to the '
                                         'function of the MTHFR gene and several other associated genes.'}

what_you_can_do = {'Average risk': [{'content': 'Talk to your Practitioner about '
                                                'how to analyze your DNA results and formulate '
                                                'the best lifestyle to address issues with or '
                                                'related to methylation.  A DNA test will reveal '
                                                'a polygenic (multiple gene) risk score for how '
                                                'likely you are or are not to have issues with '
                                                'methylation.  A doctor or counselor trained in '
                                                'assessing gene variations can help determine if '
                                                'you have an issue.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Nutritionally, the presence of this variant '
                                                'creates a potential need for higher levels of '
                                                'folate and other B Vitamins in the diet.  In '
                                                'addition to B9 (Folate), Vitamin B2, B6, and '
                                                'B12 are critical to improve this reaction along '
                                                'with the need for magnesium and Vitamin D.  '
                                                'Also, it may be wise to eat folate-rich foods: '
                                                'asparagus, green leafy vegetables, eggs, '
                                                'Brussels Sprouts, Citrus fruits, Broccoli, Nuts '
                                                'and Seeds, Papaya, and Avocado.',
                                     'title': 'Diet'},
                                    {'content': 'Try to participate in a regular exercise '
                                                'program, eliminate excessive alcohol '
                                                'consumption, avoid smoking, and limit coffee '
                                                'and caffeine consumption. Stress reduction is '
                                                'critical so get the proper amount of sleep, '
                                                'practice stress reduction techniques, utilize '
                                                'calming supplements such as CBD and magnesium '
                                                'based products, and get as many hugs as you '
                                                'can.',
                                     'title': 'Lifestyle'},
                                    {'content': 'As with anything genetic, methylation issues '
                                                'can run in families.  If you are suffering from '
                                                'methylation-related conditions or have a family '
                                                'history of heart disease, then it is important '
                                                'to take the necessary steps listed here.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Talk to your Practitioner about how '
                                             'to analyze your DNA results and formulate the best '
                                             'lifestyle to address issues with or related to '
                                             'methylation.  A DNA test will reveal a polygenic '
                                             '(multiple gene) risk score for how likely you are '
                                             'or are not to have issues with methylation.  A '
                                             'doctor or counselor trained in assessing gene '
                                             'variations can help determine if you have an '
                                             'issue.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Nutritionally, the presence of this variant '
                                             'creates a need for higher levels of folate and '
                                             'other B Vitamins in the diet.  In addition to B9 '
                                             '(Folate), Vitamin B2, B6, and B12 are critical to '
                                             'improve this reaction along with the need for '
                                             'magnesium and Vitamin D.  Also, eat folate-rich '
                                             'foods: asparagus, green leafy vegetables, eggs, '
                                             'Brussels Sprouts, Citrus fruits, Broccoli, Nuts '
                                             'and Seeds, Papaya, and Avocado.',
                                  'title': 'Diet'},
                                 {'content': 'Participate in a regular exercise program, '
                                             'eliminate excessive alcohol consumption, avoid '
                                             'smoking, and limit coffee and caffeine '
                                             'consumption. Stress reduction is critical so get '
                                             'the proper amount of sleep, practice stress '
                                             'reduction techniques, utilize calming supplements '
                                             'such as CBD and magnesium based products, and get '
                                             'as many hugs as you can.',
                                  'title': 'Lifestyle'},
                                 {'content': 'As with anything genetic, methylation issues can '
                                             'run in families.  If you are suffering from '
                                             'methylation-related conditions or have a family '
                                             'history of heart disease, then it is important to '
                                             'take the necessary steps listed here.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Talk to your Practitioner about how to '
                                            'analyze your DNA results and formulate the best '
                                            'lifestyle to address issues with or related to '
                                            'methylation.  A DNA test will reveal a polygenic '
                                            '(multiple gene) risk score for how likely you are '
                                            'or are not to have issues with methylation.  A '
                                            'doctor or counselor trained in assessing gene '
                                            'variations can help determine if you have an issue.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Nutritionally, the presence of this variant creates '
                                            'a need for higher levels of folate and other B '
                                            'Vitamins in the diet.  In addition to B9 (Folate), '
                                            'Vitamin B2, B6, and B12 are important to improve '
                                            'this reaction along with the need for magnesium and '
                                            'Vitamin D.  Also, eat folate-rich foods: asparagus, '
                                            'green leafy vegetables, eggs, Brussels Sprouts, '
                                            'Citrus fruits, Broccoli, Nuts and Seeds, Papaya, '
                                            'and Avocado.',
                                 'title': 'Diet'},
                                {'content': 'Participate in a regular exercise program, '
                                            'eliminate excessive alcohol consumption, avoid '
                                            'smoking, and limit coffee and caffeine consumption. '
                                            'Stress reduction is critical so get the proper '
                                            'amount of sleep, practice stress reduction '
                                            'techniques, utilize calming supplements such as CBD '
                                            'and magnesium based products, and get as many hugs '
                                            'as you can.',
                                 'title': 'Lifestyle'},
                                {'content': 'As with anything genetic, methylation issues can '
                                            'run in families.  If you are suffering from '
                                            'methylation-related conditions or have a family '
                                            'history of heart disease, then it is important to '
                                            'take the necessary steps listed here.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = [
    {
        "trait": "Homocysteine Levels",
        "gene": "CBS",
        "rsid": "rs1801181",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "CBS (cystathionine beta synthase) is a bodily pathways utilized to keep it healthy by inhibiting homocysteine build up. It\u2019s an enzyme required for the conversion of homocysteine into cysteine.  Negatively impacting CBS activity could cause an unhealthy rise in homocysteine levels.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with AA genotype are more likely to have elevated levels of homocysteine. It is important to maintain B vitamin levels and follow nutritional support for optimum methylation. Vitamin B6 specifically is the required co-factor for CBS."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with AG genotype are somewhat more likely to have elevated levels of homocysteine. It is important to maintain B vitamin levels and follow nutritional support for optimum methylation. Vitamin B6 specifically is the required co-factor for CBS."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with GG genotype are less likely to have elevated levels of homocysteine."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Homocysteine Levels",
        "gene": "CBS",
        "rsid": "rs2851391",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "CBS (cystathionine beta synthase) is a bodily pathways utilized to keep it healthy by inhibiting homocysteine build up. It\u2019s an enzyme required for the conversion of homocysteine into cysteine.  Negatively impacting CBS activity could cause an unhealthy rise in homocysteine levels.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with TT genotype are more likely to have elevated levels of homocysteine. It is important to maintain B vitamin levels and follow nutritional support for optimum methylation. Vitamin B6 specifically is the required co-factor for CBS."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with TC genotype are somewhat more likely to have elevated levels of homocysteine. It is important to maintain B vitamin levels and follow nutritional support for optimum methylation. Vitamin B6 specifically is the required co-factor for CBS."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with CC genotype are less likely to have elevated levels of homocysteine."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Homocysteine Levels",
        "gene": "CBS",
        "rsid": "rs234714",
        "scientific_ref": {
            "website": "",
            "pmid": "24651765"
        },
        "intro": "CBS (cystathionine beta synthase) is a bodily pathways utilized to keep it healthy by inhibiting homocysteine build up. It\u2019s an enzyme required for the conversion of homocysteine into cysteine.  Negatively impacting CBS activity could cause an unhealthy rise in homocysteine levels.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with TT genotype are more likely to have elevated levels of homocysteine. It is important to maintain B vitamin levels and follow nutritional support for optimum methylation. Vitamin B6 specifically is the required co-factor for CBS."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with TC genotype are somewhat more likely to have elevated levels of homocysteine. It is important to maintain B vitamin levels and follow nutritional support for optimum methylation. Vitamin B6 specifically is the required co-factor for CBS."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with CC genotype are less likely to have elevated levels of homocysteine."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Methylation",
        "gene": "MTR",
        "rsid": "rs1805087",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "The MTR gene is required for methionine synthase which is an enzyme required as a catalyst to methylate homosysteine to methionine. Insufficiency related to the MTR gene would cause a rise in homocysteine levels.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Highly likely",
                "description": "Those with GG genotype are highly likely to have increased MTR enzyme activity followed by decreased and likely healthier, homocysteine levels."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with AG genotype are more likely to have increased MTR enzyme activity followed by decreased and likely healthier, homocysteine levels."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with AA genotype are somewhat less likely to have increased MTR enzyme activity followed by decreased homocysteine levels. B12 is necessary for this reaction to occur.  Therefore, make sure to eat B12 foods such as fish, chicken, yogurt, and eggs as well as supplement B vitamins.  Also, optimize glutathione levels through cruciferous and allium vegetables and supplementing the 3 amino acids that make up glutathione; cysteine, glutamate, and glycine other supportive nutrients.  Test for cardiometabolic health and for signs of inflammation."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Methylation",
        "gene": "MTHFR",
        "rsid": "rs1801131",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "MTHFR (methylenetetrahydrofolate reductase) is an enzyme that plays a critical role in folate metabolism.  Folate metabolism is taking folate from nutritional intake and utilizing it for the purpose of synthesizing DNA or the re-methylation of homocysteine. Decreased MTHFR enzyme activity will lead to increased homocysteine levels. \nMethylation has a global impact in overall health and well-being.  It is a critical bodily process involved in hundreds of vital bodily function which includes gene expression.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with GG genotype are more likely to have decreased enzymes function. This would cause an increase in homocysteine levels and hinder function in other areas related to methylation, and may require greater folate, vitamin B2, B6 and B12 intake. Cardiometabolic and inflammation testing is important along with vitamin B supplementation and a diet focused on folate-rich foods: asparagus, green leafy vegetables, eggs, Brussels sprouts, citrus fruits, broccoli, nuts and seeds, papaya, and avocado."
            },
            {
                "genotype": [
                    "G",
                    "T"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with GT genotype are somewhat more likely to have decreased enzymes function. This would cause an increase in homocysteine levels and hinder function in other areas related to methylation, and may require greater folate, vitamin B2, B6 and B12 intake. Cardiometabolic and inflammation testing is important along with vitamin B supplementation and a diet focused on folate-rich foods: asparagus, green leafy vegetables, eggs, Brussels sprouts, citrus fruits, broccoli, nuts and seeds, papaya, and avocado."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with TT genotype are less likely to have decreased enzymes function and an increase in homocysteine levels."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Methylation",
        "gene": "MTHFR",
        "rsid": "rs1801133",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "MTHFR (methylenetetrahydrofolate reductase) is an enzyme that plays a critical role in folate metabolism.  Folate metabolism is taking folate from nutritional intake and utilizing it for the purpose of synthesizing DNA or the re-methylation of homocysteine. Decreased MTHFR enzyme activity will lead to increased homocysteine levels. \nMethylation has a global impact in overall health and well-being.  It is a critical bodily process involved in hundreds of vital bodily function which includes gene expression.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with AA genotype are more likely to have decreased enzymes function.  This would cause an increase in homocysteine levels and hinder function in other areas related to methylation, and may require greater folate, vitamin B2, B6 and B12 intake. Cardiometabolic and inflammation testing is important along with Vitamin B supplementation and a diet focused on folate-rich foods: asparagus, green leafy vegetables, eggs, Brussels sprouts, citrus fruits, broccoli, nuts and seeds, papaya, and avocado."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with AG genotype are somewhat more likely to have decreased enzymes function.  This would cause an increase in homocysteine levels and hinder function in other areas related to methylation, and may require greater folate, vitamin B2, B6 and B12 intake. Cardiometabolic and inflammation testing is important along with Vitamin B supplementation and a diet focused on folate-rich foods: asparagus, green leafy vegetables, eggs, Brussels sprouts, citrus fruits, broccoli, nuts and seeds, papaya, and avocado."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with GG genotype are less likely to have decreased enzymes function and an increase in homocysteine levels."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Methylation",
        "gene": "MTRR",
        "rsid": "rs1801394",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "MTRR (methionine synthase reductase) creates methylocobalamin an essential cofactor for (MTR), which is helpful maintaining adequate pools of methionine and maintaining homocysteine concentrations at non-toxic levels.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "More likely",
                "description": "The GG genotype is more likely associated with decreased enzyme efficiency.  Ensure adequate intake of folate, vitamin B12 and vitamin B6."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "The AG genotype is somewhat more likely associated with decreased enzyme efficiency.  Ensure adequate intake of folate, vitamin B12 and vitamin B6."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Less likely",
                "description": "The AA genotype is less likely associated with decreased enzyme efficiency."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Worrier vs Warrior",
        "gene": "COMT",
        "rsid": "rs4680",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "COMT (catechol-O-methyltransferase) is important in converting catechol estrogens(2-OH and 4-OH metabolites) into their respective methyl ester metabolites, which allows them to be water soluble.  \nCOMT is involved in the metabolism of certain neurotransmitters such as epinephrine, norepinephrine, and dopamine. This impacts  mood, energy, anxiety, sleep, and focus.\nAs COMT is involved with processing estrogen, a variant can throw off normal estrogen levels and hormonal balance, impact menstruation, effect menopause, lead to oxidative DNA damage, and increase risks of female cancers related to extended exposure to estrogen.  The COMT process is magnesium dependent.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "More likely",
                "description": "The AA genotype reduces the methylation activity of the COMT enzyme and is associated with an increase risk for conditions related to COMT functions such as anxiety, stress, sleep disorders, and breast cancer. Magnesium, B2, B9, and B12 are required nutrients. Minimize exposure to chemicals \u2013 plastics (BPA \u2013 xenoestrogen) and Round Up, and build glutathione up in the system through cruciferous and allium vegetables and supplementing the 3 amino acids that make up glutathione; cysteine, glutamate, and glycine and other supportive nutrients.  \nManage lifestyle through minimizing caffeine, stimulants, sugar, and carbs, getting optimal sleep, and regular exercise. It is important to manage stress, body composition, and get optimal sleep."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "The AG genotype reduces  the methylation activity of the COMT enzyme and is associated with an increase risk for conditions related to COMT functions such as anxiety, stress, sleep disorders, and breast cancer. Magnesium, B2, B9, and B12 are required nutrients. Minimize exposure to chemicals \u2013 plastics (BPA \u2013 xenoestrogen) and Round Up, and build glutathione up in the system through cruciferous and allium vegetables and supplementing the 3 amino acids that make up glutathione; cysteine, glutamate, and glycine and other supportive nutrients.  \nManage lifestyle through minimizing caffeine, stimulants, sugar, and carbs, getting optimal sleep, and regular exercise. It is important to manage stress, body composition, and get optimal sleep."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Less likely",
                "description": "The GG allele increases the methylation activity of the COMT enzyme and is associated with a lower risk for conditions related to COMT."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Reduced Methylation",
        "gene": "COMT",
        "rsid": "rs4633",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "COMT (catechol-O-methyltransferase) is important in converting catechol estrogens (2-OH and 4-OH metabolites) into their respective methyl ester metabolites, which allows them to be water soluble.  \nCOMT is involved in the metabolism of certain neurotransmitters such as epinephrine, norepinephrine, and dopamine. This impacts  mood, energy, anxiety, sleep, and focus\nAs COMT is involved with processing estrogen, a variant can throw off normal estrogen levels and hormonal balance, impact menstruation, effect menopause, lead to oxidative DNA damage, and increase risks of female cancers related to extended exposure to estrogen.  The COMT process is magnesium dependent.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "More likely",
                "description": "Those with the TT genotype are more likely at risk of reducing the methylation activity of the COMT enzyme. Also, the genotype can be associated with a increase risk for conditions related to COMT function such as anxiety, stress, sleep disorders, and breast cancer. Talk to a Practitioner about magnesium, B2, B9, B12, and antioxidants.  Minimize stress, the use of stimulants, and chemical exposure such as plastics and Round Up. Build glutathione up in the system through cruciferous and allium vegetables and supplementing the 3 amino acids that make up glutathione; cysteine, glutamate, and glycine and other supportive nutrients.  Manage lifestyle through quality sleep, addressing stress and limiting caffeine, stimulants, sugar, and carbs.  It is also important to optimize body composition (muscle to fat ratio) through exercise."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those with the TC genotype are moderately at risk of reducing the methylation activity of the COMT enzyme. The genotype can be also associated with an increased risk for conditions related to COMT function such as anxiety, stress, sleep disorders, and breast cancer.  Talk to a Practitioner about magnesium, B2, B9, B12, and antioxidants. Minimize stress, the use of stimulants, and chemical exposure such as plastics and Round Up.  Build glutathione up in the system through cruciferous and allium vegetables and supplementing the 3 amino acids that make up glutathione; cysteine, glutamate, and glycine and other supportive nutrients.  Manage lifestyle through quality sleep, addressing stress and limiting caffeine, stimulants, sugar, and carbs.  It is also important to optimize body composition (muscle to fat ratio) through exercise."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those with the CC genotype are at a low risk of reducing the methylation activity of the COMT enzyme and developing associated conditions."
            }
        ],
        "risk_allele": "T"
    }
]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_test_details = "We determined your genotype at eighteen genomic positions, which are known to influence the plasma homocysteine concentration. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle"  # =
trait_short_description = ""
trait_science = ""
trait_genotypes = []
