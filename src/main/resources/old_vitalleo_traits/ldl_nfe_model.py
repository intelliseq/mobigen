from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "LDL-C"
trait_pmids = ["30275531","18165655"]  # 30275531 supplementary table 9
trait_heritability = 0.3
trait_snp_heritability = None
trait_explained = 0.1
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Low-density lipoprotein blood level is moderately heritable, with h"+chr(0x00b2)+" estimates of 21-44% [1]. Our test is based on results of the genome wide association study (GWAS) [2], which included over 280 000 individuals, mainly of European ancestry (216 000 individuals). The study identified 217 variants significantly associated with the LDL concentration. Together these SNPs explain about 10% of between people variance in the LDL level [2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["ABCA10", "ABCA6", "ABCB11", "ABCB4", "ABCG8", "ABO", "AC005520.2", "AC009087.1", "AC010872.1", "AC011472.1",
         "AC011481.1", "AC011481.3", "AC011481.4", "AC012361.1", "AC021736.1", "AC022133.1", "AC040934.1", "AC067968.1",
         "AC073257.1", "AC073957.1", "AC079594.1", "AC089984.1", "AC091114.1", "AC092155.1", "AC099794.1", "AC114812.1",
         "AC115619.1", "AC135507.2", "ACVR1C", "AF178030.1", "AL035045.1", "AL160408.6", "AL355803.1", "AL357153.1",
         "AL359182.1", "AL445423.2", "AL512366.1", "AL603840.1", "ANGPTL8", "ANKRD31", "AP001318.3", "APOB", "APOE",
         "ARHGEF15", "ASCC3", "ATXN2", "BANF2", "BCAM", "BCL3", "BPTF", "BRCA2", "BSND", "C9orf163", "CBLC", "CD300LG",
         "CELSR2", "CMTM6", "COL4A3BP", "CSNK1G3", "CYP20A1", "CYP26A1", "CYP7A1", "DHX38", "DLG4", "DNAH11", "DNAJC13",
         "DYNC2LI1", "EEPD1", "EFNA1", "EVI2B", "FADS3", "FAM117B", "FAM24B", "FBLN1", "FUT2", "GCKR", "GPAM",
         "GSTM5P1", "H3F3B", "HBS1L", "HFE", "HGFAC", "HIST1H2AC", "HLA-B", "HMGCR", "HMGN1P2", "HNF1A", "IFT172",
         "INSIG2", "KLHDC7A", "KRI1", "LDLR", "LDLRAP1", "LIPG", "LLGL1", "LPA", "MAFB", "MIR148A", "MIR339", "MIR4422",
         "MIR6836", "MIR6886", "MIR8073", "MTCO3P1", "MTTP", "MYLIP", "MYPOP", "N4BP2L2", "NAT2", "NECTIN2", "NPC1L1",
         "OSGIN1", "PCSK9", "PEX6", "PHLDB1", "PITPNC1", "PLCE1-AS1", "PNPLA3", "PRDX2P4", "PRMT6", "PROCA1", "PTPRJ",
         "QPCTL", "RBM39", "REPS1", "RF00019", "RF00154", "RF00285", "RF00415", "RF00432", "RF01909", "RF02271",
         "RN7SL140P", "RN7SL165P", "RN7SL192P", "RN7SL615P", "RNU6-1151P", "RNU6-1243P", "RNU6-238P", "RNU6-856P",
         "RPL39P31", "RPS6", "SBNO1", "SEC11B", "SERPINA1", "SLC22A1", "SLC27A5", "SLC39A8", "SMUG1P1", "SP4", "SPC24",
         "TBC1D8", "TCP11", "TDRD15", "THOP1", "THORLNC", "TIMD4", "TIMM9P2", "TM4SF5", "TM6SF2", "TMC4", "TOM1",
         "TPI1P3", "TRAM2-AS1", "TXNL4B", "UBASH3B", "USP24", "VIM-AS1", "VLDLR", "VTN", "YIPF2", "ZDHHC24"]

# czyli zawartość cholesterolu w lipoproteinach o małej gęstości: to ten "zły cholesterol"
# na podstawie: doi:10.1038/s41588-018-0222-9;
# badania obejmowały dwie fazy "discovery" i "replication"
# faza "discovery": EUR – 216 tys., AFR – 57 tys, Hispanic – 24 tys;
# faza "replication": obiecujące warianty były dodatkowo potwierdzane z użyciem danych z '2017 GLGC exome array meta-analysis'
# lub z '2013  GLGC joint  meta-analysis’
# dane o rs: z tabeli SuppTable9 (tylko dotyczące LDL-C; te dane są z "conditional analysis"-nie wyrzucałam wariantów leżących blisko siebie)
# w sumie pozostało 219 SNPów (jeden wyrzuciłam, bo miał dziwny rs)
# efekty były podane, jako współczynniki beta z regresji, ale dane o stężeniu LDL-C były transformowane przed badaniem asocjacji:
# 'residuals were obtained after regressing on age, age2, sex and 10 principal components of ancestry. Residuals were subsequently
# inverse-normal transformed for association analysis': nie da się wspł. beta bezpośrednio odnieść do fenotypu
# odziedziczalnosc: około 30 %, zmienność wyjaśniona: około 10 % (Suppl. Table 10)
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_ldl.txt i stats_ldl.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.3187, category_name='Low risk'),
                        QuantitativeCategory(from_=-0.3187, to=0.5843, category_name='Average risk'),
                        QuantitativeCategory(from_=0.5843, category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs1730859', effect_allele='A', beta_value=-0.019),
                        beta(rs_id='rs12740374', effect_allele='T', beta_value=-0.1598),
                        beta(rs_id='rs4745', effect_allele='A', beta_value=0.0124),
                        beta(rs_id='rs2992753', effect_allele='A', beta_value=-0.0124),
                        beta(rs_id='rs867772', effect_allele='A', beta_value=-0.0258),
                        beta(rs_id='rs558971', effect_allele='A', beta_value=-0.0363),
                        beta(rs_id='rs35172831', effect_allele='T', beta_value=0.0191),
                        beta(rs_id='rs12748152', effect_allele='T', beta_value=0.026),
                        beta(rs_id='rs17111483', effect_allele='T', beta_value=0.0403),
                        beta(rs_id='rs11206510', effect_allele='T', beta_value=0.0684),
                        beta(rs_id='rs2479409', effect_allele='A', beta_value=-0.0225),
                        beta(rs_id='rs11583680', effect_allele='T', beta_value=0.0272),
                        beta(rs_id='rs10888896', effect_allele='C', beta_value=0.0289),
                        beta(rs_id='rs693668', effect_allele='A', beta_value=0.0286),
                        beta(rs_id='rs562556', effect_allele='A', beta_value=0.1298),
                        beta(rs_id='rs61739739', effect_allele='T', beta_value=0.08),
                        beta(rs_id='rs1165222', effect_allele='A', beta_value=-0.1352),
                        beta(rs_id='rs1475701', effect_allele='T', beta_value=-0.0676),
                        beta(rs_id='rs7551981', effect_allele='T', beta_value=0.0296),
                        beta(rs_id='rs10489488', effect_allele='A', beta_value=-0.0934),
                        beta(rs_id='rs12742537', effect_allele='A', beta_value=0.0153),
                        beta(rs_id='rs10874746', effect_allele='T', beta_value=-0.0154),
                        beta(rs_id='rs2792751', effect_allele='T', beta_value=0.0262),
                        beta(rs_id='rs1891110', effect_allele='A', beta_value=0.0212),
                        beta(rs_id='rs7080366', effect_allele='T', beta_value=0.0194),
                        beta(rs_id='rs41274050', effect_allele='T', beta_value=0.0774),
                        beta(rs_id='rs2068888', effect_allele='A', beta_value=-0.0171),
                        beta(rs_id='rs2274224', effect_allele='C', beta_value=-0.0142),
                        beta(rs_id='rs11603023', effect_allele='T', beta_value=0.0126),
                        beta(rs_id='rs7941030', effect_allele='T', beta_value=-0.0138),
                        beta(rs_id='rs10893500', effect_allele='T', beta_value=-0.0427),
                        beta(rs_id='rs4752805', effect_allele='A', beta_value=-0.0148),
                        beta(rs_id='rs174449', effect_allele='A', beta_value=0.0268),
                        beta(rs_id='rs2521567', effect_allele='A', beta_value=-0.0149),
                        beta(rs_id='rs3816492', effect_allele='T', beta_value=-0.0167),
                        beta(rs_id='rs3184504', effect_allele='T', beta_value=-0.0258),
                        beta(rs_id='rs1169288', effect_allele='A', beta_value=-0.0346),
                        beta(rs_id='rs10773003', effect_allele='A', beta_value=0.0237),
                        beta(rs_id='rs1521516', effect_allele='T', beta_value=-0.0161),
                        beta(rs_id='rs61754230', effect_allele='T', beta_value=0.0515),
                        beta(rs_id='rs4773173', effect_allele='A', beta_value=0.0164),
                        beta(rs_id='rs11571836', effect_allele='A', beta_value=0.0213),
                        beta(rs_id='rs3742318', effect_allele='T', beta_value=0.0201),
                        beta(rs_id='rs9646133', effect_allele='T', beta_value=-0.0195),
                        beta(rs_id='rs13379043', effect_allele='T', beta_value=0.0144),
                        beta(rs_id='rs28929474', effect_allele='T', beta_value=0.0713),
                        beta(rs_id='rs3812945', effect_allele='T', beta_value=-0.015),
                        beta(rs_id='rs35259348', effect_allele='C', beta_value=0.0197),
                        beta(rs_id='rs7197453', effect_allele='C', beta_value=0.0251),
                        beta(rs_id='rs217181', effect_allele='T', beta_value=-0.0494),
                        beta(rs_id='rs9302635', effect_allele='T', beta_value=0.055),
                        beta(rs_id='rs28555129', effect_allele='A', beta_value=0.0134),
                        beta(rs_id='rs6502640', effect_allele='A', beta_value=0.0195),
                        beta(rs_id='rs704', effect_allele='A', beta_value=0.0201),
                        beta(rs_id='rs12601110', effect_allele='A', beta_value=0.0272),
                        beta(rs_id='rs1487971', effect_allele='T', beta_value=-0.0157),
                        beta(rs_id='rs11080150', effect_allele='A', beta_value=0.0148),
                        beta(rs_id='rs72836561', effect_allele='T', beta_value=-0.0373),
                        beta(rs_id='rs4968318', effect_allele='A', beta_value=0.022),
                        beta(rs_id='rs118004742', effect_allele='T', beta_value=0.0291),
                        beta(rs_id='rs8069974', effect_allele='C', beta_value=0.0148),
                        beta(rs_id='rs12939848', effect_allele='T', beta_value=-0.014),
                        beta(rs_id='rs12602912', effect_allele='T', beta_value=0.0156),
                        beta(rs_id='rs77542162', effect_allele='A', beta_value=-0.1767),
                        beta(rs_id='rs4968839', effect_allele='T', beta_value=0.0387),
                        beta(rs_id='rs72852601', effect_allele='T', beta_value=0.0481),
                        beta(rs_id='rs2886232', effect_allele='T', beta_value=0.0409),
                        beta(rs_id='rs314253', effect_allele='T', beta_value=0.0203),
                        beta(rs_id='rs4485425', effect_allele='A', beta_value=-0.0189),
                        beta(rs_id='rs4129767', effect_allele='A', beta_value=0.0157),
                        beta(rs_id='rs871841', effect_allele='T', beta_value=-0.0146),
                        beta(rs_id='rs77960347', effect_allele='A', beta_value=-0.0797),
                        beta(rs_id='rs7241918', effect_allele='T', beta_value=0.0191),
                        beta(rs_id='rs1982074', effect_allele='A', beta_value=0.0229),
                        beta(rs_id='rs892010', effect_allele='C', beta_value=0.0395),
                        beta(rs_id='rs10417443', effect_allele='C', beta_value=0.0325),
                        beta(rs_id='rs1122608', effect_allele='T', beta_value=-0.0479),
                        beta(rs_id='rs4300767', effect_allele='A', beta_value=-0.0947),
                        beta(rs_id='rs6511721', effect_allele='A', beta_value=-0.0371),
                        beta(rs_id='rs73015030', effect_allele='A', beta_value=-0.0788),
                        beta(rs_id='rs3745677', effect_allele='A', beta_value=0.0774),
                        beta(rs_id='rs11669576', effect_allele='A', beta_value=0.0636),
                        beta(rs_id='rs45508991', effect_allele='T', beta_value=0.1064),
                        beta(rs_id='rs5927', effect_allele='A', beta_value=-0.0299),
                        beta(rs_id='rs2569538', effect_allele='A', beta_value=-0.0606),
                        beta(rs_id='rs892115', effect_allele='T', beta_value=-0.0232),
                        beta(rs_id='rs6511727', effect_allele='T', beta_value=0.0202),
                        beta(rs_id='rs4804579', effect_allele='T', beta_value=0.0244),
                        beta(rs_id='rs58542926', effect_allele='T', beta_value=-0.0978),
                        beta(rs_id='rs941408', effect_allele='T', beta_value=0.0163),
                        beta(rs_id='rs150090162', effect_allele='A', beta_value=0.1614),
                        beta(rs_id='rs8103315', effect_allele='A', beta_value=0.0277),
                        beta(rs_id='rs35106910', effect_allele='A', beta_value=0.0317),
                        beta(rs_id='rs1135062', effect_allele='A', beta_value=-0.0169),
                        beta(rs_id='rs3852856', effect_allele='A', beta_value=0.0612),
                        beta(rs_id='rs12610605', effect_allele='A', beta_value=0.1262),
                        beta(rs_id='rs8104483', effect_allele='T', beta_value=-0.0874),
                        beta(rs_id='rs6859', effect_allele='A', beta_value=0.0783),
                        beta(rs_id='rs11669338', effect_allele='T', beta_value=0.0501),
                        beta(rs_id='rs3852861', effect_allele='T', beta_value=-0.0563),
                        beta(rs_id='rs187706273', effect_allele='A', beta_value=-0.1081),
                        beta(rs_id='rs157580', effect_allele='A', beta_value=0.0754),
                        beta(rs_id='rs157582', effect_allele='T', beta_value=-0.0324),
                        beta(rs_id='rs115881343', effect_allele='T', beta_value=0.1945),
                        beta(rs_id='rs10119', effect_allele='A', beta_value=0.0668),
                        beta(rs_id='rs405509', effect_allele='T', beta_value=0.1727),
                        beta(rs_id='rs769450', effect_allele='A', beta_value=0.0605),
                        beta(rs_id='rs769452', effect_allele='T', beta_value=-0.1493),
                        beta(rs_id='rs439401', effect_allele='T', beta_value=0.0292),
                        beta(rs_id='rs59325138', effect_allele='T', beta_value=0.0982),
                        beta(rs_id='rs732841', effect_allele='A', beta_value=0.0413),
                        beta(rs_id='rs17651629', effect_allele='T', beta_value=-0.0317),
                        beta(rs_id='rs492602', effect_allele='A', beta_value=-0.0279),
                        beta(rs_id='rs35350976', effect_allele='A', beta_value=-0.0168),
                        beta(rs_id='rs10185855', effect_allele='A', beta_value=0.0138),
                        beta(rs_id='rs10490626', effect_allele='A', beta_value=-0.0447),
                        beta(rs_id='rs1808458', effect_allele='T', beta_value=0.031),
                        beta(rs_id='rs6706968', effect_allele='A', beta_value=-0.0228),
                        beta(rs_id='rs2198562', effect_allele='C', beta_value=0.0353),
                        beta(rs_id='rs2287623', effect_allele='A', beta_value=-0.0181),
                        beta(rs_id='rs6435161', effect_allele='T', beta_value=0.0236),
                        beta(rs_id='rs1473886', effect_allele='T', beta_value=-0.0146),
                        beta(rs_id='rs1048013', effect_allele='T', beta_value=0.0123),
                        beta(rs_id='rs12710745', effect_allele='A', beta_value=0.02),
                        beta(rs_id='rs6547409', effect_allele='T', beta_value=-0.0953),
                        beta(rs_id='rs1801702', effect_allele='C', beta_value=0.0969),
                        beta(rs_id='rs1042023', effect_allele='C', beta_value=0.0864),
                        beta(rs_id='rs12713843', effect_allele='T', beta_value=-0.1881),
                        beta(rs_id='rs12713844', effect_allele='C', beta_value=0.0741),
                        beta(rs_id='rs679899', effect_allele='A', beta_value=-0.0295),
                        beta(rs_id='rs515135', effect_allele='T', beta_value=-0.0882),
                        beta(rs_id='rs62122515', effect_allele='A', beta_value=0.0258),
                        beta(rs_id='rs4635554', effect_allele='T', beta_value=-0.0221),
                        beta(rs_id='rs887829', effect_allele='T', beta_value=-0.0205),
                        beta(rs_id='rs1260327', effect_allele='A', beta_value=0.0148),
                        beta(rs_id='rs814295', effect_allele='A', beta_value=0.0305),
                        beta(rs_id='rs11556157', effect_allele='A', beta_value=-0.0202),
                        beta(rs_id='rs72796748', effect_allele='T', beta_value=0.0588),
                        beta(rs_id='rs4077440', effect_allele='T', beta_value=0.1167),
                        beta(rs_id='rs6718187', effect_allele='A', beta_value=-0.0697),
                        beta(rs_id='rs4148218', effect_allele='A', beta_value=-0.0598),
                        beta(rs_id='rs11125936', effect_allele='T', beta_value=0.0245),
                        beta(rs_id='rs2143544', effect_allele='T', beta_value=-0.0165),
                        beta(rs_id='rs2745865', effect_allele='T', beta_value=0.0387),
                        beta(rs_id='rs6058302', effect_allele='T', beta_value=-0.0271),
                        beta(rs_id='rs6016373', effect_allele='A', beta_value=0.0227),
                        beta(rs_id='rs926663', effect_allele='A', beta_value=0.0144),
                        beta(rs_id='rs6072328', effect_allele='T', beta_value=-0.0264),
                        beta(rs_id='rs6062343', effect_allele='A', beta_value=-0.0146),
                        beta(rs_id='rs2833487', effect_allele='A', beta_value=-0.035),
                        beta(rs_id='rs2183573', effect_allele='A', beta_value=-0.0144),
                        beta(rs_id='rs138777', effect_allele='A', beta_value=0.0125),
                        beta(rs_id='rs738409', effect_allele='C', beta_value=0.0148),
                        beta(rs_id='rs13268', effect_allele='A', beta_value=0.042),
                        beta(rs_id='rs7616006', effect_allele='A', beta_value=0.0222),
                        beta(rs_id='rs1979848', effect_allele='A', beta_value=0.0257),
                        beta(rs_id='rs10513551', effect_allele='T', beta_value=-0.0154),
                        beta(rs_id='rs7640978', effect_allele='T', beta_value=-0.0321),
                        beta(rs_id='rs2251219', effect_allele='T', beta_value=-0.0137),
                        beta(rs_id='rs13315871', effect_allele='A', beta_value=-0.0323),
                        beta(rs_id='rs3816873', effect_allele='T', beta_value=0.0135),
                        beta(rs_id='rs13107325', effect_allele='T', beta_value=-0.0292),
                        beta(rs_id='rs3748034', effect_allele='T', beta_value=0.0179),
                        beta(rs_id='rs4530754', effect_allele='A', beta_value=0.0161),
                        beta(rs_id='rs10065787', effect_allele='T', beta_value=-0.0189),
                        beta(rs_id='rs2522056', effect_allele='A', beta_value=-0.0182),
                        beta(rs_id='rs4704825', effect_allele='A', beta_value=-0.0279),
                        beta(rs_id='rs870992', effect_allele='A', beta_value=-0.0268),
                        beta(rs_id='rs10062361', effect_allele='T', beta_value=0.0245),
                        beta(rs_id='rs3846662', effect_allele='A', beta_value=-0.0565),
                        beta(rs_id='rs11955819', effect_allele='A', beta_value=-0.0583),
                        beta(rs_id='rs17789218', effect_allele='T', beta_value=0.022),
                        beta(rs_id='rs9390698', effect_allele='A', beta_value=0.0129),
                        beta(rs_id='rs3798236', effect_allele='T', beta_value=0.0184),
                        beta(rs_id='rs9376090', effect_allele='T', beta_value=0.0275),
                        beta(rs_id='rs1044418', effect_allele='T', beta_value=0.018),
                        beta(rs_id='rs12208357', effect_allele='T', beta_value=0.0616),
                        beta(rs_id='rs34130495', effect_allele='A', beta_value=0.0478),
                        beta(rs_id='rs62440901', effect_allele='T', beta_value=0.0358),
                        beta(rs_id='rs3798220', effect_allele='T', beta_value=-0.1363),
                        beta(rs_id='rs10455872', effect_allele='A', beta_value=-0.0876),
                        beta(rs_id='rs12175867', effect_allele='T', beta_value=0.0255),
                        beta(rs_id='rs1652507', effect_allele='T', beta_value=0.027),
                        beta(rs_id='rs2235215', effect_allele='T', beta_value=0.0246),
                        beta(rs_id='rs1800562', effect_allele='A', beta_value=-0.0492),
                        beta(rs_id='rs129128', effect_allele='T', beta_value=0.0208),
                        beta(rs_id='rs2249741', effect_allele='A', beta_value=-0.0203),
                        beta(rs_id='rs13192471', effect_allele='T', beta_value=-0.0336),
                        beta(rs_id='rs3800406', effect_allele='A', beta_value=0.0278),
                        beta(rs_id='rs1129187', effect_allele='T', beta_value=-0.0123),
                        beta(rs_id='rs2239619', effect_allele='A', beta_value=0.0159),
                        beta(rs_id='rs10263252', effect_allele='A', beta_value=-0.023),
                        beta(rs_id='rs1997243', effect_allele='A', beta_value=-0.0164),
                        beta(rs_id='rs2282889', effect_allele='A', beta_value=-0.0164),
                        beta(rs_id='rs12670798', effect_allele='T', beta_value=-0.0295),
                        beta(rs_id='rs144787122', effect_allele='A', beta_value=-0.1042),
                        beta(rs_id='rs4722551', effect_allele='T', beta_value=-0.0386),
                        beta(rs_id='rs2391211', effect_allele='T', beta_value=0.0231),
                        beta(rs_id='rs4302748', effect_allele='A', beta_value=0.0152),
                        beta(rs_id='rs35803101', effect_allele='A', beta_value=-0.1387),
                        beta(rs_id='rs10260606', effect_allele='C', beta_value=0.0369),
                        beta(rs_id='rs1014283', effect_allele='A', beta_value=-0.0186),
                        beta(rs_id='rs2737245', effect_allele='T', beta_value=-0.0179),
                        beta(rs_id='rs2954029', effect_allele='A', beta_value=0.0242),
                        beta(rs_id='rs4870941', effect_allele='C', beta_value=0.0439),
                        beta(rs_id='rs2954038', effect_allele='A', beta_value=-0.0368),
                        beta(rs_id='rs4921914', effect_allele='T', beta_value=-0.0181),
                        beta(rs_id='rs9298506', effect_allele='A', beta_value=-0.0217),
                        beta(rs_id='rs2081687', effect_allele='T', beta_value=0.0268),
                        beta(rs_id='rs330093', effect_allele='C', beta_value=0.0295),
                        beta(rs_id='rs11774381', effect_allele='T', beta_value=-0.0286),
                        beta(rs_id='rs11782386', effect_allele='T', beta_value=-0.0246),
                        beta(rs_id='rs3905000', effect_allele='A', beta_value=-0.0179),
                        beta(rs_id='rs3812594', effect_allele='A', beta_value=-0.0139),
                        beta(rs_id='rs67710536', effect_allele='A', beta_value=-0.0267),
                        beta(rs_id='rs10757272', effect_allele='T', beta_value=-0.0195),
                        beta(rs_id='rs3780181', effect_allele='A', beta_value=0.0328)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'LDL Cholesterol'

about = ('Cholesterol is an essential component of cell membranes within the body. It '
         'is made up of low-density lipoprotein (LDL or "bad" cholesterol) and '
         'high-density lipoprotein (HDL or "good" cholesterol). Diet is a source of '
         'cholesterol, but the body also makes it. A high LDL cholesterol levels can '
         'cause a risk for heart disease and other health issues, especially if your '
         'HDL cholesterol level is low. Several factors contribute to overall '
         'cholesterol levels, including lifestyle and genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 219 genetic variants, specifically called '
                                  'SNPs, found in genes associated with blood LDL level. Overall, this explains '
                                  'about 10% of the genetic variation in LDL cholesterol level in the human '
                                  'population, based on current scientific knowledge.')

other_factors_statement = ('Many things impact cholesterol levels, including diet, lifestyle, and '
                           'genetics. Several genes are associated with our LDL cholesterol level.')

other_factors = [{'content': 'Diet can affect cholesterol levels, including our LDL '
                             'cholesterol level. While saturated fats, like those in full-fat '
                             'dairy products or red meat, can increase LDL cholesterol levels '
                             'in some people, the more dangerous LDLs come from an abunance of '
                             'carbohydrates. Some studies show that whey protein supplements '
                             'can lower total cholesterol levels, including LDL cholesterol.',
                  'title': 'Diet'},
                 {'content': 'Lifestyle can impact the effect of high cholesterol levels. '
                             'Smoking and lack of physical activity increase the risk of heart '
                             'disease and may lead to being overweight. These are important '
                             'risk factors to address if overall cholesterol levels are also '
                             'high.',
                  'title': 'Lifestyle'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have problems with cholesterol.  Some of these '
                             'types of risk factors, which are usually due to rare genetic '
                             'variants, may not be included in this test. If you have a family '
                             'history of cholesterol problems, talk to your '
                             'Practitioner.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your cholesterol levels and other factors, you '
                                           'may have an average risk for heart disease and other health '
                                           'issues.',
                           'High risk': 'Depending on your cholesterol levels and other factors, you may '
                                        'have a higher risk for heart disease and other health issues.',
                           'Low risk': 'Depending on your cholesterol levels and other factors, you may '
                                       'have a lower risk for heart disease and other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average level of LDL blood cholesterol.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher level of LDL blood cholesterol.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a normal levels of LDL blood cholesterol.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent high '
                                             'cholesterol and stay healthy. Certain blood tests can also '
                                             'check your cholesterol levels. Talk to your practitioner '
                                             'about what is right for you.',
                             'High risk': 'There are things you can do to try to prevent high cholesterol '
                                          'and stay healthy. Certain blood tests can also check your '
                                          'cholesterol levels. Talk to your practitioner about what is '
                                          'right for you.',
                             'Low risk': 'There are things you can do to try to prevent high cholesterol '
                                         'and stay healthy. Certain blood tests can also check your '
                                         'cholesterol levels. Talk to your practitioner about what is '
                                         'right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for high LDL cholesterol. Consider talking '
                                                'to your practitioner about blood tests to check '
                                                'your cholesterol levels, and possible next '
                                                'steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Try to pick foods that are higher in '
                                                'monousaturated fat, and lower in sodium, sugar, '
                                                'saturated fat, trans fat and vegetable oils. '
                                                'Helpful foods to manage LDL include fish, Omega '
                                                '3 plant foods, such as flax and chia seeds. '
                                                'Also adding soluble fiber from brussels sprouts '
                                                'and low-glycemic fruits such as blueberries, '
                                                'blackberries, rasberries, and green apples. '
                                                'Eating foods that are higher in fiber, like '
                                                'oatmeal, whole grains, and beans, can help '
                                                'lower LDL cholesterol.',
                                     'title': 'Diet'},
                                    {'content': 'If you smoke, try to quit -- smoking increases '
                                                'the risk for heart disease. As well, try to get '
                                                "your body moving. It's recommended that adults "
                                                'get 2.5 hours of moderate physical activity, '
                                                'like brisk walking or biking, every week. Not '
                                                'only can this lower your risk of heart disease, '
                                                'but it can also help you maintain a healthy '
                                                'weight.',
                                     'title': 'Lifestyle'},
                                    {'content': 'High cholesterol can run in families, and these '
                                                'risk factors are not included in this test. If '
                                                'you have a close relative (parent, child, '
                                                'sibling) with high cholesterol, speak to your '
                                                'practitioner about your family history. If you '
                                                'have blood tests to check your cholesterol '
                                                'levels, information about your family history '
                                                'will help with understanding your blood test '
                                                'results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your cholesterol levels. Talk to them '
                                             'about this and possible next steps that you can '
                                             'take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Pick foods that are higher in monousaturated fat, '
                                             'and lower in sodium, sugar, and saturated fat.  '
                                             'Eliminate trans fat and vegetable oils.  Helpful '
                                             'foods to manage LDL include fish, Omega 3 plant '
                                             'foods, such as flax and chia seeds.  Also adding '
                                             'soluble fiber from brussels sprouts and '
                                             'low-glycemic fruits such as blueberries, '
                                             'blackberries, rasberries, and green apples.  '
                                             'Eating foods that are higher in fiber, like '
                                             'oatmeal, whole grains, and beans, can help lower '
                                             'LDL cholesterol.',
                                  'title': 'Diet'},
                                 {'content': 'If you smoke, now is a good reason to quit -- '
                                             'smoking increases the risk for heart disease. As '
                                             "well, try to get your body moving. It's "
                                             'recommended that adults get 2.5 hours of moderate '
                                             'physical activity, like brisk walking or biking, '
                                             'every week. Not only can this lower your risk of '
                                             'heart disease, but it can also help you maintain a '
                                             'healthy weight.',
                                  'title': 'Lifestyle'},
                                 {'content': 'High cholesterol can run in families, and these '
                                             'risk factors are not included in this test. If you '
                                             'have a close relative (parent, child, sibling) '
                                             'with high cholesterol, speak to your practitioner '
                                             'about your family history. If you have blood tests '
                                             'to check your cholesterol levels, information '
                                             'about your family history will help with '
                                             'understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for high LDL cholesterol. Consider talking to your '
                                            'practitioner about blood tests to check your '
                                            'cholesterol levels, and possible next steps you can '
                                            'take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Try to pick foods that are higher in monousaturated '
                                            'fat, and lower in sodium, sugar, saturated fat, '
                                            'trans fat and vegetable oils. Helpful foods to '
                                            'manage LDL include fish, Omega 3 plant foods, such '
                                            'as flax and chia seeds. Also adding soluble fiber '
                                            'from brussels sprouts and low-glycemic fruits such '
                                            'as blueberries, blackberries, rasberries, and green '
                                            'apples. Eating foods that are higher in fiber, like '
                                            'oatmeal, whole grains, and beans, can help lower '
                                            'LDL cholesterol.',
                                 'title': 'Diet'},
                                {'content': 'If you smoke, try to quit -- smoking increases the '
                                            'risk for heart disease. As well, try to get your '
                                            "body moving. It's recommended that adults get 2.5 "
                                            'hours of moderate physical activity, like brisk '
                                            'walking or biking, every week. Not only can this '
                                            'lower your risk of heart disease, but it can also '
                                            'help you maintain a healthy weight.',
                                 'title': 'Lifestyle'},
                                {'content': 'High cholesterol can run in families, and these '
                                            'risk factors are not included in this test. If you '
                                            'have a close relative (parent, child, sibling) with '
                                            'high cholesterol, speak to your practitioner about '
                                            'your family history. If you have blood tests to '
                                            'check your cholesterol levels, information about '
                                            'your family history will help with understanding '
                                            'your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 14, 2019',
                                     'links': 'https://www.cdc.gov/cholesterol/ldl_hdl.htm1',
                                     'title': '"LDL and HDL Cholesterol: "Bad" and "Good" '
                                              'Cholesterol" from Centers for Disease '
                                              'Control and Prevention'},
                                    {'accessed': 'Accessed June 14, 2019',
                                     'links': 'https://www.mayoclinic.org/diseases-conditions/high-blood-cholesterol/in-depth/reduce-cholesterol/art-20045935',
                                     'title': '"Top 5 lifestyle changes to improve your '
                                              'cholesterol" From Mayo Clinic'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood concentration of low-density lipoprotein cholesterol"
trait_science = "Blood level of low-density lipoprotein cholesterol is moderately heritable - about 30% of differences observed among people has a genetic origin. Our analysis was based on 219 SNPs associated with LDL cholesterol. These variants jointly explain about 10% of the variation in LDL cholesterol level in human population."
trait_test_details = "We determined your genotype at all the genomic positions (SNPS), which are known to influence the LDL cholesterol blood level. Such positions are scattered across 172 genes. We summed the effects of all the individual SNPs and in this way calculated your polygenic risk score. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
