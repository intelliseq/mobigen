from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Alcohol_dependence"
trait_pmids = ["24166409", "22581016"]  # 24166409 table 2: EA only, p-meta for EA<5e-08, beta=aveg(beta[GCD],beta[SAGE])
trait_heritability = 0.55  # 0.5-0.6
trait_snp_heritability = None
trait_explained = None
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"

genes = ["ADH1B", "DARS-AS1", "DPP9-AS1"]
science_behind_the_test = "Alcohol-use disorders are moderately heritable, with heritability estimate of about 55% [1]. Our test is based on results of the genome wide association studies (GWAS) [2], which included over 10 000 individuals of European ancestry. The study identified 3 independent variants significantly associated with alcohol dependance in European population [2].\nOur analyses use genetic data (genotypes) for those SNPs to calculate the polygenic score."
 
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.025, category_name='Low risk'),
                        QuantitativeCategory(from_=0.025, to=0.076, category_name='Average risk'),
                        QuantitativeCategory(from_=0.076, category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs925966', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs1229984', effect_allele='T', beta_value=-0.02),
                        beta(rs_id='rs59514816', effect_allele='G', beta_value=0.025)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Alcohol Dependence'

about = ('Alcohol dependence, sometimes called alcoholism, is a serious form of '
         'substance dependence. People with alcohol dependence have a greater desire '
         'to drink alcohol and may feel they need alcohol as a regular part of their '
         'lives. Alcohol dependence can lead someone to regularly consume very high '
         'amounts of alcohol -- such as 1 drink a day for adult women, or up to 2 '
         'drinks a day for adult men. Alcohol dependence can lead to risky behaviors, '
         'lost workdays, lack of sleep, injuries, difficult personal relationships, '
         'mental health issues, and health problems (such as liver disease, high blood '
         "pressure, heart disease, and others). Several things impact a person's "
         'tendency for alcohol dependence, including genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 3 genetic variants, specifically called SNPs, '
                                  'associated with alcohol dependence.')

other_factors_statement = ("Many factors impact a person's tendency for alcohol dependence, including "
                           'diet, lifestyle, environment, some mental health issues, and genetics. '
                           'Genetics has a moderate overall impact on alcohol dependence when you '
                           'consider all the other factors.')

other_factors = [{'content': 'Through testing, your doctor can determine which diet such as a '
                             'Low-carb keto style plan or a Mediterranean plan is right for '
                             'you.',
                  'title': 'Lifestyle'},
                 {'content': 'Having anxiety, depression, schizophrenia, and/or bipolar '
                             'disorder can contribute to a tendency for alcohol dependence. '
                             'Being through emotional or other forms of trauma can also be '
                             'contributing factors.',
                  'title': 'Other Health Issues'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have alcohol dependence. These types of risk '
                             'factors, which are usually due to rare genetic variants, may not '
                             'included in this test. If you have a family history of alcohol '
                             'dependence, talk to your Practitioner or a genetic '
                             'counselor.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your alcohol dependence tendency and other '
                                           'factors, if you drink excessively you may have an average '
                                           'risk for liver disease, high blood pressure, heart disease, '
                                           'or other health issues.',
                           'High risk': 'Depending on your alcohol dependence tendency and other '
                                        'factors, if you drink excessively you may have a higher risk '
                                        'for liver disease, high blood pressure, heart disease, or other '
                                        'health issues.',
                           'Low risk': 'Depending on your alcohol dependence tendency and other factors, '
                                       'if you drink excessively you may have a lower risk for liver '
                                       'disease, high blood pressure, heart disease, or other health '
                                       'issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average tendency for alcohol dependence.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have an average tendency for alcohol dependence.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower tendency for alcohol dependence.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to counteract a tendency '
                                             'for alcohol dependence and stay healthy. Certain blood tests '
                                             'can also check your risk factors for liver disease, heart '
                                             'disease, and other health issues. Talk to your practitioner '
                                             'about what is right for you.',
                             'High risk': 'There are things you can do to try to counteract a tendency for '
                                          'alcohol dependence and stay healthy. Certain blood tests can '
                                          'also check your risk factors for liver disease, heart disease, '
                                          'and other health issues. Talk to your practitioner about what '
                                          'is right for you.',
                             'Low risk': 'There are things you can do to try to counteract a tendency for '
                                         'alcohol dependence and stay healthy. Certain blood tests can '
                                         'also check your risk factors for liver disease, heart disease, '
                                         'and other health issues. Talk to your practitioner about what is '
                                         'right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'tendency for alcohol dependence. If you think '
                                                'you may have a problem with alcohol, consider '
                                                'talking to your practitioner about ordering '
                                                'tests to check your risk factors for health '
                                                'issues related to alcohol dependence tendency, '
                                                'and possible next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Try to avoid binge drinking (4 or more drinks '
                                                'at one instance for women, 5 or more drinks at '
                                                'one instance for men) or heavy drinking (8 or '
                                                'more drinks per week for women, 15 or more '
                                                'drinks per week for men). These are risk '
                                                'factors for alcohol dependence if done '
                                                'regularly.',
                                     'title': 'Lifestyle'},
                                    {'content': 'Alcohol dependence can run in families, and '
                                                'these risk factors may not be included in this '
                                                'test. If you have a close relative (parent, '
                                                'child, sibling) with alcohol dependence, speak '
                                                'to your practitioner about your family history. '
                                                'If you have blood tests to look for health risk '
                                                'factors, information about your family history '
                                                'will help with understanding your blood test '
                                                'results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your risk factors for health issues related '
                                             'to alcohol dependence, like liver disease, heart '
                                             'disease or high blood pressure. They may also be '
                                             'able to guide you to support with reducing your '
                                             'alcohol intake or with mental health issues, if '
                                             'you need it. Talk to them about this and possible '
                                             'next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Avoid binge drinking (4 or more drinks at one '
                                             'instance for women, 5 or more drinks at one '
                                             'instance for men) or heavy drinking (8 or more '
                                             'drinks per week for women, 15 or more drinks per '
                                             'week for men). These are risk factors for alcohol '
                                             'dependence if done regularly. Speak to your '
                                             'practitioner if you need help on ways to cut back '
                                             'on your alcohol intake.',
                                  'title': 'Lifestyle'},
                                 {'content': 'If you have anxiety, depression, schizophrenia, '
                                             'bipolar disorder, and/or have been through '
                                             'emotional or other trauma, all of this can '
                                             'contribute to a tendency for alcohol dependence. '
                                             'Speak to your practitioner if you need support and '
                                             'about how this factors into your alcohol '
                                             'dependence tendency.',
                                  'title': 'Other Health Issues'},
                                 {'content': 'Alcohol dependence can run in families, and these '
                                             'risk factors may not be included in this test. If '
                                             'you have a close relative (parent, child, sibling) '
                                             'with alcohol dependence, speak to your '
                                             'practitioner about your family history. If you '
                                             'have blood tests to look for health risk factors, '
                                             'information about your family history will help '
                                             'with understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your '
                                            'tendency for alcohol dependence. If you think you '
                                            'may have a problem with alcohol, consider talking '
                                            'to your practitioner about ordering tests to check '
                                            'your risk factors for health issues related to '
                                            'alcohol dependence tendency, and possible next '
                                            'steps that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Try to avoid binge drinking (4 or more drinks at '
                                            'one instance for women, 5 or more drinks at one '
                                            'instance for men) or heavy drinking (8 or more '
                                            'drinks per week for women, 15 or more drinks per '
                                            'week for men). These are risk factors for alcohol '
                                            'dependence if done regularly.',
                                 'title': 'Lifestyle'},
                                {'content': 'Alcohol dependence can run in families, and these '
                                            'risk factors may not be included in this test. If '
                                            'you have a close relative (parent, child, sibling) '
                                            'with alcohol dependence, speak to your practitioner '
                                            'about your family history. If you have blood tests '
                                            'to look for health risk factors, information about '
                                            'your family history will help with understanding '
                                            'your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [{'link': 'https://www.ncbi.nlm.nih.gov/pubmed/24166409'}],
              'website_text_data': [{'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.cdc.gov/alcohol/fact-sheets/alcohol-use.htm',
                                     'title': '"Fact Sheets - Alcohol Use and Your Health" '
                                              'from Centers for Disease Control and '
                                              'Prevention'},
                                    {'accessed': 'Accessed July 1, 2019. \n',
                                     'links': 'https://www.drinkaware.co.uk/alcohol-facts/drinking-habits-and-behaviours/am-i-alcohol-dependent/',
                                     'title': '"Am I alcohol dependent?" from Drinkaware'},
                                    {'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.mayoclinic.org/diseases-conditions/alcohol-use-disorder/symptoms-causes/syc-20369243',
                                     'title': '"Alcohol use disorder" from Mayo Clinic'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Predisposition toward alcohol dependence"
trait_science = "Alcohol-use disorders are moderately heritable. Our analysis was based on 3 common genetic variants associated with alcohol dependence."
trait_test_details = "We determined your genotype at all the genomic positions, which are associated with alcohol dependence risk. The summed effects of all the individual SNPs were used to calculate your polygenic risk score."
