from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

# POLA DLA ISEQ I NOTATKI (SKAD DOKLADNIE DANE, CO USUNELAM, LICZEBNOSC KOHORT) I TAKIE TAM
trait_title = "Blood_oxidative_enzymes_level"  # Superoxide dismutase [Mn] (SOD2), mitochondrial and Catalase (CAT)
trait_pmids = ["28240269"]
trait_heritability = None # heritability of analyzed protein not given, but according to PMID:25652787 the mean heritability of plazma protein concentration is 13.6 (estimatd for sample of 342 proteins)
trait_snp_heritability = None
trait_explained = None  # up to 60% of the variance in the blood plasma levels of essential proteins can be explained by two or more independent variants of a single gene located on another chromosome’
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip, nie uzupelnione przez Intelliger
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "We used result of the genome wide association study (GWAS), in which associations between genotype and concentration of 1,124 plasma proteins were analyzed. This stydy included 1000 individuals of European ancestry [1]. From this dataset we choose three variants, which were significantly associated with blood concentration of the oxidative enzymes: superoxide dismutase [MN] (SOD2) and catalase (CAT). The average heritability of plasma protein concentration is about 14% [2].\nOur analyses use genetic data (genotypes) for those three SNPs to calculate the polygenic score."
genes = ['GRK6', 'CELSR2', 'CAT']

# This study quantifies levels of 1,124 proteins in blood plasma samples of 1000 individuals of European ancestry (KORA) and 338 individuals from Quatar (QMDiab)
# GWAS statistics from Suppl. Data1 (values obtained for KORA)
# Effect allele == minor allele (confirmed in GWAS catalog)
# As Levels of these two enzymes are  independent traits and I do not know which enzyme is more important for the oxidative status I re-calibrated the beta coefficients:
# 1) absolute value of beta for the snp associated with Sod2 level = 1
# 2) sum of absolute values of two variants associated with Cat level equals 1
# P-values are not corrected for multiple testing => but the variants used here remain significant even after Bonferoni correction applied (treshold value: P<8.72 e−11, p-value inv)
# low_level category: about 16% of population; high_level category: about 18% of population;

# MODEL
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-2.001, category_name='High risk'),
                        QuantitativeCategory(from_=-2.001, to=-0.001, category_name='Average risk'),
                        QuantitativeCategory(from_=-0.001, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs2545801', effect_allele='T', beta_value=-1),
                        beta(rs_id='rs646776', effect_allele='C', beta_value=-0.397),
                        beta(rs_id='rs2284367', effect_allele='C', beta_value=-0.603)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Oxidative Stress'

about = ('Oxidative stress comes from the presence of free radicals surrounding us in '
         'our lives from areas like stress, toxic and chemical exposures, electronic '
         'frequencies, and even normal metabolic processes.  These free radicals are '
         'implicated in many chronic diseases and common, painful conditions.')

how_your_result_was_calculated = ('This trait is moderately heritable and about 45% of variation in '
                                  'homocysteine level is caused by genetic factors.')

other_factors_statement = ('Free radicals are highly reactive with other molecules, and can damage DNA, '
                           'proteins and cell membranes.  Oxidative damage is part of ordinary life.  '
                           'Therefore, maintaining a balance between oxidation by free radicals and '
                           'protective anti-oxidation from diet and supplements such as vitamins A, C, '
                           'and E, along with other important phytonutrients is a core need for '
                           'sustaining health.')

other_factors = [{'content': 'Avoid trans-fats, vegetable, nut, and seed oils.  Consume '
                             'plant-based foods, many of which come packed with antioxidants '
                             'to naturally help protect you from the damaging effects of '
                             'oxidative stress.  Berries such as blueberries, cranberries, and '
                             'raspberries along with the super-food Goji berries, kale, '
                             'spinach, green tea, artichokes, red or purple grapes, and beans '
                             'are all strong sources of antioxidants.  Dark chocolate is '
                             'another great resource, but make it un-sweetened as the sugar '
                             'supports a negative surge in free radicals.',
                  'title': 'Diet'},
                 {'content': 'In today’s mentally and chemically stressful world, antioxidants '
                             'are quickly used up leaving people exposed.  It is important to '
                             'supplement in order to replace antioxidants like Vitamins A, C, '
                             'E, selenium, and several others along with supplementing '
                             'beneficial phytonutrients such as resveratrol, green tea '
                             'extracts, grape seed, and pine bark.',
                  'title': 'Supplements'},
                 {'content': 'Greater exposure to stress, lack of sleep, smoke, chemicals, and '
                             'electronics will increase free-radical exposure. Additionally, '
                             'minimal consumption of fruits and vegetables and a sedentary '
                             'lifestyle increase the risk of experiencing the damaging effects '
                             'of oxidative stress.  Therefore, it is important for those in '
                             'particular that have gene variants for oxidative stress to have '
                             'an adequate intake of key fruits and vegetables, find healthy '
                             'ways to stay active, and address sources of exposure.',
                  'title': 'Lifestyle'},
                 {'content': 'Genetic factors can play a key role in the body’s ability to '
                             'manage free radicals, maintain anti-oxidants, prolong aging, and '
                             'prevent disease.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your ability to manage antioxidant levels, you '
                                           'may have an average risk for  health issues such as '
                                           'inflammation, cancer, heart disease, and joint and muscle '
                                           'pain, respiratory issues, and other disorders.',
                           'High risk': 'Depending on your ability to manage antioxidant levels, you may '
                                        'have a higher risk for  health issues such as inflammation, '
                                        'cancer, heart disease, and joint and muscle pain, respiratory '
                                        'issues, and other disorders.',
                           'Low risk': 'Depending on your ability to manage antioxidant levels, you may '
                                       'have a low risk for  health issues such as inflammation, cancer, '
                                       'heart disease, and joint and muscle pain, respiratory issues, '
                                       'and other disorders.'}

result_statement = {'Average risk': 'Free radicals are oxygen-containing compounds that react '
                                    'with your body’s molecules.  Oxidative stress can occur when '
                                    'there is an imbalance between the amount of free radical '
                                    'exposure and the amount of antioxidants in your body '
                                    'necessary to protect itself.  You have an average risk of '
                                    'having oxidative stress.',
                    'High risk': 'Free radicals are oxygen-containing compounds that react with '
                                 'your body’s molecules.  Oxidative stress occurs when there is '
                                 'an imbalance between the amount of free radical exposure and '
                                 'the amount of antioxidants in your body necessary to protect '
                                 'itself.  You have a high risk of having oxidative stress.',
                    'Low risk': 'Free radicals are oxygen-containing compounds that react with '
                                'your body’s molecules.  Oxidative stress occurs when there is an '
                                'imbalance between the amount of free radical exposure and the '
                                'amount of antioxidants in your body necessary to protect '
                                'itself.  You have a low risk of having oxidative stress.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to help prevent oxidative '
                                             'stress and to stay healthy. Certain blood tests can also '
                                             'check your risk factors for health issues related to '
                                             'environmental toxins. Talk to your practitioner about what '
                                             'is right for you.',
                             'High risk': 'There are things you can do to try to help prevent oxidative '
                                          'stress and to stay healthy. Certain blood tests can also check '
                                          'your risk factors for health issues related to environmental '
                                          'toxins. Talk to your practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to help prevent oxidative '
                                         'stress and to stay healthy. Certain blood tests can also check '
                                         'your risk factors for health issues related to environmental '
                                         'toxins. Talk to your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Genetics and other factors play a role in the '
                                                'body’s ability to manage damage from '
                                                'oxidation.  There may be other tests to verify '
                                                'the impact that free radicals have had on '
                                                'health and there are many pro-active lifestyle '
                                                'steps you can take to re-build and maintain '
                                                'healthy oxidative balance.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Industrial vegetable oils such as canola, '
                                                'soybean, peanut, and safflower oils\n'
                                                'are susceptible to oxidative damage and become '
                                                'rancid fats.  They become further oxidized when '
                                                'cooking, so should be eliminated from the '
                                                'diet.  Rather, eat a diet rich in berries, '
                                                'vegetables, and power-packed anti-oxidant foods '
                                                'including; as blueberries, cranberries, and '
                                                'raspberries along with the super-food Goji '
                                                'berries, kale, spinach, green tea, artichokes, '
                                                'red or purple grapes, and dark chocolate',
                                     'title': 'Diet'},
                                    {'content': 'Vitamins A, C, E, selenium, and CoQ10 are core '
                                                'antioxidants, proven to protect the body from '
                                                'the dangerous side-effects of oxidation.  Other '
                                                'key nutrients include L-carnitine, lycopene, '
                                                'D-Ribose, grape-seed extract, resveratrol, '
                                                'green tea extracts, grape seed, pine bark, '
                                                'garlic, and ginger root.  Many of these other '
                                                'forms of antioxidant are cardio-protective '
                                                'along with helping other vital organs defend '
                                                'against free-radical damage.',
                                     'title': 'Supplements'},
                                    {'content': 'Try to limit your intake of processed foods, '
                                                'sugar, and any vegetable, nut, seed oil, or '
                                                'hydrogenated fats, alcohol, and work to '
                                                'minimize the use of medications.  Quit smoking, '
                                                'avoid second hand smoke, and reduce exposure to '
                                                'toxins found in the home, car, and office.  '
                                                'Look at reducing forms of stress, focus on the '
                                                'required amount of sleep, and participate in a '
                                                'regular exercise program. Areas that impact '
                                                'your body’s ability to function such as spinal '
                                                'subluxation, leaky gut or digestive disorders, '
                                                'and nutrient deficiencies will substantially '
                                                'impact the issue of free-radical exposure.',
                                     'title': 'Lifestyle'}],
                   'High risk': [{'content': 'Genetics and other factors play a role in the '
                                             'body’s ability to manage damage from oxidation.  '
                                             'There may be other tests to verify the impact that '
                                             'free radicals have had on health and there are '
                                             'many pro-active lifestyle steps you can take to '
                                             're-build and maintain healthy oxidative balance.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Industrial vegetable oils such as canola, soybean, '
                                             'peanut, and safflower oils\n'
                                             'are susceptible to oxidative damage and become '
                                             'rancid fats.  They become further oxidized when '
                                             'cooking, so should be eliminated from the diet.  '
                                             'Rather, eat a diet rich in berries, vegetables, '
                                             'and power-packed anti-oxidant foods including; as '
                                             'blueberries, cranberries, and raspberries along '
                                             'with the super-food Goji berries, kale, spinach, '
                                             'green tea, artichokes, red or purple grapes, and '
                                             'dark chocolate',
                                  'title': 'Diet'},
                                 {'content': 'Vitamins A, C, E, selenium, and CoQ10 are core '
                                             'antioxidants, proven to protect the body from the '
                                             'dangerous side-effects of oxidation.  Other key '
                                             'nutrients include L-carnitine, lycopene, D-Ribose, '
                                             'grape-seed extract, resveratrol, green tea '
                                             'extracts, grape seed, pine bark, garlic, and '
                                             'ginger root.  Many of these other forms of '
                                             'antioxidant are cardio-protective along with '
                                             'helping other vital organs defend against '
                                             'free-radical damage.',
                                  'title': 'Supplements'},
                                 {'content': 'Limit your intake of processed foods, sugar, and '
                                             'any vegetable, nut, seed oil, or hydrogenated '
                                             'fats, alcohol, and work to minimize the use of '
                                             'medications.  Quit smoking, avoid second hand '
                                             'smoke, and reduce exposure to toxins found in the '
                                             'home, car, and office.  Look at reducing forms of '
                                             'stress, focus on the required amount of sleep, and '
                                             'participate in a regular exercise program. Areas '
                                             'that impact your body’s ability to function such '
                                             'as spinal subluxation, leaky gut or digestive '
                                             'disorders, and nutrient deficiencies will '
                                             'substantially impact the issue of free-radical '
                                             'exposure.',
                                  'title': 'Lifestyle'}],
                   'Low risk': [{'content': 'Genetics and other factors play a role in the '
                                            'body’s ability to manage damage from oxidation.  '
                                            'There may be other tests to verify the impact that '
                                            'free radicals have had on health and there are many '
                                            'pro-active lifestyle steps you can take to re-build '
                                            'and maintain healthy oxidative balance.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Industrial vegetable oils such as canola, soybean, '
                                            'peanut, and safflower oils\n'
                                            'are susceptible to oxidative damage and become '
                                            'rancid fats.  They become further oxidized when '
                                            'cooking, so should be eliminated from the diet.  '
                                            'Rather, eat a diet rich in berries, vegetables, and '
                                            'power-packed anti-oxidant foods including; as '
                                            'blueberries, cranberries, and raspberries along '
                                            'with the super-food Goji berries, kale, spinach, '
                                            'green tea, artichokes, red or purple grapes, and '
                                            'dark chocolate',
                                 'title': 'Diet'},
                                {'content': 'Vitamins A, C, E, selenium, and CoQ10 are core '
                                            'antioxidants, proven to protect the body from the '
                                            'dangerous side-effects of oxidation.  Other key '
                                            'nutrients include L-carnitine, lycopene, D-Ribose, '
                                            'grape-seed extract, resveratrol, green tea '
                                            'extracts, grape seed, pine bark, garlic, and ginger '
                                            'root.  Many of these other forms of antioxidant are '
                                            'cardio-protective along with helping other vital '
                                            'organs defend against free-radical damage.',
                                 'title': 'Supplements'},
                                {'content': 'Try to limit your intake of processed foods, sugar, '
                                            'and any vegetable, nut, seed oil, or hydrogenated '
                                            'fats, alcohol, and work to minimize the use of '
                                            'medications.  Quit smoking, avoid second hand '
                                            'smoke, and reduce exposure to toxins found in the '
                                            'home, car, and office.  Look at reducing forms of '
                                            'stress, focus on the required amount of sleep, and '
                                            'participate in a regular exercise program. Areas '
                                            'that impact your body’s ability to function such as '
                                            'spinal subluxation, leaky gut or digestive '
                                            'disorders, and nutrient deficiencies will '
                                            'substantially impact the issue of free-radical '
                                            'exposure.',
                                 'title': 'Exercise'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = [
    {
        "trait": "Increased Oxidative Stress",
        "gene": "SOD2",
        "rsid": "rs4880",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": "15864132"
        },
        "intro": "Oxidative stress by free radicals causes cell damage; increasing susceptibility to faster aging and disease. Superoxide dismutase 2 (SOD2) is a mitochondrial enzyme involved in the elimination of free radicals and is encoded by the SOD2.",
        "risk_allele":'G',
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "More likely",
                "description": "Individuals with the AA genotype are more likely expected to have normal activity of this antioxidant enzyme. Those with the other genotypes can be more susceptible to the damaging effects of free radicals caused by oxidative stress."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Individuals with the AG genotype are more likely expected to have normal activity of this antioxidant enzyme. Those with the other genotypes can be more susceptible to the damaging effects of free radicals caused by oxidative stress."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Less likely",
                "description": "Individuals with the GG genotype are more likely expected to have decreased activity of this antioxidant enzyme. Avoid smoking, minimize exposure to toxins, better manage stress, and increase their intake of fruits and vegetables. Supplementation with antioxidant nutrients is also important."
            }
        ]
    },
    {
        "trait": "Increased Oxidative Stress",
        "gene": "NOS3",
        "rsid": "rs1799983",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "The NOS3 gene is involved in encoding nitric oxide synthase 3 (eNOS); an enzyme critical for cardiovascular system function.  NOS3 levels facilitate the production of nitric oxide (NO) which helps in the dilation, tone, and over-all function of blood vessels as well as other important metabolic processes.",
        "risk_allele":'T',
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "High Risk",
                "description": "Individuals with the TT genotype have a higher risk as they are more likely to have less active NOS3 enzyme and reduced nitric oxide (NO).  This can cause harm to the walls of the blood vessels and increase atherosclerosis risks. This could have a positive correlation with high blood pressure, kidney disease, and preeclampsia. T allele carriers should stay consistent with aerobic exercise, supplement antioxidants, omega-3 fatty acids, and increase intake of arginine foods. Taking L-theonine and CBD for calming effect can also be beneficial."
            },
            {
                "genotype": [
                    "G",
                    "T"
                ],
                "gene_impact_string": "Moderate Risk",
                "description": "Individuals with the TG genotype have a somewhat more likely risk to having less active NOS3 enzyme and reduced nitric oxide (NO).  This can cause harm to the walls of the blood vessels and increase atherosclerosis risks. This could have a positive correlation with high blood pressure, kidney disease, and preeclampsia. T allele carriers should stay consistent with aerobic exercise, supplement antioxidants, omega-3 fatty acids, and increase intake of arginine foods. Taking L-theonine and CBD for calming effect can also be beneficial."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Low Risk",
                "description": "Individuals with the GG genotype are less likely to have under-active NOS3 enzyme so are not at risk of reduced nitric oxide (NO)."
            }
        ]
    }
]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_genotypes = ["hom", "het", "hom"]  # for one snip analysis
trait_short_description = ""
trait_test_details = ""  # teraztwpisane tu jest zdanie o liczeniu polygenic risk score, tylko dla cech z liczonym prs
trait_test_details = "nie ma odpowiedniego pola w pliku od intelliger, teraz zdanie o liczeniu polygenic risk score, tu john chcialby miec opis 'actionable snps/genes'."  # tylko dla cech z liczonym prs
