from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Blood_pressure"
trait_pmids = ["27618448", "17278972"]  # 27618448 Suppl.Table3 and Suppl.Table4
trait_heritability = 0.3  # 17278972: 0.24 -0.37 for blood pressure related traits
trait_snp_heritability = None  # as fraction
trait_explained = 0.03  # all known + novel (27618448) loci - not all included here, see notes below
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Blood pressure related traits are moderately heritable, with h"+chr(0x00b2)+" estimates of 22-37% [1]. Our test is based on results of the genome wide association study (GWAS) [2], which included over 300 000 individuals, mainly of European ancestry. The study identified 36 variants significantly associated with blood pressure in European population. Together these SNPs explain about 3% of between people variance in blood pressure[2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["CASZ1", "MTHFR", "AGT", "PRPS1P1", "RPS20P15", "AC074033.1", "FGF5", "SLC39A8", "NPR3", "HFE", "HOXA4",
         "NOS3", "AL450384.2", "CABCOCO1", "AS3MT", "ADRB1", "TNNT3", "RN7SKP90", "KCNJ11", "PCNX3", "ATP2B1",
         "RF00066", "AC009804.1", "CPLX3", "FURIN", "TBX2", "RGL3", "AL050403.2", "ZNF831", "PRDM16", "SULT1C3",
         "AC116366.2", "PHIP", "MSH5-SAPCD1", "CRIP3", "AC073115.2", "HRCT1", "ADO", "CERS5", "DPEP1", "INSR", "ZNRF3"]

# Discovery phase: I used this data to cover (at least to some extent) previously published blood pressureloci
# Discovery phase: 146,562 individuals comprising EA (n = 120,473), AA (n = 21,503), and HA (n = 4,586) individuals contributed from 16 studies
# Discovery phase: GWAS statistics from Suppl.Table3 - I took statistisc for mean arterial pressure (MAP=1/3 SBP + 2/3 DBP) with p-value<5e-08, SNPs; effect allele=coded allele
# Discovery phase:I filtered linked variants with snpclip r2 <0.1, EUR (non-finish) population (https://ldlink.nci.nih.gov/?tab=snpclip)
# Follow up: for candidate loci from Discovery phase, GWAS statistics provided for novel loci only
# Follow-up: meta-analysis performed for 77 variants on results from 180,726 individuals from the CHD Exome+ Consortium, ExomeBP Consortium, GoT2DGenes Consortium, and T2D-GENES Consortium
# Follow-up: GWAS statistics from Suppl.Table4 (European ancestry only); I took only variants with p-value for DBP or/and SBP <5e-08 (“Combined”)
# Follow-up: I calculated beta for MAP (not given for this phase) as 1/3beta for SBP + 2/3beta for DBP (if SBP beta or DBP beta not given or not significant => I assumed that it equals 0);
# Follow-up: I took GWAS statistics from this phase only for loci that were not significant in the discovery phase but reached significance in the follow-up
# Alleles for variants: rs880315, rs1173727, rs2681472, rs653178, rs409558 were given in respect to -1 strain (I guess and hope => I substituted T<->A, G<->C)
# In both phases: I did not used GWAS statistics (loci) associated with pulse pressure and hypertension
# description_genotype["low_trait"] = "hom" # for one snip test
# description_genotype["average_trait"] = "het" #for one snip test
# description_genotype["high_trait"] = "hom" # for one snip test

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-2.0250, category_name='Low risk'),
                        QuantitativeCategory(from_=-2.0250, to=0.9520, category_name='Average risk'),
                        QuantitativeCategory(from_=0.9520, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs17367504', effect_allele='G', beta_value=-0.598),
                        beta(rs_id='rs699', effect_allele='A', beta_value=-0.329),
                        beta(rs_id='rs10204405', effect_allele='A', beta_value=-0.291),
                        beta(rs_id='rs3755652', effect_allele='T', beta_value=0.307),
                        beta(rs_id='rs448378', effect_allele='G', beta_value=0.408),
                        beta(rs_id='rs16998073', effect_allele='T', beta_value=0.417),
                        beta(rs_id='rs13107325', effect_allele='T', beta_value=-0.635),
                        beta(rs_id='rs1799945', effect_allele='G', beta_value=0.577),
                        beta(rs_id='rs6969780', effect_allele='C', beta_value=0.391),
                        beta(rs_id='rs891511', effect_allele='A', beta_value=-0.275),
                        beta(rs_id='rs11014166', effect_allele='T', beta_value=-0.348),
                        beta(rs_id='rs1530440', effect_allele='T', beta_value=-0.362),
                        beta(rs_id='rs11191454', effect_allele='G', beta_value=-0.564),
                        beta(rs_id='rs7076938', effect_allele='C', beta_value=-0.303),
                        beta(rs_id='rs909116', effect_allele='C', beta_value=0.253),
                        beta(rs_id='rs11024074', effect_allele='C', beta_value=0.330),
                        beta(rs_id='rs5219', effect_allele='T', beta_value=0.293),
                        beta(rs_id='rs12801636', effect_allele='A', beta_value=-0.356),
                        beta(rs_id='rs2384550', effect_allele='A', beta_value=-0.281),
                        beta(rs_id='rs6495122', effect_allele='C', beta_value=-0.368),
                        beta(rs_id='rs17514846', effect_allele='C', beta_value=-0.330),
                        beta(rs_id='rs8068318', effect_allele='C', beta_value=-0.283),
                        beta(rs_id='rs1887320', effect_allele='A', beta_value=0.307),
                        beta(rs_id='rs16982520', effect_allele='G', beta_value=0.577),
                        beta(rs_id='rs2493292', effect_allele='T', beta_value=0.293),
                        beta(rs_id='rs6722745', effect_allele='C', beta_value=0.107),
                        beta(rs_id='rs2188962', effect_allele='T', beta_value=-0.133),
                        beta(rs_id='rs10943605', effect_allele='A', beta_value=0.113),
                        beta(rs_id='rs2270860', effect_allele='T', beta_value=0.107),
                        beta(rs_id='rs11977526', effect_allele='A', beta_value=-0.107),
                        beta(rs_id='rs76452347', effect_allele='T', beta_value=-0.140),
                        beta(rs_id='rs10995311', effect_allele='G', beta_value=-0.140),
                        beta(rs_id='rs7302981', effect_allele='A', beta_value=0.287),
                        beta(rs_id='rs1126464', effect_allele='C', beta_value=0.180),
                        beta(rs_id='rs7248104', effect_allele='A', beta_value=-0.103),
                        beta(rs_id='rs4823006', effect_allele='G', beta_value=-0.090)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Blood pressure'

about = ('Blood pressure is a measurement of how hard blood in the body is pushing '
         'against the arteries as it flows, and of how much blood the heart is '
         'pumping. High blood pressure is relatively common across the world, and it '
         'is a risk factor for heart disease, chronic kidney disease, and other health '
         "issues. Many people don't know they have high blood pressure because it "
         "often doesn't cause symptoms. Some people may feel dizziness, facial "
         'flushing, or have blood spots in the eyes. Maintaining a healthy blood '
         'pressure can improve overall well being.')

how_your_result_was_calculated = ('Your result is a risk based on 42 genetic variants, specifically called '
                                  'SNPs, in genes associated with blood pressure. Overall, this explains about '
                                  '3% of the the genetic variation in blood pressure in the human population, '
                                  'based on current scientific knowledge.')

other_factors_statement = ("Many factors impact a person's blood pressure, including, neurological "
                           'health, spinal subluxation, age, biological sex, ethnicity, lifestyle, other '
                           'health issues, and genetics. Genetics has a moderate overall impact on blood '
                           'pressure when you consider all the other factors.')

other_factors = [{'content': 'People of African descent have a higher risk for high blood '
                             'pressure. It can also be more severe, with some medications '
                             'being less effective for people of this ethnic background.',
                  'title': 'Ethnic Background'},
                 {'content': 'Having another health issue, such as diabetes, chronic kidney '
                             'disease, thyroid problems, or sleep apnea, are risk factors for '
                             'high blood pressure.',
                  'title': 'Other Health Issues'},
                 {'content': 'Smoking, using tobacco products, or taking certain drugs (such '
                             'as cocaine and amphetamines) can cause high blood pressure. '
                             'Being overweight, having less physical activity, having a '
                             'high-salt diet, regular heavy alcohol use, and high amounts of '
                             'stress are also risk factors for high blood pressure.',
                  'title': 'Lifestyle'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have high blood pressure. These risk factors, '
                             'which are usually due to rare genetic variants, are not included '
                             'in this test. If you have a family history of high blood '
                             'pressure, talk to your Practitioner.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your blood pressure and other factors, you may '
                                           'have an average risk for heart disease, kidney disease, or '
                                           'other health issues related to high blood pressure.',
                           'High risk': 'Depending on your blood pressure and other factors, you may '
                                        'have a higher risk for heart disease, kidney disease, or other '
                                        'health issues related to high blood pressure.',
                           'Low risk': 'Depending on your blood pressure and other factors, you may have '
                                       'a lower risk for heart disease, kidney disease, or other health '
                                       'issues related to high blood pressure.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average risk for high blood pressure.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher risk for high blood pressure.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower risk for high blood pressure.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent high blood '
                                             'pressure and stay healthy. Certain blood tests can also '
                                             'check your risk factors for high blood pressure and related '
                                             'health issues. Talk to your practitioner about what is right '
                                             'for you.',
                             'High risk': 'There are things you can do to try to prevent high blood '
                                          'pressure and stay healthy. Certain blood tests can also check '
                                          'your risk factors for high blood pressure and related health '
                                          'issues. Talk to your practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent high blood '
                                         'pressure and stay healthy. Certain blood tests can also check '
                                         'your risk factors for high blood pressure and related health '
                                         'issues. Talk to your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk factors for high blood pressure or other '
                                                'health issues. Consider talking to your '
                                                'practitioner about ordering a cardiometabolic '
                                                'blood tests or other tests to check your risk '
                                                'factors for other health issues, and possible '
                                                'next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'A healthy diet has many benefits, including '
                                                'helping to maintain your blood pressure. Look '
                                                'to minimize salt, saturated fat, trans fat, and '
                                                'cholesterol while boosting your intake of '
                                                'vegetables, fruits, whole grains, lean skinless '
                                                'meats, low-fat dairy products, nuts, and '
                                                'legumes. A diet of high fiber plant products '
                                                'such as vegetables and low-glycemic fruits like '
                                                'berries, good fats, and quality, lean proteins '
                                                'will help you to lower your risk.  Talk to your '
                                                'practitioner to determine whether a '
                                                'Mediterranean plan, Modified Mediterranean, or '
                                                'more of a low-carb/keto plan is right for you.',
                                     'title': 'Diet'},
                                    {'content': 'If you smoke, try to quit -- smoking increases '
                                                'the risk for high blood pressure and heart '
                                                'disease. As well, try to get your body moving. '
                                                "It's recommended that adults get 2.5 hours of "
                                                'moderate physical activity, like brisk walking '
                                                'or biking, every week. Not only can this lower '
                                                'your risk of high blood pressure, but it can '
                                                'also help you maintain a healthy weight. Talk '
                                                'to your practitioner about adding '
                                                'resistance exercise, aerobic exercise, and/or '
                                                'High Intensity Interval Training (HIIT) to your '
                                                'weekly routine.',
                                     'title': 'Lifestyle'},
                                    {'content': 'High blood pressure can run in families, and '
                                                'some of these risk factors are not included in '
                                                'this test. If you have a close relative '
                                                '(parent, child, sibling) with high blood '
                                                'pressure, speak to your practitioner about your '
                                                'family history. If you have blood tests to look '
                                                'for related health issues, information about '
                                                'your family history will help with '
                                                'understanding your blood test results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order '
                                             'cardiometabolic blood tests or other tess to check '
                                             'your risk factors for high blood pressure or other '
                                             'health issues. Treatment, such as medication, may '
                                             'be available if your blood pressure is high. Talk '
                                             'to them about this and possible next steps that '
                                             'you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Minimizing salt, saturated fat, trans fat, and '
                                             'cholesterol in your diet can help maintain a '
                                             'healthy blood pressure. This means boosting your '
                                             'intake of vegetables, fruits, whole grains, lean '
                                             'skinless meats, low-fat dairy products, nuts, and '
                                             'legumes. A diet of high fiber plant products such '
                                             'as vegetables and low-glycemic fruits like '
                                             'berries, good fats, and quality, lean proteins '
                                             'will help you to lower your risk.  Talk to your '
                                             'practitioner to determine whether a Mediterranean '
                                             'plan, Modified Mediterranean, or more of a '
                                             'low-carb/keto plan is right for you.',
                                  'title': 'Diet'},
                                 {'content': 'If you smoke, now is a good time to quit -- '
                                             'smoking increases the risk for high blood pressure '
                                             'and heart disease. As well, try to get your body '
                                             "moving. It's recommended that adults get 2.5 hours "
                                             'of moderate physical activity, like brisk walking '
                                             'or biking, every week. Not only can this lower '
                                             'your risk of high blood pressure, but it can also '
                                             'help you maintain a healthy weight. Talk to your '
                                             'practitioner about adding resistance '
                                             'exercise, aerobic exercise, and/or High Intensity '
                                             'Interval Training (HIIT) to your weekly routine.',
                                  'title': 'Lifestyle'},
                                 {'content': 'High blood pressure can run in families, and some '
                                             'of these risk factors are not included in this '
                                             'test. If you have a close relative (parent, child, '
                                             'sibling) with high blood pressure, speak to your '
                                             'practitioner about your family history. If you '
                                             'have blood tests to look for related health '
                                             'issues, information about your family history will '
                                             'help with understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'factors for high blood pressure or other health '
                                            'issues. Consider talking to your practitioner about '
                                            'ordering a cardiometabolic blood test or other '
                                            'tests to check your risk factors for other health '
                                            'issues, and possible next steps that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'A healthy diet has many benefits, including helping '
                                            'to maintain your blood pressure. Look to minimize '
                                            'salt, saturated fat, trans fat, and cholesterol '
                                            'while boosting your intake of vegetables, fruits, '
                                            'whole grains, lean skinless meats, low-fat dairy '
                                            'products, nuts, and legumes. A diet of high fiber '
                                            'plant products such as vegetables and low-glycemic '
                                            'fruits like berries, good fats, and quality, lean '
                                            'proteins will help you to lower your risk.  Talk to '
                                            'your practitioner to determine whether a '
                                            'Mediterranean plan, Modified Mediterranean, or more '
                                            'of a low-carb/keto plan is right for you.',
                                 'title': 'Diet'},
                                {'content': 'If you smoke, try to quit -- smoking increases the '
                                            'risk for high blood pressure and heart disease. As '
                                            "well, try to get your body moving. It's recommended "
                                            'that adults get 2.5 hours of moderate physical '
                                            'activity, like brisk walking or biking, every week. '
                                            'Not only can this lower your risk of high blood '
                                            'pressure, but it can also help you maintain a '
                                            'healthy weight. Talk to your '
                                            'practitioner about adding resistance exercise, '
                                            'aerobic exercise, and/or High Intensity Interval '
                                            'Training (HIIT) to your weekly routine.',
                                 'title': 'Lifestyle'},
                                {'content': 'High blood pressure can run in families, and some '
                                            'of these risk factors are not included in this '
                                            'test. If you have a close relative (parent, child, '
                                            'sibling) with high blood pressure, speak to your '
                                            'practitioner about your family history. If you have '
                                            'blood tests to look for related health issues, '
                                            'information about your family history will help '
                                            'with understanding your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood pressure"
trait_science = "About 24 - 37% of differences in blood pressure observed among people has a genetic origin. Our analysis was based on 42 SNPs associated with diastolic or/and systolic blood pressure. These variants jointly explain about 3% of the variation in trait between people."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the main arterial pressure. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
