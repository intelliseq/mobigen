from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "HDL-C"
trait_pmids = ["30275531","18165655"]  # 30275531 supplementary table 9
trait_heritability = 0.3
trait_snp_heritability = None
trait_explained = 0.12
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "High-density lipoprotein blood level is moderately heritable, with h"+chr(0x00b2)+" estimates of 27-48% [1]. Our test is based on results of the genome wide association study (GWAS) [2], which included over 280 000 individuals, mainly of European ancestry (216 000 individuals). The study identified 309 variants significantly associated with the HDL concentration. Together these SNPs explain about 12% of between people variance in the HDL level [2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score." 
genes = ["ABCA1", "ABCA8", "ABO", "AC004540.2", "AC005514.1", "AC005520.2", "AC006272.1", "AC008514.2", "AC008755.1",
         "AC010280.2", "AC011481.1", "AC011481.4", "AC016831.6", "AC018410.1", "AC018554.1", "AC018618.2", "AC018904.2",
         "AC019181.1", "AC020743.2", "AC021736.1", "AC022431.1", "AC022509.4", "AC041039.1", "AC062015.1", "AC068790.1",
         "AC073167.1", "AC073592.8", "AC073957.1", "AC078841.1", "AC084781.2", "AC090621.1", "AC091046.2", "AC091114.1",
         "AC091180.2", "AC091492.1", "AC091939.1", "AC100802.1", "AC104958.2", "AC113410.1", "AC129102.1", "AC147055.1",
         "AC245884.5", "AC245884.6", "ACP1", "ADRB1", "AF178030.1", "AFF1", "AKNA", "AL022322.2", "AL136988.1",
         "AL138847.1", "AL157371.2", "AL160408.6", "AL354813.1", "AL355310.3", "AL355803.1", "AL357134.1", "AL359182.1",
         "AL359711.1", "AL445423.2", "AL590133.2", "AL590326.1", "ALDH1A2", "AMPD2", "ANGPTL4", "ANGPTL8", "AOC1",
         "AP000936.1", "AP001266.1", "AP002026.1", "APOA1-AS", "APOB", "APOC1", "APOC3", "APOC4-APOC2", "APOE",
         "ARMC2-AS1", "ATXN2", "BACE1", "BAZ1B", "BPTF", "BUD13", "C1orf220", "C2-AS1", "C5orf67", "CCDC92", "CCL22",
         "CD300LG", "CDK12", "CELSR2", "CETP", "CHAC2", "COX16", "CSF1", "CTCFL", "CYCSP55", "CYP26A1", "DAGLB", "DDB2",
         "DNAH17", "DPY19L3", "EDEM2", "ELF1", "ELOCP2", "ERLIN1", "ETV5", "FADS2", "FAM114A2", "FAM117B", "FAM13A",
         "FTO", "GAK", "GALNT2", "GAPDHP54", "GFOD2", "GIMAP7", "GPAM", "HACD2", "HCAR3", "HELZ2", "HERPUD1", "HNF1A",
         "IGHMBP2", "INSR", "KANK2", "KRT8P37", "LACTB", "LDLR", "LILRB5", "LINC00678", "LINC00880", "LINC01625",
         "LINC02140", "LINC02357", "LINC02576", "LIPC", "LIPC-AS1", "LIPG", "LMF1-AS1", "LPA", "LPL", "LYPLAL1-AS1",
         "MAP1A", "MAP2K7", "MAP3K5", "MARCH8", "MARVELD3", "MIR1908", "MIR3646", "MIR4304", "MIR4755", "MIR548AQ",
         "MIR548H2", "MIR6815", "MIR6863", "MLN", "MTCH2", "MTCO3P1", "MTF2", "MTND5P34", "NBEAL2", "NCOA2", "NCOA3",
         "NFKBIL1", "NR0B2", "NUP93", "PABPC4", "PCCB", "PDGFC", "PEPD", "PEX6", "PIGC", "PINX1", "PLCB3", "PLIN1",
         "PNPLA2", "PNPLA3", "PNPO", "PPARG", "PPIAP51", "PRKAG1", "PRMT6", "PSKH1", "PSMD3", "PSORS1C2", "PTTG1IP",
         "RAB11B", "RBM5", "RF00017", "RF00019", "RF00415", "RF02271", "RFT1", "RN7SKP151", "RN7SL559P", "RN7SL786P",
         "RN7SL801P", "RNA5SP245", "RNF111", "RNU6-680P", "RNU6-761P", "RNU6-879P", "RNU6-927P", "RNY3P15", "RNY4P6",
         "RPL21P8", "RPL30P9", "RPS10-NUDT3", "RPS6KA1", "RPS7", "RRBP1", "RRNAD1", "RSPH6A", "RSPO3", "SBNO1",
         "SCARB1", "SEC14L4", "SLC12A3", "SLC22A18", "SLC39A8", "SLC45A3", "SMUG1P1", "SNX13", "SPIC", "SPON1-AS1",
         "ST3GAL4", "STAB1", "TCAP", "TECTB", "TMEM18", "TNFSF13", "TPI1P3", "TRANK1", "TRIB1", "TTC39B", "TULP1",
         "UBASH3B", "UBE2L2", "UBE3B", "VEGFA", "VGLL4", "VIM-AS1", "VTN", "WT1-AS", "YPEL5P1", "ZC3H12C", "ZCCHC8",
         "ZNF335", "ZNRF3", "ZPR1"]

# czyli zawartość cholesterolu w lipoproteinach o dużej gęstości: to "dobry cholesterol"
# na podstawie: doi:10.1038/s41588-018-0222-9 (artykuł i SupplTable9 w katalogu ldl-c);
# badania obejmowały dwie fazy "discovery" i "replication"
# faza "discovery": EUR – 216 tys., AFR – 57 tys, Hispanic – 24 tys;
# faza "replication": obiecujące warianty były dodatkowo potwierdzane z użyciem danych z '2017 GLGC exome array meta-analysis'
# lub z '2013  GLGC joint  meta-analysis’
# dane o rs: z tabeli SuppTable9 (tylko dotyczące HDL-C; te dane są z "conditional analysis"-nie wyrzucałam wariantów leżących blisko siebie)
# w sumie pozostało 312 SNPów (dwa wyrzuciłam, bo miały dziwne rs)
# efekty były podane, jako współczynniki beta z regresji, ale dane o stężeniu HDL-C były transformowane przed badaniem asocjacji:
# 'residuals were obtained after regressing on age, age2, sex and 10 principal components of ancestry. Residuals were subsequently
# inverse-normal transformed for association analysis': nie da się wspł. beta bezpośrednio odnieść do fenotypu
# odziedziczalnosc: około 30 %, zmienność wyjaśniona: około 12 % (Suppl. Table 10)
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_hdl.txt i stats_hdl.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=3.2466, category_name='High risk'),
                        QuantitativeCategory(from_=3.2466, to=4.0875, category_name='Average risk'),
                        QuantitativeCategory(from_=4.0875, category_name='Low risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs2878349', effect_allele='G', beta_value=-0.015),
                        beta(rs_id='rs12740374', effect_allele='G', beta_value=0.0454),
                        beta(rs_id='rs7550711', effect_allele='C', beta_value=-0.049),
                        beta(rs_id='rs28362581', effect_allele='G', beta_value=-0.0276),
                        beta(rs_id='rs333947', effect_allele='G', beta_value=-0.0267),
                        beta(rs_id='rs267738', effect_allele='G', beta_value=-0.0239),
                        beta(rs_id='rs12145743', effect_allele='G', beta_value=-0.014),
                        beta(rs_id='rs1011731', effect_allele='G', beta_value=0.0134),
                        beta(rs_id='rs4650994', effect_allele='G', beta_value=-0.0193),
                        beta(rs_id='rs2243976', effect_allele='G', beta_value=0.0289),
                        beta(rs_id='rs16856110', effect_allele='G', beta_value=0.0199),
                        beta(rs_id='rs6694509', effect_allele='C', beta_value=-0.0195),
                        beta(rs_id='rs2807834', effect_allele='G', beta_value=-0.0238),
                        beta(rs_id='rs4846914', effect_allele='G', beta_value=0.0468),
                        beta(rs_id='rs1043900', effect_allele='G', beta_value=0.0234),
                        beta(rs_id='rs558971', effect_allele='G', beta_value=-0.0138),
                        beta(rs_id='rs1767141', effect_allele='C', beta_value=-0.0263),
                        beta(rs_id='rs6668958', effect_allele='G', beta_value=0.0204),
                        beta(rs_id='rs17162330', effect_allele='C', beta_value=0.0413),
                        beta(rs_id='rs12144891', effect_allele='G', beta_value=0.0168),
                        beta(rs_id='rs4660293', effect_allele='G', beta_value=0.04),
                        beta(rs_id='rs1168089', effect_allele='C', beta_value=-0.0153),
                        beta(rs_id='rs4847399', effect_allele='G', beta_value=-0.0216),
                        beta(rs_id='rs2862954', effect_allele='C', beta_value=-0.0173),
                        beta(rs_id='rs2792751', effect_allele='C', beta_value=0.0277),
                        beta(rs_id='rs2148489', effect_allele='C', beta_value=0.0223),
                        beta(rs_id='rs7076938', effect_allele='C', beta_value=0.0172),
                        beta(rs_id='rs10904908', effect_allele='G', beta_value=-0.0118),
                        beta(rs_id='rs970548', effect_allele='C', beta_value=-0.0245),
                        beta(rs_id='rs11255744', effect_allele='C', beta_value=-0.0182),
                        beta(rs_id='rs2068888', effect_allele='G', beta_value=0.0205),
                        beta(rs_id='rs746463', effect_allele='C', beta_value=-0.0165),
                        beta(rs_id='rs180349', effect_allele='T', beta_value=0.0171),
                        beta(rs_id='rs10488698', effect_allele='G', beta_value=0.0447),
                        beta(rs_id='rs964184', effect_allele='G', beta_value=0.1321),
                        beta(rs_id='rs138326449', effect_allele='G', beta_value=0.7155),
                        beta(rs_id='rs138407155', effect_allele='T', beta_value=0.3555),
                        beta(rs_id='rs12281729', effect_allele='C', beta_value=0.0769),
                        beta(rs_id='rs10892063', effect_allele='C', beta_value=0.0418),
                        beta(rs_id='rs12269901', effect_allele='G', beta_value=0.0268),
                        beta(rs_id='rs593245', effect_allele='C', beta_value=0.0146),
                        beta(rs_id='rs7941030', effect_allele='C', beta_value=-0.0229),
                        beta(rs_id='rs4937122', effect_allele='G', beta_value=0.0317),
                        beta(rs_id='rs6486121', effect_allele='C', beta_value=-0.0152),
                        beta(rs_id='rs2303975', effect_allele='G', beta_value=0.0192),
                        beta(rs_id='rs925946', effect_allele='G', beta_value=-0.0122),
                        beta(rs_id='rs16928809', effect_allele='G', beta_value=-0.0246),
                        beta(rs_id='rs7927401', effect_allele='C', beta_value=-0.021),
                        beta(rs_id='rs3824866', effect_allele='C', beta_value=0.0331),
                        beta(rs_id='rs326214', effect_allele='G', beta_value=-0.0248),
                        beta(rs_id='rs10838738', effect_allele='G', beta_value=0.0193),
                        beta(rs_id='rs61897793', effect_allele='G', beta_value=0.0238),
                        beta(rs_id='rs174583', effect_allele='C', beta_value=-0.0424),
                        beta(rs_id='rs35169799', effect_allele='C', beta_value=-0.0396),
                        beta(rs_id='rs644740', effect_allele='C', beta_value=0.016),
                        beta(rs_id='rs622082', effect_allele='G', beta_value=0.0155),
                        beta(rs_id='rs499974', effect_allele='C', beta_value=-0.0254),
                        beta(rs_id='rs140201358', effect_allele='G', beta_value=0.0523),
                        beta(rs_id='rs2373459', effect_allele='C', beta_value=0.016),
                        beta(rs_id='rs7298565', effect_allele='G', beta_value=0.028),
                        beta(rs_id='rs3184504', effect_allele='C', beta_value=-0.0243),
                        beta(rs_id='rs72650673', effect_allele='G', beta_value=-0.1716),
                        beta(rs_id='rs1183910', effect_allele='G', beta_value=0.0127),
                        beta(rs_id='rs12369179', effect_allele='C', beta_value=-0.031),
                        beta(rs_id='rs1798192', effect_allele='G', beta_value=-0.0193),
                        beta(rs_id='rs940904', effect_allele='G', beta_value=-0.0226),
                        beta(rs_id='rs4759375', effect_allele='C', beta_value=0.0487),
                        beta(rs_id='rs12317176', effect_allele='C', beta_value=-0.0224),
                        beta(rs_id='rs863750', effect_allele='C', beta_value=-0.0172),
                        beta(rs_id='rs12230272', effect_allele='G', beta_value=0.0223),
                        beta(rs_id='rs838880', effect_allele='C', beta_value=-0.0249),
                        beta(rs_id='rs10773105', effect_allele='C', beta_value=-0.0255),
                        beta(rs_id='rs150728540', effect_allele='C', beta_value=0.4756),
                        beta(rs_id='rs5891', effect_allele='C', beta_value=0.0776),
                        beta(rs_id='rs7306660', effect_allele='G', beta_value=-0.0286),
                        beta(rs_id='rs7298751', effect_allele='G', beta_value=-0.0426),
                        beta(rs_id='rs7134375', effect_allele='C', beta_value=0.0215),
                        beta(rs_id='rs7134150', effect_allele='G', beta_value=-0.0314),
                        beta(rs_id='rs4963975', effect_allele='G', beta_value=-0.0198),
                        beta(rs_id='rs1126930', effect_allele='G', beta_value=-0.0373),
                        beta(rs_id='rs784563', effect_allele='G', beta_value=0.0153),
                        beta(rs_id='rs11613352', effect_allele='C', beta_value=0.0226),
                        beta(rs_id='rs17532301', effect_allele='G', beta_value=-0.0266),
                        beta(rs_id='rs4983559', effect_allele='G', beta_value=-0.0252),
                        beta(rs_id='rs10483776', effect_allele='G', beta_value=0.0153),
                        beta(rs_id='rs8021180', effect_allele='G', beta_value=0.0142),
                        beta(rs_id='rs13379043', effect_allele='C', beta_value=-0.0176),
                        beta(rs_id='rs9944249', effect_allele='G', beta_value=-0.0132),
                        beta(rs_id='rs55707100', effect_allele='C', beta_value=-0.0934),
                        beta(rs_id='rs4622454', effect_allele='C', beta_value=0.0205),
                        beta(rs_id='rs4775041', effect_allele='G', beta_value=0.0388),
                        beta(rs_id='rs16940147', effect_allele='G', beta_value=0.0499),
                        beta(rs_id='rs117901517', effect_allele='C', beta_value=0.0378),
                        beta(rs_id='rs34718390', effect_allele='G', beta_value=0.0528),
                        beta(rs_id='rs1532085', effect_allele='G', beta_value=0.062),
                        beta(rs_id='rs7165077', effect_allele='C', beta_value=-0.0273),
                        beta(rs_id='rs6494003', effect_allele='G', beta_value=0.0927),
                        beta(rs_id='rs16940233', effect_allele='C', beta_value=-0.0478),
                        beta(rs_id='rs12912415', effect_allele='G', beta_value=0.04),
                        beta(rs_id='rs6494006', effect_allele='C', beta_value=0.0436),
                        beta(rs_id='rs17301746', effect_allele='C', beta_value=0.0879),
                        beta(rs_id='rs936960', effect_allele='G', beta_value=0.0524),
                        beta(rs_id='rs1869138', effect_allele='C', beta_value=-0.0281),
                        beta(rs_id='rs6083', effect_allele='G', beta_value=-0.0162),
                        beta(rs_id='rs17269397', effect_allele='G', beta_value=-0.0261),
                        beta(rs_id='rs424346', effect_allele='C', beta_value=0.0545),
                        beta(rs_id='rs181181625', effect_allele='C', beta_value=0.3478),
                        beta(rs_id='rs12148597', effect_allele='G', beta_value=-0.0158),
                        beta(rs_id='rs34317102', effect_allele='C', beta_value=-0.019),
                        beta(rs_id='rs2228510', effect_allele='C', beta_value=-0.0117),
                        beta(rs_id='rs139271800', effect_allele='G', beta_value=-0.2482),
                        beta(rs_id='rs12928822', effect_allele='C', beta_value=0.0179),
                        beta(rs_id='rs1421085', effect_allele='C', beta_value=0.0282),
                        beta(rs_id='rs12929759', effect_allele='G', beta_value=0.0183),
                        beta(rs_id='rs4238772', effect_allele='G', beta_value=0.0286),
                        beta(rs_id='rs8044753', effect_allele='G', beta_value=0.0248),
                        beta(rs_id='rs36049418', effect_allele='G', beta_value=0.0656),
                        beta(rs_id='rs11648751', effect_allele='C', beta_value=-0.0304),
                        beta(rs_id='rs37029', effect_allele='G', beta_value=-0.0242),
                        beta(rs_id='rs9989419', effect_allele='G', beta_value=-0.0336),
                        beta(rs_id='rs72786786', effect_allele='G', beta_value=0.0577),
                        beta(rs_id='rs76315536', effect_allele='C', beta_value=0.1614),
                        beta(rs_id='rs4783961', effect_allele='G', beta_value=0.026),
                        beta(rs_id='rs1800775', effect_allele='C', beta_value=0.0317),
                        beta(rs_id='rs34065661', effect_allele='G', beta_value=-0.4847),
                        beta(rs_id='rs7203984', effect_allele='C', beta_value=0.0741),
                        beta(rs_id='rs1532624', effect_allele='C', beta_value=0.1128),
                        beta(rs_id='rs12708974', effect_allele='C', beta_value=-0.0857),
                        beta(rs_id='rs5883', effect_allele='C', beta_value=0.1674),
                        beta(rs_id='rs289714', effect_allele='G', beta_value=0.0822),
                        beta(rs_id='rs5880', effect_allele='G', beta_value=-0.1378),
                        beta(rs_id='rs506829', effect_allele='C', beta_value=0.0192),
                        beta(rs_id='rs16962034', effect_allele='G', beta_value=-0.0157),
                        beta(rs_id='rs7202185', effect_allele='G', beta_value=-0.0373),
                        beta(rs_id='rs16942887', effect_allele='G', beta_value=0.0742),
                        beta(rs_id='rs1345868', effect_allele='C', beta_value=0.0144),
                        beta(rs_id='rs12443634', effect_allele='C', beta_value=-0.0313),
                        beta(rs_id='rs7202647', effect_allele='C', beta_value=0.017),
                        beta(rs_id='rs2071379', effect_allele='G', beta_value=0.0143),
                        beta(rs_id='rs11078915', effect_allele='C', beta_value=-0.0267),
                        beta(rs_id='rs11869286', effect_allele='G', beta_value=0.0239),
                        beta(rs_id='rs11556624', effect_allele='G', beta_value=0.0867),
                        beta(rs_id='rs4794822', effect_allele='C', beta_value=-0.0177),
                        beta(rs_id='rs72836561', effect_allele='C', beta_value=-0.1875),
                        beta(rs_id='rs231539', effect_allele='C', beta_value=-0.0357),
                        beta(rs_id='rs17679445', effect_allele='G', beta_value=0.0295),
                        beta(rs_id='rs11652146', effect_allele='G', beta_value=-0.0172),
                        beta(rs_id='rs12602912', effect_allele='C', beta_value=-0.0176),
                        beta(rs_id='rs10852765', effect_allele='G', beta_value=0.0156),
                        beta(rs_id='rs3803800', effect_allele='G', beta_value=-0.0134),
                        beta(rs_id='rs2292642', effect_allele='C', beta_value=0.0319),
                        beta(rs_id='rs2289750', effect_allele='G', beta_value=-0.0283),
                        beta(rs_id='rs1788783', effect_allele='C', beta_value=0.015),
                        beta(rs_id='rs8093249', effect_allele='G', beta_value=0.0337),
                        beta(rs_id='rs77960347', effect_allele='G', beta_value=-0.2505),
                        beta(rs_id='rs117623631', effect_allele='C', beta_value=0.3432),
                        beta(rs_id='rs3786248', effect_allele='C', beta_value=-0.059),
                        beta(rs_id='rs7241918', effect_allele='G', beta_value=0.0741),
                        beta(rs_id='rs4939886', effect_allele='G', beta_value=0.0198),
                        beta(rs_id='rs11660468', effect_allele='C', beta_value=0.0253),
                        beta(rs_id='rs9956279', effect_allele='C', beta_value=-0.0168),
                        beta(rs_id='rs6511720', effect_allele='G', beta_value=0.0201),
                        beta(rs_id='rs17616661', effect_allele='G', beta_value=0.0258),
                        beta(rs_id='rs737337', effect_allele='C', beta_value=0.057),
                        beta(rs_id='rs2111504', effect_allele='T', beta_value=0.0167),
                        beta(rs_id='rs731839', effect_allele='G', beta_value=0.0178),
                        beta(rs_id='rs12975319', effect_allele='G', beta_value=-0.0153),
                        beta(rs_id='rs2075650', effect_allele='G', beta_value=0.0379),
                        beta(rs_id='rs77301115', effect_allele='G', beta_value=-0.066),
                        beta(rs_id='rs7412', effect_allele='C', beta_value=0.0856),
                        beta(rs_id='rs439401', effect_allele='C', beta_value=0.0141),
                        beta(rs_id='rs4420638', effect_allele='G', beta_value=0.042),
                        beta(rs_id='rs5167', effect_allele='G', beta_value=-0.0376),
                        beta(rs_id='rs8111071', effect_allele='G', beta_value=0.0278),
                        beta(rs_id='rs2303108', effect_allele='C', beta_value=0.0137),
                        beta(rs_id='rs3752125', effect_allele='C', beta_value=-0.0204),
                        beta(rs_id='rs12975366', effect_allele='C', beta_value=0.0212),
                        beta(rs_id='rs10408844', effect_allele='C', beta_value=0.018),
                        beta(rs_id='rs4804833', effect_allele='G', beta_value=-0.015),
                        beta(rs_id='rs116843064', effect_allele='G', beta_value=0.2456),
                        beta(rs_id='rs2913968', effect_allele='C', beta_value=0.0166),
                        beta(rs_id='rs13389219', effect_allele='C', beta_value=0.0327),
                        beta(rs_id='rs6435161', effect_allele='G', beta_value=0.0177),
                        beta(rs_id='rs676210', effect_allele='G', beta_value=0.0617),
                        beta(rs_id='rs562338', effect_allele='G', beta_value=0.0178),
                        beta(rs_id='rs2943641', effect_allele='C', beta_value=0.0391),
                        beta(rs_id='rs11553746', effect_allele='C', beta_value=0.0158),
                        beta(rs_id='rs4850047', effect_allele='C', beta_value=0.0201),
                        beta(rs_id='rs36020289', effect_allele='G', beta_value=0.0462),
                        beta(rs_id='rs2867125', effect_allele='C', beta_value=0.0173),
                        beta(rs_id='rs12990465', effect_allele='C', beta_value=0.0263),
                        beta(rs_id='rs1132274', effect_allele='C', beta_value=-0.0159),
                        beta(rs_id='rs2268086', effect_allele='G', beta_value=0.0144),
                        beta(rs_id='rs1415771', effect_allele='G', beta_value=-0.0129),
                        beta(rs_id='rs1800961', effect_allele='C', beta_value=-0.1421),
                        beta(rs_id='rs3827066', effect_allele='C', beta_value=0.0496),
                        beta(rs_id='rs8123864', effect_allele='C', beta_value=0.0477),
                        beta(rs_id='rs1211644', effect_allele='C', beta_value=-0.0178),
                        beta(rs_id='rs11700063', effect_allele='G', beta_value=0.0206),
                        beta(rs_id='rs4239651', effect_allele='C', beta_value=-0.021),
                        beta(rs_id='rs6025606', effect_allele='C', beta_value=0.0119),
                        beta(rs_id='rs310631', effect_allele='C', beta_value=0.0133),
                        beta(rs_id='rs6062343', effect_allele='G', beta_value=0.0137),
                        beta(rs_id='rs235314', effect_allele='C', beta_value=-0.015),
                        beta(rs_id='rs12482088', effect_allele='C', beta_value=0.0187),
                        beta(rs_id='rs181362', effect_allele='C', beta_value=-0.0303),
                        beta(rs_id='rs4823006', effect_allele='G', beta_value=-0.013),
                        beta(rs_id='rs17738540', effect_allele='C', beta_value=-0.0184),
                        beta(rs_id='rs738322', effect_allele='G', beta_value=-0.0198),
                        beta(rs_id='rs738409', effect_allele='G', beta_value=0.0155),
                        beta(rs_id='rs11712666', effect_allele='G', beta_value=-0.0141),
                        beta(rs_id='rs35000036', effect_allele='C', beta_value=-0.0132),
                        beta(rs_id='rs2067819', effect_allele='G', beta_value=0.0211),
                        beta(rs_id='rs2292101', effect_allele='C', beta_value=-0.0438),
                        beta(rs_id='rs1279840', effect_allele='C', beta_value=0.028),
                        beta(rs_id='rs3773910', effect_allele='G', beta_value=-0.0169),
                        beta(rs_id='rs900399', effect_allele='G', beta_value=-0.0213),
                        beta(rs_id='rs7633675', effect_allele='G', beta_value=0.0133),
                        beta(rs_id='rs4234589', effect_allele='G', beta_value=0.0222),
                        beta(rs_id='rs6777217', effect_allele='G', beta_value=-0.0129),
                        beta(rs_id='rs2305637', effect_allele='C', beta_value=-0.0264),
                        beta(rs_id='rs6762477', effect_allele='G', beta_value=0.0232),
                        beta(rs_id='rs13326165', effect_allele='G', beta_value=0.0213),
                        beta(rs_id='rs11242', effect_allele='C', beta_value=0.015),
                        beta(rs_id='rs2602836', effect_allele='G', beta_value=0.0127),
                        beta(rs_id='rs112519623', effect_allele='G', beta_value=-0.0472),
                        beta(rs_id='rs13107325', effect_allele='C', beta_value=-0.0777),
                        beta(rs_id='rs6855363', effect_allele='C', beta_value=-0.0185),
                        beta(rs_id='rs10019888', effect_allele='G', beta_value=0.0243),
                        beta(rs_id='rs293429', effect_allele='C', beta_value=-0.0133),
                        beta(rs_id='rs11248051', effect_allele='C', beta_value=-0.019),
                        beta(rs_id='rs10023050', effect_allele='G', beta_value=-0.0152),
                        beta(rs_id='rs3822072', effect_allele='G', beta_value=-0.0214),
                        beta(rs_id='rs4705986', effect_allele='G', beta_value=-0.0348),
                        beta(rs_id='rs390299', effect_allele='G', beta_value=-0.0145),
                        beta(rs_id='rs2434612', effect_allele='G', beta_value=0.0225),
                        beta(rs_id='rs7730898', effect_allele='G', beta_value=-0.0183),
                        beta(rs_id='rs7735253', effect_allele='G', beta_value=-0.021),
                        beta(rs_id='rs459193', effect_allele='G', beta_value=0.0265),
                        beta(rs_id='rs9686661', effect_allele='C', beta_value=-0.0343),
                        beta(rs_id='rs4976033', effect_allele='G', beta_value=0.0134),
                        beta(rs_id='rs10057967', effect_allele='C', beta_value=-0.0205),
                        beta(rs_id='rs2754820', effect_allele='G', beta_value=0.0196),
                        beta(rs_id='rs884366', effect_allele='G', beta_value=-0.0143),
                        beta(rs_id='rs3756772', effect_allele='C', beta_value=0.0128),
                        beta(rs_id='rs2745353', effect_allele='C', beta_value=-0.0202),
                        beta(rs_id='rs6925103', effect_allele='C', beta_value=-0.0121),
                        beta(rs_id='rs643381', effect_allele='C', beta_value=0.0208),
                        beta(rs_id='rs41272114', effect_allele='C', beta_value=0.0663),
                        beta(rs_id='rs1652507', effect_allele='C', beta_value=0.046),
                        beta(rs_id='rs1265099', effect_allele='G', beta_value=0.017),
                        beta(rs_id='rs184070214', effect_allele='G', beta_value=-0.0445),
                        beta(rs_id='rs9332739', effect_allele='G', beta_value=-0.029),
                        beta(rs_id='rs3135006', effect_allele='C', beta_value=-0.0195),
                        beta(rs_id='rs2894342', effect_allele='C', beta_value=0.0146),
                        beta(rs_id='rs1759645', effect_allele='C', beta_value=0.0239),
                        beta(rs_id='rs16885998', effect_allele='G', beta_value=0.0506),
                        beta(rs_id='rs11755393', effect_allele='G', beta_value=0.0304),
                        beta(rs_id='rs41270076', effect_allele='C', beta_value=0.0386),
                        beta(rs_id='rs4711698', effect_allele='C', beta_value=0.018),
                        beta(rs_id='rs2274517', effect_allele='C', beta_value=0.0156),
                        beta(rs_id='rs6905288', effect_allele='G', beta_value=-0.0295),
                        beta(rs_id='rs35349911', effect_allele='C', beta_value=-0.0138),
                        beta(rs_id='rs881858', effect_allele='G', beta_value=-0.0138),
                        beta(rs_id='rs1997243', effect_allele='G', beta_value=-0.0222),
                        beta(rs_id='rs11556924', effect_allele='C', beta_value=0.0126),
                        beta(rs_id='rs972283', effect_allele='G', beta_value=0.0276),
                        beta(rs_id='rs3735080', effect_allele='C', beta_value=-0.0137),
                        beta(rs_id='rs7787577', effect_allele='G', beta_value=0.0314),
                        beta(rs_id='rs10282707', effect_allele='C', beta_value=-0.0258),
                        beta(rs_id='rs1534696', effect_allele='C', beta_value=0.0185),
                        beta(rs_id='rs2726070', effect_allele='G', beta_value=-0.0172),
                        beta(rs_id='rs4917014', effect_allele='G', beta_value=-0.0138),
                        beta(rs_id='rs2303361', effect_allele='C', beta_value=-0.0238),
                        beta(rs_id='rs1178979', effect_allele='C', beta_value=-0.0335),
                        beta(rs_id='rs2957447', effect_allele='G', beta_value=0.0138),
                        beta(rs_id='rs9657541', effect_allele='C', beta_value=-0.0246),
                        beta(rs_id='rs2293889', effect_allele='G', beta_value=-0.0304),
                        beta(rs_id='rs4871137', effect_allele='G', beta_value=-0.0213),
                        beta(rs_id='rs17405319', effect_allele='C', beta_value=-0.0252),
                        beta(rs_id='rs2954026', effect_allele='G', beta_value=-0.0493),
                        beta(rs_id='rs1801177', effect_allele='G', beta_value=-0.12),
                        beta(rs_id='rs264', effect_allele='G', beta_value=0.0325),
                        beta(rs_id='rs268', effect_allele='G', beta_value=0.2379),
                        beta(rs_id='rs13702', effect_allele='C', beta_value=-0.0589),
                        beta(rs_id='rs17091872', effect_allele='G', beta_value=-0.0559),
                        beta(rs_id='rs2410622', effect_allele='C', beta_value=-0.0264),
                        beta(rs_id='rs6983170', effect_allele='C', beta_value=0.0933),
                        beta(rs_id='rs6651485', effect_allele='G', beta_value=0.0539),
                        beta(rs_id='rs2083637', effect_allele='G', beta_value=-0.0484),
                        beta(rs_id='rs7837677', effect_allele='C', beta_value=-0.0336),
                        beta(rs_id='rs10106652', effect_allele='G', beta_value=0.0306),
                        beta(rs_id='rs34859606', effect_allele='G', beta_value=-0.0194),
                        beta(rs_id='rs6586892', effect_allele='C', beta_value=0.036),
                        beta(rs_id='rs6983999', effect_allele='T', beta_value=0.0239),
                        beta(rs_id='rs4512408', effect_allele='C', beta_value=0.0255),
                        beta(rs_id='rs11774381', effect_allele='C', beta_value=-0.0278),
                        beta(rs_id='rs4841132', effect_allele='G', beta_value=-0.108),
                        beta(rs_id='rs13292026', effect_allele='G', beta_value=-0.0437),
                        beta(rs_id='rs2230808', effect_allele='C', beta_value=-0.0236),
                        beta(rs_id='rs76881554', effect_allele='G', beta_value=-0.1641),
                        beta(rs_id='rs2066714', effect_allele='C', beta_value=-0.0514),
                        beta(rs_id='rs3905000', effect_allele='G', beta_value=-0.0547),
                        beta(rs_id='rs10120087', effect_allele='C', beta_value=0.0444),
                        beta(rs_id='rs1800978', effect_allele='G', beta_value=0.0574),
                        beta(rs_id='rs13284054', effect_allele='C', beta_value=0.0541),
                        beta(rs_id='rs1800977', effect_allele='G', beta_value=0.0256),
                        beta(rs_id='rs10733608', effect_allele='G', beta_value=0.0139),
                        beta(rs_id='rs581080', effect_allele='G', beta_value=0.037)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'HDL Cholesterol'

about = ('Cholesterol is an essential component of cell membranes within the body. It '
         'is made up of low-density lipoprotein (LDL or "bad" cholesterol) and '
         'high-density lipoprotein (HDL or "good" cholesterol). Diet is a source of '
         'cholesterol, but the body also makes it. A low HDL cholesterol level can '
         'cause a risk for heart disease and other health issues, especially if your '
         'triglycerides, LDL cholesterol, and/or inflammatory biomarker levels are '
         'high.  A positive goal is to raise HDL. Several factors contribute to '
         'overall cholesterol levels, including lifestyle, and genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 312 genetic variants, specifically called '
                                  'SNPs, found in 250 genes associated with blood HDL level. Overall, this '
                                  'explains about 12% of the genetic variation in HDL cholesterol level in the '
                                  'human population, based on current scientific knowledge.')

other_factors_statement = ('Many things impact cholesterol levels, including diet, fitness levels, '
                           'lifestyle, and genetics. Several genes are associated with our HDL '
                           'cholesterol level.')

other_factors = [{'content': 'Diet can affect cholesterol levels, including our HDL '
                             'cholesterol level. For example, trans fats found in some '
                             'prepared foods can lower HDL cholesterol levels. High-fiber '
                             'foods can increase HDL cholesterol. Some other key factors, '
                             'reduce carbs, weight loss, eat foods rich in anthocyanins, use '
                             'coconut oil, stop smoking, and use quality supplements like an '
                             'Omega 3s.',
                  'title': 'Diet'},
                 {'content': 'Lifestyle can impact the effect of high cholesterol levels. '
                             'Smoking and lack of physical activity increase the risk of heart '
                             'disease and may lead to being overweight. These are important '
                             'risk factors to address if overall cholesterol levels are also '
                             'high.',
                  'title': 'Lifestyle'},
                 {'content': 'Genetic risk factors can cause multiple people in the same '
                             'family to have problems with cholesterol. Some of these types of '
                             'risk factors, which are usually due to rare genetic variants, '
                             'may not be included in this test. If you have a family history '
                             'of cholesterol problems, talk to your Practitioner.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your cholesterol levels and other factors, you '
                                           'may have an average risk for heart disease and other health '
                                           'issues.',
                           'High risk': 'Depending on your cholesterol levels and other factors, you may '
                                        'have a higher risk for heart disease and other health issues.',
                           'Low risk': 'Depending on your cholesterol levels and other factors, you may '
                                       'have a lower risk for heart disease and other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average level of HDL blood cholesterol.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a lower level of HDL blood cholesterol.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a higher level of HDL blood cholesterol.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent high '
                                             'cholesterol and stay healthy. Certain blood tests can also '
                                             'check your cholesterol levels. Talk to your practitioner '
                                             'about what is right for you.',
                             'High risk': 'There are things you can do to try to raise healthy, HDL '
                                          'cholesterol.  Talk to your practitioner about what testing and '
                                          'evaluating your cholesterol and determing the diet, lifestyle '
                                          'and supplementation that is right for you.',
                             'Low risk': 'There are things you can do to try to prevent high cholesterol '
                                         'and stay healthy. Certain blood tests can also check your '
                                         'cholesterol levels. Talk to your practitioner about what is '
                                         'right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for low HDL cholesterol. Consider talking '
                                                'to your practitioner about blood tests to check '
                                                'your cholesterol levels, and possible next '
                                                'steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Reduce carbs focusing on more of an '
                                                'anti-inflammatory, ketogenic or a modified '
                                                'mediterranean diet.  Increase monounsaturated '
                                                'fats like extra virgin olive oil and fish oil.  '
                                                'Eliminate trans fats and rancid fats from '
                                                'vegetable, nut, and seed oils.  Add coconut oil '
                                                'and eat foods rich in anthocyanins such as '
                                                'blueberries, eggplant, red cabbage, and '
                                                'blackberries.',
                                     'title': 'Diet'},
                                    {'content': 'If you smoke, try to quit -- smoking increases '
                                                'the risk for heart disease. As well, try to get '
                                                "your body moving. It's recommended that adults "
                                                'get 2.5 hours of moderate physical activity, '
                                                'like brisk walking or biking, every week. Not '
                                                'only can this lower your risk of heart disease, '
                                                'but it can also help you maintain a healthy '
                                                'weight. Resistance training, high intensity '
                                                'interval training and aerobic exercise all '
                                                'raise HDL.',
                                     'title': 'Lifestyle'},
                                    {'content': 'High cholesterol can run in families, and these '
                                                'risk factors are not included in this test. If '
                                                'you have a close relative (parent, child, '
                                                'sibling) with high cholesterol, speak to your '
                                                'practitioner about your family history. If you '
                                                'have blood tests to check your cholesterol '
                                                'levels, information about your family history '
                                                'will help with understanding your blood test '
                                                'results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your cholesterol levels. Talk to them '
                                             'about this and possible next steps that you can '
                                             'take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Reduce carbs focusing on more of an '
                                             'anti-inflammatory, ketogenic or a modified '
                                             'mediterranean diet.  Increase monounsaturated fats '
                                             'like extra virgin olive oil and fish oil.  '
                                             'Eliminate trans fats and rancid fats from '
                                             'vegetable, nut, and seed oils.  Add coconut oil '
                                             'and eat foods rich in anthocyanins such as '
                                             'blueberries, eggplant, red cabbage, and '
                                             'blackberries.',
                                  'title': 'Diet'},
                                 {'content': 'If you smoke, now is a good time to quit -- '
                                             'smoking increases the risk for heart disease. As '
                                             "well, try to get your body moving. It's "
                                             'recommended that adults get 2.5 hours of moderate '
                                             'physical activity, like brisk walking or biking, '
                                             'every week. Not only can this lower your risk of '
                                             'heart disease, but it can also help you maintain a '
                                             'healthy weight. Resistance training, high '
                                             'intensity interval training and aerobic exercise '
                                             'all raise HDL.',
                                  'title': 'Lifestyle'},
                                 {'content': 'High cholesterol can run in families, and these '
                                             'risk factors are not included in this test. If you '
                                             'have a close relative (parent, child, sibling) '
                                             'with high cholesterol, speak to your practitioner '
                                             'about your family history. If you have blood tests '
                                             'to check your cholesterol levels, information '
                                             'about your family history will help with '
                                             'understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for low HDL cholesterol. Consider talking to your '
                                            'practitioner about blood tests to check your '
                                            'cholesterol levels, and possible next steps you can '
                                            'take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Try to reduce carbs focusing on more of an '
                                            'anti-inflammatory mediterranean style diet. '
                                            'Increase monounsaturated fats like extra virgin '
                                            'olive oil and fish oil. Eliminate trans fats and '
                                            'rancid fats from vegetable, nut, and seed oils. Add '
                                            'coconut oil and eat foods rich in anthocyanins such '
                                            'as blueberries, eggplant, red cabbage, and '
                                            'blackberries.',
                                 'title': 'Diet'},
                                {'content': 'If you smoke, try to quit -- smoking increases the '
                                            'risk for heart disease. As well, try to get your '
                                            "body moving. It's recommended that adults get 2.5 "
                                            'hours of moderate physical activity, like brisk '
                                            'walking or biking, every week. Not only can this '
                                            'lower your risk of heart disease, but it can also '
                                            'help you maintain a healthy weight. Resistance '
                                            'training, high intensity interval training and '
                                            'aerobic exercise all raise HDL.',
                                 'title': 'Lifestyle'},
                                {'content': 'High cholesterol can run in families, and these '
                                            'risk factors are not included in this test. If you '
                                            'have a close relative (parent, child, sibling) with '
                                            'high cholesterol, speak to your practitioner about '
                                            'your family history. If you have blood tests to '
                                            'check your cholesterol levels, information about '
                                            'your family history will help with understanding '
                                            'your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [{'link': 'https://www.ncbi.nlm.nih.gov/pubmed/17447017'},
                                {'link': 'https://www.ncbi.nlm.nih.gov/pubmed/25274026'},
                                {'link': 'https://www.ncbi.nlm.nih.gov/pubmed/26011257'},
                                {'link': 'https://www.ncbi.nlm.nih.gov/pubmed/19437058'}],
              'website_text_data': [{'accessed': 'Accessed June 14, 2019',
                                     'links': 'https://www.cdc.gov/cholesterol/ldl_hdl.htm',
                                     'title': '"LDL and HDL Cholesterol: "Bad" and "Good" '
                                              'Cholesterol" from Centers for Disease '
                                              'Control and Prevention'},
                                    {'accessed': 'Accessed June 14, 2019',
                                     'links': 'https://www.mayoclinic.org/diseases-conditions/high-blood-cholesterol/in-depth/hdl-cholesterol/art-200463881',
                                     'title': '"HDL cholesterol: How to boost your \'good\' '
                                              'cholesterol" from Mayo Clinic'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood concentration of high-density lipoprotein cholesterol"
trait_science = "High-density lipoprotein level is moderately heritable and about 30% of differences observed among people has a genetic origin. Our analysis was based on 312 SNPs associated with blood HDL level. These variants are scattered across 250 genes and jointly explain about 12% of the variation in HDL cholesterol level in human population."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence HDL cholesterol level. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
