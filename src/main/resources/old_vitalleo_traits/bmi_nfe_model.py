from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Body_mass_index"
trait_pmids = ["28448500"]  # 28448500 supplementary table 7
trait_heritability = 0.8
trait_snp_heritability = None
trait_explained = 0.12
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Body mass index is highly heritable. However, there is substantial variation in BMI heritability estimates, which range from 24% to 90%. Moreover, the genetic contribution to BMI appears to vary with age and may have a greater influence during childhood than adult life [1]. Our test is based on results of the genome wide association studies (GWAS) [2], which included over 180 000 individuals of European ancestry. The study identified 49 variants significantly associated with the BMI in European population. Together these SNPs explain about 12% of between people variance in BMIl [2].\nOur analyses use genetic data (genotypes) for those SNPs to calculate the polygenic score."
genes = ["AC007092.1", "AC007238.1", "AC008755.1", "AC022469.2", "AC090559.2", "AC091826.1", "AC103591.3", "AC108467.1",
         "AC109458.1", "AC131157.1", "ADCY3", "ADCY9", "AKAP6", "AL079352.1", "AL133166.1", "AL161630.1", "AL450423.1",
         "ATXN2L", "BDNF", "CADM2-AS2", "CRTC1", "DTX2P1", "ETV5", "FBXL17", "FHIT", "FTH1P5", "FTO", "GNAT2", "GPRC5B",
         "HNF4G", "KCTD10P1", "KCTD15", "KRT8P44", "LEMD2", "LMOD1", "MAP2K5", "MIR642B", "MRAS", "PACSIN1", "POC5",
         "RNA5SP125", "RNU4-17P", "RPL31P12", "SEC16B", "SLC39A8", "TMEM18", "TNNI3K", "TRIM66", "YWHAQP6"]

# na podstawie: doi: 10.1371/journal.pgen.1006528; to meta-analiza 60 wcześniejszych prac
# wzięłam dane dotyczące tylko populacji EUR, N=180423, ta praca badała też interakcje pomiędzy genotypem i aktywnościa fizyczną
# dane o rs: z tabeli SuppTable7 (połączone dane dla obu płci i osób aktywnych i nieaktywnych fizycznie)
# wybrałam warianty dla których wartość p(adjPA) <5e-08
# zostawiłam po jednym wariancie dla regionów do około 500 tys. pz (wybierałam wariant o większym efekcie)
# w sumie pozostało 49 SNPów
# efekty były podane, jako współczynniki beta z regresji, EA==wariant zwiększający BMI (bo wszystkie bety dodatnie)
# odziedziczalnosc: około 80 %, zmiennosc wyjasniona: 12% lub 16% (zależy od aktywnności fizycznej, wyższa wartość dla leniuchów)
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_bmi.txt i stats_bmi.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=1.3820, category_name='Low risk'),
                        QuantitativeCategory(from_=1.3820, to=1.75891, category_name='Average risk'),
                        QuantitativeCategory(from_=1.75891, category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs11209951', effect_allele='T', beta_value=0.032),
                        beta(rs_id='rs7551507', effect_allele='C', beta_value=0.023),
                        beta(rs_id='rs4130548', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs17024393', effect_allele='C', beta_value=0.084),
                        beta(rs_id='rs633715', effect_allele='C', beta_value=0.053),
                        beta(rs_id='rs2820315', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs2867125', effect_allele='C', beta_value=0.07),
                        beta(rs_id='rs6752378', effect_allele='A', beta_value=0.033),
                        beta(rs_id='rs2075171', effect_allele='G', beta_value=0.026),
                        beta(rs_id='rs759250', effect_allele='A', beta_value=0.03),
                        beta(rs_id='rs6804842', effect_allele='G', beta_value=0.023),
                        beta(rs_id='rs2365389', effect_allele='C', beta_value=0.028),
                        beta(rs_id='rs9852127', effect_allele='A', beta_value=0.03),
                        beta(rs_id='rs1720825', effect_allele='A', beta_value=0.026),
                        beta(rs_id='rs2035935', effect_allele='G', beta_value=0.044),
                        beta(rs_id='rs6809651', effect_allele='G', beta_value=0.048),
                        beta(rs_id='rs10938397', effect_allele='G', beta_value=0.035),
                        beta(rs_id='rs13107325', effect_allele='T', beta_value=0.051),
                        beta(rs_id='rs2112347', effect_allele='T', beta_value=0.023),
                        beta(rs_id='rs6870983', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs40067', effect_allele='G', beta_value=0.035),
                        beta(rs_id='rs6864049', effect_allele='G', beta_value=0.021),
                        beta(rs_id='rs943466', effect_allele='G', beta_value=0.024),
                        beta(rs_id='rs7757419', effect_allele='T', beta_value=0.028),
                        beta(rs_id='rs4715210', effect_allele='T', beta_value=0.039),
                        beta(rs_id='rs13191362', effect_allele='A', beta_value=0.036),
                        beta(rs_id='rs2430307', effect_allele='T', beta_value=0.043),
                        beta(rs_id='rs17405819', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs10968577', effect_allele='T', beta_value=0.03),
                        beta(rs_id='rs1928295', effect_allele='T', beta_value=0.022),
                        beta(rs_id='rs4929923', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs2049045', effect_allele='G', beta_value=0.043),
                        beta(rs_id='rs7124681', effect_allele='A', beta_value=0.026),
                        beta(rs_id='rs7138803', effect_allele='A', beta_value=0.03),
                        beta(rs_id='rs12429545', effect_allele='A', beta_value=0.031),
                        beta(rs_id='rs10132280', effect_allele='C', beta_value=0.023),
                        beta(rs_id='rs12885454', effect_allele='C', beta_value=0.022),
                        beta(rs_id='rs17522122', effect_allele='T', beta_value=0.021),
                        beta(rs_id='rs7141420', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs2241423', effect_allele='G', beta_value=0.033),
                        beta(rs_id='rs2531995', effect_allele='T', beta_value=0.031),
                        beta(rs_id='rs12446632', effect_allele='G', beta_value=0.036),
                        beta(rs_id='rs12325113', effect_allele='C', beta_value=0.03),
                        beta(rs_id='rs9930333', effect_allele='G', beta_value=0.084),
                        beta(rs_id='rs571312', effect_allele='A', beta_value=0.059),
                        beta(rs_id='rs757318', effect_allele='C', beta_value=0.021),
                        beta(rs_id='rs185350', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs11672660', effect_allele='C', beta_value=0.029),
                        beta(rs_id='rs2303108', effect_allele='C', beta_value=0.025)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Body Mass Index (BMI)'

about = ("Body mass index (BMI) is a measure of an adult's healthy weight, based on "
         'their current weight and height. It usually gives results in give '
         'categories: underweight, normal/healthy weight, overweight, and obese. These '
         'categories are a general guide and do not fit everyone perfectly, such as '
         'body builders, older adults, or athletes. But in general, a high BMI is a '
         'risk factor for heart disease, metabolic syndrome, diabetes, sleep apnea, '
         'and other health issues.')

how_your_result_was_calculated = ('Your result is a risk based on 49 genetic variants, specifically called '
                                  'SNPs, found in genes associated with body mass index. Overall, this explains '
                                  'about 12% of the genetic variation in body mass index between people, based '
                                  'on current scientific knowledge.')

other_factors_statement = ('Many factors impact body mass index, including diet, lifestyle, and '
                           'genetics. Genetics has a moderate overall impact on bone mineral density '
                           'when you consider all the other factors.')

other_factors = [{'content': 'A diet high in carbohydrates, processed foods, and bad fats '
                             'along with a sedentary lifestyle contribute to poor '
                             'body-mass-index (BMI). Switching to a whole-food based, low-carb '
                             'diet and incorporating HIIT (High intensity, interval training) '
                             'can radically improve your BMI.',
                  'title': 'Diet'},
                 {'content': 'Lifestyle can impact the effect of a high body mass index. '
                             'Smoking and lack of physical activity increase the risk of heart '
                             'disease and may lead to being overweight. These are important '
                             'risk factors to address if body mass index is also high. High '
                             'intensity, interval training can radically improve your BMI.',
                  'title': 'Lifestyle'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have a high body mass index. These types of risk '
                             'factors, which are usually due to rare genetic variants, are not '
                             'included in this test. If you have a family history of a high '
                             'body mass index, talk to your Practitioner or a '
                             'genetic counselor.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your body mass index and other factors, you may '
                                           'have an average risk for heart disease, diabetes, or other '
                                           'health issues.',
                           'High risk': 'Depending on your body mass index and other factors, you may '
                                        'have a higher risk for heart disease, diabetes, or other health '
                                        'issues.',
                           'Low risk': 'Depending on your body mass index and other factors, you may '
                                       'have a lower risk for heart disease, diabetes, or other health '
                                       'issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average body mass index (BMI).',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher body mass index (BMI).',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower body mass index (BMI).'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent a high body '
                                             'mass index and stay healthy. Certain blood tests can also '
                                             'check your risk factors for heart disease, diabetes, and '
                                             'other health issues. Talk to your practitioner about what is '
                                             'right for you.',
                             'High risk': 'There are things you can do to try to prevent a high body mass '
                                          'index and stay healthy. Certain blood tests can also check your '
                                          'risk factors for heart disease, diabetes, and other health '
                                          'issues. Talk to your practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent a high body mass '
                                         'index and stay healthy. Certain blood tests can also check your '
                                         'risk factors for heart disease, diabetes, and other health '
                                         'issues. Talk to your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a high body mass index. Consider '
                                                'talking to your practitioner about ordering '
                                                'blood tests to check your risk factors for '
                                                'health issues related to a high body mass '
                                                'index, and possible next steps that you can '
                                                'take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'In general, try to pick foods that are low in '
                                                'calories, saturated fat, sodium, and trans fat. '
                                                'These include lean meats, nuts, fat-free or '
                                                'low-fat dairy products, fruits, and vegetables. '
                                                'A diet higher in plant products such as '
                                                'vegetables and low-glycemic fruits like '
                                                'berries, good fats, and quality proteins will '
                                                'help you to lower your risk.  Talk to your '
                                                'practitioner to determine whether a '
                                                'Mediterranean plan, Modified Mediterranean, or '
                                                'a low-carb/keto plan is right for you. Sticking '
                                                'to recommended limits for alcohol intake (2 '
                                                'drinks max/day for men, 1 drink/day max for '
                                                'women) and portion sizes (especially when '
                                                'dining out) can also help maintain a healthy '
                                                'weight.',
                                     'title': 'Diet'},
                                    {'content': 'If you smoke, try to quit -- smoking increases '
                                                'the risk for heart disease. As well, try to get '
                                                "your body moving. It's recommended that adults "
                                                'get 2.5 hours of moderate physical activity, '
                                                'like brisk walking or biking, every week. Not '
                                                'only can this lower your risk of heart disease, '
                                                'but it can also help you maintain a healthy '
                                                'weight. HIIT exercise (High Intensity-Interval '
                                                'Training) will help to stimulate the body along '
                                                'with the good hormones necessary to mobilize '
                                                'fat and build muscle.',
                                     'title': 'Lifestyle'},
                                    {'content': 'High body mass index can run in families, and '
                                                'these risk factors are not included in this '
                                                'test. If you have a close relative (parent, '
                                                'child, sibling) with a high body mass index, '
                                                'speak to your practitioner about your family '
                                                'history. If you have blood tests to look for '
                                                'health risk factors, information about your '
                                                'family history will help with understanding '
                                                'your blood test results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your risk factors for health issues '
                                             'related to a high body mass index, like heart '
                                             'disease or diabetes. Talk to them about this and '
                                             'possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Try to pick foods that are low in calories, '
                                             'saturated fat, sodium, and trans fat. These '
                                             'include lean meats, nuts, fat-free or low-fat '
                                             'dairy products, fruits, and vegetables. A diet '
                                             'higher in plant products such as vegetables and '
                                             'low-glycemic fruits like berries, good fats, and '
                                             'quality proteins will help you to lower your '
                                             'risk.  Talk to your practitioner to determine '
                                             'whether a Mediterranean plan, Modified '
                                             'Mediterranean, or a low-carb/keto plan is right '
                                             'for you. Sticking to recommended limits for '
                                             'alcohol intake (2 drinks max/day for men, 1 '
                                             'drink/day max for women) and portion sizes '
                                             '(especially when dining out) can also help '
                                             'maintain a healthy weight.',
                                  'title': 'Diet'},
                                 {'content': 'If you smoke, now is a good time to quit -- '
                                             'smoking increases the risk for heart disease. As '
                                             "well, try to get your body moving. It's "
                                             'recommended that adults get 2.5 hours of moderate '
                                             'physical activity, like brisk walking or biking, '
                                             'every week. Not only can this lower your risk of '
                                             'heart disease, but it can also help you maintain a '
                                             'healthy weight. HIIT exercise (High '
                                             'Intensity-Interval Training) will help to '
                                             'stimulate the body along with the good hormones '
                                             'necessary to mobilize fat and build muscle.',
                                  'title': 'Lifestyle'},
                                 {'content': 'High body mass index can run in families, and '
                                             'these risk factors are not included in this test. '
                                             'If you have a close relative (parent, child, '
                                             'sibling) with a high body mass index, speak to '
                                             'your practitioner about your family history. If '
                                             'you have blood tests to look for health risk '
                                             'factors, information about your family history '
                                             'will help with understanding your blood test '
                                             'results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a high body mass index. Consider talking to '
                                            'your practitioner about ordering blood tests to '
                                            'check your risk factors for health issues related '
                                            'to a high body mass index, and possible next steps '
                                            'that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': "It's always a good idea to try to pick foods that "
                                            'are low in calories, saturated fat, sodium, sugar, '
                                            'and trans fat. These include lean meats, nuts, '
                                            'fat-free or low-fat dairy products, fruits, and '
                                            'vegetables. A diet higher in plant products such as '
                                            'vegetables and low-glycemic fruits like berries, '
                                            'good fats, and quality proteins will help you to '
                                            'lower your risk. Talk to your practitioner to '
                                            'determine whether a Mediterranean plan, Modified '
                                            'Mediterranean, or a low-carb/keto plan is right for '
                                            'you. Sticking to recommended limits for alcohol '
                                            'intake (2 drinks max/day for men, 1 drink/day max '
                                            'for women) and portion sizes (especially when '
                                            'dining out) can also help maintain a healthy '
                                            'weight.',
                                 'title': 'Diet'},
                                {'content': 'If you smoke, try to quit -- smoking increases the '
                                            'risk for heart disease. As well, try to get your '
                                            "body moving. It's recommended that adults get 2.5 "
                                            'hours of moderate physical activity, like brisk '
                                            'walking or biking, every week. Not only can this '
                                            'lower your risk of heart disease, but it can also '
                                            'help you maintain a healthy weight. HIIT exercise '
                                            '(High Intensity-Interval Training) will help to '
                                            'stimulate the body along with the good hormones '
                                            'necessary to mobilize fat and build muscle.',
                                 'title': 'Lifestyle'},
                                {'content': 'High body mass index can run in families, and these '
                                            'risk factors are not included in this test. If you '
                                            'have a close relative (parent, child, sibling) with '
                                            'a high body mass index, speak to your practitioner '
                                            'about your family history. If you have blood tests '
                                            'to look for health risk factors, information about '
                                            'your family history will help with understanding '
                                            'your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 23, 2019.\n',
                                     'links': 'https://medlineplus.gov/ency/article/007196.htm',
                                     'title': '"Body mass index" from Medline Plus at '
                                              'National Institutes of Health'},
                                    {'accessed': 'Accessed June 23, 2019',
                                     'links': 'https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/index.html',
                                     'title': '"About Adult BMI" from Centers for Disease '
                                              'Control and Prevention'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Body mass index"
trait_science = "Body mass index is highly heritable - about 80% of differences observed among people has a genetic origin. Our analysis was based on 49 SNPs associated with BMI. These variants jointly explain about 12% of the variation in BMI between people."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the body mass index. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
