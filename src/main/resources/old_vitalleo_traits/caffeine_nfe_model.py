from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Caffeine_metabolism"
trait_pmids = ["27702941",
               "27509179"]  # GWAS statistics from 27702941 Table1 [trait:17X/137X;beta=Z-score;p-values from fixed effect meta-analysis]
trait_heritability = 0.89  # PMID:27509179
trait_snp_heritability = None  # as fraction
trait_explained = None  # as fraction
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"  # "Single SNP analysis" if one snip
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "Caffeine sensitivity is highly heritable, with heritability estimates of about 0.89 [1]. Our test is based on results of the meta-analysis study, which combained results of six previously published genome wide association studies (GWAS) [2]. Together these studies included about 6000 individuals of European ancestry and identified 24 variants significantly associated with caffeine sensitivity.\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score. "
genes = ["RF00017", "AC073332.1", "RF00092", "AC012435.2", "CLK3", "CYP1A1", "NUMBL", "CYP2F2P", "AC008537.3", "CYP2A6",
         "CYP2A7", "CYP2A7P2", "CYP2G1P", "CYP2B6"]

# I took all “independent” SNPs  with p-value < 5e-08 [authors criteria for LD pruning: LD <0.3 or distance between markers > 500 kb]
# Two variants not in our gnomAD database - removed
# study design: meta-analysis of the summary statistics from six previously published GWAS; all studies were on individuals of European ancestry
# N:5323-6147
# description_genotype["low_trait"] = "hom" # for one snip test
# description_genotype["average_trait"] = "het" #for one snip test
# description_genotype["high_trait"] = "hom" # for one snip test

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-3.86, category_name='High risk'),
                        QuantitativeCategory(from_=-3.86, to=47.59, category_name='Average risk'),
                        QuantitativeCategory(from_=47.59, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs6968554', effect_allele='A', beta_value=-8.07),
                        beta(rs_id='rs10275488', effect_allele='T', beta_value=-8.44),
                        beta(rs_id='rs2892838', effect_allele='A', beta_value=-5.98),
                        beta(rs_id='rs11400459', effect_allele='A', beta_value=6.01),
                        beta(rs_id='rs10683220', effect_allele='G', beta_value=6.23),
                        beta(rs_id='rs12909047', effect_allele='A', beta_value=6.11),
                        beta(rs_id='rs62005807', effect_allele='C', beta_value=-7.58),
                        beta(rs_id='rs2470893', effect_allele='T', beta_value=6.65),
                        beta(rs_id='rs2472297', effect_allele='T', beta_value=7.85),
                        beta(rs_id='rs66500423', effect_allele='T', beta_value=9.58),
                        beta(rs_id='rs4803373', effect_allele='C', beta_value=5.83),
                        beta(rs_id='rs78011401', effect_allele='T', beta_value=6.9),
                        beta(rs_id='rs11668399', effect_allele='C', beta_value=-5.78),
                        beta(rs_id='rs56113850', effect_allele='T', beta_value=6.37),
                        beta(rs_id='rs56267346', effect_allele='A', beta_value=9.59),
                        beta(rs_id='rs28399442', effect_allele='A', beta_value=-6.13),
                        beta(rs_id='rs67210567', effect_allele='T', beta_value=6.48),
                        beta(rs_id='rs7260629', effect_allele='T', beta_value=5.94),
                        beta(rs_id='rs5828081', effect_allele='AT', beta_value=6.73),
                        beta(rs_id='rs28602288', effect_allele='T', beta_value=-5.71),
                        beta(rs_id='rs184589612', effect_allele='T', beta_value=-6.64),
                        beta(rs_id='rs72480748', effect_allele='A', beta_value=-5.92),
                        beta(rs_id='rs10425738', effect_allele='A', beta_value=-6.08),
                        beta(rs_id='rs56881024', effect_allele='A', beta_value=-5.65)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Caffeine sensitivity'

about = ("Caffeine is consumed worldwide, and the rate at which someone's body "
         'metabolizes it may be slower or faster. People who metabolize caffeine more '
         'slowly tend to be more sensitive to it, which also happens more commonly as '
         'we age. Those who are sensitive to caffeine may feel jittery or restless, '
         'and tend to consume less of it overall. Other symptoms of caffeine '
         'sensitivity include stomach upset, sleep problems, headaches, and mood '
         'swings. People who metabolize caffeine faster tend to consume a higher '
         'amount of caffeine, and may be less sensitive to its effects. Caffeine is a '
         'stimulant, so it can disrupt sleep and reduce sleep quality. Caffeine '
         'sensitivity is in part inherited.')

how_your_result_was_calculated = ('Your result is a risk based on 24 genetic variants, specifically called '
                                  'SNPs, in genes associated with caffeine sensitivity.')

other_factors_statement = ("Many factors impact a person's caffeine sensitivity, including lifestyle, "
                           'medications, body mass, age, and genetics. Genetics has a relatively high '
                           'overall impact on caffeine sensitivity when you consider all the other '
                           'factors.')

other_factors = [{'content': 'Certain cultures and customs incorporate caffeinenated drinks or '
                             'food, like tea, coffee, or chocolate. Someone who is more '
                             'sensitive to caffeine may encounter more issues if they are '
                             'living in a culture where caffeine has an everyday presence, and '
                             'they consume it regularly.',
                  'title': 'Lifestyle'},
                 {'content': 'Some medications (like certain headache medications) contain '
                             'caffeine, and can increase feelings of jitteriness and '
                             'restlessness. Other medications (like birth control pills for '
                             'women) can cause the body to metabolize caffeine more slowly, '
                             'and make someone more sensitive to its effects.',
                  'title': 'Medications'}]

what_your_result_means = {'Average risk': 'Depending on your caffeine intake and other factors, you may '
                                           'have an average risk for feelings of jitteriness or sleep '
                                           'disturbances due to caffeine sensitivity. If you consume '
                                           'caffeine, you tend to have as much as the average person.',
                           'High risk': 'Depending on your caffeine intake and other factors, you may '
                                        'have a higher risk for feelings of jitteriness or sleep '
                                        'disturbances due to caffeine sensitivity. If you consume '
                                        'caffeine, you tend to have less than the average person.',
                           'Low risk': 'Depending on your caffeine intake and other factors, you may '
                                       'have a lower risk for feelings of jitteriness or sleep '
                                       'disturbances due to caffeine sensitivity. If you consume '
                                       'caffeine, you tend to have more than the average person.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average sensitivity to caffeine.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher sensitivity to caffeine.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower sensitivity to caffeine.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to manage caffeine '
                                             'sensitivity and stay healthy. Certain tests can also check '
                                             'your risk factors for caffeine sensitivity. Talk to your '
                                             'practitioner about what is right for you.',
                             'High risk': 'There are things you can do to try to manage caffeine '
                                          'sensitivity and stay healthy. Certain tests can also check your '
                                          'risk factors for caffeine sensitivity. Talk to your '
                                          'practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to manage caffeine '
                                         'sensitivity and stay healthy. Certain tests can also check your '
                                         'risk factors for caffeine sensitivity. Talk to your practitioner '
                                         'about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for caffeine sensitivity. Consider talking '
                                                'to your practitioner about ordering certain '
                                                'tests to check for health issues related to '
                                                'caffeine sensitivity, such as sleep tests if '
                                                'getting good sleep is hard for you. They can '
                                                'tell you more about this and possible next '
                                                'steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Keeping a food diary to get a sense of your '
                                                'caffeine intake (amount and time of day) is '
                                                'always a good idea. 400mg of caffeine is the '
                                                'daily recommended max. This is roughly 4 '
                                                'standard cups of brewed coffee, 2 energy '
                                                'drinks, or 10 cans of caffeinated soda. Keeping '
                                                'a sleep diary can help if caffeine is '
                                                'disrupting your sleep. You could try slowly '
                                                'removing caffeine from your diet to see if your '
                                                'sleep quality improves.',
                                     'title': 'Diet'}],
                   'High risk': [{'content': 'Your practitioner may be able to order certain '
                                             'tests to check for health issues related to '
                                             'caffeine sensitivity, such as sleep tests if '
                                             'getting good sleep is a problem for you. Talk to '
                                             'them about this and possible next steps that you '
                                             'can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Since you have a higher tendency for caffeine '
                                             'sensitivity, consider if and when you want to '
                                             'consume things that contain caffeine. Try to keep '
                                             'track of your caffeine intake (amount and time of '
                                             'day) with a food diary. 400mg of caffeine is the '
                                             'daily recommended max. This is roughly 4 standard '
                                             'cups of brewed coffee, 2 energy drinks, or 10 cans '
                                             'of caffeinated soda. Keeping a sleep diary can '
                                             'help if caffeine is disrupting your sleep. You '
                                             'could try slowly removing caffeine from your diet '
                                             'to see if your sleep quality improves.',
                                  'title': 'Diet'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for caffeine sensitivity. Consider talking to your '
                                            'practitioner about ordering certain tests to check '
                                            'for health issues related to caffeine sensitivity, '
                                            'such as sleep tests if getting good sleep is hard '
                                            'for you. They can tell you more about this and '
                                            'possible next steps that you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Keeping a food diary to get a sense of your '
                                            'caffeine intake (amount and time of day) is a good '
                                            'idea, particularly because you might have more than '
                                            'you realize. 400mg of caffeine is the daily '
                                            'recommended max. This is roughly 4 standard cups of '
                                            'brewed coffee, 2 energy drinks, or 10 cans of '
                                            'caffeinated soda. Keeping a sleep diary can help if '
                                            'caffeine is disrupting your sleep. You could try '
                                            'slowly removing caffeine from your diet to see if '
                                            'your sleep quality improves.',
                                 'title': 'Diet'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_short_description = "Caffeine sensitivity"  # [paraxanthine to caffeine ratio=17X/137X] I would like to leave caffeine metabolism
trait_science = "Caffeine metabolism is highly heritable - about 90% of differences in the rate of caffeine clearance has a genetic origin. Our analysis was based on 24 SNPs associated with caffeine sensitivity."
trait_genotypes = ["hom", "het", "hom"]  # for one snip analysis
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence caffeine sensitivity. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
