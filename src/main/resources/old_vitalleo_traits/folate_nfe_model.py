from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Blood_folate"
trait_pmids = [
    "23754956"]  # 23754956 table1; p-value: combined, effect: average weighted by number of individuals from each study population [37283 Iceland; 2812 DanishHealth2006; 5481 Danishinter99]
trait_heritability = 0.56  # in this study estimated as 0.17
trait_snp_heritability = None
trait_explained = 0.01
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test = "Blood folate leve is moderately heritable, with heritability estimates of 17-56% [1]. Our test is based on results of the genome wide association studies (GWAS) [1], which included over 46, 000 individuals of European ancestry. The study reported only two variants significantly associated with folate blood concentration. Together they explain only 1% of between people variance in folate level [1].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["FOLR3", "MTHFR"]

# Iceland (37283) and Denmark populations (8293); imputation based on WGS (1176 from Iceland population);
# WES (2000 Danes) performed to design sequencing arrays used for genotyping in this population

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.10, category_name='High risk'),
                        QuantitativeCategory(from_=0.10, to=0.2891, category_name='Average risk'),
                        QuantitativeCategory(from_=0.2891, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs652197', effect_allele='C', beta_value=0.069),
                        beta(rs_id='rs1801133', effect_allele='G', beta_value=0.11)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Vitamin B9 (folate)'

about = ('Vitamin B9, also called folate, is part of the B vitamin group. Folate is '
         'naturally found in some foods. Folate is critical to making DNA and helping '
         'cells divide, which is important for red blood cells. Low folate levels can '
         'be a risk factor for a form of anemia (a type of blood disease) and other '
         "health issues. Folate also helps a baby's neural tube (brain and spine) form "
         'during pregnancy. Folic acid is a form of folate that is found in '
         'supplements and fortified foods - it is synthetic and is not absorbed well '
         'by the body. Folate specifically helps to assist in methylation and correct '
         'homocysteine levels.  An over-production of homocysteine can cause the '
         'development of heart disease.')

how_your_result_was_calculated = ('Your result is a risk based on 2 genetic variants, specifically called SNPs, '
                                  'found in 2 genes associated with blood folate level. Overall, this explains '
                                  'as low as 1% of the genetic variation in folate level in the human '
                                  'population, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact folate levels, including diet, biological sex, age, '
                           'other health issues, lifestyle, and genetics. Genetics has a limited overall '
                           'impact on folate level when you consider all the other factors.')

other_factors = [{'content': 'Folate supplements can help raise vitamin B9 to recommended '
                             'daily levels. Requirements can change due to a number of health '
                             'and lifestyle factors including age. Women who are trying to '
                             'have a baby, pregnant, or breastfeeding need higher vitamin B9 '
                             'levels.',
                  'title': 'Supplements'},
                 {'content': 'Eat more foods rich in B9 including lentils, asparagus, spinach, '
                             'brocoli, cabbage, cauliflower, avocadoes, mangos and lettuces.',
                  'title': 'Diet'},
                 {'content': "People with Crohn's disease, ulcerative colitis, or celiac "
                             'disease may have lower folate levels.  This may also cause '
                             'problems with methylation and conditions such as heart disease, '
                             'anxiety, and hormonal issues.',
                  'title': 'Other Health Issues'}]

what_your_result_means = {'Average risk': 'You may be at an average risk for making DNA and helping '
                                           'cells divide, which is important for red blood cells. Low '
                                           'folate levels can be a risk factor for a form of anemia (a '
                                           'type of blood disease) and other health issues. Folate also '
                                           "helps a baby's neural tube (brain and spine) form during "
                                           'pregnancy. Folate specifically helps to assist in '
                                           'methylation and correct homocysteine levels. An '
                                           'over-production of homocysteine can cause the development of '
                                           'heart disease.',
                           'High risk': 'Folate is critical to making DNA and helping cells divide, '
                                        'which is important for red blood cells. Low folate levels can '
                                        'be a risk factor for a form of anemia (a type of blood disease) '
                                        "and other health issues. Folate also helps a baby's neural tube "
                                        '(brain and spine) form during pregnancy. Folate specifically '
                                        'helps to assist in methylation and correct homocysteine '
                                        'levels.  An over-production of homocysteine can cause the '
                                        'development of heart disease.',
                           'Low risk': 'Depending on your folate level and other factors, you may be at '
                                       'a low risk for making DNA and helping cells divide, which is '
                                       'important for red blood cells. Low folate levels can be a risk '
                                       'factor for a form of anemia (a type of blood disease) and other '
                                       "health issues. Folate also helps a baby's neural tube (brain and "
                                       'spine) form during pregnancy. Folate specifically helps to '
                                       'assist in methylation and correct homocysteine levels. An '
                                       'over-production of homocysteine can cause the development of '
                                       'heart disease.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you may be more likely '
                                    'to have an average blood folate level.',
                    'High risk': 'Based on the genetic factors tested, you may be more likely to '
                                 'have a lower blood folate level.',
                    'Low risk': 'Based on the genetic factors tested, you may be more likely to '
                                'have a higher blood folate level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent low folate '
                                             'levels and stay healthy.  Talk to your practitioner about '
                                             'what is right for you.',
                             'High risk': 'There are things you can do to try to prevent low folate levels '
                                          'and stay healthy.  Talk to your practitioner about what is '
                                          'right for you.',
                             'Low risk': 'There are things you can do to try to prevent low folate levels '
                                         'and stay healthy.  Talk to your practitioner about what is right '
                                         'for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a low folate level. Consider talking '
                                                'to your practitioner about tests to check your '
                                                'folate level, and possible next steps you can '
                                                'take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'The recommended amount of folate per day for '
                                                'adults is 400mcg. Women who are pregnant or '
                                                'trying to get pregnant need 600 mcg to reduce '
                                                'the risk that their baby will have spina bifida '
                                                'or other neural tube disorders. This amount is '
                                                'often in a prenatal vitamin. There are some key '
                                                'supplements available over the counter, but '
                                                'talk to your practitioner about the dosage to '
                                                'take. Too much folic acid can be harmful. There '
                                                'are several other factors to consider when '
                                                'looking at dosage; be sure to talk to your '
                                                'Practitioner.',
                                     'title': 'Supplements'},
                                    {'content': 'A balanced and healthy diet is always a good '
                                                'idea. Some vegetables (like asparagus and '
                                                'spinach), fruits (like oranges), nuts, beans, '
                                                'peas, and beef liver are naturally rich in '
                                                'folate.',
                                     'title': 'Diet'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your folate level. Talk to them about this '
                                             'and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'The recommended amount of folate per day for '
                                             'adults is 400mcg.  Women who are pregnant or '
                                             'trying to get pregnant need 600 mcg to reduce the '
                                             'risk that their baby will have spina bifida or '
                                             'other neural tube disorders. This amount is often '
                                             'in a prenatal vitamin. There are some key '
                                             'supplements available over the counter, but talk '
                                             'to your practitioner about the dosage to take. Too '
                                             'much folic acid can be harmful.  There are several '
                                             'other factors to consider when looking at dosage; '
                                             'be sure to talk to your Practitioner.',
                                  'title': 'Supplements'},
                                 {'content': 'Some vegetables (like asparagus and spinach), '
                                             'fruits (like oranges), nuts, beans, peas, and beef '
                                             'liver are naturally rich in folate.',
                                  'title': 'Diet'},
                                 {'content': "If you have Crohn's disease, ulcerative colitis, "
                                             'or celiac disease you may naturally have lower '
                                             'folate levels. This may also cause problems with '
                                             'methylation and conditions such as heart disease, '
                                             'anxiety, and hormonal issues. Speak to your '
                                             'Practitioner.',
                                  'title': 'Other Health Issues'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a low folate level. Consider talking to your '
                                            'practitioner about tests to check your folate '
                                            'level, and possible next steps you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'The recommended amount of folate per day for adults '
                                            'is 400mcg. Women who are pregnant or trying to get '
                                            'pregnant need 600 mcg to reduce the risk that their '
                                            'baby will have spina bifida or other neural tube '
                                            'disorders. This amount is often in a prenatal '
                                            'vitamin. There are some key supplements available '
                                            'over the counter, but talk to your practitioner '
                                            'about the dosage to take. Too much folic acid can '
                                            'be harmful. There are several other factors to '
                                            'consider when looking at dosage; be sure to talk to '
                                            'your Practitioner.',
                                 'title': 'Supplements'},
                                {'content': 'A balanced and healthy diet is always a good idea. '
                                            'Some vegetables (like asparagus and spinach), '
                                            'fruits (like oranges), nuts, beans, peas, and beef '
                                            'liver are naturally rich in folate.',
                                 'title': 'Diet'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 18, 2019',
                                     'links': 'https://ods.od.nih.gov/factsheets/Folate-Consumer/',
                                     'title': '"Folate Fact Sheet for Consumers" from '
                                              'National Institutes of Health'},
                                    {'accessed': 'Accessed June 19, 2019',
                                     'links': 'https://www.healthlinkbc.ca/healthlinkbc-files/folate-neural-tube-defects',
                                     'title': '"Pregnancy and Nutrition: Folate and '
                                              'Preventing Neural Tube Defects" from '
                                              'HealthLinkBC'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood folate level"
trait_science = "Blood folate level is partially controlled by genetic factors. We analyzed 2 SNPs associated with folate level, which accounted for as little as 1% of the variation in this vitamin level between people."
trait_test_details = "We determined your genotype at two genomic positions, which are known to influence the blood folate level. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
