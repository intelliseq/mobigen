from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Fasting_glucose"
trait_pmids = ["20081858"]  # 20081858 table 2
trait_heritability = 0.3
trait_snp_heritability = None
trait_explained = 0.03
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Fasting glucose level is moderately heritable, with h"+chr(0x00b2)+" estimate of about 30% [1]. Our test is based on results of the genome wide association study (GWAS) [1], which included over 45,000 non-diabetic individuals of European ancestry. The study identified 11 variants significantly associated with the fasting glucose. Together these SNPs explain about 3% of between people variance in blood glucose level [2].\nOur analyses use genetic data (genotypes) for all those SNPs to calculate the polygenic score."
genes = ["AC090582.1", "AC112503.1", "AL162419.1", "FADS1", "G6PC2", "GCK", "GCKR", "GTF3AP5", "MTNR1B", "RPS15AP30",
         "SLC30A8"]

# na podstawie: doi:10.1038/ng.520. (to meta-analiza wyników 21 badań gwas)
# poziom glukozy na czczo dla co najmniej 45 tys. osób pochodzenia europejskiego nie chorujących na cukrzycę [w mmol/L];
# dane o rs: z Tabeli2 (główny artykuł), effekt allelu podany jako współczynnik regresji (dane raczej nie były transformowane)
# pozostawiłam tylko warianty z p-value poniżej 5e-08; w sumie 11
# odziedziczalnosc: około 30 %, zmiennosc wyjaśniona przez istotne warianty około 3% (ale to dla 14 wariantów z tabeli, ja wyrzuciłam z nich 3)
# model na podstawie symulacji (plik symul.py), wyniki w pliku dist_fg.txt i stats_fg.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
# czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/);
# (file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.2979, category_name='Low risk'),
                        QuantitativeCategory(from_=0.2979, to=0.6991, category_name='Average risk'),
                        QuantitativeCategory(from_=0.6991, category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs560887', effect_allele='C', beta_value=0.075),
                        beta(rs_id='rs10830963', effect_allele='G', beta_value=0.067),
                        beta(rs_id='rs4607517', effect_allele='A', beta_value=0.062),
                        beta(rs_id='rs2191349', effect_allele='T', beta_value=0.03),
                        beta(rs_id='rs780094', effect_allele='C', beta_value=0.029),
                        beta(rs_id='rs11708067', effect_allele='A', beta_value=0.027),
                        beta(rs_id='rs7944584', effect_allele='A', beta_value=0.021),
                        beta(rs_id='rs174550', effect_allele='T', beta_value=0.017),
                        beta(rs_id='rs7034200', effect_allele='A', beta_value=0.018),
                        beta(rs_id='rs13266634', effect_allele='C', beta_value=0.027),
                        beta(rs_id='rs7903146', effect_allele='T', beta_value=0.23)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Glucose Levels'

about = ('After not having food or drink for a few hours (fasting), the body releases '
         "glucose (sugar) into the blood to keep the body's cells working. Sometimes, "
         "the body's glucose control is not ideal, which can lead to fasting glucose "
         'levels that are too high. Repeated high fasting glucose levels can be a risk '
         'factor for heart disease and diabetes. Several things contribute to fasting '
         'glucose levels, including genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 11 genetic variants, specifically called '
                                  'SNPs, found in genes associated with blood glucose level. Overall, this '
                                  'explains about 3% of the genetic variation in fasting glucose level in the '
                                  'human population, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact fasting glucose levels, including diet, lifestyle, '
                           'ethnic background, and genetics. Although several genes are associated with '
                           'our fasting glucose level, genetics has a moderate overall impact on this '
                           'when you consider all the other factors.')

other_factors = [{'content': 'Diet can affect fasting glucose levels. Having a diet that is '
                             'high in carbohydrates (sugars) and calories, not eating at '
                             'regular times, or skipping meals can cause blood glucose to '
                             'spike up.',
                  'title': 'Diet'},
                 {'content': 'Lifestyle can impact the effect of a high fasting glucose level. '
                             'Smoking and lack of physical activity increases the risk of '
                             'being overweight, which can lead to diabetes and other health '
                             'issues. These are important risk factors to address if the '
                             'fasting glucose level is also high.',
                  'title': 'Lifestyle'},
                 {'content': 'People that are African American, Hispanic/Latino American, '
                             'American Indian, or Alaska Native have a higher risk for '
                             'diabetes.',
                  'title': 'Ethnic Background'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have a high fasting blood glucose level or '
                             'diabetes. These types of risk factors, which are usually due to '
                             'rare genetic variants, may not be included in this test. If you '
                             'have a family history of these issues, talk to your '
                             'Practitioner.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your blood glucose levels and other factors, '
                                           'you may have an average risk for diabetes and other health '
                                           'issues.',
                           'High risk': 'Depending on your blood glucose levels and other factors, you '
                                        'may have a higher risk for diabetes and other health issues.',
                           'Low risk': 'Depending on your blood glucose levels and other factors, you '
                                       'may have a lower risk for diabetes and other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average fasting blood glucose level.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher fasting blood glucose level.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower fasting blood glucose level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent diabetes and '
                                             'stay healthy. Certain blood tests can also check for signs '
                                             'of diabetes. Talk to your practitioner about what is right '
                                             'for you.',
                             'High risk': 'There are things you can do to try to prevent diabetes and stay '
                                          'healthy. Certain blood tests can also check for signs of '
                                          'diabetes. Talk to your practitioner about what is right for '
                                          'you.',
                             'Low risk': 'There are things you can do to try to prevent diabetes and stay '
                                         'healthy. Certain blood tests can also check for signs of '
                                         'diabetes. Talk to your practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a high fasting glucose level or '
                                                'diabetes. Consider talking to your practitioner '
                                                'about blood tests to check your glucose levels, '
                                                'and possible next steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': "If you're overweight, losing even a small "
                                                'amount can go a long way to lower your risk to '
                                                "get diabetes. It's always recommended that "
                                                'adults get 2.5 hours of moderate physical '
                                                'activity, like brisk walking or biking, every '
                                                'week. Not only can this help you maintain a '
                                                'healthy weight, it reduces your chance to get '
                                                'diabetes, heart disease, and other health '
                                                'issues. Managing stress and trying to get '
                                                'enough sleep every night is also helpful.',
                                     'title': 'Lifestyle'},
                                    {'content': 'Mediterranean or a more ketogenic diet; lower '
                                                'in refined carbohydrates, grains, sugar, and '
                                                'foods that turn into sugar. Try to eat a '
                                                'balanced diet that includes non-starchy '
                                                'vegetables (like salad or green beans), far '
                                                'less carbs (bread, potatoes, cereals, and '
                                                'pasta) and fewer starches (like rice or corn). '
                                                'Eliminate sugary items (including drinks, like '
                                                'sweetened tea or juices). If you eat smaller '
                                                'meals more often, that can help balance blood '
                                                'sugar levels. Skipping meals is not a good idea '
                                                'because it can throw off blood sugar levels. If '
                                                'you like, consider discussing these options '
                                                'with your Practitioner.',
                                     'title': 'Diet'},
                                    {'content': 'Some forms of diabetes can run in families, and '
                                                'these risk factors are not included in this '
                                                'test. If you have a close relative (parent, '
                                                'child, sibling) with diabetes, speak to your '
                                                'practitioner about your family history. If you '
                                                'have blood tests to check your fasting glucose '
                                                'level, information about your family history '
                                                'will help with understanding your blood test '
                                                'results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order blood tests '
                                             'to check your fasting blood glucose level or other '
                                             'risk factors for diabetes. Talk to them about this '
                                             'and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': "If you're overweight, losing even a small amount "
                                             'can go a long way to lower your risk to get '
                                             "diabetes. It's recommended that adults get a "
                                             'minimum of 2.5 hours of moderate physical '
                                             'activity, like brisk walking or biking, every '
                                             'week. Not only can this help you maintain a '
                                             'healthy weight, it reduces your chance to get '
                                             'diabetes, heart disease, and other health issues. '
                                             'Managing stress and trying to get enough sleep '
                                             'every night is also helpful.',
                                  'title': 'Lifestyle'},
                                 {'content': 'Modified mediterranean or a more ketogenic diet; '
                                             'lower in refined carbohydrates, grains, sugar, and '
                                             'foods that turn into sugar.  Try to eat a balanced '
                                             'diet that includes non-starchy vegetables (like '
                                             'salad or green beans), far less carbs (bread, '
                                             'potatoes, cereals, and pasta) and fewer starches '
                                             '(like rice or corn).  Eliminate sugary items '
                                             '(including drinks, like sweetened tea or juices).  '
                                             'If you eat smaller meals more often, that can help '
                                             'balance blood sugar levels. Skipping meals is not '
                                             'a good idea because it can throw off blood sugar '
                                             'levels. If you like, consider discussing these '
                                             'options with your Practitioner.',
                                  'title': 'Diet'},
                                 {'content': 'Some forms of diabetes can run in families, and '
                                             'some of these risk factors may not be included in '
                                             'this test. If you have a close relative (parent, '
                                             'child, sibling) with diabetes, speak to your '
                                             'practitioner about your family history. If you '
                                             'have blood tests to check your fasting glucose '
                                             'level, information about your family history will '
                                             'help with understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a high fasting glucose level or diabetes. '
                                            'Consider talking to your practitioner about blood '
                                            'tests to check your glucose levels, and possible '
                                            'next steps you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': "If you're overweight, losing even a small amount "
                                            'can go a long way to lower your risk to get '
                                            "diabetes. It's always recommended that adults get "
                                            '2.5 hours of moderate physical activity, like brisk '
                                            'walking or biking, every week. Not only can this '
                                            'help you maintain a healthy weight, it reduces your '
                                            'chance to get diabetes, heart disease, and other '
                                            'health issues. Managing stress and trying to get '
                                            'enough sleep every night is also helpful.',
                                 'title': 'Lifestyle'},
                                {'content': 'Try to eat a balanced diet that includes '
                                            'non-starchy vegetables (like salad or green beans), '
                                            'far less carbs (bread, potatoes, cereals, and '
                                            'pasta) and fewer starches (like rice or corn). '
                                            'Reduce sugary items (including drinks, like '
                                            'sweetened tea or juices). If you eat smaller meals '
                                            'more often, that can help balance blood sugar '
                                            'levels. Skipping meals is not a good idea because '
                                            'it can throw off blood sugar levels. If you like, '
                                            'consider discussing these options with your '
                                            'Practitioner.',
                                 'title': 'Diet'},
                                {'content': 'Some forms of diabetes can run in families, and '
                                            'these risk factors are not included in this test. '
                                            'If you have a close relative (parent, child, '
                                            'sibling) with diabetes, speak to your practitioner '
                                            'about your family history. If you have blood tests '
                                            'to check your fasting glucose level, information '
                                            'about your family history will help with '
                                            'understanding your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 14, 2019',
                                     'links': 'https://www.cdc.gov/diabetes/basics/risk-factors.html',
                                     'title': '"Diabetes: Who\'s at Risk?" from Centers for '
                                              'Disease Control and Prevention'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Blood glucose level after an overnight fast"
trait_science = "Fasting glucose is only moderately heritable - about 30% of differences observed among people has a genetic origin. Our analysis was based on 11 SNPs associated with blood glucose level. These variants jointly explain about 3% of the variation in fasting glucose between people."
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the blood glucose level after an overnight fast. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
