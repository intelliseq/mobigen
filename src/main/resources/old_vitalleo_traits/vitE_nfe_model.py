from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Vitamin_E"
trait_pmids = ["21729881"]  # 21729881 table2; values from meta-analysis
trait_heritability = None  # I found values for avocado and tomatoes only ;)
trait_snp_heritability = None
trait_explained = 0.017
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"
science_behind_the_test ="Our test is based on results of the genome wide association study (GWAS) [1], which included over 5,000 individuals of European ancestry. The study identified 3 variants significantly associated with blood vitamin E concentration. Together these SNPs explain about 1.7% of between people variance in the this vitamin level [1].\nOur analyses use genetic data (genotypes) for these two SNPs to calculate the polygenic score."
genes = ["CYP4F2", "SCARB1", "ZPR1"]

# 5006 men of European descent (mainly Finnish)
# low_vitaminE category ~ 25% of genotypes
# log alpha-tocopherol level"

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.001, category_name='High risk'),
                        QuantitativeCategory(from_=0.001, to=0.069, category_name='Average risk'),
                        QuantitativeCategory(from_=0.069, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs964184', effect_allele='G', beta_value=0.04),
                        beta(rs_id='rs2108622', effect_allele='T', beta_value=0.03),
                        beta(rs_id='rs11057830', effect_allele='A', beta_value=0.03)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Vitamin E'

about = ('Vitamin E is a potent, fat-soluble antioxidant, which helps to protect the '
         "body's cells from damage. Compounds in foods and the environment, called "
         'free radicals, cause this type of cell damage. Vitamin E also helps the '
         'immune system and the blood vessel system. Low vitamin E levels may be '
         'related to some forms of heart disease, vision problems, and other health '
         'issues.')

how_your_result_was_calculated = ('Your result is a risk based on 3 genetic variants, specifically called SNPs, '
                                  'found in genes associated with blood vitamin E level. Overall, this explains '
                                  'about 2% of the the genetic variation in vitamin E level in the human '
                                  'population, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact vitamin E levels, including diet, other health issues, '
                           'and genetics. Genetics has a limited overall impact on vitamin E level when '
                           'you consider all the other factors.')

other_factors = [{'content': 'Almonds, avocados, sunflower sees, spinach, kiwi, butternut '
                             'squash are rich in Vitamin E.',
                  'title': 'Diet'},
                 {'content': 'People with certain conditions that have digestive symptoms, '
                             "like Crohn's disease or cystic fibrosis, may have lower vitamin "
                             'E levels.',
                  'title': 'Other Health Issues'},
                 {'content': 'There are 4 important Vitamin E tocopherols, but many '
                             'supplements only contain alpha-tocopherol.',
                  'title': 'Supplements'}]

what_your_result_means = {'Average risk': 'Depending on your vitamin E level and other factors, you may '
                                           'have an average risk for some forms of heart disease, vision '
                                           'problems, and other health issues.',
                           'High risk': 'Depending on your vitamin E level and other factors, you may '
                                        'have a higher risk for some forms of heart disease, vision '
                                        'problems, and other health issues.',
                           'Low risk': 'Depending on your vitamin E level and other factors, you may '
                                       'have a lower risk for some forms of heart disease, vision '
                                       'problems, and other health issues.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average blood vitamin E level.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a lower blood vitamin E level.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a higher blood vitamin E level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent low vitamin E '
                                             'levels and stay healthy. Certain tests can also check your '
                                             'vitamin E level. Talk to your practitioner about what is '
                                             'right for you.',
                             'High risk': 'There are things you can do to try to prevent low vitamin E '
                                          'levels and stay healthy. Certain tests can also check your '
                                          'vitamin E level. Talk to your practitioner about what is right '
                                          'for you.',
                             'Low risk': 'There are things you can do to try to prevent low vitamin E '
                                         'levels and stay healthy. Certain tests can also check your '
                                         'vitamin E level. Talk to your practitioner about what is right '
                                         'for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a low vitamin E level. Consider '
                                                'talking to your practitioner about tests to '
                                                'check your vitamin E level, and possible next '
                                                'steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'A balanced and healthy diet is always a good '
                                                'idea. Some foods naturally contain vitamin E, '
                                                'such as almonds, avocados, sunflower sees, '
                                                'spinach, kiwi, butternut squash are rich in '
                                                'Vitamin E.',
                                     'title': 'Diet'},
                                    {'content': 'Talk to your practitioner about the dosage to '
                                                'take.  Ideally, supplement with mixed '
                                                'tocopherol, rather than just alpha-tocopherol, '
                                                'should be taken.',
                                     'title': 'Supplements'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your vitamin E level. Talk to them about '
                                             'this and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'A balanced and healthy diet is always a good idea. '
                                             'Some foods naturally contain vitamin E, such as '
                                             'almonds, avocados, sunflower sees, spinach, kiwi, '
                                             'butternut squash are rich in Vitamin E.',
                                  'title': 'Diet'},
                                 {'content': 'Talk to your practitioner about the dosage to '
                                             'take.  Ideally, supplement with mixed tocopherol, '
                                             'rather than just alpha-tocopherol, should be '
                                             'taken.',
                                  'title': 'Supplements'},
                                 {'content': 'If you have certain medical conditions with '
                                             "digestive symptoms, like Crohn's disease or cystic "
                                             'fibrosis, you may have lower vitamin E levels. '
                                             'Speak to your practitioner about how this factors '
                                             'into things, if your magnesium levels are found to '
                                             'be low from tests.',
                                  'title': 'Other Health Issues'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a low vitamin E level. Consider talking to your '
                                            'practitioner about tests to check your vitamin E '
                                            'level, and possible next steps you can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'A balanced and healthy diet is always a good idea. '
                                            'Some foods naturally contain vitamin E, such as '
                                            'almonds, avocados, sunflower sees, spinach, kiwi, '
                                            'butternut squash are rich in Vitamin E.',
                                 'title': 'Diet'},
                                {'content': 'Talk to your practitioner about the dosage to '
                                            'take.  Ideally, supplement with mixed tocopherol, '
                                            'rather than just alpha-tocopherol, should be taken.',
                                 'title': 'Supplements'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 23, 2019',
                                     'links': 'https://ods.od.nih.gov/factsheets/VitaminE-Consumer/',
                                     'title': '"Vitamin E Fact Sheet for Consumers" from '
                                              'National Institutes of Health'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Vitamin E blood level"
trait_science = "Vitamin E blood level is partially controlled by genetic factors (SNPs). Our analysis was based on 3 such SNPs. These variants jointly explain only about 2% of the variation in this vitamin blood level between people."
trait_test_details = "We determined your genotype at three genomic positions, which are known to influence the blood vitamin E concentration. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
