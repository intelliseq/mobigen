from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Alcohol-related_liver_cirrhosis"
trait_pmids = [
    "26482880"]  # 26482880 table 2: combined meta p-value and OR, EA = 'Ref' from table2 (I confirmed it in GWAS catalog)
trait_heritability = 0.5
trait_snp_heritability = None
trait_explained = None
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"
science_behind_the_test = "Predisposition to alcohol-related liver cirrhosis is moderately heritable and about 50% of differences observed among heavy drinkers has a genetic origin [1]. Our test is based on results of the genome wide association study (GWAS) [1], which included over 4,000 heavy drinking individuals of European ancestry. The study identified two variants significantly associated with alcohol-related liver cirrhosis.\nOur analyses use genetic data (genotypes) for these SNPs to calculate the polygenic score."
genes = ["AC004475.1", "PNPLA3", "TMC4"]

# N= ~1800 cases and ~2300 controls (heavy drinkers of European descent)
# low risk cateory ==> empty

model = Choice(
    {
        Priority(0):
            {
                'when': always_true,  # nie ma dodatkowych warunków, jak przy kolore włosów
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.26, category_name='Low risk'),
                        QuantitativeCategory(from_=0.26, to=0.5831, category_name='Average risk'),
                        QuantitativeCategory(from_=0.5831, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        odds_ratio(rs_id='rs738409', effect_allele='G', odds_ratio_value=2.19),
                        odds_ratio(rs_id='rs10401969', effect_allele='C', odds_ratio_value=1.715)
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

trait = 'Alcohol-Related Liver Disease'

about = ('Heavy alcohol intake (8 or more drinks per week for women, 15 or more drinks '
         'per week for men), if done regularly, can take its toll on the body. Health '
         'problems related to long-term heavy drinking include high blood pressure, '
         'heart disease, diabetes complications, liver disease, and other health '
         'issues. Since the liver processes alcohol within the body, a portion '
         '(10-15%) of regular heavy drinkers can go on to develop alcohol-related '
         'liver disease. Over time this can include liver cirrhosis, which is scar '
         'tissue that damages the liver and can stop it from working properly. Several '
         "things impact a person's risk for alcohol-related liver disease, including "
         'genetics.')

how_your_result_was_calculated = ('Your result is a risk based on 3 genetic variants, specifically called SNPs, '
                                  'associated with alcohol-related cirrhosis, a form of liver disease.')

other_factors_statement = ("Many factors impact a person's risk for alcohol-related liver disease, "
                           'including lifestyle, diet, biological sex, environment, other health issues, '
                           'and genetics. Genetics has a moderate overall impact on alcohol-related '
                           'liver disease when you consider all the other factors.')

other_factors = [{'content': 'Try to avoid binge drinking (4 or more drinks at one instance for women, 5 or more drinks'
                             ' at one instance for men) or heavy drinking (8 or more drinks per week for women, 15 or'
                             ' more drinks per week for men). These are risk factors for alcohol dependence if done'
                             ' regularly.', 'title': 'Lifestyle'},
                 {'content': 'Eating a high-fat diet is a risk factor for alcohol-related '
                             'liver disease.',
                  'title': 'Diet'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have alcohol-related liver disease. These types '
                             'of risk factors, which are usually due to rare genetic variants, '
                             'are not included in this test. If you have a family history of '
                             'alcohol-related liver disease, talk to your '
                             'Practitioner or a genetic counselor.',
                  'title': 'Family History'}]

what_your_result_means = {'Average risk': 'Depending on your test result and other factors, such as '
                                           'excessive drinking over a long period of time, you may have '
                                           'an average risk for alcohol-related liver cirrhosis.',
                           'High risk': 'Depending on your test result and other factors, such as '
                                        'excessive drinking over a long period of time, you may have a '
                                        'higher risk for alcohol-related liver cirrhosis.',
                           'Low risk': 'Depending on your test result and other factors, such as '
                                       'excessive drinking over a long period of time, you may have a '
                                       'lower risk for alcohol-related liver cirrhosis.'}

result_statement = {'Average risk': 'Based on the genetic factors tested, you are more likely to '
                                    'have an average risk for alcohol-related liver disease.',
                    'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher risk for alcohol-related liver disease.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower risk for alcohol-related liver disease.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent '
                                             'alcohol-related liver cirrhosis and stay healthy. Certain '
                                             'blood tests can also check your risk factors for liver '
                                             'disease. Talk to your practitioner about what is right for '
                                             'you.',
                             'High risk': 'There are things you can do to try to prevent alcohol-related '
                                          'liver cirrhosis and stay healthy. Certain blood tests can also '
                                          'check your risk factors for liver disease. Talk to your '
                                          'practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent alcohol-related '
                                         'liver cirrhosis and stay healthy. Certain blood tests can also '
                                         'check your risk factors for liver disease. Talk to your '
                                         'practitioner about what is right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for alcohol-related liver disease, '
                                                'including cirrhosis. Consider talking to your '
                                                'practitioner about ordering tests to check your '
                                                'risk factors for liver disease, and possible '
                                                'next steps that you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Try to avoid regular heavy drinking (8 or more '
                                                'drinks per week for women, 15 or more drinks '
                                                'per week for men). This a risk factor for '
                                                'alcohol-related liver cirrhosis if done '
                                                'long-term, such as 8 years or longer.',
                                     'title': 'Lifestyle'},
                                    {'content': 'Alcohol-related liver disease can run in '
                                                'families, and these risk factors are not '
                                                'included in this test. If you have a close '
                                                'relative (parent, child, sibling) with '
                                                'alcohol-related liver disease, speak to your '
                                                'practitioner about your family history. If you '
                                                'have blood tests to look for risk factors, '
                                                'information about your family history will help '
                                                'with understanding your blood test results.',
                                     'title': 'Family History'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your risk factors for alcohol-related liver '
                                             'disease, including cirrhosis. They may also be '
                                             'able to guide you to support with reducing your '
                                             'alcohol intake or with mental health issues, if '
                                             'you need it. Talk to them about this and possible '
                                             'next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Avoid regular heavy drinking (8 or more drinks per '
                                             'week for women, 15 or more drinks per week for '
                                             'men). This is a risk factor for alcohol-related '
                                             'liver cirrhosis if done long-term, such as 8 years '
                                             'or longer. Speak to your practitioner if you need '
                                             'help on ways to cut back on your alcohol intake.',
                                  'title': 'Lifestyle'},
                                 {'content': 'Alcohol-related liver disease can run in families, '
                                             'and these risk factors are not included in this '
                                             'test. If you have a close relative (parent, child, '
                                             'sibling) with alcohol-related liver disease, speak '
                                             'to your practitioner about your family history. If '
                                             'you have blood tests to look for risk factors, '
                                             'information about your family history will help '
                                             'with understanding your blood test results.',
                                  'title': 'Family History'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for alcohol-related liver disease, including '
                                            'cirrhosis. Consider talking to your practitioner '
                                            'about ordering tests to check your risk factors for '
                                            'liver disease, and possible next steps that you can '
                                            'take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Try to avoid regular heavy drinking (8 or more '
                                            'drinks per week for women, 15 or more drinks per '
                                            'week for men). This a risk factor for '
                                            'alcohol-related liver cirrhosis if done long-term, '
                                            'such as 8 years or longer.',
                                 'title': 'Lifestyle'},
                                {'content': 'Alcohol-related liver disease can run in families, '
                                            'and these risk factors are not included in this '
                                            'test. If you have a close relative (parent, child, '
                                            'sibling) with alcohol-related liver disease, speak '
                                            'to your practitioner about your family history. If '
                                            'you have blood tests to look for risk factors, '
                                            'information about your family history will help '
                                            'with understanding your blood test results.',
                                 'title': 'Family History'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = []

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.merckmanuals.com/en-ca/professional/hepatic-and-biliary-disorders/alcoholic-liver-disease/alcoholic-liver-disease',
                                     'title': 'Orfanidis, NT. "Alcoholic Liver Disease" '
                                              'from Merck Manual: Professional Version'},
                                    {'accessed': 'Accessed June 26, 2019. \n',
                                     'links': 'https://www.cdc.gov/alcohol/fact-sheets/alcohol-use.htm',
                                     'title': '"Fact Sheets - Alcohol Use and Your Health" '
                                              'from Centers for Disease Control and '
                                              'Prevention'},
                                    {'accessed': 'Accessed June 26, 2019. \n',
                                     'links': 'https://www.mayoclinic.org/diseases-conditions/alcohol-use-disorder/symptoms-causes/syc-20369243',
                                     'title': '"Alcohol use disorder" from Mayo Clinic'},
                                    {'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.nature.com/articles/ng.3417',
                                     'title': 'Buch S., et al. Nature Genetics. 2015'},
                                    {'accessed': 'Accessed June 26, 2019',
                                     'links': 'https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(95)91685-7/fulltext',
                                     'title': 'Teli MR., et al. Lancet. 1995'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Predisposition for alcohol-related liver cirrhosis"
trait_science = "Predisposition to alcohol-related liver cirrhosis is moderately heritable and about 50% of differences observed among hard drinkers has a genetic origin. Our analysis was based on three SNPs associated with this trait."
trait_test_details = "We determined your genotype at three genomic positions, which are known to influence the alcohol-related liver cirrhosis predisposition. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, including lifestyle."
