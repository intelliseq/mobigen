from src.main.python.lib.seqql import GenotypeGetter

name = 'INFAMMATION'

variants = [
    {
        "trait": "Inflammation",
        "gene": "TNFA",
        "rsid": "rs1800629",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Tumor necrosis factor-a (TNFA) is a pro-inflammatory cytokine.  It\u2019s secreted by fat and immune cells and connected to the development of obesity, dyslipidemia,  insulin resistance, immunological, and inflammatory conditions.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the AA genotype have a higher risk in the production of TNFA and an increased risk of obesity, a bad cholesterol profile, and inflammatory conditions. This is aggravated by a diet high in saturated and omega-6 fatty acids. Switch to a Modified Mediterranean Diet, low in saturated fats, high in omega-3 fatty acids, and high in antioxidant rich foods as red & purple berries and vegetables (rich in antioxidants and phytonutrients).  Supplement omega-3 fats along with anti-inflammatory nutrients such as tumeric and Boswellia."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those individuals with the AG genotype have a somewhat higher risk in the production of TNFA and an increased risk of obesity, a bad cholesteroal profile, and inflammatory conditions. Switch to a Modified Mediterranean Diet, low in saturated fats, high in omega-3 fatty acids, and high in antioxidant rich foods as red & purple berries and vegetables (rich in antioxidants and phytonutrients).  Supplement omega-3 fats along with anti-inflammatory nutrients such as tumeric and Boswellia."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those individuals with the GG genotype may have a lower risk in the production of TNFA and related inflammatory conditions."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "IL6 level",
        "gene": "IL6",
        "rsid": "rs1800795",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "IL-6 is a pro-inflammatory cytokine that plays a crucial role in inflammation and regulates expression of C-reactive protein (CRP). IL-6 and CRP concentrations and has been associated with body fat,  obesity,  inflammation and exercise injury prone, as well as raised systolic blood pressure.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher Risk",
                "description": "Individuals with the CC genotype may be producing raised levels of IL6 and should follow anti-inflammatory lifestyle.  This includes; Modified Mediterranean diet, eating red & purple berries and vegetables, increase omega-3 fatty acids, decrease saturated fatty acids, and increase antioxidants through plant foods and supplementation.  Achieving a healthy body composition and avoiding smoking are essential."
            },
            {
                "genotype": [
                    "C",
                    "G"
                ],
                "gene_impact_string": "Moderate Risk",
                "description": "Individuals with the CG genotype may be producing raised levels of IL6 and should follow anti-inflammatory lifestyle.  This includes; Modified Mediterranean diet, eating red & purple berries and vegetables, increase omega-3 fatty acids, decrease saturated fatty acids, and increase antioxidants through plant foods and supplementation.  Achieving a healthy body composition and avoiding smoking are essential."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Less Risk",
                "description": "Individuals with the GG genotype may be producing less IL6."
            }
        ],
        "risk_allele": "C"
    }
]

# result-dependent. It's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])
