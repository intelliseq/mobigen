from src.main.python.lib.seqql import GenotypeGetter

name = 'WEIGHT LOSS/EXERCISE'

variants = [
    {
        "trait": "Sensitivity to saturated fats",
        "gene": "FABP2",
        "rsid": "rs1799883",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Fatty acid binding protein 2 is involved in removing sugar from the blood and absorbing fat in the intestines. Two copies of the Thr allele in the FABP2 (TT genotype)  is associated with significantly increased sensitivity to saturated fats. Also contributes to increased refined carb sensitivity.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Increased sensitivity",
                "description": "Those individuals with the TT genotype can have increased sensitivity to negative effects from saturated fats and refined carbohydrates.  Follow a Mediterranean Diet and pariticpate in a HITT (High Intensity-Interval Training) plan.  Excess FABP2 can be found with leaky gut syndrome."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat increased sensitivity",
                "description": "Those individuals with the CT genotype can have moderately increased sensitivity to negative effects from saturated fats and refined carbohydrates.  Follow a Mediterranean Diet and pariticpate in a HITT (High Intensity-Interval Training) plan.  Excess FABP2 can be found with leaky gut syndrome."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Not increased sensitivity",
                "description": "Those individuals with the CC genotype are likely to have normal sensitivity to saturated fats and refined carbohydrates."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Weight gain on high fat diet",
        "gene": "APOA5",
        "rsid": "rs662799",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "The protein encoded by the APOA5 gene is an apolipoprotein that plays an important role in regulating the plasma triglyceride levels, a major risk factor for coronary artery disease.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Low risk",
                "description": "Individuals with the GG genotype are less likely to be at risk of weight gain and high triglycerides."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Moderate risk",
                "description": "Individuals with the AG genotype are moderately likely to be at risk of weight gain and high triglycerides. Switch to a more keto-genic or Modified (Low in carbs; grains, starches, and sugars) Mediterranean Whole Food Diet. Address other areas of weight-loss resistance: toxicity, sleep, and HIIT exercise."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "High risk",
                "description": "Individuals with the AA genotype are more likely to be at risk of weight gain and high triglycerides. Switch to a keto-genic or Modified (Low in carbs; grains, starches, and sugars) Mediterranean Whole Food Diet. Address other areas of weight-loss resistance: toxicity, sleep, and HIIT exercise."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Obesity",
        "gene": "PLIN1",
        "rsid": "rs894160",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "PLIN1 gene is involved with the metabolism of fats stored in the cells.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those with the CC genotype have a lower risk of obesity and weight loss resistance."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those with the CT genotype can a have somewhat higher risk risk of obesity and weight loss resistance. Follow a Modified (Low in carbs; grains, starches, and sugars) Mediterranean Diet, avoiding refined carbohydrates. Address other areas of weight-loss resistance: toxicity, sleep, and HIIT exercise."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those with the TT genotype can have a higher risk of obesity and weight loss resistance. Follow a Modified (Low in carbs; grains, starches, and sugars) Mediterranean Diet, avoiding refined carbohydrates. Address other areas of weight-loss resistance: toxicity, sleep, and HIIT exercise."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Carbohydrate Responsiveness",
        "gene": "ANKK1",
        "rsid": "rs1800497",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "This gene may play an important role in both addiction and normal eating behavior as they are involved in reward processing and making individuals less sensitive to the activation of dopamine-based reward circuitry and rendering them lore likely to overeat, especially on energy foods.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Increased risk",
                "description": "Individuals with the AA genotype may have an increased risk for poor food satiety.\u00a0 This person may have lower levels of dopamine which can reduce sensitivity to the dopamine reward system; leaving someone less satisfied from eating. The result is that someone would be more inclined to overeat higher reward foods like carbohydrates.\u00a0 Those should avoid dense or refined carbs and follow a more ketogenic or Modified Mediterranean Diet."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat increase risk",
                "description": "Individuals with the AG genotype may have a somewhat increased risk for poor food satiety.\u00a0 This person may have lower levels of dopamine which can reduce sensitivity to the dopamine reward system; leaving someone less satisfied from eating. The result is that someone would be more inclined to overeat higher reward foods like carbohydrates.\u00a0 Those should avoid dense or refined carbs and follow a more ketogenic or Modified Mediterranean Diet."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Individuals with the GG genotype may have a lower risk for poor food satiety."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Weight loss resistance",
        "gene": "ADRB3",
        "rsid": "rs4994",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "ADRB3 is involved in fat absorption, carbohydrate metabolism, and exercise responsiveness. People with GG genotype may have more difficulty burning abdominal fat in response to exercise, have a slower metabolism, and be considered weight loss resistant.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "More likely",
                "description": "Those individuals with the GG genotype may be more likely to gain fat mass and thus have greater risk of high fat mass, weight loss resistance, and cardiovascular disease. \nWould benefit from intermittent fasting, low-carb, and HIIT - high intensity exercise regimen to lose weight."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those individuals with the AG genotype may be somewhat more likely to gain fat mass and thus have greater risk of high fat mass, weight loss resistance, and cardiovascular disease. \nWould benefit from intermittent fasting, low-carb, and HIIT - high intensity exercise regimen to lose weight."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those individuals with the AA genotype may be less likely to gain fat mass."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Increased Body Mass Index and Food Intake",
        "gene": "APOA2",
        "rsid": "rs5082",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "APOA2 is involved in fat metabolism, obesity and satiety.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "More likely",
                "description": "Those individuals with the GG genotype are more likely to have a higher risk for over eating, obesity, insulin resistance, and inflammation. Should minimize bad fat (trans-fats, vegetable, nut, and seed oil) and saturated fat consumption. Follow the Mediterranean Diet."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Those individuals with the AG genotype are somewhat more likely to have a higher risk for over eating, obesity, insulin resistance, and inflammation. Should minimize bad fat (trans-fats, vegetable, nut, and seed oil) and saturated fat consumption. Follow the Mediterranean Diet."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Less likely",
                "description": "Those individuals with the AA genotype are less likely to have a higher risk for over eating and related conditions."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Metabolic Regulation",
        "gene": "CLOCK",
        "rsid": "rs1801260",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "CLOCK, (circadian locomotor output cycles kaput) an essential element of the human biological clock, is involved in metabolic regulation.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Individuals with the GG genotype are predisposed to a slower metabolic regulation and may be less successful at losing weight. \nIn addition, may have reduced sleep, report morning fatigue and show an evening preference for activities. They also have higher ghrelin levels which regulates appetite, potentially altering eating behavior and a struggle with weight loss."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Individuals with the AG genotype can be predisposed to a slower metabolic regulation and may be less successful at losing weight. \nIn addition, may have reduced sleep, report morning fatigue and show an evening preference for activities. They also have higher ghrelin levels which regulates appetite, potentially altering eating behavior and a struggle with weight loss."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Individuals with the AA genotype can have a lower risk of normal metabolic regulation and may be more successful at losing weight. \nThey are more acctive in the morning and may also have lower ghrelin levels which helps them in weight loss."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Obesity risk, metabolic syndrome",
        "gene": "MC4R",
        "rsid": "rs2229616",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Melanocortin-4 receptor (MC4R) plays critical roles in regulating food intake and energy balance. Its variant associates strongly with obesity and insulin resistance.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher risk",
                "description": "The CC genotype may be associated with greater hunger, a prevalence to overeat, a higher intakes of total calories and dietary fat, a larger tendency to snack. All leading to an higher risk of obesity and diabetes.  \nA good plan for those with this variant is to avoid snacking, to put together a clear nutritional plan, and practice intermittent fasting."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "The CT genotype may be associated with greater hunger, a prevalence to overeat, a higher intakes of total calories and dietary fat, a larger tendency to snack. All leading to an somewhat higher risk of obesity and diabetes.  \nA good plan for those with this variant is to avoid snacking, to put together a clear nutritional plan, and practice intermittent fasting."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Lower risk",
                "description": "The TT genotype has lower risk for increased hunger and the related higher caloric intake."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "Increased risk of metabolic syndrome",
        "gene": "APOC3",
        "rsid": "rs5128",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": "19424489"
        },
        "intro": "This gene inhibits lipoprotein lipase and hepatic lipase, delaying catabolism of triglyceride - rich particles. Thus, raising triglyceride levels.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the GG genotype have higher circulating APOC3 and raised TG levels, and have approx. 4-fold increased risk of hypertriglyceridemia. But they also show greatest response to dietary fat modification.  Decrease in saturated fat and an increase in monounsaturated fats with carb modification. Follow the Modified Mediterranean Diet."
            },
            {
                "genotype": [
                    "C",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those individuals with the CG genotype can have a somewhat higher risk of elevated triglycerides.  Decrease in saturated fat and an increase in monounsaturated fats with carb modification. Follow the Modified Mediterranean Diet."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those individuals with the CC genotype can have a lower risk of elevated triglycerides."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "IL-10 level",
        "gene": "IL-10",
        "rsid": "rs1800896",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "An anti-inflammatory cytokine can be associated with certain health related issues.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher Level of IL-10",
                "description": "Those individuals with the CC genotype may be producing higher levels of beneficial IL-10.  IL-10 is a major anti-inflammatory cytokine assisting the body in minimizing an unhealthy inflammatory response."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat lower level of IL-10",
                "description": "Those individuals with the CT genotype may be producing more moderate levels of beneficial IL-10.  IL-10 is a major anti-inflammatory cytokine assisting the body in minimizing an unhealthy inflammatory response."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Lower level of IL-10",
                "description": "Those individuals with the TT genotype may be producing lower levels of IL-10.  IL-10 is a major anti-inflammatory cytokine assisting the body in minimizing inflammation.  Poorer expression of IL-10 can be connected to inflammatory conditions such as irritable bowel syndrome, auto-immune illness, asthma, prostatitis, and other disorders related to inflammation.  Should strongly consider anti-inflammatory diet and supplementation."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Detoxification",
        "gene": "NQ01",
        "rsid": "rs2070999",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "NQ01 has an antioxidant function to protect cells from dangerous free radicals and is part of the detoxification process for potential cancer causing factors that come from exposure to tobacco smoke, diet, and estrogen metabolism.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Higher risk",
                "description": "The AA genotype is at higher risk of inadequate NQ01 enzyme activity. People with this variant can be more prone to the mutagenic and carcinogenic effects of smoke and other environmental toxins or volatile organic compounds (VOCs).\nSolutions include increasing glutathione and supporting liver functions. Milk thistle, green tea extracts, sulforaphane from cruciferous vegetables (Broccoli, Bok choy, cabbage, kale, Brussels, sprouts, and cauliflower), and antioxidants like Vitamin E from supplements are all helpful."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "The AG genotype is at a somewhat higher risk of inadequate NQ01 enzyme activity. People with this variant can be more prone to the mutagenic and carcinogenic effects of smoke and other environmental toxins or volatile organic compounds (VOCs).\nSolutions include increasing glutathione and supporting liver functions. Milk thistle, green tea extracts, sulforaphane from cruciferous vegetables (Broccoli, Bok choy, cabbage, kale, Brussels, sprouts, and cauliflower), and antioxidants like Vitamin E from supplements are all helpful."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Lower risk",
                "description": "The GG genotype is at lower risk of inadequate NQ01 enzyme activity"
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Caffeine Metabolism",
        "gene": "CYP1A2",
        "rsid": "rs762551",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Coffee and tea are a major source of caffeine, which is metabolized by the cytochrome enzyme (CYP1A2).  Therefore, a greater presence of CYP1A2 gene allows for better caffeine metabolism.  A decrease allows caffeine to stay in the system, disrupt a number of metabolic functions, and cause trouble with sleep.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Faster",
                "description": "Individuals who have the AA genotype may be able have the ability to metabolize caffeine faster."
            },
            {
                "genotype": [
                    "A",
                    "C"
                ],
                "gene_impact_string": "Somewhat faster",
                "description": "Individuals who have the CA genotype may be able have the ability to metabolize caffeine somewhat faster. A moderate to high intake of caffeinated beverages, such as coffee, can be associated with increased risk of heart disease."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Slower",
                "description": "Individuals who have the CC genotype may be able have the ability to metabolize caffeine slower. A moderate to high intake of caffeinated beverages, such as coffee, can be associated with increased risk of heart disease."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "Soft tissue",
        "gene": "COL5A1",
        "rsid": "rs12722",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": "296326"
        },
        "intro": "Collagen type V is a requirement for building soft tissues such as tendons and ligaments.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Less likely",
                "description": "Individuals with the CC genotype are less likely to have healthier soft tissues.\nSupplement with collagen, bone broth, magnesium, and vitamin D along with eating green-leafy vegetables."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Less likely",
                "description": "Individuals with the CT genotype are somewhat less likely to have healthier soft tissues.\nSupplement with collagen, bone broth, magnesium, and vitamin D along with eating green-leafy vegetables."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "More likely",
                "description": "Individuals with the TT genotype are more likely to have healthier soft tissues."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "Fat Metabolism",
        "gene": "PPARGC1A",
        "rsid": "rs8192678",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": "18467552"
        },
        "intro": "PPARG (peroxisome proliferator-activated receptor gamma) is associated with a higher risk of cardiovascular disease and diabetes with high saturated fat diet.  A negative PPART variant is associated with an impaired ability to metabolize fats.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Higher risk",
                "description": "These individuals with the TT genotype can have a higher risk of weight gain, obesity, diabetes, and cardiovascular disease."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "These individuals with the CT genotype can have a somewhat higher risk of weight gain, obesity, diabetes, and cardiovascular disease."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Lower risk",
                "description": "These individuals with the CC genotype can have a lower risk of weight gain, obesity, diabetes, and cardiovascular disease."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Lean Body Mass",
        "gene": "TRHR",
        "rsid": "rs7832552",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "TRHR (thyrotropin releasing hormone receptor) is involved in generating the release of T3 and T4 thyroid hormones; important for brain, heart, liver, and muscle function.  T4 and T3 are involved in mobilizing fuel during exercise as well as building and maintaining lean body mass.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "More favorable",
                "description": "Those individuals with the TT genotype been found to more favorably stimulate the release of thyroid hormones T3 and T4; leading to an increased metabolic rate and more lean muscle mass."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Moderately favorable",
                "description": "Those individuals with the CT genotype been found to somewhat more favorably stimulate the release of thyroid hormones T3 and T4; leading to an increased metabolic rate and more lean muscle mass."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Less favorable",
                "description": "Those individuals with the CC genotype been found to somewhat less favorably stimulate the release of thyroid hormones T3 and T4; leading to a decreased metabolic rate and less lean muscle mass. It is recommended test thyroid health and lead a thyroid-supportive lifestyle. Eat a Mediterranean-style\u00a0diet, loading up on vegetables of all color, low glycemic fruit, and healthy fats. Manage stress, toxicity, and increase exercise intensity and regularity."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "Power vs Endurance",
        "gene": "ACTN3",
        "rsid": "rs1815739",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Protein encoded by this gene can be found in muscle fibers and greatly influences power development. It also plays a role in muscle fiber type specialization, diameter, and metabolism.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Better Power & Speed",
                "description": "This genotype (CC) gives you an advantage when it comes to strength, speed, but a disadvantage with endurance."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Average Power and Endurance",
                "description": "This genotype (TC) gives you a moderate abilities when it comes to power, strength, and endurance."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Better endurance",
                "description": "This genotype (TT) gives you a disadvantage when it comes to power, however, potentially a better advantage with endurance."
            }
        ],
        "risk_allele": ""
    },
    {
        "trait": "C-Reactive Protein Levels",
        "gene": "CRP",
        "rsid": "rs3093066",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "CRP is a biomarker of inflammation.  While a critical part of the immune system, chronic inflammation and raised CRP is foundational to chronic illness, pain, and autoimmune disease.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Higher Levels",
                "description": "Those individuals with a GG genotype is linked to higher levels of CRP.\nBeing overweight and with a diet higher in grains, sugars, and other carbohydrates is associated with higher levels of inflammation and CRP. A ketogenic or modified Mediterranean diet, omega-3 fatty acids, foods and supplements containing turmeric and Boswellia, and a regular moderately intense exercise program can improve CRP levels."
            },
            {
                "genotype": [
                    "G",
                    "T"
                ],
                "gene_impact_string": "Somewhat Higher Levels",
                "description": "Those individuals with a GT genotype is linked to somewhat higher levels of CRP.\nBeing overweight and with a diet higher in grains, sugars, and other carbohydrates is associated with higher levels of inflammation and CRP. A ketogenic or modified Mediterranean diet, omega-3 fatty acids, foods and supplements containing turmeric and Boswellia, and a regular moderately intense exercise program can improve CRP levels."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Lower Levels",
                "description": "Those individuals with a TT genotype is linked to lower levels of CRP."
            }
        ],
        "risk_allele": "G"
    }
]

# result-dependent. It's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])
