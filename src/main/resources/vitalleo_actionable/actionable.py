from src.main.python.lib.seqql import GenotypeGetter

name = 'ACTIONABLE GENES'

variants = [
    {
        "trait": "Warrior",
        "gene": "MAOA",
        "rsid": "rs909525",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Monoamine oxidase A (MAO-A) is an enzyme which is encoded by the MAOA gene. \nMAO-A breaks down neurotransmitters such as noradrenaline, adrenaline, serotonin and dopamine, and as such, plays an important role in regulating mood. \nIt is commonly referred to as the \u201cwarrior gene\u201d as certain polymorphisms have been linked to an increased risk of violence and aggression, however the exact contribution of MAO-A is likely to be low and the area remains controversial.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Likely to be warrior",
                "description": "The CC genotype has shown reduced MAO-A enzyme activity and has been linked with increased aggressiveness, therefore, likely to be a warrior."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat likely to be warrior",
                "description": "The CT genotype has shown reduced MAO-A enzyme activity and has been linked with increased aggressiveness, therefore, somewhat likely to be a warrior."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Not likely to be warrior",
                "description": "The TT genotype has shown normal MAO-A enzyme activity and has been linked with average aggressiveness, therefore, not likely to be a warrior.  To manage unhealthy MAO-A, it is important to eat a low glycemic diet with moderate protein and health fats. Exercise, stress management, adequate sleep, detox, and minimizing toxic exposure are also key environmental choices."
            }
        ],
        "risk_allele": ""
    },
    {
        "trait": "Fatty Liver",
        "gene": "PEMT",
        "rsid": "rs7946",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "The PEMT gene affects your body\u2019s ability to produce phosphatidylcholine, an essential compound that you need to maintain cell membranes, bile flow, muscle health, and brain development.  When PEMT is unhealthy: Gallbladder disorders, small intestine bacterial overgrowth (SIBO), pregnancy complications, cell membrane weakness, and muscle pain.  Birth-control pills, hormone replacement therapy, and even bioidentical hormones can strain your MTHFR and COMT.  Even if you\u2019re not taking medications, your genetic expression can be disrupted by poor diet, lack of exercise, too much exercise, not enough sleep, environmental toxins, and plain old everyday stress\u2014and those are just the most common problems.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the TT genotype are less likely  to produce sufficient levels of phosphatidylcholine. Green tea leaf extract is rich in polyphenols which have strong anti-oxidant, hypolipidemic, and anti-inflammatory effects, countering the damaging effects of lipid accumulation in the liver. Green tea has been shown to significantly reduce both the occurrence of NAFLD (Non-alcoholic Fatty Liver Disease) and its progression."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those individuals with the CT genotype are somewhat  less likely  to produce sufficient levels of phosphatidylcholine. Green tea leaf extract is rich in polyphenols which have strong anti-oxidant, hypolipidemic, and anti-inflammatory effects, countering the damaging effects of lipid accumulation in the liver. Green tea has been shown to significantly reduce both the occurrence of NAFLD and its progression."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Low risk",
                "description": "Those individuals with the CC genotype are likely to produce sufficient levles of phosphatidylcholine."
            }
        ],
        "risk_allele": "T"
    },
    {
        "trait": "Obesity",
        "gene": "ADRB2",
        "rsid": "rs1042714",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "ADRB2 plays a role in energy production through metabolizing lipids stored in fat tissues.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Increased risks",
                "description": "Those individuals with the GG genotype could have increased risk for fat storage for energy and have a higher risk of insulin resistance and obesity, particularly on a diet high in carbohydrates. Therefore, these people should follow a more keto-genic type diet. Regular exercise is essential with HIIT (High intensity, interval training) being ideal."
            },
            {
                "genotype": [
                    "C",
                    "G"
                ],
                "gene_impact_string": "Somewhat increased risks",
                "description": "Those individuals with the GC genotype could have somewhat increased risk for fat storage for energy and have a higher risk of insulin resistance and obesity, particularly on a diet high in carbohydrates. Therefore, these people should follow a more keto-genic type diet. Regular exercise is essential with HIIT (High intensity, interval training) being ideal."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Average risks",
                "description": "Those individuals with the CC genotype could have an average risk for fat storage for energy and have a higher risk of insulin resistance and obesity, particularly on a diet high in carbohydrates. Therefore, these people should follow a more keto-genic type diet. Regular exercise is essential with HIIT (High intensity, interval training) being ideal."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Using Fat for Energy",
        "gene": "ADRB2",
        "rsid": "rs1042713",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "ADRB2 plays a role in energy production through metabolizing lipids stored in fat tissues.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those individuals with the AA genotype have a lower risk for fat storage and related issues."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Average risk",
                "description": "Those individuals with the GA genotype are at a average risk for storing fat for energy, a average risk of insulin resistance, and potential increased obesity; particularly on a diet high in carbohydrates.  Therefore, these people should follow a more keto-genic type diet.  Regular exercise is essential to increase fat mobilization."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "High risk",
                "description": "Those individuals with the GG genotype are at a higher risk for increased fat storage for energy, a higher risk of insulin resistance, and increased obesity; particularly on a diet high in carbohydrates.  Therefore, these people should follow a more keto-genic type diet.  Regular exercise is essential with HITT (high intensity, interval training) being ideal to increase fat mobilization."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Obesity Risk",
        "gene": "FTO",
        "rsid": "rs9939609",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Fat-mass-and-obesity-associated (FTO) gene is connected to food intake, appetite, and the body\u2019s ability to mobilize fat for energy. Those with the negative FTO variant will tend to have increased risk of insulin resistance and diabetes, higher body mass index (BMI), greater body fat, and a larger waist circumference.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Increased risk",
                "description": "Individuals with the AA genotype can have a increased risk for excess body fat, larger waist circumference, high BMI, and are at greater risk of insulin resistance and diabetes. A low glycemic, lower fat diet utilizing good fats and devoid of bad fats is important. Regular exercise is essential with HIIT (High intensity, interval training) being ideal."
            },
            {
                "genotype": [
                    "A",
                    "T"
                ],
                "gene_impact_string": "Somewhat increased risk",
                "description": "Individuals with the AT genotype can have somewhat increased risk for excess body fat, larger waist circumference, high BMI, and are at greater risk of insulin resistance and diabetes. A low glycemic, lower fat diet utilizing good fats and devoid of bad fats is important. Regular exercise is essential with HIIT (High intensity, interval training) being ideal."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Low risk",
                "description": "Individuals with the TT genotype can have a low risk for excess weight gain, excess body fat, and related health concerns."
            }
        ],
        "risk_allele": "A"
    },
    {
        "trait": "Cardiovascular Disease",
        "gene": "PPARG",
        "rsid": "rs1801282",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "PPARG (peroxisome proliferator-activated receptor gamma) is associated with a higher risk of cardiovascular disease and diabetes with high saturated fat diet.  A negative PPART variant is associated an impaired ability to metabolize fats.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the CC genotype are at higher risk of weight gain, obesity, and diabetes due to an inability to breakdown and utilize saturated fats.  Minimize calories, especially those related to saturated fats and perform regular physical activity to manage weight. Follow a Modified Mediterranean diet, focused on low glycemic foods, omega-3, and monounsaturated fats."
            },
            {
                "genotype": [
                    "C",
                    "G"
                ],
                "gene_impact_string": "Somewhat increased risk",
                "description": "Those individuals with the CG genotype are at a moderately higher risk of weight gain, obesity, and diabetes due to an inability to breakdown and utilize saturated fats.  Minimize calories, especially those related to saturated fats and perform regular physical activity to manage weight. Follow a Modified Mediterranean diet, focused on low glycemic foods, omega-3, and monounsaturated fats."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those individuals with the GG genotype have a lower risk to breakdown and utilize saturated fats and are a lower risk of weight gain, obesity, and diabetes."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "Higher Sugar Consumption",
        "gene": "SLC2A2",
        "rsid": "rs5400",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "SLC2A2 helps to regulate the process of secreting insulin when glucose is present and assist in the glucose getting into the cells.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "More likely",
                "description": "Individuals with the AA genotype may be more likely at risk for sugar consumption.  The A allele may be associated with higher risk of Type 2 diabetes and symptoms of diabetes. Low carb, keto-genic diet with strong focus on restriction of grains and sugars."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat more likely",
                "description": "Individuals with the AG genotype may be somewhat more likely at risk for sugar consumption.  The A genotype may be associated with higher risk of Type 2 diabetes and symptoms of diabetes. Low carb, keto-genic diet with strong focus on restriction of grains and sugars."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Less likely",
                "description": "Individuals with the GG genotype may be less likely at risk for sugar consumption.  The G allele is associated with lower risk of Type 2 diabetes and symptoms of diabetes."
            }
        ],
        "risk_allele": "A"
    }
]

# result-dependent. It's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])
