from src.main.python.lib.seqql import GenotypeGetter

name = 'HDL/LDL'

variants = [
    {
        "trait": "APOE genotype ( APOE-\u03b54 allele )",
        "gene": "APOE",
        "rsid": "rs429358",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Apo E is involved in the transport and metabolism of triglycerides, cholesterol, other lipids throughout the brain and body.  It also plays an important role in binding LDL throughout the body and in the central nervous system.  As a result of its impact on lipid levels throughout the body and in the brain, it can be a significant risk factor for Alzheimer's disease, cognitive function, immunity, and cardiovascular disease.",
        "genotypes": [
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the CC genotype have a higher risk of irregular transporting and metabolism of triglycerides, cholesterol, other lipids throughout the brain and body. The APOE-\u03b54 allele has a strong influence on the risk of Alzheimer\u2019s, cardiovascular disease, and immune-related illness.\nSignificantly decrease saturated fats, eliminate trans fats, no smoking, minimal to no alcohol, increase anti-oxidant rich foods and supplements, and weight management and moderate exercise is essential."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those individuals with the CT genotype have a somewhat higher risk of irregular transporting and metabolism of triglycerides, cholesterol, other lipids throughout the brain and body. The APOE-\u03b54 allele has a strong influence on the risk of Alzheimer\u2019s, cardiovascular disease, and immune-related illness.\nSignificantly decrease saturated fats, eliminate trans fats, no smoking, minimal to no alcohol, increase anti-oxidant rich foods and supplements, and weight management and moderate exercise is essential."
            },
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those individuals with the TT genotype have a lower risk of APOE-4 related conditions."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "APOE genotype ( APOE-\u03b52 allele )",
        "gene": "APOE",
        "rsid": "rs7412",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "Apo E is involved in the transport and metabolism of triglycerides, cholesterol, other lipids throughout the brain and body.",
        "genotypes": [
            {
                "genotype": [
                    "T",
                    "T"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Those individuals with the TT genotype are more likely to have a healthier triglyceride levels.  Research has suggested that APOE E2 allele can have a cardio-protective benefit."
            },
            {
                "genotype": [
                    "C",
                    "T"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Those individuals with the CT genotype are somewhat more  likely to have a healthier triglyceride levels.  Research has suggested that APOE E2 allele can have a cardio-protective benefit."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the CC genotype are at a higher risk of increased triglycerides levels.  These individuals should manage carbohydrate consumption and follow diet, supplement, and exercise recommendation designed to support healthy lipid and triglyceride levels."
            }
        ],
        "risk_allele": "C"
    },
    {
        "trait": "HDL level",
        "gene": "CETP",
        "rsid": "rs708272",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "CETP (cholesterol esther transfer protein) plays a role in mediating HDL-C levels.  Depending on the genotype, CETP will either cause HDL-C to be higher or lower and therefore is a strong independent risk factor for CAD.",
        "genotypes": [
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Lower risk",
                "description": "Individuals with the AA (B2B2) genotype  may have a decreased plasma (CETP), which may be associated with  higher  levels of \"good\",  high density lipoprotein (HDL) cholesterols."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Somewhat lower risk",
                "description": "Individuals with the AG (B2B2) genotype  may have a somewhat decreased plasma (CETP), which may be associated with  higher  levels of \"good\",  high density lipoprotein (HDL) cholesterols."
            },
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Those individuals with the GG (B2B2) genotype may have a higher risk of increased plasma (CETP), which may be associated with decreased levels of \"good\", high density lipoprotein (HDL) cholesterols.  Should follow dietary and supplementation protocols related to increasing HDL levels."
            }
        ],
        "risk_allele": "G"
    },
    {
        "trait": "Hyperlipoproteinemia",
        "gene": "LPL",
        "rsid": "rs328",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "LPL (lipoprotein lipase) is attached to the vascular endothelium and has the positive effect of removing VLDL-C (very low density lipoprotein cholesterol) and triglycerides from circulation.",
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Somewhat lower risk",
                "description": "Individuals with the GG genotype are likely to have lower VLDL-C and triglyceride and higher HDL-C plasma levels.  They are also more likely to have lower blood pressure."
            },
            {
                "genotype": [
                    "C",
                    "G"
                ],
                "gene_impact_string": "Somewhat higher risk",
                "description": "Individuals with the CG genotype are likely to have somewhat higher VLDL-C and triglyceride and higher HDL-C plasma levels.  They are also more likely to have lower blood pressure."
            },
            {
                "genotype": [
                    "C",
                    "C"
                ],
                "gene_impact_string": "Higher risk",
                "description": "Individuals with the CC genotype are likely to have higher VLDL-C, increased triglycerides,  and decseased HDL-C plasma levels. These individuals should have cardiometabolic testing and follow a specific heart health program including a low glycemic index diet, avoiding refined carbohydrates, exercising regularly, and taking certain prescribed supplementation."
            }
        ],
        "risk_allele": "C"
    }
]

# result-dependent. It's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])
