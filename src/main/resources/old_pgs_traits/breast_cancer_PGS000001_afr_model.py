from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Breast_Cancer"
trait_pmids = ["25855707"] 
trait_pgs_id = "PGS000001"
trait_heritability = None
trait_snp_heritability = None
trait_explained = None
trait_authors = ["taken from the PGS catalog"]
trait_version = "1.0"
trait_was_prepared_for_population = "afr"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
category_on_the_left_side_of_plot = "Low risk"
genes = []

model = Choice(
    {
        Priority(0): 
            {
                'when': always_true,  
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.5445, category_name='Low risk'),
                        QuantitativeCategory(from_=0.5445, to=1.6456, category_name='Average risk'),
                        QuantitativeCategory(from_=1.6456 , category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs78540526', effect_allele='T', beta_value=0.16220387987485377),
                        beta(rs_id='rs75915166', effect_allele='A', beta_value=0.023618865598634027),
                        beta(rs_id='rs554219', effect_allele='G', beta_value=0.1167157996959607),
                        beta(rs_id='rs7726159', effect_allele='A', beta_value=0.03527061408191933),
                        beta(rs_id='rs10069690', effect_allele='T', beta_value=0.023911820046312877),
                        beta(rs_id='rs2736108', effect_allele='T', beta_value=-0.06411194546724429),
                        beta(rs_id='rs2588809', effect_allele='T', beta_value=0.06456977064930007),
                        beta(rs_id='rs999737', effect_allele='T', beta_value=-0.07915143830543754),
                        beta(rs_id='rs10759243', effect_allele='A', beta_value=0.05278218543896948),
                        beta(rs_id='rs865686', effect_allele='G', beta_value=-0.10702857275852336),
                        beta(rs_id='rs2981579', effect_allele='A', beta_value=0.22506171047011356),
                        beta(rs_id='rs11199914', effect_allele='T', beta_value=-0.06187540371808753),
                        beta(rs_id='rs7072776', effect_allele='A', beta_value=0.05647484692797899),
                        beta(rs_id='rs11814448', effect_allele='C', beta_value=0.19721016928770527),
                        beta(rs_id='rs13387042', effect_allele='G', beta_value=-0.12851542223542736),
                        beta(rs_id='rs16857609', effect_allele='T', beta_value=0.06961934188005707),
                        beta(rs_id='rs11552449', effect_allele='T', beta_value=0.0778865386570712),
                        beta(rs_id='rs11249433', effect_allele='G', beta_value=0.09467361360268099),
                        beta(rs_id='rs1045485', effect_allele='C', beta_value=-0.03624913267512103),
                        beta(rs_id='rs4973768', effect_allele='T', beta_value=0.0896578719305354),
                        beta(rs_id='rs10941679', effect_allele='G', beta_value=0.11315009793265578),
                        beta(rs_id='rs889312', effect_allele='C', beta_value=0.11118352896061494),
                        beta(rs_id='rs12662670', effect_allele='G', beta_value=0.13032626167557426),
                        beta(rs_id='rs2046210', effect_allele='A', beta_value=0.04602443831127925),
                        beta(rs_id='rs13281615', effect_allele='G', beta_value=0.09075436326846412),
                        beta(rs_id='rs1011970', effect_allele='T', beta_value=0.04898062222162188),
                        beta(rs_id='rs2380205', effect_allele='T', beta_value=-0.023166278031839585),
                        beta(rs_id='rs10995190', effect_allele='A', beta_value=-0.15513449694999074),
                        beta(rs_id='rs704010', effect_allele='T', beta_value=0.06756518616242388),
                        beta(rs_id='rs3817198', effect_allele='C', beta_value=0.07176236622689079),
                        beta(rs_id='rs10771399', effect_allele='G', beta_value=-0.14745646946787522),
                        beta(rs_id='rs1292011', effect_allele='G', beta_value=-0.08131852117758834),
                        beta(rs_id='rs3803662', effect_allele='A', beta_value=0.20351210936498243),
                        beta(rs_id='rs6504950', effect_allele='A', beta_value=-0.06827884075329438),
                        beta(rs_id='rs8170', effect_allele='A', beta_value=0.03091710263472161),
                        beta(rs_id='rs2363956', effect_allele='T', beta_value=0.0260575343192896),
                        beta(rs_id='rs2823093', effect_allele='A', beta_value=-0.0753703070247766),
                        beta(rs_id='rs17879961', effect_allele='G', beta_value=0.3098348770929142),
                        beta(rs_id='rs616488', effect_allele='G', beta_value=-0.060068526466119515),
                        beta(rs_id='rs4849887', effect_allele='T', beta_value=-0.08479565170471633),
                        beta(rs_id='rs2016394', effect_allele='A', beta_value=-0.05087233037375655),
                        beta(rs_id='rs1550623', effect_allele='G', beta_value=-0.0570995920405728),
                        beta(rs_id='rs6762644', effect_allele='G', beta_value=0.06400712997429242),
                        beta(rs_id='rs12493607', effect_allele='C', beta_value=0.051548261880576554),
                        beta(rs_id='rs9790517', effect_allele='T', beta_value=0.04697900119399464),
                        beta(rs_id='rs6828523', effect_allele='A', beta_value=-0.09915757153321861),
                        beta(rs_id='rs10472076', effect_allele='C', beta_value=0.04104596943600102),
                        beta(rs_id='rs1353747', effect_allele='G', beta_value=-0.08196956286725067),
                        beta(rs_id='rs11242675', effect_allele='C', beta_value=-0.05879504651050549),
                        beta(rs_id='rs204247', effect_allele='G', beta_value=0.049075837646592645),
                        beta(rs_id='rs720475', effect_allele='A', beta_value=-0.05635873366938424),
                        beta(rs_id='rs9693444', effect_allele='A', beta_value=0.07045846364856137),
                        beta(rs_id='rs6472903', effect_allele='G', beta_value=-0.0916767885719041),
                        beta(rs_id='rs2943559', effect_allele='G', beta_value=0.1252219647533818),
                        beta(rs_id='rs11780156', effect_allele='T', beta_value=0.06681717303731709),
                        beta(rs_id='rs7904519', effect_allele='G', beta_value=0.05675833381860888),
                        beta(rs_id='rs3903072', effect_allele='T', beta_value=-0.057417270869950646),
                        beta(rs_id='rs11820646', effect_allele='T', beta_value=-0.044683607626640025),
                        beta(rs_id='rs12422552', effect_allele='C', beta_value=0.0321767316952212),
                        beta(rs_id='rs17356907', effect_allele='G', beta_value=-0.09673118895977176),
                        beta(rs_id='rs11571833', effect_allele='T', beta_value=0.23182575169704359),
                        beta(rs_id='rs2236007', effect_allele='A', beta_value=-0.08305557513732584),
                        beta(rs_id='rs941764', effect_allele='G', beta_value=0.06165938038672779),
                        beta(rs_id='rs17817449', effect_allele='G', beta_value=-0.07257069283483537),
                        beta(rs_id='rs13329835', effect_allele='G', beta_value=0.07306457085700523),
                        beta(rs_id='rs527616', effect_allele='C', beta_value=-0.04363845702982949),
                        beta(rs_id='rs1436904', effect_allele='G', beta_value=-0.05487866150998945),
                        beta(rs_id='rs4808801', effect_allele='G', beta_value=-0.06731570728486712),
                        beta(rs_id='rs3760982', effect_allele='A', beta_value=0.05382508669490737),
                        beta(rs_id='rs132390', effect_allele='C', beta_value=0.10354887562855884),
                        beta(rs_id='rs6001930', effect_allele='C', beta_value=0.1261920252377594),
                        beta(rs_id='rs4245739', effect_allele='C', beta_value=0.02868463385990886),
                        beta(rs_id='rs6678914', effect_allele='A', beta_value=-0.011060947359424948),
                        beta(rs_id='rs11075995', effect_allele='T', beta_value=0.03613904661587314)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

