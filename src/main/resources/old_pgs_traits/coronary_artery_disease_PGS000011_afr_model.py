from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Coronary_artery_disease"
trait_pmids = ["26392438"] 
trait_pgs_id = "PGS000011"
trait_heritability = None
trait_snp_heritability = None
trait_explained = None
trait_authors = ["taken from the PGS catalog"]
trait_version = "1.0"
trait_was_prepared_for_population = "afr"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score"
category_on_the_left_side_of_plot = "Low risk"
genes = []
about = 'Narrowing of the coronary arteries due to fatty deposits inside the arterial walls'
model = Choice(
    {
        Priority(0): 
            {
                'when': always_true,  
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=2.9360, category_name='Low risk'),
                        QuantitativeCategory(from_=2.9360, to=3.7369, category_name='Average risk'),
                        QuantitativeCategory(from_=3.7369 , category_name='High risk')

                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs646776', effect_allele='T', beta_value=0.10436001532424286),
                        beta(rs_id='rs17114036', effect_allele='A', beta_value=0.10436001532424286),
                        beta(rs_id='rs11206510', effect_allele='T', beta_value=0.0769610411361284),
                        beta(rs_id='rs17465637', effect_allele='C', beta_value=0.131028262406404),
                        beta(rs_id='rs9818870', effect_allele='T', beta_value=0.06765864847381486),
                        beta(rs_id='rs17609940', effect_allele='G', beta_value=0.06765864847381486),
                        beta(rs_id='rs12526453', effect_allele='G', beta_value=0.09531017980432493),
                        beta(rs_id='rs12190287', effect_allele='C', beta_value=0.06765864847381486),
                        beta(rs_id='rs3798220', effect_allele='C', beta_value=0.412109650826833),
                        beta(rs_id='rs11556924', effect_allele='C', beta_value=0.08617769624105241),
                        beta(rs_id='rs4977574', effect_allele='G', beta_value=0.25464221837358075),
                        beta(rs_id='rs1746048', effect_allele='C', beta_value=0.06765864847381486),
                        beta(rs_id='rs12413409', effect_allele='G', beta_value=0.11332868530700327),
                        beta(rs_id='rs964184', effect_allele='G', beta_value=0.12221763272424911),
                        beta(rs_id='rs2259816', effect_allele='T', beta_value=0.0769610411361284),
                        beta(rs_id='rs3184504', effect_allele='T', beta_value=0.06765864847381486),
                        beta(rs_id='rs4773144', effect_allele='G', beta_value=0.06765864847381486),
                        beta(rs_id='rs2895811', effect_allele='C', beta_value=0.058268908123975824),
                        beta(rs_id='rs3825807', effect_allele='A', beta_value=0.0769610411361284),
                        beta(rs_id='rs12936587', effect_allele='G', beta_value=0.058268908123975824),
                        beta(rs_id='rs216172', effect_allele='C', beta_value=0.06765864847381486),
                        beta(rs_id='rs318090', effect_allele='A', beta_value=0.058268908123975824),
                        beta(rs_id='rs1122608', effect_allele='G', beta_value=0.09531017980432493),
                        beta(rs_id='rs9982601', effect_allele='T', beta_value=0.12221763272424911),
                        beta(rs_id='rs4845625', effect_allele='T', beta_value=0.03922071315328133),
                        beta(rs_id='rs2028900', effect_allele='T', beta_value=0.04879016416943205),
                        beta(rs_id='rs4299376', effect_allele='G', beta_value=0.058268908123975824),
                        beta(rs_id='rs515135', effect_allele='C', beta_value=0.0769610411361284),
                        beta(rs_id='rs2252641', effect_allele='C', beta_value=0.03922071315328133),
                        beta(rs_id='rs1878406', effect_allele='T', beta_value=0.058268908123975824),
                        beta(rs_id='rs7692387', effect_allele='G', beta_value=0.058268908123975824),
                        beta(rs_id='rs273909', effect_allele='G', beta_value=0.08617769624105241),
                        beta(rs_id='rs10947789', effect_allele='T', beta_value=0.058268908123975824),
                        beta(rs_id='rs2048327', effect_allele='C', beta_value=0.058268908123975824),
                        beta(rs_id='rs4252120', effect_allele='T', beta_value=0.058268908123975824),
                        beta(rs_id='rs11984041', effect_allele='T', beta_value=0.06765864847381486),
                        beta(rs_id='rs10953541', effect_allele='C', beta_value=0.0769610411361284),
                        beta(rs_id='rs2954029', effect_allele='A', beta_value=0.03922071315328133),
                        beta(rs_id='rs3217992', effect_allele='T', beta_value=0.14842000511827322),
                        beta(rs_id='rs2487928', effect_allele='A', beta_value=0.058268908123975824),
                        beta(rs_id='rs2047009', effect_allele='G', beta_value=0.04879016416943205),
                        beta(rs_id='rs1412444', effect_allele='T', beta_value=0.058268908123975824),
                        beta(rs_id='rs11226029', effect_allele='G', beta_value=0.06765864847381486),
                        beta(rs_id='rs9319428', effect_allele='A', beta_value=0.04879016416943205),
                        beta(rs_id='rs9515203', effect_allele='T', beta_value=0.0769610411361284),
                        beta(rs_id='rs7173743', effect_allele='T', beta_value=0.06765864847381486),
                        beta(rs_id='rs17514846', effect_allele='A', beta_value=0.04879016416943205)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

