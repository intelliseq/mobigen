from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Alzheimer disease"
trait_short_description = "Alzheimer's disease"
trait_description = "" #from john
trait_pmids = ["30617256"] #30617256 Suppl.Table8: all independent lead SNPs which remained significant after conditional analysis (Suppl. Table 4); EA=Allele1, beta=Zscore
trait_heritability = 0.8 #estimates range from 0.6 to 0.8 
trait_snp_heritability = None 
trait_explained = 0.071  
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Predisposition to Alzheimer's disease is highly heritable - about 80% of differences observed among people has a genetic origin. Our analysis was based on 38 SNPs associated with late-onset Alzheimer's disease. These variants jointly explain about 7% of the variation in predisposition for this disease between people."
test_type = "Polygenic Risk Score" #"Single SNP analysis" if one snip
trait_classes=["low_risk","average_risk","high_risk"]
#trait_genotypes = ["hom", "het", "hom"] # for one snip analysis

#455258 individuals of European ancestry 
#GWAS performed in three phases: 
#phase1: case-control study (24,087 clinically diagnosed late-onset AD cases, paired with 55,058 controls);
#phase2: AD-by-proxy phenotype, based on individuals in the UK Biobank (UKB) for whom parental AD status was available (N proxy cases =​  47,793; N proxy controls =​ 328,320)
#phase3: meta-analysis results from phases 1 and 2
#statistics from phase3 used in our model; 

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-28.7, category_name='low_risk'),
                        QuantitativeCategory(from_=-28.7, to=68.3, category_name='average_risk'),
                        QuantitativeCategory(from_=68.3, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs4575098', effect_allele='A', beta_value=6.36),
                        beta(rs_id='rs2093760', effect_allele='A', beta_value=8.82),
                        beta(rs_id='rs4663105', effect_allele='C', beta_value=13.94),
                        beta(rs_id='rs10407439', effect_allele='G', beta_value=-6.13),
                        beta(rs_id='rs184384746', effect_allele='T', beta_value=5.69),
                        beta(rs_id='rs6448453', effect_allele='A', beta_value=6.00),
                        beta(rs_id='rs9469112', effect_allele='T', beta_value=6.44),
                        beta(rs_id='rs187370608', effect_allele='A', beta_value=8.26),
                        beta(rs_id='rs143332484', effect_allele='T', beta_value=5.54),
                        beta(rs_id='rs9381563', effect_allele='C', beta_value=6.33),
                        beta(rs_id='rs1859788', effect_allele='A', beta_value=-7.93),
                        beta(rs_id='rs7810606', effect_allele='T', beta_value=-6.62),
                        beta(rs_id='rs114360492', effect_allele='T', beta_value=5.99),
                        beta(rs_id='rs28834970', effect_allele='C', beta_value=6.80),
                        beta(rs_id='rs4236673', effect_allele='A', beta_value=-8.98),
                        beta(rs_id='rs11257238', effect_allele='C', beta_value=5.69),
                        beta(rs_id='rs2081545', effect_allele='A', beta_value=-7.97),
                        beta(rs_id='rs867611', effect_allele='G', beta_value=-8.75),
                        beta(rs_id='rs11218343', effect_allele='C', beta_value=-6.79),
                        beta(rs_id='rs12590654', effect_allele='A', beta_value=-6.39),
                        beta(rs_id='rs442495', effect_allele='C', beta_value=-6.07),
                        beta(rs_id='rs117618017', effect_allele='T', beta_value=5.52),
                        beta(rs_id='rs59735493', effect_allele='A', beta_value=-5.49),
                        beta(rs_id='rs113260531', effect_allele='A', beta_value=6.12),
                        beta(rs_id='rs28394864', effect_allele='A', beta_value=5.62),
                        beta(rs_id='rs76726049', effect_allele='C', beta_value=5.52),
                        beta(rs_id='rs111278892', effect_allele='G', beta_value=6.50),
                        beta(rs_id='rs28399637', effect_allele='A', beta_value=25.51),
                        beta(rs_id='rs10407439', effect_allele='A', beta_value=-16.14),
                        beta(rs_id='rs41289512', effect_allele='G', beta_value=35.50),
                        beta(rs_id='rs138607350', effect_allele='G', beta_value=17.06),
                        beta(rs_id='rs6859', effect_allele='A', beta_value=26.32),
                        beta(rs_id='rs79701229', effect_allele='A', beta_value=16.99),
                        beta(rs_id='rs7412', effect_allele='T', beta_value=-21.20),
                        beta(rs_id='rs1081105', effect_allele='C', beta_value=32.50),
                        beta(rs_id='rs76320948', effect_allele='T', beta_value=5.46),
                        beta(rs_id='rs3865444', effect_allele='A', beta_value=-5.81),
                        beta(rs_id='rs6014724', effect_allele='G', beta_value=-6.18)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["low_risk"] = "" #from john
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk "
description_risk["average_risk"] = "Moderate risk" 
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: Elevated risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["ADAMTS4", "AL137789.1", "BIN1", "NECTIN2", "HESX1", "LINC02498", "HLA-DRA", "UNC5CL", "TREM2", "AL355353.1", "PILRA", "EPHA1-AS1", "RF02271", "PTK2B", "MIR6843", "AL512631.1", "MS4A4A", "PICALM", "SORL1", "SLC24A4", "AC091046.2", "APH1B", "AC135050.6", "AC087500.2", "AC091180.5", "AC105105.3", "CNN2", "BCAM", "NECTIN2", "AC011481.3", "APOE", "AC011481.4", "BHMG1", "CD33", "CASS4"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the predisposition to Alzheimer's disease. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle." 

