from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Heart attack"
trait_short_description = "Heart attack"
trait_description = ""
trait_pmids = ["30349118","12270005"] #pmid:30349118 GeneAtlas paper, pmid:12270005 article on heritability
trait_heritability = 0.5 #0.4-0.6 heritability of death from coronary heart disease
trait_snp_heritability = 0.19 #30349118 Suppl. Table S1
trait_explained = None  
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Coronary heart disease is moderately heritable - about 40-50% of differences in susceptibility to this disease has a genetic origin. Our analysis was based on 7 SNPs associated with heart attack. These variants jointly explain a tiny portion of the variation in predisposition to heart attack."
test_type = "Polygenic Risk Score" #"Single SNP analysis" if one snip
trait_classes=["low_risk","average_risk","high_risk"]

#N=452264 individuals of European descent [Biobank UK]; 10356 cases 
#SNPs with p-value<5e-08; one SNP of the highest effect per ~ 500 kb regions chosen; effect=beta
#allele frequencies from gnomad

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.0012, category_name='low_risk'),
                        QuantitativeCategory(from_=0.0012, to=0.014, category_name='average_risk'),
                        QuantitativeCategory(from_=0.014, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs660240', effect_allele='C', beta_value=0.0029212),
                        beta(rs_id='rs3002139', effect_allele='A', beta_value=0.002846),
                        beta(rs_id='rs9349379', effect_allele='A', beta_value=-0.0028421),
                        beta(rs_id='rs140570886', effect_allele='C', beta_value=0.012542),
                        beta(rs_id='rs1537373', effect_allele='G', beta_value=0.0041039),
                        beta(rs_id='rs4766578', effect_allele='A', beta_value=-0.0019459),
                        beta(rs_id='rs7412', effect_allele='T', beta_value=-0.0039562)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: Elevated risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["CELSR2", "MIA3", "PHACTR1", "LPA", "RF01909", "ATXN2", "APOE"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are associated with heart attack. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
#description_genotype["low_trait"] = "hom" # for one snip test
#description_genotype["average_trait"] = "het" #for one snip test
#description_genotype["high_trait"] = "hom" # for one snip test
