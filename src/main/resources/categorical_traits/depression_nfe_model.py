from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Depression"
trait_short_description = "Recurrent depressive disorder"
trait_description = ""
trait_pmids = ["30349118","28608123"] #pmid:30349118 => GeneAtlas article, pmid:28608123 =>article on depression heritability 
trait_heritability = 0.37 
trait_snp_heritability = 0.099 #30349118 Suppl. Table S1
trait_explained = None  
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Depression susceptibility is moderately heritable - about 37% of differences observed among people has a genetic origin. Our analysis was based on 23 SNPs associated with predisposition to recurrent depression."
test_type = "Polygenic Risk Score" 
trait_classes=["average_risk","elevated_risk","high_risk"]
#N=452264 individuals of European descent [Biobank UK]; 792 cases
#data downloaded from: from http://geneatlas.roslin.ed.ac.uk 
#I analyzed variants with p<5e-08; one SNP of the biggest effect per region of 500 kb 
#risk categories assigned differently than usually (due to variant low frequencies): 
#low risk => all homozygotes not at risk
#moderate risk => at least one risk allele present [only one "large effect" or several "small effect" risk alleles] 
#high risk => two or more risk alleles present, for two risk alleles - at least one of them must be of "largr effect" (SNPs:rs28936704,rs531738678,rs201207803,rs185811837,rs201468340,rs146486931,rs141226767) 
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.307, category_name='average_risk'),
                        QuantitativeCategory(from_=-0.307, to=-0.275, category_name='elevated_risk'),
                        QuantitativeCategory(from_=-0.275, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs149745380', effect_allele='T', beta_value=0.0066555),
                        beta(rs_id='rs113797671', effect_allele='A', beta_value=0.0030622),
                        beta(rs_id='rs185187990', effect_allele='A', beta_value=0.0081296),
                        beta(rs_id='rs11695639', effect_allele='G', beta_value=0.0035024),
                        beta(rs_id='rs567439147', effect_allele='A', beta_value=0.0078839),
                        beta(rs_id='rs561423669', effect_allele='T', beta_value=0.0066447),
                        beta(rs_id='rs144638301', effect_allele='T', beta_value=0.0063347),
                        beta(rs_id='rs28936704', effect_allele='T', beta_value=-0.019908),
                        beta(rs_id='rs9869172', effect_allele='A', beta_value=-0.0069929),
                        beta(rs_id='rs143233462', effect_allele='C', beta_value=0.0033938),
                        beta(rs_id='rs201207803', effect_allele='G', beta_value=-0.021192),
                        beta(rs_id='rs185811837', effect_allele='G', beta_value=-0.014395),
                        beta(rs_id='rs192223194', effect_allele='T', beta_value=0.0086259),
                        beta(rs_id='rs201468340', effect_allele='G', beta_value=-0.019119),
                        beta(rs_id='rs145677127', effect_allele='C', beta_value=-0.012398),
                        beta(rs_id='rs139418808', effect_allele='A', beta_value=-0.0092642),
                        beta(rs_id='rs571538137', effect_allele='C', beta_value=0.0025973),
                        beta(rs_id='rs17052490', effect_allele='C', beta_value=0.0036256),
                        beta(rs_id='rs76985852', effect_allele='A', beta_value=0.0066762),
                        beta(rs_id='rs184076345', effect_allele='A', beta_value=0.0070908),
                        beta(rs_id='rs146486931', effect_allele='C', beta_value=-0.030614),
                        beta(rs_id='rs141226767', effect_allele='G', beta_value=-0.019676),
                        beta(rs_id='rs74740935', effect_allele='T', beta_value=0.0029677)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["elevated_risk"] = ""
description_title["elevated_risk"] = "Your Result: Elevated risk"
description_risk["elevated_risk"] = "Elevated risk"
description_recommendation["elevated_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: High risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["AC098484.2", "AL663023.1", "EPCAM-DT", "AC018880.2", "RPSAP23", "AC067945.1", "PCGEM1", "HESX1", "ENPP7P3", "AC110800.1", "EHMT2", "RF00019", "AC006970.3", "PRF1", "FBXL15", "RF02277", "AP000852.1", "PCDH8", "DAPK2", "RNU6-19P", "RLN3", "VPS16", "PTPRT"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the depression susceptibility. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
