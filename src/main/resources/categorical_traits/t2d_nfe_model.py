
from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Type 2 diabetes"
trait_short_description = "Type 2 diabetes"
trait_description = ""
trait_pmids = ["28869590","23961321"] 
trait_heritability = 0.5 # estimates range from 0.2 -0.8; taken from pmid:2396132
trait_snp_heritability = None 
trait_explained = None 
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Type 2 diabetes is moderately heritable - it is estimated that about 20-80% of differences in type 2 diabetes susceptibility has a genetic origin. Our analysis was based on 41 SNPs associated with this disease."
test_type = "Polygenic Risk Score" 
trait_classes=["low_risk","average_risk","high_risk"]

#data for EUR population only, GWAS statistics taken from Suppl. Table5: one SNP of the biggest effect per 500 kbp region
#4258 "cases" and 18519 "controls" (info on it from SuppTable1)
#EA frquencies: nfe gnomad

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.32, category_name='low_risk'),
                        QuantitativeCategory(from_=-0.32, to=0.68, category_name='average_risk'),
                        QuantitativeCategory(from_=0.68, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs10811661', effect_allele='C', beta_value=-0.156),
                        beta(rs_id='rs10830963', effect_allele='G', beta_value=0.094),
                        beta(rs_id='rs10842994', effect_allele='T', beta_value=-0.081),
                        beta(rs_id='rs11063069', effect_allele='G', beta_value=0.074),
                        beta(rs_id='rs1111875', effect_allele='T', beta_value=-0.107),
                        beta(rs_id='rs11257655', effect_allele='T', beta_value=0.077),
                        beta(rs_id='rs11708067', effect_allele='G', beta_value=-0.104),
                        beta(rs_id='rs12571751', effect_allele='G', beta_value=-0.066),
                        beta(rs_id='rs12970134', effect_allele='A', beta_value=0.071),
                        beta(rs_id='rs13266634', effect_allele='T', beta_value=-0.111),
                        beta(rs_id='rs1359790', effect_allele='A', beta_value=-0.077),
                        beta(rs_id='rs1470579', effect_allele='C', beta_value=0.103),
                        beta(rs_id='rs1552224', effect_allele='C', beta_value=-0.101),
                        beta(rs_id='rs17168486', effect_allele='T', beta_value=0.093),
                        beta(rs_id='rs1801282', effect_allele='G', beta_value=-0.113),
                        beta(rs_id='rs2237892', effect_allele='T', beta_value=-0.139),
                        beta(rs_id='rs2421016', effect_allele='T', beta_value=-0.056),
                        beta(rs_id='rs2796441', effect_allele='A', beta_value=-0.064),
                        beta(rs_id='rs2943640', effect_allele='C', beta_value=0.082),
                        beta(rs_id='rs3130501', effect_allele='G', beta_value=0.069),
                        beta(rs_id='rs340874', effect_allele='C', beta_value=0.06),
                        beta(rs_id='rs3794991', effect_allele='T', beta_value=0.13),
                        beta(rs_id='rs4457053', effect_allele='A', beta_value=-0.08),
                        beta(rs_id='rs459193', effect_allele='G', beta_value=0.078),
                        beta(rs_id='rs4607103', effect_allele='T', beta_value=-0.065),
                        beta(rs_id='rs4689388', effect_allele='A', beta_value=0.084),
                        beta(rs_id='rs516946', effect_allele='C', beta_value=0.078),
                        beta(rs_id='rs5215', effect_allele='T', beta_value=-0.075),
                        beta(rs_id='rs6813195', effect_allele='T', beta_value=-0.064),
                        beta(rs_id='rs7178572', effect_allele='G', beta_value=0.062),
                        beta(rs_id='rs7202877', effect_allele='G', beta_value=-0.106),
                        beta(rs_id='rs7578597', effect_allele='C', beta_value=-0.138),
                        beta(rs_id='rs7607980', effect_allele='C', beta_value=-0.093),
                        beta(rs_id='rs7674212', effect_allele='T', beta_value=-0.063),
                        beta(rs_id='rs7754840', effect_allele='C', beta_value=0.123),
                        beta(rs_id='rs7903146', effect_allele='T', beta_value=0.329),
                        beta(rs_id='rs7957197', effect_allele='A', beta_value=-0.077),
                        beta(rs_id='rs8042680', effect_allele='A', beta_value=0.063),
                        beta(rs_id='rs8050136', effect_allele='A', beta_value=0.107),
                        beta(rs_id='rs864745', effect_allele='C', beta_value=-0.085),
                        beta(rs_id='rs9505118', effect_allele='G', beta_value=-0.063)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: Elevated risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["CDKN2B-AS1", "MTNR1B", "RN7SKP15", "CCND2-AS1", "RF00019", "CDC123", "AC112503.1", "ZMIZ1", "AC090771.1", "SLC30A8", "AL137781.1", "IGF2BP2", "RPS12P20", "AC006150.1", "AC091492.1", "KCNQ1-AS1", "MIR3941", "AL591368.1", "AC062015.1", "POU5F1", "AC011700.1", "GATAD2A", "HMGB1P35", "C5orf67", "ADAMTS9-AS2", "WFS1", "AC113133.1", "KCNJ11", "AC023424.1", "AC090984.1", "CTRB1", "RN7SL531P", "SNORA70F", "BDH2", "AL035090.1", "RPS15AP30", "OASL", "RF00019", "FTO", "RNU6-979P", "AL139095.4"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence predisposition toward type 2 diabetes. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."

