from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Metabolic syndrome"
trait_short_description = "Metabolic syndrome"
trait_description = ""
trait_pmids = ["21386085","23918046"] #main paper:21386085, SNP data from table2; article on h2: 23918046
trait_heritability = 0.4 #0.3 - 0.5 for the metabolic syndrome related traits
trait_snp_heritability = None #as fraction
trait_explained = None  #as fraction
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "The concomitant traits which are characteristic for the metabolic syndrome (such as obesity, high glucose, cholesterol and trigliceryde blood levels) are moderately heritable - about 30 - 50% of differences within these traits has a genetic origin. Our analysis was based on three SNPs which are simultaneously associated with at least three metabolic-syndrome related traits."
test_type = "Polygenic Risk Score" #"Single SNP analysis" if one snip
trait_classes=["low_risk","average_risk","high_risk"]

#22161 participants of European ancestry, meta-analysis of 7 GWAS studies
#I took SNIPs associated with MetS per se (that means thet associated with 3 ot more metabolic-syndrome traits): Table2, EA=coded allele
#one SNP (having the largest effect) per ~ 500 kbp region 

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.321, category_name='low_risk'),
                        QuantitativeCategory(from_=0.321, to=0.681, category_name='average_risk'),
                        QuantitativeCategory(from_=0.681, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs295', effect_allele='A', beta_value=0.17),
                        beta(rs_id='rs2075290', effect_allele='C', beta_value=0.26),
                        beta(rs_id='rs173539', effect_allele='C', beta_value=0.16)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}
#description_genotype = {} #for single SNP test
description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: High risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["LPL", "ZPR1", "CETP"]
other_factors = "" 
trait_test_details = "We determined your genotype at three genomic positions, which are known to influence the metabolic syndrome susceptibility. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle." 
