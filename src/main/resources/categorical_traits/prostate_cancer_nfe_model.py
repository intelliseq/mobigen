from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Prostate cancer"
trait_short_description = "Prostate cancer"
trait_description = ""
trait_pmids = ["29892016","27294245"] #29892016 Suppl.Table16, heritability estimation - pmid:
trait_heritability = 0.58 #
trait_snp_heritability = None #as fraction
trait_explained = 0.28  #this is explained familial risk estimate
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Prostate cancer is one of the most heritable cancers - about 60% of differences in this cancer susceptibility has a genetic origin. Our analysis was based on 142 SNPs associated with this disease. These variants are scattered across 140 genes and they jointly explain about 28% of the variation in prostate cancer risk."
test_type = "Polygenic Risk Score" 
trait_classes=["low_risk","average_risk","high_risk"]

#study design:Meta-analysis of genotype data from a custom high-density array of 46,939 PrCa cases and 27,910 controls of European ancestry
# with previously genotyped data of 32,255 PrCa cases and 33,202 controls of European ancestry
#I took vatiants (beta, EA) from pmid:29892016 Suppl.Table16 (85 previously reported PrCa loci replicating in the overall meta-analysis and 62 novel loci associated with overall PrCa risk)
#3 variants not in gnomad/not at array were substituted by LD proxy with LD > 0.99 (https://ldlink.nci.nih.gov/)
#4 variants not in gnomAD/not at array were removed

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=11.59, category_name='low_risk'),
                        QuantitativeCategory(from_=11.59, to=13.23, category_name='average_risk'),
                        QuantitativeCategory(from_=13.23, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs12139208', effect_allele='T', beta_value=0.047),
                        beta(rs_id='rs62106670', effect_allele='T', beta_value=0.052),
                        beta(rs_id='rs74702681', effect_allele='T', beta_value=0.159),
                        beta(rs_id='rs11691517', effect_allele='T', beta_value=0.064),
                        beta(rs_id='rs34925593', effect_allele='C', beta_value=0.047),
                        beta(rs_id='rs59308963', effect_allele='T', beta_value=0.051),
                        beta(rs_id='rs1283104', effect_allele='G', beta_value=0.047),
                        beta(rs_id='rs79223973', effect_allele='A', beta_value=0.089),
                        beta(rs_id='rs142436749', effect_allele='G', beta_value=0.221),
                        beta(rs_id='rs10793821', effect_allele='T', beta_value=0.053),
                        beta(rs_id='rs76551843', effect_allele='A', beta_value=0.271),
                        beta(rs_id='rs4976790', effect_allele='T', beta_value=0.074),
                        beta(rs_id='rs12665339', effect_allele='G', beta_value=0.062),
                        beta(rs_id='rs9296068', effect_allele='T', beta_value=0.048),
                        beta(rs_id='rs9469899', effect_allele='A', beta_value=0.048),
                        beta(rs_id='rs4711748', effect_allele='T', beta_value=0.052),
                        beta(rs_id='rs527510716', effect_allele='C', beta_value=0.059),
                        beta(rs_id='rs11452686', effect_allele='T', beta_value=0.05),
                        beta(rs_id='rs17621345', effect_allele='A', beta_value=0.072),
                        beta(rs_id='rs1048169', effect_allele='C', beta_value=0.061),
                        beta(rs_id='rs10122495', effect_allele='T', beta_value=0.05),
                        beta(rs_id='rs1182', effect_allele='A', beta_value=0.058),
                        beta(rs_id='rs141536087', effect_allele='GCGCA', beta_value=0.081),
                        beta(rs_id='rs1935581', effect_allele='C', beta_value=0.048),
                        beta(rs_id='rs7094871', effect_allele='G', beta_value=0.044),
                        beta(rs_id='rs1881502', effect_allele='T', beta_value=0.058),
                        beta(rs_id='rs375314312', effect_allele='G', beta_value=0.047),
                        beta(rs_id='rs2277283', effect_allele='C', beta_value=0.056),
                        beta(rs_id='rs12785905', effect_allele='C', beta_value=0.116),
                        beta(rs_id='rs11290954', effect_allele='AC', beta_value=0.061),
                        beta(rs_id='rs1800057', effect_allele='G', beta_value=0.15),
                        beta(rs_id='rs138466039', effect_allele='T', beta_value=0.281),
                        beta(rs_id='rs878987', effect_allele='G', beta_value=0.064),
                        beta(rs_id='rs2066827', effect_allele='T', beta_value=0.056),
                        beta(rs_id='rs10845938', effect_allele='G', beta_value=0.057),
                        beta(rs_id='rs7968403', effect_allele='T', beta_value=0.059),
                        beta(rs_id='rs5799921', effect_allele='GA', beta_value=0.061),
                        beta(rs_id='rs7295014', effect_allele='G', beta_value=0.052),
                        beta(rs_id='rs1004030', effect_allele='T', beta_value=0.046),
                        beta(rs_id='rs11629412', effect_allele='C', beta_value=0.057),
                        beta(rs_id='rs4924487', effect_allele='C', beta_value=0.062),
                        beta(rs_id='rs33984059', effect_allele='A', beta_value=0.176),
                        beta(rs_id='rs11863709', effect_allele='C', beta_value=0.148),
                        beta(rs_id='rs28441558', effect_allele='C', beta_value=0.151),
                        beta(rs_id='rs142444269', effect_allele='C', beta_value=0.067),
                        beta(rs_id='rs2680708', effect_allele='G', beta_value=0.046),
                        beta(rs_id='rs8093601', effect_allele='C', beta_value=0.045),
                        beta(rs_id='rs28607662', effect_allele='C', beta_value=0.075),
                        beta(rs_id='rs12956892', effect_allele='T', beta_value=0.05),
                        beta(rs_id='rs10460109', effect_allele='T', beta_value=0.044),
                        beta(rs_id='rs11666569', effect_allele='C', beta_value=0.052),
                        beta(rs_id='rs118005503', effect_allele='G', beta_value=0.09),
                        beta(rs_id='rs61088131', effect_allele='T', beta_value=0.062),
                        beta(rs_id='rs11480453', effect_allele='C', beta_value=0.046),
                        beta(rs_id='rs6091758', effect_allele='G', beta_value=0.072),
                        beta(rs_id='rs9625483', effect_allele='A', beta_value=0.134),
                        beta(rs_id='rs17321482', effect_allele='C', beta_value=0.067),
                        beta(rs_id='rs17599629', effect_allele='G', beta_value=0.065),
                        beta(rs_id='rs1218582', effect_allele='G', beta_value=0.046),
                        beta(rs_id='rs4245739', effect_allele='A', beta_value=0.092),
                        beta(rs_id='rs9287719', effect_allele='C', beta_value=0.066),
                        beta(rs_id='rs9306895', effect_allele='C', beta_value=0.077),
                        beta(rs_id='rs1465618', effect_allele='T', beta_value=0.083),
                        beta(rs_id='rs721048', effect_allele='A', beta_value=0.097),
                        beta(rs_id='rs10187424', effect_allele='T', beta_value=0.074),
                        beta(rs_id='rs12621278', effect_allele='A', beta_value=0.242),
                        beta(rs_id='rs2292884', effect_allele='G', beta_value=0.061),
                        beta(rs_id='rs3771570', effect_allele='T', beta_value=0.084),
                        beta(rs_id='rs2660753', effect_allele='T', beta_value=0.12),
                        beta(rs_id='rs7611694', effect_allele='A', beta_value=0.083),
                        beta(rs_id='rs10934853', effect_allele='A', beta_value=0.099),
                        beta(rs_id='rs6763931', effect_allele='A', beta_value=0.043),
                        beta(rs_id='rs10936632', effect_allele='A', beta_value=0.097),
                        beta(rs_id='rs10009409', effect_allele='T', beta_value=0.056),
                        beta(rs_id='rs1894292', effect_allele='G', beta_value=0.062),
                        beta(rs_id='rs12500426', effect_allele='A', beta_value=0.069),
                        beta(rs_id='rs17021918', effect_allele='C', beta_value=0.085),
                        beta(rs_id='rs7679673', effect_allele='C', beta_value=0.12),
                        beta(rs_id='rs2242652', effect_allele='G', beta_value=0.16),
                        beta(rs_id='rs12653946', effect_allele='T', beta_value=0.079),
                        beta(rs_id='rs2121875', effect_allele='C', beta_value=0.048),
                        beta(rs_id='rs4713266', effect_allele='C', beta_value=0.051),
                        beta(rs_id='rs7767188', effect_allele='A', beta_value=0.054),
                        beta(rs_id='rs3096702', effect_allele='A', beta_value=0.056),
                        beta(rs_id='rs3129859', effect_allele='G', beta_value=0.06),
                        beta(rs_id='rs1983891', effect_allele='T', beta_value=0.082),
                        beta(rs_id='rs9443189', effect_allele='A', beta_value=0.064),
                        beta(rs_id='rs2273669', effect_allele='G', beta_value=0.069),
                        beta(rs_id='rs339331', effect_allele='T', beta_value=0.084),
                        beta(rs_id='rs1933488', effect_allele='A', beta_value=0.076),
                        beta(rs_id='rs9364554', effect_allele='T', beta_value=0.104),
                        beta(rs_id='rs12155172', effect_allele='A', beta_value=0.093),
                        beta(rs_id='rs10486567', effect_allele='G', beta_value=0.134),
                        beta(rs_id='rs56232506', effect_allele='A', beta_value=0.054),
                        beta(rs_id='rs6465657', effect_allele='C', beta_value=0.101),
                        beta(rs_id='rs2928679', effect_allele='A', beta_value=0.053),
                        beta(rs_id='rs1512268', effect_allele='T', beta_value=0.128),
                        beta(rs_id='rs11135910', effect_allele='T', beta_value=0.078),
                        beta(rs_id='rs12543663', effect_allele='C', beta_value=0.111),
                        beta(rs_id='rs10086908', effect_allele='T', beta_value=0.126),
                        beta(rs_id='rs183373024', effect_allele='G', beta_value=1.068),
                        beta(rs_id='rs16901979', effect_allele='A', beta_value=0.445),
                        beta(rs_id='rs620861', effect_allele='G', beta_value=0.139),
                        beta(rs_id='rs6983267', effect_allele='G', beta_value=0.2),
                        beta(rs_id='rs1447295', effect_allele='A', beta_value=0.345),
                        beta(rs_id='rs17694493', effect_allele='G', beta_value=0.073),
                        beta(rs_id='rs76934034', effect_allele='T', beta_value=0.115),
                        beta(rs_id='rs10993994', effect_allele='T', beta_value=0.208),
                        beta(rs_id='rs3850699', effect_allele='A', beta_value=0.07),
                        beta(rs_id='rs4962416', effect_allele='C', beta_value=0.059),
                        beta(rs_id='rs7127900', effect_allele='A', beta_value=0.17),
                        beta(rs_id='rs7931342', effect_allele='G', beta_value=0.157),
                        beta(rs_id='rs11568818', effect_allele='T', beta_value=0.074),
                        beta(rs_id='rs11214775', effect_allele='G', beta_value=0.071),
                        beta(rs_id='rs80130819', effect_allele='A', beta_value=0.096),
                        beta(rs_id='rs10875943', effect_allele='C', beta_value=0.069),
                        beta(rs_id='rs902774', effect_allele='A', beta_value=0.126),
                        beta(rs_id='rs1270884', effect_allele='A', beta_value=0.07),
                        beta(rs_id='rs8008270', effect_allele='C', beta_value=0.083),
                        beta(rs_id='rs7141529', effect_allele='C', beta_value=0.051),
                        beta(rs_id='rs8014671', effect_allele='G', beta_value=0.047),
                        beta(rs_id='rs684232', effect_allele='C', beta_value=0.083),
                        beta(rs_id='rs11649743', effect_allele='G', beta_value=0.122),
                        beta(rs_id='rs4430796', effect_allele='A', beta_value=0.197),
                        beta(rs_id='rs138213197', effect_allele='T', beta_value=1.348),
                        beta(rs_id='rs11650494', effect_allele='A', beta_value=0.099),
                        beta(rs_id='rs1859962', effect_allele='G', beta_value=0.161),
                        beta(rs_id='rs7241993', effect_allele='C', beta_value=0.076),
                        beta(rs_id='rs8102476', effect_allele='C', beta_value=0.09),
                        beta(rs_id='rs11672691', effect_allele='G', beta_value=0.092),
                        beta(rs_id='rs2735839', effect_allele='G', beta_value=0.167),
                        beta(rs_id='rs12480328', effect_allele='T', beta_value=0.107),
                        beta(rs_id='rs2427345', effect_allele='C', beta_value=0.045),
                        beta(rs_id='rs6062509', effect_allele='T', beta_value=0.078),
                        beta(rs_id='rs1041449', effect_allele='G', beta_value=0.051),
                        beta(rs_id='rs58133635', effect_allele='T', beta_value=0.068),
                        beta(rs_id='rs5759167', effect_allele='G', beta_value=0.142),
                        beta(rs_id='rs2405942', effect_allele='A', beta_value=0.049),
                        beta(rs_id='rs5945619', effect_allele='C', beta_value=0.104),
                        beta(rs_id='rs2807031', effect_allele='C', beta_value=0.058),
                        beta(rs_id='rs5919432', effect_allele='T', beta_value=0.043),
                        beta(rs_id='rs6625711', effect_allele='A', beta_value=0.008)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}
description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: High risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["AL445437.1", "AC092617.1", "MEIS1-AS3", "BCL2L11", "CDCA7", "CASP8", "DUBR", "MBNL1-AS1", "AC074033.1", "RNU6-456P", "AC008680.1", "COL23A1", "PTMAP1", "HLA-DOA", "RF00019", "AL136131.2", "MIR4655", "RF00275", "SUGCT", "HAUS6", "RN7SKP114", "TOR1A", "LARP4B", "RNLS", "AL158212.2", "MOB2", "MIR4487", "INCENP", "AP001885.1", "EMSY", "RF00019", "AP001007.3", "B3GAT1", "AC008115.3", "GNAI2P1", "MIR548C", "RNU6-148P", "FBRSL1", "MMP14", "PAX9", "RN7SL376P", "RFX7", "ADGRG1", "SCARNA21", "AC007923.1", "RNF43", "MBD2", "TCF4-AS2", "AC040963.1", "TSHZ1", "AC020908.3", "RNA5SP471", "POU2F2", "DNMT3B", "SUMO1P1", "MIR5739", "AC002366.1", "GOLPH3L", "KCNN3", "MDM4", "NOL10", "AC012065.4", "RNU6-958P", "AC007098.2", "RN7SL126P", "AC078883.2", "MLPH", "MIR3133", "PPATP1", "SIDT1", "EEFSEC", "ZBTB38", "AC073288.2", "RNU4ATAC9P", "AFM", "PDLIM5", "AC004069.1", "TERT", "AC025183.2", "FGF10-AS1", "NEDD9", "TRIM31-AS1", "NOTCH4", "HLA-DRA", "FOXP4", "RNU6-155P", "ARMC2", "RFX6", "RGS17", "SLC22A3", "AC080068.1", "JAZF1", "TNS3", "LMTK2", "SLC25A37", "NKX3-1", "EBF2", "AC024382.1", "CASC19", "PRNCR1", "AC020688.1", "POU5F1B", "CCAT2", "CASC8", "RF02045", "MARCH8", "MSMB", "TRIM8", "AL731571.1", "MIR4686", "AP003071.2", "MMP7", "HTR3B", "AC004801.4", "TUBA1C", "RPL7P41", "LINC02459", "AL139317.4", "AL121820.1", "RN7SL77P", "VPS53", "HNF1B", "HOXB13", "AC004797.1", "CASC17", "SALL3", "SPINT2", "PCAT19", "KLK2", "PSMD10P1", "AL121832.1", "AL121845.4", "TMPRSS2", "TNRC6B", "BIK", "HMGN1P33", "NUDT11", "XAGE3", "BMI1P1", "SLC7A3"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the prostate cancer susceptibility. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."

