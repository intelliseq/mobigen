from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio

trait_title = "Lung cancer"
trait_short_description = "Lung cancer"
trait_description = ""
trait_pmids = ["28604730"] #GWAS statistics from 28604730 Table2, one variant has now different name rs116822326 => rs3094604
trait_heritability = 0.18
trait_snp_heritability = None 
trait_explained = 0.12 #this is explained familial relative risk, not variance explained
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Lung cancer suctebility depends partially on genetics. It is estimated that only about 18% of differences in this cancer risk has a genetic origin. Our analysis was based on 17 SNPs, which jointly explain about 12% of the differences in lung cancer risk."
test_type = "Polygenic Risk Score" 
trait_classes=["low_risk","average_risk","high_risk"]
#14,803 cases and 12,262 controls of European descent were genotyped on the OncoArray 
#and combined with existing data for an aggregated GWAS analysis of lung cancer on 29,266 patients and 56,450 controls
#EA frequecnies from gnomAD (nfe)
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.74, category_name='low_risk'),
                        QuantitativeCategory(from_=0.74, to=2.04, category_name='average_risk'),
                        QuantitativeCategory(from_=2.04, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        odds_ratio(rs_id='rs71658797', effect_allele='T', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs6920364', effect_allele='G', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs11780471', effect_allele='G', odds_ratio_value=0.9),
                        odds_ratio(rs_id='rs11571833', effect_allele='A', odds_ratio_value=1.6),
                        odds_ratio(rs_id='rs66759488', effect_allele='G', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs55781567', effect_allele='C', odds_ratio_value=1.3),
                        odds_ratio(rs_id='rs56113850', effect_allele='C', odds_ratio_value=0.9),
                        odds_ratio(rs_id='rs13080835', effect_allele='G', odds_ratio_value=0.9),
                        odds_ratio(rs_id='rs7705526', effect_allele='C', odds_ratio_value=1.3),
                        odds_ratio(rs_id='rs4236709', effect_allele='A', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs885518', effect_allele='A', odds_ratio_value=1.2),
                        odds_ratio(rs_id='rs11591710', effect_allele='A', odds_ratio_value=1.2),
                        odds_ratio(rs_id='rs1056562', effect_allele='C', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs77468143', effect_allele='T', odds_ratio_value=0.9),
                        odds_ratio(rs_id='rs41309931', effect_allele='G', odds_ratio_value=1.2),
                        odds_ratio(rs_id='rs7953330', effect_allele='G', odds_ratio_value=0.9),
                        odds_ratio(rs_id='rs17879961', effect_allele='A', odds_ratio_value=0.4)
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}
description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_riskt"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = ""
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: Elevated risk"
description_risk["high_risk"] = "Elevated risk"
description_recommendation["high_risk"] = ""
trait_genes = ["AK5", "RNASET2", "EPHX2", "BRCA2", "AC084882.1", "CHRNA5", "CYP2A6", "AC078809.1", "TERT", "AC083977.1", "TUBB8P1", "STN1", "MPZL2", "AC013452.2", "TNFRSF6B", "WNK1", "HSCB"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the lung cancer risk. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle." 

