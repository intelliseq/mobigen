from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio

trait_title = "Skin melanoma"
trait_short_description = "Skin melanoma"
trait_description = ""
trait_pmids = ["18488026","19578364","21983785","21983787","23455637","24980573","26237428","28212542"] 
trait_heritability = None 
trait_snp_heritability = None #as fraction
trait_explained = 0.19 #this is explained by the 20 known melanoma loci, estimation from pmid: 26237428 
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Our analysis was based on 19 SNPs associated with melanoma. These variants jointly explain about 19% of the variation in this skin cancer susceptibility."
test_type = "Polygenic Risk Score" #"Single SNP analysis" if one snip
trait_classes=["low_risk","average_risk","high_risk"]
#based on GWAS catalog; disease/trait: Melanoma/Cutaneus malignant melanoma, risk allele given, p-value <5e-08
#I left not corellated loci (https://ldlink.nci.nih.gov/?tab=snpclip)
#I changed EA for rs401681 (from A to T, A is not known, all other cancers are associated with T)
#for pmid:26237428 - I converted  coefficients beta to OR [OR=exp(beta) => I checked that it works for variants for which both values were given]
#allel frequencies from gnomad

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.142, category_name='low_risk'),
                        QuantitativeCategory(from_=0.142, to=0.483, category_name='average_risk'),
                        QuantitativeCategory(from_=0.483, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        odds_ratio(rs_id='rs7412746', effect_allele='T', odds_ratio_value=1.15),
                        odds_ratio(rs_id='rs6750047', effect_allele='A', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs13016963', effect_allele='A', odds_ratio_value=1.14),
                        odds_ratio(rs_id='rs401681', effect_allele='T', odds_ratio_value=1.2),
                        odds_ratio(rs_id='rs187843643', effect_allele='T', odds_ratio_value=1.96),
                        odds_ratio(rs_id='rs35407', effect_allele='G', odds_ratio_value=2.08),
                        odds_ratio(rs_id='rs62389423', effect_allele='A', odds_ratio_value=1.24),
                        odds_ratio(rs_id='rs6914598', effect_allele='C', odds_ratio_value=1.11),
                        odds_ratio(rs_id='rs1636744', effect_allele='T', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs10739221', effect_allele='T', odds_ratio_value=1.13),
                        odds_ratio(rs_id='rs2995264', effect_allele='G', odds_ratio_value=1.17),
                        odds_ratio(rs_id='rs498136', effect_allele='A', odds_ratio_value=1.13),
                        odds_ratio(rs_id='rs1393350', effect_allele='A', odds_ratio_value=1.29),
                        odds_ratio(rs_id='rs4778138', effect_allele='G', odds_ratio_value=0.84),
                        odds_ratio(rs_id='rs16953002', effect_allele='A', odds_ratio_value=1.16),
                        odds_ratio(rs_id='rs258322', effect_allele='A', odds_ratio_value=1.67),
                        odds_ratio(rs_id='rs4785763', effect_allele='A', odds_ratio_value=1.36),
                        odds_ratio(rs_id='rs6059655', effect_allele='A', odds_ratio_value=1.38),
                        odds_ratio(rs_id='rs45430', effect_allele='T', odds_ratio_value=1.12)
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}
description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: High risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["CTXND2", "CYP1B1", "ALS2CR12", "CLPTM1L", "LINC02218", "SLC45A2", "IRF4", "CDKAL1", "AC098592.1", "AL732323.1", "STN1", "LINC01488", "CBX3P7", "OCA2", "AC007347.2", "LINC02166", "AFG3L1P", "MIR4755", "MX2"]
other_factors = "" #Skin tan, nevus count 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the skin melanoma predisposition. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the sun exposition."

