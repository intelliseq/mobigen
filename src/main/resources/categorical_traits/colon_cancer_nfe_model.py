from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio

trait_title = "Colorectal cancer"
trait_short_description = "Colorectal cancer"
trait_description = ""
trait_pmids = ["30510241","29917119","21655089"] #30510241 table1 plus Suppl Table3; values from meta-analysis (Stage3)
trait_heritability = 0.35
trait_snp_heritability = 0.22
trait_explained = 0.11 #relative risk explained
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Colorectal cancer is moderately heritable - about 35% of differences observed among people has a genetic origin. Our analysis was based on 59 SNPs associated with this cancer. These variants jointly explain about 11% of the variation in colon and rectal cancer risk."
test_type = "Polygenic Risk Score" #"Single SNP analysis" if one snip
trait_classes=["low_risk","average_risk","high_risk"]

#Stage1: WGS of 1439 cases + 720 controls (European ancestry) => discovered sequence variants and Haplotype Reference Consortium panel variants into  data 30 existing GWAS 
#=> GWAS (34,869 cases and 29,051 controls, predominantly of European (91.7%) and East Asian ancestry (8.3%))
#Stage2: Findings were followed up in an additional 23,262 cases and 38,296 controls  
# Stage3: meta-analysis Stage1+Stage2
#GWAS statistics about novel SNPs from Table1 (meta-analysis, stage3)
#GWAS statistics for established SNPs from Suppl. Table3 (meta-analysis, stage; p-value <5e-08, flaged as European or European and East Asian)
#Alleles in Suppl. Table 3 were not correct (not existing, to assigned risk allele I checked all SNPs and allele status in 1)GWAS catalog or 2)pmid:29917119 or 3)pmid:21655089 => SNPs not found were removed)
#The above step took me two hours
#I left one significant SNP per region of 500 kbp (largest effect)

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.84, category_name='low_risk'),
                        QuantitativeCategory(from_=0.84, to=2.32, category_name='average_risk'),
                        QuantitativeCategory(from_=2.32, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        odds_ratio(rs_id='rs4360494', effect_allele='G', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs12144319', effect_allele='C', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs10911251', effect_allele='A', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs6691170', effect_allele='T', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs448513', effect_allele='C', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs983402', effect_allele='T', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs992157', effect_allele='A', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs35360328', effect_allele='A', odds_ratio_value=1.10),
                        odds_ratio(rs_id='rs72942485', effect_allele='G', odds_ratio_value=1.19),
                        odds_ratio(rs_id='rs10049390', effect_allele='A', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs1370821', effect_allele='T', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs1391441', effect_allele='A', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs11727676', effect_allele='C', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs2735940', effect_allele='G', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs145364999', effect_allele='T', odds_ratio_value=1.74),
                        odds_ratio(rs_id='rs647161', effect_allele='A', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs9271695', effect_allele='G', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs6906359', effect_allele='C', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs62404968', effect_allele='C', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs12672022', effect_allele='T', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs16892766', effect_allele='C', odds_ratio_value=1.20),
                        odds_ratio(rs_id='rs6983267', effect_allele='G', odds_ratio_value=1.16),
                        odds_ratio(rs_id='rs1537372', effect_allele='G', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs34405347', effect_allele='T', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs10980628', effect_allele='C', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs10795668', effect_allele='G', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs10994860', effect_allele='C', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs1035209', effect_allele='T', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs3824999', effect_allele='G', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs2186607', effect_allele='T', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs3802842', effect_allele='C', odds_ratio_value=1.12),
                        odds_ratio(rs_id='rs3217810', effect_allele='T', odds_ratio_value=1.13),
                        odds_ratio(rs_id='rs11169552', effect_allele='C', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs4759277', effect_allele='A', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs3184504', effect_allele='C', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs7333607', effect_allele='G', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs78341008', effect_allele='C', odds_ratio_value=1.12),
                        odds_ratio(rs_id='rs8000189', effect_allele='T', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs4444235', effect_allele='C', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs17094983', effect_allele='G', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs11632715', effect_allele='A', odds_ratio_value=1.15),
                        odds_ratio(rs_id='rs56324967', effect_allele='C', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs9929218', effect_allele='G', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs9930005', effect_allele='C', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs2696839', effect_allele='G', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs1078643', effect_allele='A', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs983318', effect_allele='A', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs75954926', effect_allele='G', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs4939827', effect_allele='T', odds_ratio_value=1.17),
                        odds_ratio(rs_id='rs34797592', effect_allele='T', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs10411210', effect_allele='T', odds_ratio_value=1.21),
                        odds_ratio(rs_id='rs73068325', effect_allele='T', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs961253', effect_allele='A', odds_ratio_value=1.10),
                        odds_ratio(rs_id='rs2423279', effect_allele='C', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs2295444', effect_allele='C', odds_ratio_value=1.05),
                        odds_ratio(rs_id='rs6031311', effect_allele='T', odds_ratio_value=1.06),
                        odds_ratio(rs_id='rs6066825', effect_allele='A', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs1810502', effect_allele='C', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs4925386', effect_allele='C', odds_ratio_value=1.12)
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}
#description_genotype = {} #for single SNP test
description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: High risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["SF3A3", "TTC22", "LAMC1-AS1", "LINC02257", "GSTM3P2", "AC020718.1", "TMBIM1", "AC099560.1", "BOC", "SLCO2A1", "AC096746.1", "TET2-AS1", "RF02271", "TERT", "CHD1", "AC008406.1", "HLA-DQA1", "AL033519.2", "BMP5", "TBRG4", "EIF3H", "CCAT2", "RF01909", "AL136084.1", "RNU6-432P", "RNA5SP299", "A1CF", "SLC25A28", "RANP3", "AP000776.1", "COLCA2", "AC008012.1", "ATF1", "LRP1-AS", "ATXN2", "SMAD9", "RNY1P8", "COL4A2-AS2", "MIR5580", "AL049873.1", "AC090877.2", "AC012568.1", "AC099314.1", "AC092130.1", "LINC01081", "TMEM238L", "AC007639.1", "AC144831.1", "SMAD7", "AC020911.2", "AC008521.1", "MZF1", "CASC20", "AL031679.1", "PIGU", "RN7SL443P", "AL035106.1", "RN7SL636P", "LAMA5-AS1"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the colon or rectum cancer risk. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
