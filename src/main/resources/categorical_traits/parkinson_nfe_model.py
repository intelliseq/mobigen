from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "Parkinson's disease"
trait_short_description = "Parkinson's disease"
trait_description = ""
trait_pmids = ["30349118", "20203693"] # 30349118 - main article; 20203693 heritability
trait_heritability = 0.4 #pmid: 20203693
trait_snp_heritability = 0.15 # pmid: 3034118, Suppl. Table S1
trait_explained = 0.05 # 0.4-0.6, depending on prevalence used https://gump.qimr.edu.au/genroc/genroc_calc_nrw.cgi?Prevalence=0.01&AUCMax=0.81&AUCMaxH=0.72&lambda=1.73&AUCMaxQ=0.66&AUCFH2=0.53&h2l=0.26&R2=0.06&h2lprop=0.21&AUC=0.65&lambdax=1.1&lambdaratio=0.18&T=1.75&z=0.09&i=2.15&K1=0.07&T1=1.48&T1x=1.69 
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Trait is moderately heritable - about 40% of differences observed among people has a genetic origin. Our analysis was based on 10 SNPs associated with predisposition to Parkinson's disease. These variants explain about 5% of  variation in Parkinson's disease susceptibility."
test_type = "Polygenic Risk Score" 
trait_classes=["low_risk","average_risk","high_risk"]

#GWAS statistics downloaded from http://geneatlas.roslin.ed.ac.uk
#SNPs with p-value<5e-08; one SNP of the highest effect per 500 kb regions chosen [exeptions: no rsid, none of the biggest effect SNPs on the array]; effect=beta
#N=452264 individuals of European descent [Biobank UK]; 1239 cases

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=-0.0183, category_name='low_risk'),
                        QuantitativeCategory(from_=-0.0183, to=-0.0169, category_name='average_risk'),
                        QuantitativeCategory(from_=-0.0169, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs148770722', effect_allele='G', beta_value=-0.0084672),
                        beta(rs_id='rs356219', effect_allele='A', beta_value=-0.00066764),
                        beta(rs_id='rs59601482', effect_allele='T', beta_value=0.009008),
                        beta(rs_id='rs563232539', effect_allele='A', beta_value=0.0092998),
                        beta(rs_id='rs72979569', effect_allele='T', beta_value=0.0012756),
                        beta(rs_id='rs780056077', effect_allele='A', beta_value=0.0090714),
                        beta(rs_id='rs143319539', effect_allele='T', beta_value=0.0075727),
                        beta(rs_id='rs77861384', effect_allele='G', beta_value=0.009693),
                        beta(rs_id='rs148027105', effect_allele='A', beta_value=0.0053505),
                        beta(rs_id='rs113863578', effect_allele='G', beta_value=0.0085547)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = ""
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["average_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = ""
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: Elevated risk"
description_risk["high_risk"] = ""
description_recommendation["high_risk"] = ""
trait_genes = ["SEMA6C", "SNCA", "YTHDC2", "RBMXP1", "RNA5SP219", "AL049844.1", "AC005088.1", "ITGA8", "HEBP1", "RNA5SP395"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the Parkinson's disease susceptibility. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle."
