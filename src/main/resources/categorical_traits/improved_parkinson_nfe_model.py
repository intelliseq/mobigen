from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio

trait_title = "Parkinson's disease"
trait_short_description = "Parkinson's disease"
trait_description = ""
trait_pmids = ["28892059","21482443"] #GWAS statistics from 28892059 table2 + http://research-pub.gene.com/chang_et_al_2017; 
trait_heritability = 0.4 # pmid:21482443
trait_snp_heritability = 0.21
trait_explained = None  
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Parkinson's disease is moderately heritable - about 40% of differences in Parkinson's disease risk has a genetic origin. Our analysis was based on 45 SNPs associated with this disorder." # These variants jointly explain about 12% of the variation in trait between people.""
test_type = "Polygenic Risk Score" #"Single SNP analysis" if one snip
trait_classes=["low_risk","average_risk","high_risk"]
#Study design: 
#Stage1: meta-analysis of two GWAS studies PDWBS (6476 cases+302042 controls; 23andMe web based, European ancestry) and PDGene (13708 cases + 95282 controls)
#Stage 2: replication meta-analysis (data from NeuroX added 5851 cases + 5866 controls, European ancestry)
#the discovery phase meta-analysis data downloaded from http://research-pub.gene.com/chang_et_al_2017) - Stage1;
#Stage1: I filtered out snips with p-value>5e-08 (in meta-analysis),
#then I took one snip per block of significant snips (such regions had size of ~ 500 to 1000kb), usually I took SNP having the highest effect,
#but I left several lower effect snips that are on our genotyping array
#Stage2: novel snips taken from Table2 – only for regions which were not significant in Stage1 (OR  for Stage2)
#frequencies used in simulation: from gnomAD
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.041, category_name='low_risk'),
                        QuantitativeCategory(from_=0.041, to=0.122, category_name='average_risk'),
                        QuantitativeCategory(from_=0.122, category_name='high_risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        odds_ratio(rs_id='rs145331499', effect_allele='G', odds_ratio_value=0.5),
                        odds_ratio(rs_id='rs12748961', effect_allele='C', odds_ratio_value=1.149),
                        odds_ratio(rs_id='rs4653767', effect_allele='C', odds_ratio_value=0.917),
                        odds_ratio(rs_id='rs10797576', effect_allele='T', odds_ratio_value=1.118),
                        odds_ratio(rs_id='rs34043159', effect_allele='C', odds_ratio_value=1.073),
                        odds_ratio(rs_id='rs6430538', effect_allele='T', odds_ratio_value=0.891),
                        odds_ratio(rs_id='rs1474055', effect_allele='C', odds_ratio_value=0.825),
                        odds_ratio(rs_id='rs353116', effect_allele='T', odds_ratio_value=0.94),
                        odds_ratio(rs_id='rs73038319', effect_allele='C', odds_ratio_value=1.183),
                        odds_ratio(rs_id='rs12497850', effect_allele='G', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs143918452', effect_allele='G', odds_ratio_value=0.68),
                        odds_ratio(rs_id='rs12637471', effect_allele='A', odds_ratio_value=0.847),
                        odds_ratio(rs_id='rs34311866', effect_allele='C', odds_ratio_value=1.232),
                        odds_ratio(rs_id='rs12651314', effect_allele='T', odds_ratio_value=0.883),
                        odds_ratio(rs_id='rs6812193', effect_allele='T', odds_ratio_value=0.916),
                        odds_ratio(rs_id='rs151186370', effect_allele='T', odds_ratio_value=0.687),
                        odds_ratio(rs_id='rs78738012', effect_allele='C', odds_ratio_value=1.135),
                        odds_ratio(rs_id='rs11744756', effect_allele='T', odds_ratio_value=1.151),
                        odds_ratio(rs_id='rs9468199', effect_allele='A', odds_ratio_value=1.125),
                        odds_ratio(rs_id='rs532965', effect_allele='G', odds_ratio_value=0.849),
                        odds_ratio(rs_id='rs6978475', effect_allele='A', odds_ratio_value=1.122),
                        odds_ratio(rs_id='rs1736084', effect_allele='T', odds_ratio_value=0.907),
                        odds_ratio(rs_id='rs55672297', effect_allele='A', odds_ratio_value=0.916),
                        odds_ratio(rs_id='rs2280104', effect_allele='T', odds_ratio_value=1.07),
                        odds_ratio(rs_id='rs10963229', effect_allele='A', odds_ratio_value=1.124),
                        odds_ratio(rs_id='rs10906923', effect_allele='C', odds_ratio_value=0.928),
                        odds_ratio(rs_id='rs2296887', effect_allele='C', odds_ratio_value=1.1),
                        odds_ratio(rs_id='rs117896735', effect_allele='A', odds_ratio_value=1.654),
                        odds_ratio(rs_id='rs3793947', effect_allele='A', odds_ratio_value=0.928),
                        odds_ratio(rs_id='rs3802920', effect_allele='T', odds_ratio_value=1.105),
                        odds_ratio(rs_id='rs140722239', effect_allele='C', odds_ratio_value=0.652),
                        odds_ratio(rs_id='rs10847864', effect_allele='T', odds_ratio_value=1.172),
                        odds_ratio(rs_id='rs9671503', effect_allele='C', odds_ratio_value=1.119),
                        odds_ratio(rs_id='rs1555399', effect_allele='T', odds_ratio_value=1.086),
                        odds_ratio(rs_id='rs8005172', effect_allele='T', odds_ratio_value=1.077),
                        odds_ratio(rs_id='rs3889108', effect_allele='T', odds_ratio_value=0.877),
                        odds_ratio(rs_id='rs11343', effect_allele='T', odds_ratio_value=1.074),
                        odds_ratio(rs_id='rs8062719', effect_allele='A', odds_ratio_value=0.916),
                        odds_ratio(rs_id='rs4784227', effect_allele='T', odds_ratio_value=1.09),
                        odds_ratio(rs_id='rs11654121', effect_allele='C', odds_ratio_value=1.08),
                        odds_ratio(rs_id='rs1529536', effect_allele='A', odds_ratio_value=0.649),
                        odds_ratio(rs_id='rs74894391', effect_allele='G', odds_ratio_value=1.102),
                        odds_ratio(rs_id='rs4130047', effect_allele='C', odds_ratio_value=1.099),
                        odds_ratio(rs_id='rs2074546', effect_allele='A', odds_ratio_value=1.084),
                        odds_ratio(rs_id='rs55785911', effect_allele='A', odds_ratio_value=0.936)
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["low_risk"] = ""
description_title["low_risk"] = "Your Result: Low risk"
description_risk["low_risk"] = "Low risk"
description_recommendation["low_risk"] = ""
description["average_risk"] = ""
description_title["averagage_risk"] = "Your Result: Moderate risk"
description_risk["average_risk"] = "Moderate risk"
description_recommendation["average_risk"] = ""
description["high_risk"] = ""
description_title["high_risk"] = "Your Result: High risk"
description_risk["high_risk"] = "High risk"
description_recommendation["high_risk"] = ""
trait_genes = ["POU5F1P4", "NUCKS1", "ITPKB", "SIPA1L2", "MAP4K4", "VDAC2P4", "STK39", "MAPRE1P3", "SATB1", "IP6K2", "ITIH1", "MCCC1-AS1", "TMEM175", "AC005798.1", "FAM47E-STBD1", "AC097478.3", "CAMK2D", "AC022445.1", "AL009179.1", "HLA-DQA1", "AK3P3", "AC025857.2", "RN7SL474P", "BIN3", "SH3GL2", "ITGA8", "GBF1", "RN7SL846P", "LDHAL6DP", "IGSF9B", "RPL30P13", "HIP1R", "WDHD1", "TMEM229B", "GPR65", "AC018618.1", "SYT17", "STX1B", "CASC16", "ATP5MGP7", "STH", "RF00019", "RIT2", "SPPL2B", "LZTS3"]
other_factors = "" 
trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the Parkinson's disease predisposition. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle." #for poygenic trait

