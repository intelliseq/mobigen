from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Lactose_intolerance"
trait_pmids = ["22826639", "15114531"]
trait_heritability = None  # as fraction
trait_snp_heritability = None  # as fraction
trait_explained = None  # as fraction
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Single SNP analysis"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "Low risk"

# It is not sure if this variant is causative, as it lies within very long haplotype (all variants within this region are tightly linked)
# Both genotypes of LCT-13910CT and LCT-13910TT are associated with the lactase-persistence phenotype (dominant phenotype): -1 strand
# G -allele for lactose intolerance; A  allele for lactose persistence
# after calculations - GG: 2 => high_risk, AG: 1 => low_risk,AA: 0 => low_risk
# trait_genes = []
# trait_test_details = "We determined your genotype at all the genomic positions, which are known to influence the trait. Your polygenic risk score was calculated by summing up the effects of all the individual SNPs. The estimated effect of the genetic factors may be enhanced or alleviated by the environment, especially by the diet and lifestyle." #for poygenic trait

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=1.5, category_name='Low risk'),
                        QuantitativeCategory(from_=1.5, category_name='High risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs4988235', effect_allele='G', beta_value=1)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Lactose intolerance'

about = ('Lactose intolerance is a relatively common condition around the world, in '
         'which the body lacks or has a weaker enzyme (lactase) to metabolize lactose. '
         'Lactose is a type of sugar commonly found in milk and dairy products. People '
         'with lactose intolerance typically experience a range of symptoms, including '
         'gas, bloating, belly gurgling, abdominal cramps, diarrhea, and vomiting. '
         'Following a diet to reduce or eliminate lactose can be very helpful for '
         'these individuals. Lactase supplements are available over-the-counter, and '
         'these can also help.')

how_your_result_was_calculated = ('Your result is a risk based on 1 genetic variant, specifically called a SNP, '
                                  'in 1 gene associated with lactose intolerance.')

other_factors_statement = ("Many factors impact a person's risk for lactose intolerance, including "
                           'certain illnesses, ethnic background, and genetics.  The enzymes required to '
                           'break down lactose diminish greatly as we age leading to an inability to '
                           'digest lactose; which can also be influenced by genetics.')

other_factors = [{'content': 'Having a stomach flu can cause an increased risk for lactose '
                             'intolerance, which may be temporary.  Having a medical condition '
                             'or issue, such as cystic fibrosis or stomach surgery, can also '
                             'increase the risk for lactose intolerance.',
                  'title': 'Other Health Issues'},
                 {'content': 'People of Asian, First Nations, African, and South American '
                             'descent have an increased risk for lactose intolerance.',
                  'title': 'Ethnic Background'},
                 {'content': 'Sometimes, genetic risk factors can cause multiple people in the '
                             'same family to have lactose intolerance. These risk factors, '
                             'which are usually due to rare genetic variants, are not included '
                             'in this test. If you have a family history of lactose '
                             'intolerance, talk to your Practitioner or a genetic '
                             'counselor.',
                  'title': 'Family History'}]

what_your_result_means = {'High risk': 'Depending on your lactose intake and other factors, you may '
                                        'have a higher risk for feeling sick (related to lactose '
                                        'intolerance symptoms) after consuming lactose.',
                           'Low risk': 'Depending on your lactose intake and other factors, you may have '
                                       'a lower risk for feeling sick (related to lactose intolerance '
                                       'symptoms) after consuming lactose.'}

result_statement = {'High risk': 'Based on the genetic factors tested, you are more likely to '
                                 'have a higher risk for lactose intolerance.',
                    'Low risk': 'Based on the genetic factors tested, you are more likely to have '
                                'a lower risk for lactose intolerance.'}

what_you_can_do_statement = {'High risk': 'There are things you can do to try to prevent symptoms of '
                                          'lactose intolerance and stay healthy. Certain tests can also '
                                          'check your risk factors for lactose intolerance. Talk to your '
                                          'practitioner about what is right for you.',
                             'Low risk': 'There are things you can do to try to prevent symptoms of '
                                         'lactose intolerance and stay healthy. Certain tests can also '
                                         'check your risk factors for lactose intolerance. Talk to your '
                                         'practitioner about what is right for you.'}

what_you_can_do = {'High risk': [{'content': 'Your practitioner may be able to order certain '
                                             'blood tests focused on food sensitivities or other '
                                             'tests to check if you are digesting lactose '
                                             'properly. Talk to them about this and possible '
                                             'next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Keep a food diary and track how you feel after '
                                             "consuming lactose if you're repeatedly "
                                             'experiencing gas, diarrhea, bloating, or abdominal '
                                             'cramps after having dairy products. Removing dairy '
                                             'from your diet is something you can do, but just '
                                             "make sure you're not missing something important "
                                             '(like calcium) if you do this -- speaking to a '
                                             'registered dietician can help you do this in a '
                                             'healthy way. Foods or drinks with less/no lactose '
                                             'or lactose supplements (like Lactaid pills) may '
                                             'also be something to try.',
                                  'title': 'Diet'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for lactose intolerance. Consider talking to your '
                                            'practitioner about ordering food sensitivity tests '
                                            'or other tests to check if you are digesting '
                                            'lactose properly, and possible next steps that you '
                                            'can take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': "You're at lower risk for lactose intolerance, but "
                                            'consider keeping a food diary if you repeatedly '
                                            'experiencing gas, diarrhea, bloating, or abdominal '
                                            "cramps after having dairy products. If you're "
                                            'concerned about lactose intolerance, speak to your '
                                            'practitioner to explore it further.',
                                 'title': 'Diet'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = [
    {
        "trait": "Lactose intolerant as an adult",
        "gene": "MCM6",
        "rsid": "rs4988235",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "MCM6 is involved with regulating the LCT gene, a gene that encode the lactase enzyme. Lactase plays a role in breaking down the lactose sugar found dairy products.",
        "risk_allele": 'G',
        "genotypes": [
            {
                "genotype": [
                    "G",
                    "G"
                ],
                "gene_impact_string": "Intolerant",
                "description": "The TT genotype is associated with lactase persistence in the Caucasian population\nAvoid cow\u2019s milk or any dairy products.  Can attempt to utilize goat and sheep\u2019s milk products."
            },
            {
                "genotype": [
                    "A",
                    "G"
                ],
                "gene_impact_string": "Persistent",
                "description": "The TT genotype is associated with lactase persistence in the Caucasian population\nAvoid cow\u2019s milk or any dairy products.  Can attempt to utilize goat and sheep\u2019s milk products."
            },
            {
                "genotype": [
                    "A",
                    "A"
                ],
                "gene_impact_string": "Persistent",
                "description": "The TT genotype is associated with lactase persistence in the Caucasian population\nAvoid cow\u2019s milk or any dairy products.  Can attempt to utilize goat and sheep\u2019s milk products."
            }
        ]
    }
]

genes = [variant['gene'] for variant in variants]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {}

# NIEPOTRZEBNE POLA
trait_short_description = "Lactose intolerance"
trait_science = "Our analysis was based on a single SNP associated with lactose intolerance (rs4988235). This variant lies 13910 nucleotides upstream the LCT gene. The LCT gene encodes lactase, an enzyme which enables lactose digestion."
trait_genotypes = ["AA", "AG", "GG"]
