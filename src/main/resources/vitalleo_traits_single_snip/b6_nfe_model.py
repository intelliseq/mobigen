from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta
from src.main.python.lib.seqql import GenotypeGetter

trait_title = "Vitamin_B6"
trait_pmids = ["25147783"]  # 25147783 table2; minor allele=EA (confirmed in gwas catalog)
trait_heritability = None
trait_snp_heritability = None
trait_explained = 0.015
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Single SNP analysis"
trait_was_prepared_for_population = "nfe"
category_on_the_left_side_of_plot = "High risk"

# 1725 EUR; 258 AFR; 117 OTHERS
# low_vitamin_B6 and high_vitamin_B6 categories = homozygotes

model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.01, category_name='High risk'),
                        QuantitativeCategory(from_=0.01, to=0.33, category_name='Average risk'),
                        QuantitativeCategory(from_=0.33, category_name='Low risk')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs1697421', effect_allele='T', beta_value=0.173)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

trait = 'Vitamin B6'

about = ('Vitamin B6 is part of the B vitamin group. Several foods naturally contain '
         'vitamin B6. Vitamin B6 is important for the blood, immune system, central '
         'nervous system, and metabolism. Low vitamin B6 levels can be a risk for '
         'anemia (a type of blood disease), a weaker immune system, and other health '
         "issues. Vitamin B6 also helps a baby's brain develop during pregnancy. "
         'Pyridoxine is a form of vitamin B6 that is found in supplements and '
         'fortified foods. Most people get enough vitamin B6 from their diet.  Vitamin '
         'B6 specifically helps assist in methylation.')

how_your_result_was_calculated = ('Your result is a risk based on 1 genetic variant, specifically called a SNP, '
                                  'found in 1 gene associated with blood vitamin B6 level. Overall, this '
                                  'explains as low as 1.5% of the genetic variation in vitamin B6 level in the '
                                  'human population, based on current scientific knowledge.')

other_factors_statement = ('Many factors impact vitamin B6 levels, including diet, other health issues, '
                           'lifestyle, and genetics. Genetics has a limited overall impact on vitamin B6 '
                           'level when you consider all the other factors.')

other_factors = [{'content': 'Vitamin B6 is part of the B vitamin group.  Several foods '
                             'naturally contain vitamin B6.  Eat more Chicken, turkey, beef, '
                             'and fish.  Vegetables such as bell peppers, spinach, baked '
                             'potatoes (skin included), green peas, yams, broccoli, asparagus '
                             'and turnip greens are all excellent sources of vitamin B6. '
                             'Include also nuts, seeds, whole grains, and bran.',
                  'title': 'Diet'},
                 {'content': 'People with kidney disease and some autoimmune disorders (such '
                             'as rheumatoid arthritis or celiac disease) may have lower '
                             'vitamin B6 levels.',
                  'title': 'Other Health Issues'},
                 {'content': 'Pyridoxine supplements can help raise vitamin B6 to recommended '
                             'daily levels, if needed. Requirements will vary by These '
                             'supplements should be used with care because you do not want too '
                             'much vitamin B6.  Talk with your Practitioner.',
                  'title': 'Supplements'}]

what_your_result_means = {'Average risk': 'Depending on your vitamin B6 level and other factors, you '
                                           'may have a averag risk for a form of anemia, neuropathy, and '
                                           'issues with methylation such as heart disease, anxiety, and '
                                           'hormonal issues.',
                           'High risk': 'Depending on your vitamin B6 level and other factors, you may '
                                        'have a higher risk for a form of anemia, neuropathy, and issues '
                                        'with methylation such as heart disease, anxiety, and hormonal '
                                        'issues.',
                           'Low risk': 'Depending on your vitamin B6 level and other factors, you may '
                                       'have a low risk for a form of anemia, neuropathy, and issues '
                                       'with methylation such as heart disease, anxiety, and hormonal '
                                       'issues.'}

result_statement = {'Average risk': 'Based on the genetic factor tested, you may be more likely '
                                    'to have an average blood vitamin B6 level.',
                    'High risk': 'Based on the genetic factor tested, you may be more likely to '
                                 'have a lower blood vitamin B6 level.',
                    'Low risk': 'Based on the genetic factor tested, you may be more likely to '
                                'have a higher blood vitamin B6 level.'}

what_you_can_do_statement = {'Average risk': 'There are things you can do to try to prevent low vitamin B6 '
                                             'levels and stay healthy. Talk to your practitioner about '
                                             'what is right for you.',
                             'High risk': 'There are things you can do to try to prevent low vitamin B6 '
                                          'levels and stay healthy.  Talk to your practitioner about what '
                                          'is right for you.',
                             'Low risk': 'There are things you can do to try to prevent low vitamin B6 '
                                         'levels and stay healthy. Talk to your practitioner about what is '
                                         'right for you.'}

what_you_can_do = {'Average risk': [{'content': 'Other factors besides genetics may impact your '
                                                'risk for a low vitamin B6 level. Consider '
                                                'talking to your practitioner about blood tests '
                                                'to check your vitamin B6 level, and possible '
                                                'next steps you can take.',
                                     'title': 'Talk to Your Practitioner'},
                                    {'content': 'Vitamin B6 is part of the B vitamin group. '
                                                'Several foods naturally contain vitamin B6. Eat '
                                                'more Chicken, turkey, beef, and fish. '
                                                'Vegetables such as bell peppers, spinach, baked '
                                                'potatoes (skin included), green peas, yams, '
                                                'broccoli, asparagus and turnip greens are all '
                                                'excellent sources of vitamin B6. Include also '
                                                'nuts, seeds, whole grains, and bran.',
                                     'title': 'Diet'},
                                    {'content': 'Pyridoxine (B6) supplements can help raise '
                                                'vitamin B6 to recommended daily levels, if '
                                                'needed. Requirements will vary by personal '
                                                'need.  These supplements should be used with '
                                                'care so you should discuss quality and dosage '
                                                'with your practitioner.',
                                     'title': 'Supplements'},
                                    {'content': 'If you have kidney disease or some autoimmune '
                                                'disorders (such as rheumatoid arthritis or '
                                                'celiac disease), you may have lower vitamin B6 '
                                                'levels. Speak to your practitioner about how '
                                                'this factors into things, if your vitamin B6 '
                                                'levels are found to be low from blood tests.',
                                     'title': 'Other Health Issues'}],
                   'High risk': [{'content': 'Your practitioner may be able to order tests to '
                                             'check your vitamin B6 level. Talk to them about '
                                             'this and possible next steps that you can take.',
                                  'title': 'Talk to Your Practitioner'},
                                 {'content': 'Vitamin B6 is part of the B vitamin group. Several '
                                             'foods naturally contain vitamin B6. Eat more '
                                             'Chicken, turkey, beef, and fish. Vegetables such '
                                             'as bell peppers, spinach, baked potatoes (skin '
                                             'included), green peas, yams, broccoli, asparagus '
                                             'and turnip greens are all excellent sources of '
                                             'vitamin B6. Include also nuts, seeds, whole '
                                             'grains, and bran.',
                                  'title': 'Diet'},
                                 {'content': 'Pyridoxine (B6) supplements can help raise vitamin '
                                             'B6 to recommended daily levels, if needed. '
                                             'Requirements will vary by personal need.  These '
                                             'supplements should be used with care so you should '
                                             'discuss quality and dosage with your practitioner.',
                                  'title': 'Supplements'},
                                 {'content': 'If you have kidney disease or some autoimmune '
                                             'disorders (such as rheumatoid arthritis or celiac '
                                             'disease), you may have lower vitamin B6 levels. '
                                             'Speak to your practitioner about how this factors '
                                             'into things, if your vitamin B6 levels are found '
                                             'to be low from blood tests.',
                                  'title': 'Other Health Issues'}],
                   'Low risk': [{'content': 'Other factors besides genetics may impact your risk '
                                            'for a low vitamin B6 level. Consider talking to '
                                            'your practitioner about blood tests to check your '
                                            'vitamin B6 level, and possible next steps you can '
                                            'take.',
                                 'title': 'Talk to Your Practitioner'},
                                {'content': 'Vitamin B6 is part of the B vitamin group. Several '
                                            'foods naturally contain vitamin B6. Try to eat more '
                                            'Chicken, turkey, beef, and fish. Vegetables such as '
                                            'bell peppers, spinach, baked potatoes (skin '
                                            'included), green peas, yams, broccoli, asparagus '
                                            'and turnip greens are all excellent sources of '
                                            'vitamin B6. Include also nuts, seeds, whole grains, '
                                            'and bran.',
                                 'title': 'Diet'},
                                {'content': 'Pyridoxine (B6) supplements can help raise vitamin '
                                            'B6 to recommended daily levels, if needed. '
                                            'Requirements will vary by personal need.  These '
                                            'supplements should be used with care so you should '
                                            'discuss quality and dosage with your practitioner.',
                                 'title': 'Supplements'}]}
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."

variants = [
    {
        "trait": "B6 level",
        "gene": "Intergenic region",
        "rsid": "rs1697421",
        "scientific_ref": {
            "website": "https://www.snpedia.com",
            "pmid": ""
        },
        "intro": "",
        "risk_allele": 'C',
        "genotypes":
            [
                {
                    "genotype": ['C', 'C'],
                    "gene_impact_string": "High risk",
                    "description": "CC\nLikely to have lower vitamin B6 level"
                },
                {
                    "genotype": ['C', 'T'],
                    "gene_impact_string": "Somewhat higher risk",
                    "description": "CT\nLikely to have average vitamin B6 level"
                },
                {
                    "genotype": ['T', 'T'],
                    "gene_impact_string": "Low risk",
                    "description": "TT\nLikely to have higher vitamin B6 level"
                }
            ]
    }
]

genes = [variant['gene'] for variant in variants]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

# references
references = {'article_links': [],
              'website_text_data': [{'accessed': 'Accessed June 21, 2019',
                                     'links': 'https://ods.od.nih.gov/factsheets/VitaminB6-Consumer/',
                                     'title': '"Vitamin B6 Fact Sheet for Consumers" from '
                                              'National Institutes of Health'},
                                    {'accessed': 'Accessed June 21, 2019',
                                     'links': 'https://www.dietitians.ca/getattachment/ea1272c8-602f-4586-8ffb-d7b4a2535634/FACTSHEET-Food-Sources-of-Vitamin-B6.pdf.aspx',
                                     'title': '"Food Sources of Vitamin B6" from Dietitians '
                                              'of Canada'}]}

# NIEPOTRZEBNE POLA
trait_short_description = "Vitamin B6 blood concentration"
trait_science = "Our analysis was based on a single SNP associated with vitamin B6 blood level (rs1697421). This variant lies within the NBPF3 gene and explains about 1.5% of the variation observed between people in vitamin B6 level."
trait_genotypes = ["TT", "CT", "CC"]
