#!/usr/bin/env python3

import argparse
import re
import glob
import os
import json
from collections import namedtuple
from typing import Tuple
from typing import List
from typing import Dict
from typing import Union

__version__ = '0.0.1'

BETA_RE = re.compile("beta\(rs_id=['\"](.*)['\"],\seffect_allele=['\"](.*)['\"],\sbeta_value=(.*)\)")
OR_RE = re.compile("odds_ratio\(rs_id=['\"](.*)['\"],\seffect_allele=['\"](.*)['\"],\sodds_ratio_value=(.*)\)")

module_path = os.path.abspath(__file__).rsplit(os.path.sep, 5)[0]


SNP_data = namedtuple('SNP_data', 'rsid effect_allele effect effect_allele_frequency')


def get_data_for_trait(trait_path:str, allele_frequencies: Dict[str, Dict[str, Union[str, int]]], population_string:str) -> List[SNP_data]:
    ret = []
    for rsid, effect_allele, effect in get_rsid_allele_effect_from_model(trait_path):
        ea_frequency = get_ea_frequency_for_rsid(rsid, effect_allele, allele_frequencies, population_string)
        ret.append(SNP_data(rsid, effect_allele, effect, ea_frequency))
    return ret


def get_rsid_allele_effect_from_model(model_file_path: str) ->List[Tuple[str]]:
    with open(model_file_path) as f:
        content = f.read()
    found = BETA_RE.findall(content)
    if found:
        return found
    with open(model_file_path) as f:
        content = f.read()
        return OR_RE.findall(content)


def get_gnomad_allele_frequency() -> Dict[str, Dict[str, Union[str, int]]]:
    with open(os.path.join(module_path, 'src', 'main', 'resources', 'allele_frequencies', 'gnomad.json')) as f:
        return json.load(f)


def get_ea_frequency_for_rsid(rsid:str, effect_allele:str, allele_frequencies: Dict[str, Dict[str, Union[str, int]]], population_string:str) -> float:
    freq_data = allele_frequencies[rsid]
    if freq_data['alt'] == effect_allele:
        return freq_data[population_string]
    elif freq_data['ref'] == effect_allele:
        return 1-freq_data[population_string] #TODO returns improper results for multiallelic SNPs
    raise NotImplementedError


def write_data(data:List[SNP_data], path:str) -> None:
    with open(path, 'w') as f:
        for item in data:
            f.write('{},{},{},{}\n'.format(item.rsid, item.effect_allele, item.effect_allele_frequency, item.effect))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='jakiś sensowny opis, co robi skrypt')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-d', '--trait_directories', nargs='+', default=['vitalleo_traits', 'pgs_traits'],
                        help='Names of trait directories (space-separated)')
    parser.add_argument('-o', '--output_directory', type=str, required=True)
    parser.add_argument('-p', '--population_code', type=str, default='nfe',
                        choices=['', 'nfe', 'eas', 'afr', 'amr', 'asj', 'fin', 'oth'],
                        help='''Population code:
    empty - use average allele frequency in all population,
    'nfe' - Non-Finnish European ancestry,
    'eas' - East Asian ancestry,
    'afr' - African-American/African ancestry,
    'amr' - Latino ancestry,
    'asj' - Ashkenazi Jewish ancestry, 
    'fin' - Finnish ancestry,
    'oth' - Other ancestry''')

    arguments = parser.parse_args()

    out_dir = os.path.abspath(os.path.expanduser(arguments.output_directory))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    population_string = 'AF_{}'.format(arguments.population_code) if arguments.population_code else 'AF'

    allele_frequencies = get_gnomad_allele_frequency()

    for trait_dir in arguments.trait_directories:
        trait_dir_path = os.path.join(module_path, 'src', 'main', 'resources', trait_dir)
        for path in glob.glob(os.path.join(trait_dir_path, '*_model.py')):
            data_for_trait = get_data_for_trait(path, allele_frequencies, population_string)
            fname = os.path.basename(path)
            write_data(data_for_trait, os.path.join(out_dir, 'do_symulacji_{}'.format(fname.rsplit('.', 1)[0])))
