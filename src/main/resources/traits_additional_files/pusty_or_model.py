from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import odds_ratio
from src.main.python.lib.seqql import GenotypeGetter

# zmieniony schemat opisow
# POLA DLA ISEQ I NOTATKI (SKAD DOKLADNIE DANE, CO USUNELAM, LICZEBNOSC KOHORT) I TAKIE TAM

trait_title = "wpisac tytul cechy, Static by trait, Trait, kolumna A"
trait_pmids = ["21729881"]
trait_heritability = None  # as fraction
trait_snp_heritability = None  # as fraction
trait_explained = None  # as fraction
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
test_type = "Polygenic Risk Score" # alternative: "Single SNP analysis"


# MODEL
model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=0.1, category_name='low_trait'),
                        QuantitativeCategory(from_=0.1, to=0.9, category_name='average_trait'),
                        QuantitativeCategory(from_=0.9, category_name='high_trait')
                    ],
                    coefficients_and_snps_to_be_considered=[
                        # tu wklej liste rs, po reformat_or.sh: plik btrait.txt
                    ],
                    function_to_apply='multiply'
                )
            }
    }
)

# POLA UZUPELNIANE PRZEZ INTELLIGER
# NIEZMIENNE DLA CECHY
trait = "wpisac tytul cechy, Static by trait, Trait, kolumna A"
about = "wpisac opis cechy, Static by trait, About, kolumna B"
how_your_result_was_calculated = "wpisac tekst o liczbie snp, odziedziczalnosci itp, static by trait, kolumna L"
other_factors_statement = "wyjasnienie, ze nie genetyczne czynniki moga wplywac na ceche, Static by trait, Other factors statement, kolumna C"
other_factors = [{'title': '',
    'content': ''},
   {'title': '',
    'content': ''}
] # wpisac pozostale czynniki wplywajace na ceche, Static by trait, kolumny D-K
science_behind_your_result = "Your results are based on genetic studies published in peer-revieved scientific and \
medical articles. Sometimes, the effects of many different genetic factors are combined into one risk estimate."
how_your_result_was_calculated = '' #delivered by intelliger

genes = []  # wpisac liste genow, to nie jest w ogole ujete przez Intelliger (bo zmapowali tylko strone  dla testu typu "one snp"

# POLA UZUPELNIANE PRZEZ INTELLIGER
# ZALEZNE OD WYNIKU

what_your_result_means = {
    'low_trait': "opis klasy ryzyka, Results dependent, What your results means, kolumna D",
    'average_trait': "opis klasy ryzyka, Results dependent, What your results means, kolumna D",
    'high_trait': "opis klasy ryzyka, Results dependent, What your results means, kolumna D"
}

result_statement = {
    'low_trait': "krotki opis wyniku, Results dependent, Results Statement, kolumna C",
    'average_trait': "krotki opis wyniku, Results dependent, Results Statement, kolumna C",
    'high_trait': "krotki opis wyniku, Results dependent, Results Statement, kolumna C"
}

what_you_can_do_statement = {
    'low_trait': "zdanie infomujace, ze predyspozycje genetyczne, to nie wszystko, Result dependent, What you can do statement, kolumna E",
    'average_trait': "zdanie infomujace, ze predyspozycje genetyczne, to nie wszystko, Result dependent, What you can do statement, kolumna E",
    'high_trait': "zdanie infomujace, ze predyspozycje genetyczne, to nie wszystko, Result dependent, What you can do statement, kolumna E"
}

what_you_can_do = {
    'low_trait': [{"title1": "", "content1": ""}, {"title2": "", "content2": ""}, ...],
    # wpisac zalecenia dla danej klasy ryzyka, Results dependent, What you can do, kolumny F-Q
    'average_trait': [{"title1": "", "content1": ""}, {"title2": "", "content2": ""}, ...],
    # wpisac zalecenia dla danej klasy ryzyka, Results dependent, What you can do, kolumny F-Q
    'high_trait': [{"title1": "", "content1": ""}, {"title2": "", "content2": ""}, ...]
    # wpisac zalecenia dla danej klasy ryzyka, Results dependent, What you can do, kolumny F-Q
}

what_you_can_do = {
    'low_trait': [{'title': '', 'content': ''}, {'title': '', 'content': ''}],
    # wpisac zalecenia dla danej klasy ryzyka, Results dependent, What you can do, kolumny F-Q
    'average_trait': [{'title': '', 'content': ''}, {'title': '', 'content': ''}],
    # wpisac zalecenia dla danej klasy ryzyka, Results dependent, What you can do, kolumny F-Q
    'high_trait': [{'title': '', 'content': ''}, {'title': '', 'content': ''}],
    # wpisac zalecenia dla danej klasy ryzyka, Results dependent, What you can do, kolumny F-Q
}

# variants - zawartosc kolumn M-O (Static by trait) pasuje tylko dla pojedyńczych wariantów
variants = [
    {
        "gene": "jakiś gen",
        "rsid": "rsid1",
        "genotypes":
            {
                "genotype_risk-risk": {"genotype":['T','T'], "description":"opis"},
                "genotype_nonrisk-risk": {"genotype":['C','T'], "description":"opis"},
                "genotype_nonrisk-nonrisk": {"genotype":['C','C'], "description":"opis"}
            }
    },
    {
        "gene": "jakiś gen",
        "rsid": "rsid2",
        "genotypes":
            {
                "genotype_risk-risk": {"genotype":["A","A"], "description":"opis"},
                "genotype_nonrisk-risk": {"genotype":["A","AT"], "description":"opis"},
                "genotype_nonrisk-nonrisk": {"genotype":["AT","AT"], "description":"opis"}
            }
    }
]

# result-dependent it's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])

references = {} # delivered by Intelliger

# NIEPOTRZEBNE POLA
# trait_genotypes = ["hom", "het", "hom"]  # for one snip analysis
# trait_short_description = ""
# trait_test_details = ""  # teraztwpisane tu jest zdanie o liczeniu polygenic risk score, tylko dla cech z liczonym prs
# trait_test_details = "nie ma odpowiedniego pola w pliku od intelliger, teraz zdanie o liczeniu polygenic risk score, tu john chcialby miec opis 'actionable snps/genes'."  # tylko dla cech z liczonym prs

