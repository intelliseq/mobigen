from src.main.python.lib.seqql import GenotypeGetter

name = 'jakaś nazwa da całości'

variants = [
    {
        "trait": "cośtam",
        "gene": "jakiś gen",
        "rsid": "rsid",
        "scientific_ref": "cośtam",
        "intro_content": "cośtam",
        "genotypes":
            [
                {
                    "genotype": ['T', 'T'],
                    "gene_impact": "cośtam",
                    "description": "opis"
                },
                {
                    "genotype": ['T', 'C'],
                    "gene_impact": "cośtam",
                    "description": "opis"
                },
                {
                    "genotype": ['C', 'C'],
                    "gene_impact": "cośtam",
                    "description": "opis"
                }
            ]
    },
    {
        "trait": "cośtam",
        "gene": "jakiś gen",
        "rsid": "rsid",
        "scientific_ref": "cośtam",
        "intro_content": "cośtam",
        "genotypes":
            [
                {
                    "genotype": ['A', 'A'],
                    "gene_impact": "cośtam",
                    "description": "opis"
                },
                {
                    "genotype": ['A', 'C'],
                    "gene_impact": "cośtam",
                    "description": "opis"
                },
                {
                    "genotype": ['C', 'C'],
                    "gene_impact": "cośtam",
                    "description": "opis"
                }
            ]
    }
]

# result-dependent. It's a way to mark patient genotype on the picture
patient_genotypes = GenotypeGetter([variant['rsid'] for variant in variants])
