#!/bin/bash
#imput file, tab delimited, 1st column: rs_id, 2nd column: EA, 3rd column: beta/or
while read f
do
    rsid=$( echo $f | cut -f1 -d ' ' )
    ea=$( echo $f | cut -f2 -d ' ' )
    effect=$( echo $f | cut -f3 -d ' ' )
    ref=$( docker run --user $(id -u):$(id -g) --rm intelliseqarray/gnomad_allele_frequencies:1.0.1 --rsid $rsid | jq ."$rsid".ref | sed 's/\"//g' )
    alt=$( docker run --user $(id -u):$(id -g) --rm intelliseqarray/gnomad_allele_frequencies:1.0.1 --rsid $rsid | jq ."$rsid".alt | sed 's/\"//g' )
    freq=$( docker run --user $(id -u):$(id -g) --rm intelliseqarray/gnomad_allele_frequencies:1.0.1 --rsid $rsid | jq ."$rsid".AF_nfe )
    ba=$( docker run --user $(id -u):$(id -g) --rm intelliseqarray/gnomad_allele_frequencies:1.0.1 --rsid $rsid | jq ."$rsid".is_biallelic )
    if [[ $ea == $alt ]]
    then
       ea_freq=$freq
    elif [[ $ea == $ref ]]
    then
        ea_freq=$( node -pe 1-$freq )
    else
        ea_freq=NA
    fi
    echo $rsid,$ea,$ea_freq,$effect >> do_symulacji_"$1"
    echo $rsid,$ref,$alt,$freq,$ba >> in_case_"$1"
done <$1   

#zastanow sie czego potrzebujesz dalej i co zrobic, gdy ea nie bedzie ani alt ani ref 
