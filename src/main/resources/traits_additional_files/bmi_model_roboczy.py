from src.main.python.lib.seqql import Choice
from src.main.python.lib.seqql import Priority
from src.main.python.lib.seqql import always_true
from src.main.python.lib.seqql import get_category_and_value
from src.main.python.lib.seqql import QuantitativeCategory
from src.main.python.lib.seqql import beta

trait_title = "body_mass_index"
trait_short_description = "Body mass index"
trait_description = "Body mass index (BMI) is a score defined as the body mass (weight) divided by the square of the height of an individual. It was created as an attempt to categorize people as underweight, normal weight, overweight, or obese. Commonly accepted BMI ranges are: underweight (score under 18.5), normal weight (score 18.5 to 25), overweight (score 25 to 30), obese (score over 30). Obesity may cause many health complications such as metabolic syndrome, high blood pressure, atherosclerosis, heart disease, diabetes, high blood cholesterol, cancer, and sleep disorders. BMI is determined by genetic and environmental factors."
trait_pmids = ["28832619"] #28832619 supplementary table 7
trait_heritability = 0.8
trait_snp_heritability = None
trait_explained = 0.12
trait_authors = ["Katarzyna Tomala"]
trait_version = "1.0"
trait_expiry = "10.03.2021"
trait_copyright = "Intelliseq all rights reserved"
trait_science = "Body mass index is highly heritable and about 80% of differences observed among people has a genetic origin. Our analyses were based on 49 SNPs associated with BMI. These variants jointly explain about 12% of the variation in BMI between people."
test_type = "Polygenic Risk Score"

#na podstawie: doi: 10.1371/journal.pgen.1006528; to meta-analiza 60 wcześniejszych prac
#wzięłam dane dotyczące tylko populacji EUR, N=180423, ta praca badała też interakcje pomiędzy genotypem i aktywnościa fizyczną
#dane o rs: z tabeli SuppTable7 (połączone dane dla obu płci i osób aktywnych i nieaktywnych fizycznie)
#wybrałam warianty dla których wartość p(adjPA) <5e-08 
#zostawiłam po jednym wariancie dla regionów do około 500 tys. pz (wybierałam wariant o większym efekcie)
#w sumie pozostało 49 SNPów
#efekty były podane, jako współczynniki beta z regresji, EA==wariant zwiększający BMI (bo wszystkie bety dodatnie)
#odziedziczalnosc: około 80 %, zmiennosc wyjasniona: 12% lub 16% (zależy od aktywnności fizycznej, wyższa wartość dla leniuchów)
#model na podstawie symulacji (plik symul.py), wyniki w pliku dist_bmi.txt i stats_bmi.txt (w tym ostatnim wyniki z 6 powtórzeń symulacji)
#czestosci dla EA w populacji EUR z  http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/); 
#(file:  ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf)


model = Choice(
    {
        Priority(0):  # najniższy priorytet - defaultowe akcje,
        # jeśli nie zwróciliśmy żadnej kategorii dla wyższego priorytetu
            {
                'when': always_true,  # nie ma warunku, mamy akcje defaultowe:
                'action': get_category_and_value(
                    categories=[
                        QuantitativeCategory(to=1.36, category_name='low_BMI'),
                        QuantitativeCategory(from_=1.36, to=1.74, category_name='average_BMI'),
                        QuantitativeCategory(from_=1.74, category_name='high_BMI')
                        
                    ],
                    coefficients_and_snps_to_be_considered=[
                        beta(rs_id='rs11209951', effect_allele='T', beta_value=0.032),
                        beta(rs_id='rs7551507', effect_allele='C', beta_value=0.023),
                        beta(rs_id='rs4130548', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs17024393', effect_allele='C', beta_value=0.084),
                        beta(rs_id='rs633715', effect_allele='C', beta_value=0.053),
                        beta(rs_id='rs2820315', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs2867125', effect_allele='C', beta_value=0.070),
                        beta(rs_id='rs6752378', effect_allele='A', beta_value=0.033),
                        beta(rs_id='rs2075171', effect_allele='G', beta_value=0.026),
                        beta(rs_id='rs759250', effect_allele='A', beta_value=0.030),
                        beta(rs_id='rs6804842', effect_allele='G', beta_value=0.023),
                        beta(rs_id='rs2365389', effect_allele='C', beta_value=0.028),
                        beta(rs_id='rs9852127', effect_allele='A', beta_value=0.030),
                        beta(rs_id='rs1720825', effect_allele='A', beta_value=0.026),
                        beta(rs_id='rs2035935', effect_allele='G', beta_value=0.044),
                        beta(rs_id='rs6809651', effect_allele='G', beta_value=0.048),
                        beta(rs_id='rs10938397', effect_allele='G', beta_value=0.035),
                        beta(rs_id='rs13107325', effect_allele='T', beta_value=0.051),
                        beta(rs_id='rs2112347', effect_allele='T', beta_value=0.023),
                        beta(rs_id='rs6870983', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs40067', effect_allele='G', beta_value=0.035),
                        beta(rs_id='rs6864049', effect_allele='G', beta_value=0.021),
                        beta(rs_id='rs943466', effect_allele='G', beta_value=0.024),
                        beta(rs_id='rs7757419', effect_allele='T', beta_value=0.028),
                        beta(rs_id='rs4715210', effect_allele='T', beta_value=0.039),
                        beta(rs_id='rs13191362', effect_allele='A', beta_value=0.036),
                        beta(rs_id='rs2430307', effect_allele='T', beta_value=0.043),
                        beta(rs_id='rs17405819', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs10968577', effect_allele='T', beta_value=0.030),
                        beta(rs_id='rs1928295', effect_allele='T', beta_value=0.022),
                        beta(rs_id='rs4929923', effect_allele='C', beta_value=0.025),
                        beta(rs_id='rs2049045', effect_allele='G', beta_value=0.043),
                        beta(rs_id='rs7124681', effect_allele='A', beta_value=0.026),
                        beta(rs_id='rs7138803', effect_allele='A', beta_value=0.030),
                        beta(rs_id='rs12429545', effect_allele='A', beta_value=0.031),
                        beta(rs_id='rs10132280', effect_allele='C', beta_value=0.023),
                        beta(rs_id='rs12885454', effect_allele='C', beta_value=0.022),
                        beta(rs_id='rs17522122', effect_allele='T', beta_value=0.021),
                        beta(rs_id='rs7141420', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs2241423', effect_allele='G', beta_value=0.033),
                        beta(rs_id='rs2531995', effect_allele='T', beta_value=0.031),
                        beta(rs_id='rs12446632', effect_allele='G', beta_value=0.036),
                        beta(rs_id='rs12325113', effect_allele='C', beta_value=0.030),
                        beta(rs_id='rs9930333', effect_allele='G', beta_value=0.084),
                        beta(rs_id='rs571312', effect_allele='A', beta_value=0.059),
                        beta(rs_id='rs757318', effect_allele='C', beta_value=0.021),
                        beta(rs_id='rs185350', effect_allele='T', beta_value=0.024),
                        beta(rs_id='rs11672660', effect_allele='C', beta_value=0.029),
                        beta(rs_id='rs2303108', effect_allele='C', beta_value=0.025)
                    ],
                    function_to_apply='add'
                )
            }
    }
)

description = {}
description_title = {}
description_risk = {}
description_recommendation = {}

description["low_BMI"] = " You have a genetic predisposition for low body mass index. Great! Relying on your genotypes, you are at low risk for obesity."
description_title["low_BMI"] = "Your Result: Low BMI"
description_risk["low_BMI"] = "Your Result: Low risk"
description_recommendation["low_BMI"] = ""
description["average_BMI"] = "You don't have a genetic predisposition for high body mass index. To stay in shape, follow the recommendations below."
description_title["average_BMI"] = "Your Result: Average BMI"
description_risk["average_BMI"] = "Your Result: Moderate risk"
description_recommendation["average_BMI"] = "- choose low-fat, low-calorie foods,\n- eat smaller portions,\n- drink water instead of sugary drinks,\n- be physically active; it was scientifically demonstrated that a moderate amount of aerobic exercise is the most effective exercise mode for reducing fat and body mass."
description["high_BMI"] = "You have a genetic predisposition for high body mass index. This means that you may be at risk for obesity. To better control your weight, follow the recommendations below."
description_title["high_BMI"] = "Your result: High BMI"
description_risk["high_BMI"] = "Your Result: High risk"
description_recommendation["high_BMI"] = "- choose low-fat, low-calorie foods,\n- eat smaller portions,\n- drink water instead of sugary drinks,\n- be physically active; it was scientifically demonstrated that a moderate amount of aerobic exercise is the most effective exercise mode for reducing fat and body mass."
trait_genes = "AC007092.1, AC007238.1, AC008755.1, AC022469.2, AC090559.2, AC091826.1, AC103591.3, AC108467.1, AC109458.1, AC131157.1, ADCY3, ADCY9, AKAP6, AL079352.1, AL133166.1, AL161630.1, AL450423.1, ATXN2L, BDNF, CADM2-AS2, CRTC1, DTX2P1, ETV5, FBXL17, FHIT, FTH1P5, FTO, GNAT2, GPRC5B, HNF4G, KCTD10P1, KCTD15, KRT8P44, LEMD2, LMOD1, MAP2K5, MIR642B, MRAS, PACSIN1, POC5, RNA5SP125, RNU4-17P, RPL31P12, SEC16B, SLC39A8, TMEM18, TNNI3K, TRIM66, YWHAQP6"
