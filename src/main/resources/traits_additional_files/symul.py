#!/usr/bin/env python3

import argparse
import random
##sys.argv[1]=name of the input_file where:
##1st_column=rs_id, 2nd_column=effect_allele(EA), 3rd_column=EA_freq, 4th_coulunn=effect size;
##sys.argv[2]=beta/or
##distribution of aggregated risk scores in simulated EUR population ==> file dist_imput_file
##statistics needed for model ==> file stats_imput_file 
## usage(command line):
## python symul.py imput_file beta (for regression model)
## python symul.py imput_file or (for odd ratios)

__version__ = '1.0.0'

def readFile(f=sys.argv[1]):
    """wczytuje plik podany jako pierwszy argument wiersza polecen
    dla kazdego locus oblicza czestosci genotypow z prawa H-W; 
    przelicza czestosci na liczby osob o danym genotypie sposrod 10000;
    potem tworzy liste ktora zawiera odpowiedno wiele razy trzy rozne fenotypy obliczone z wsp. beta albo or ==> v"""
    pheno=[]
    for i in open(f):
        v=[]
        line=i.strip().split(",")
        ea_fq=float(line[2])
        n_hom_ea=int(round((ea_fq**2)*10000))
        n_het=int(round(2*ea_fq*(1-ea_fq)*10000))
        n_hom_aa=10000 - n_het - n_hom_ea
        if sys.argv[2]=="beta":
            for j1 in range(n_hom_ea):
                v.append(2*float(line[3]))
            for j2 in range(n_het):
                v.append(float(line[3]))
            for j3 in range(n_hom_aa):
                v.append(0.0)
            pheno.append(v)
        if sys.argv[2]=="or":
            for j1 in range(n_hom_ea):
                v.append(float(line[3]))
            for j2 in range(n_het):
                v.append(1.0)
            for j3 in range(n_hom_aa):
                v.append(1.0/float(line[3]))
            pheno.append(v)
    #print pheno
    return pheno   

def calculate_sum_beta(pheno):
    """losowo wybiera jeden fenotyp dla kazego locus i sumuje wybrane wartosci 
    ==> to jest fenotyp wynikajacy ze wszystkich wariantow""" 
    p=0.0
    for i in pheno:
        p+=random.choice(i)
   
        #print p
    return p

def calculate_product_or(pheno):
    """losowo wybiera jeden fenotyp dla kadgo locus i mnozy wybrane wartosci 
    ==> to jest fenotyp wynikajacy ze wszystkich wariantow"""
    p=1.0
    for i in pheno:
        p=p*random.choice(i)
        #print p
    return p

def getDistr(pheno):
    """wylicza fenotyp dla 100000 'wymyslonych' osob"""
    a=[]
    if sys.argv[2]=="beta":
        for i in range(100000):
            a.append(calculate_sum_beta(pheno))
        return a
    if sys.argv[2]=="or":
        for i in range(100000):
            a.append(calculate_product_or(pheno))
        return a
     
 
def main():
    pheno=readFile()
    #print pheno
    dist=getDistr(pheno)
    dist.sort()
    txt=str(dist).replace(", ","\n").replace("[","").replace("]","")
    f1=open( "dist_"+sys.argv[1], "w")
    f1.write(txt)
    f1.close()
    f2=open("stats_"+sys.argv[1], "a+")
    names=["min: ","10%: ","median: ","90%: ","max: "]
    numb=[dist[0],dist[9999],dist[49999],dist[89999],dist[99999]]
    for i in range(len(names)):
        t=names[i]+str(numb[i])+"\n"
        f2.write(t)
        f2.flush()
    f2.close() 
    print(sum(dist)/float(len(dist)))

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='jakiś sensowny opis, co robi skrypt')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, required=True, help='Input file path')
    parser.add_argument('-o', '--output', type=str, required=True, help='Output file path')
    parser.add_argument('-t', '--type', type=str, required=True, choices=['beta', 'or'], help='Either "beta" or "or"')

    arguments = parser.parse_args()

    main()

