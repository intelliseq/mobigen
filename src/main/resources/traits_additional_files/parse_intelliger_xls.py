import re
import argparse
from typing import List
from typing import Tuple
from openpyxl import load_workbook
from src.main.python.lib.references import WebsiteTextData
from src.main.python.lib.references import ArticleLink
from src.main.python.lib.references import References


def process_cell(text: str) -> References:
    unmatched_text, found_website_data = find_website_article_data(text, [])
    return References(found_website_data, find_links(unmatched_text))


def find_website_article_data(text: str, already_found: List[WebsiteTextData]) -> Tuple[str, List[WebsiteTextData]]:
    found = re.search('^([^\n]+) (Accessed .*?)(http\S+)', text, re.DOTALL | re.MULTILINE)
    if found:
        already_found.append(WebsiteTextData(*found.groups()))
        return find_website_article_data(text[found.end():], already_found)
    return text, already_found


def find_links(text) -> List[ArticleLink]:
    return [ArticleLink(x) for x in
            re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_path', type=str, help='path to the Intelliger description xlsx')

    args = parser.parse_args()
    wb = load_workbook(filename=args.file_path)
    sheet_ranges = wb['Static by trait']

    i = 2
    while True:
        content = sheet_ranges['P{}'.format(i)].value
        i += 1
        if not content:
            break
        print(content)
        print(process_cell(content))
        print('--------------------------------------------------')
