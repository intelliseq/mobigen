from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "afr"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=2.9360, category_name='Low risk'),
        QuantitativeCategory(from_=2.9360, to=3.7369, category_name='Average risk'),
        QuantitativeCategory(from_=3.7369, category_name='High risk')

    ],
    snips_and_coefficients={
        'rs646776': ModelData(effect_allele='T', coeff_value=0.10436001532424286),
        'rs17114036': ModelData(effect_allele='A', coeff_value=0.10436001532424286),
        'rs11206510': ModelData(effect_allele='T', coeff_value=0.0769610411361284),
        'rs17465637': ModelData(effect_allele='C', coeff_value=0.131028262406404),
        'rs9818870': ModelData(effect_allele='T', coeff_value=0.06765864847381486),
        'rs17609940': ModelData(effect_allele='G', coeff_value=0.06765864847381486),
        'rs12526453': ModelData(effect_allele='G', coeff_value=0.09531017980432493),
        'rs12190287': ModelData(effect_allele='C', coeff_value=0.06765864847381486),
        'rs3798220': ModelData(effect_allele='C', coeff_value=0.412109650826833),
        'rs11556924': ModelData(effect_allele='C', coeff_value=0.08617769624105241),
        'rs4977574': ModelData(effect_allele='G', coeff_value=0.25464221837358075),
        'rs1746048': ModelData(effect_allele='C', coeff_value=0.06765864847381486),
        'rs12413409': ModelData(effect_allele='G', coeff_value=0.11332868530700327),
        'rs964184': ModelData(effect_allele='G', coeff_value=0.12221763272424911),
        'rs2259816': ModelData(effect_allele='T', coeff_value=0.0769610411361284),
        'rs3184504': ModelData(effect_allele='T', coeff_value=0.06765864847381486),
        'rs4773144': ModelData(effect_allele='G', coeff_value=0.06765864847381486),
        'rs2895811': ModelData(effect_allele='C', coeff_value=0.058268908123975824),
        'rs3825807': ModelData(effect_allele='A', coeff_value=0.0769610411361284),
        'rs12936587': ModelData(effect_allele='G', coeff_value=0.058268908123975824),
        'rs216172': ModelData(effect_allele='C', coeff_value=0.06765864847381486),
        'rs318090': ModelData(effect_allele='A', coeff_value=0.058268908123975824),
        'rs1122608': ModelData(effect_allele='G', coeff_value=0.09531017980432493),
        'rs9982601': ModelData(effect_allele='T', coeff_value=0.12221763272424911),
        'rs4845625': ModelData(effect_allele='T', coeff_value=0.03922071315328133),
        'rs2028900': ModelData(effect_allele='T', coeff_value=0.04879016416943205),
        'rs4299376': ModelData(effect_allele='G', coeff_value=0.058268908123975824),
        'rs515135': ModelData(effect_allele='C', coeff_value=0.0769610411361284),
        'rs2252641': ModelData(effect_allele='C', coeff_value=0.03922071315328133),
        'rs1878406': ModelData(effect_allele='T', coeff_value=0.058268908123975824),
        'rs7692387': ModelData(effect_allele='G', coeff_value=0.058268908123975824),
        'rs273909': ModelData(effect_allele='G', coeff_value=0.08617769624105241),
        'rs10947789': ModelData(effect_allele='T', coeff_value=0.058268908123975824),
        'rs2048327': ModelData(effect_allele='C', coeff_value=0.058268908123975824),
        'rs4252120': ModelData(effect_allele='T', coeff_value=0.058268908123975824),
        'rs11984041': ModelData(effect_allele='T', coeff_value=0.06765864847381486),
        'rs10953541': ModelData(effect_allele='C', coeff_value=0.0769610411361284),
        'rs2954029': ModelData(effect_allele='A', coeff_value=0.03922071315328133),
        'rs3217992': ModelData(effect_allele='T', coeff_value=0.14842000511827322),
        'rs2487928': ModelData(effect_allele='A', coeff_value=0.058268908123975824),
        'rs2047009': ModelData(effect_allele='G', coeff_value=0.04879016416943205),
        'rs1412444': ModelData(effect_allele='T', coeff_value=0.058268908123975824),
        'rs11226029': ModelData(effect_allele='G', coeff_value=0.06765864847381486),
        'rs9319428': ModelData(effect_allele='A', coeff_value=0.04879016416943205),
        'rs9515203': ModelData(effect_allele='T', coeff_value=0.0769610411361284),
        'rs7173743': ModelData(effect_allele='T', coeff_value=0.06765864847381486),
        'rs17514846': ModelData(effect_allele='A', coeff_value=0.04879016416943205)
    },
    model_type='beta'
)
