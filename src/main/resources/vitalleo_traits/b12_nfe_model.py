from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=1.2039, category_name='High risk'),
        QuantitativeCategory(from_=1.2039, to=1.9501, category_name='Average risk'),
        QuantitativeCategory(from_=1.9501, category_name='Low risk')

    ],
    snips_and_coefficients={
        'rs2336573': ModelData(effect_allele='T', coeff_value=0.31),
        'rs1131603': ModelData(effect_allele='C', coeff_value=0.22),
        'rs3742801': ModelData(effect_allele='T', coeff_value=0.053),
        'rs2270655': ModelData(effect_allele='G', coeff_value=0.11),
        'rs12272669': ModelData(effect_allele='A', coeff_value=0.51),
        'rs34324219': ModelData(effect_allele='C', coeff_value=0.24),
        'rs7788053': ModelData(effect_allele='A', coeff_value=0.046),
        'rs602662': ModelData(effect_allele='A', coeff_value=0.17),
        'rs1801222': ModelData(effect_allele='G', coeff_value=0.12),
        'rs41281112': ModelData(effect_allele='C', coeff_value=0.19),
        'rs1141321': ModelData(effect_allele='C', coeff_value=0.071)
    },
    model_type='beta'
)
