from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=0.025, category_name='Low risk'),
        QuantitativeCategory(from_=0.025, to=0.076, category_name='Average risk'),
        QuantitativeCategory(from_=0.076, category_name='High risk')

    ],
    snips_and_coefficients={
        'rs925966': ModelData(effect_allele='C', coeff_value=0.025),
        'rs1229984': ModelData(effect_allele='T', coeff_value=-0.02),
        'rs59514816': ModelData(effect_allele='G', coeff_value=0.025)
    },
    model_type='beta'
)
