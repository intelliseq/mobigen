from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-2.0250, category_name='Low risk'),
        QuantitativeCategory(from_=-2.0250, to=0.9520, category_name='Average risk'),
        QuantitativeCategory(from_=0.9520, category_name='High risk')
    ],
    snips_and_coefficients={
        'rs17367504': ModelData(effect_allele='G', coeff_value=-0.598),
        'rs699': ModelData(effect_allele='A', coeff_value=-0.329),
        'rs10204405': ModelData(effect_allele='A', coeff_value=-0.291),
        'rs3755652': ModelData(effect_allele='T', coeff_value=0.307),
        'rs448378': ModelData(effect_allele='G', coeff_value=0.408),
        'rs16998073': ModelData(effect_allele='T', coeff_value=0.417),
        'rs13107325': ModelData(effect_allele='T', coeff_value=-0.635),
        'rs1799945': ModelData(effect_allele='G', coeff_value=0.577),
        'rs6969780': ModelData(effect_allele='C', coeff_value=0.391),
        'rs891511': ModelData(effect_allele='A', coeff_value=-0.275),
        'rs11014166': ModelData(effect_allele='T', coeff_value=-0.348),
        'rs1530440': ModelData(effect_allele='T', coeff_value=-0.362),
        'rs11191454': ModelData(effect_allele='G', coeff_value=-0.564),
        'rs7076938': ModelData(effect_allele='C', coeff_value=-0.303),
        'rs909116': ModelData(effect_allele='C', coeff_value=0.253),
        'rs11024074': ModelData(effect_allele='C', coeff_value=0.330),
        'rs5219': ModelData(effect_allele='T', coeff_value=0.293),
        'rs12801636': ModelData(effect_allele='A', coeff_value=-0.356),
        'rs2384550': ModelData(effect_allele='A', coeff_value=-0.281),
        'rs6495122': ModelData(effect_allele='C', coeff_value=-0.368),
        'rs17514846': ModelData(effect_allele='C', coeff_value=-0.330),
        'rs8068318': ModelData(effect_allele='C', coeff_value=-0.283),
        'rs1887320': ModelData(effect_allele='A', coeff_value=0.307),
        'rs16982520': ModelData(effect_allele='G', coeff_value=0.577),
        'rs2493292': ModelData(effect_allele='T', coeff_value=0.293),
        'rs6722745': ModelData(effect_allele='C', coeff_value=0.107),
        'rs2188962': ModelData(effect_allele='T', coeff_value=-0.133),
        'rs10943605': ModelData(effect_allele='A', coeff_value=0.113),
        'rs2270860': ModelData(effect_allele='T', coeff_value=0.107),
        'rs11977526': ModelData(effect_allele='A', coeff_value=-0.107),
        'rs76452347': ModelData(effect_allele='T', coeff_value=-0.140),
        'rs10995311': ModelData(effect_allele='G', coeff_value=-0.140),
        'rs7302981': ModelData(effect_allele='A', coeff_value=0.287),
        'rs1126464': ModelData(effect_allele='C', coeff_value=0.180),
        'rs7248104': ModelData(effect_allele='A', coeff_value=-0.103),
        'rs4823006': ModelData(effect_allele='G', coeff_value=-0.090)
    },
    model_type='beta'
)
