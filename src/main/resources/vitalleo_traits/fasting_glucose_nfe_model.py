from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=0.2979, category_name='Low risk'),
        QuantitativeCategory(from_=0.2979, to=0.6991, category_name='Average risk'),
        QuantitativeCategory(from_=0.6991, category_name='High risk')

    ],
    snips_and_coefficients={
        'rs560887': ModelData(effect_allele='C', coeff_value=0.075),
        'rs10830963': ModelData(effect_allele='G', coeff_value=0.067),
        'rs4607517': ModelData(effect_allele='A', coeff_value=0.062),
        'rs2191349': ModelData(effect_allele='T', coeff_value=0.03),
        'rs780094': ModelData(effect_allele='C', coeff_value=0.029),
        'rs11708067': ModelData(effect_allele='A', coeff_value=0.027),
        'rs7944584': ModelData(effect_allele='A', coeff_value=0.021),
        'rs174550': ModelData(effect_allele='T', coeff_value=0.017),
        'rs7034200': ModelData(effect_allele='A', coeff_value=0.018),
        'rs13266634': ModelData(effect_allele='C', coeff_value=0.027),
        'rs7903146': ModelData(effect_allele='T', coeff_value=0.23)
    },
    model_type='beta'
)
