from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=0.10, category_name='High risk'),
        QuantitativeCategory(from_=0.10, to=0.2891, category_name='Average risk'),
        QuantitativeCategory(from_=0.2891, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs652197': ModelData(effect_allele='C', coeff_value=0.069),
        'rs1801133': ModelData(effect_allele='G', coeff_value=0.11)
    },
    model_type='beta'
)
