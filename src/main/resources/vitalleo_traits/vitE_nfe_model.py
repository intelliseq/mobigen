from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=0.001, category_name='High risk'),
        QuantitativeCategory(from_=0.001, to=0.069, category_name='Average risk'),
        QuantitativeCategory(from_=0.069, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs964184': ModelData(effect_allele='G', coeff_value=0.04),
        'rs2108622': ModelData(effect_allele='T', coeff_value=0.03),
        'rs11057830': ModelData(effect_allele='A', coeff_value=0.03)
    },
    model_type='beta'
)
