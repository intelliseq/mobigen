from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-2.001, category_name='High risk'),
        QuantitativeCategory(from_=-2.001, to=-0.001, category_name='Average risk'),
        QuantitativeCategory(from_=-0.001, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs2545801': ModelData(effect_allele='T', coeff_value=-1),
        'rs646776': ModelData(effect_allele='C', coeff_value=-0.397),
        'rs2284367': ModelData(effect_allele='C', coeff_value=-0.603)
    },
    model_type='beta'
)
