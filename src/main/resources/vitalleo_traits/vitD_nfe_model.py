from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-0.0441, category_name='High risk'),
        QuantitativeCategory(from_=-0.0441, to=0.1331, category_name='Average risk'),
        QuantitativeCategory(from_=0.1331, category_name='Low risk')

    ],
    snips_and_coefficients={
        'rs3755967': ModelData(effect_allele='T', coeff_value=-0.089),
        'rs12785878': ModelData(effect_allele='T', coeff_value=0.036),
        'rs10741657': ModelData(effect_allele='A', coeff_value=0.031),
        'rs17216707': ModelData(effect_allele='T', coeff_value=0.026),
        'rs10745742': ModelData(effect_allele='T', coeff_value=0.017),
        'rs8018720': ModelData(effect_allele='C', coeff_value=-0.017)
    },
    model_type='beta'
)
