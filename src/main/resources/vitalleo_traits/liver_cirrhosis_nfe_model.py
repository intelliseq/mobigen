from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=0.26, category_name='Low risk'),
        QuantitativeCategory(from_=0.26, to=0.5831, category_name='Average risk'),
        QuantitativeCategory(from_=0.5831, category_name='High risk')
    ],
    snips_and_coefficients={
        'rs738409': ModelData(effect_allele='G', coeff_value=2.19),
        'rs10401969': ModelData(effect_allele='C', coeff_value=1.715)
    },
    model_type='odds_ratio'
)
