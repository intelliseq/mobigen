from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=1.2941, category_name='Low risk'),
        QuantitativeCategory(from_=1.2941, to=1.7179, category_name='Average risk'),
        QuantitativeCategory(from_=1.7179, category_name='High risk')

    ],
    snips_and_coefficients={
        'rs11589125': ModelData(effect_allele='T', coeff_value=0.075),
        'rs272564': ModelData(effect_allele='A', coeff_value=0.046),
        'rs61765646': ModelData(effect_allele='A', coeff_value=0.056),
        'rs1899492': ModelData(effect_allele='T', coeff_value=0.04),
        'rs17362588': ModelData(effect_allele='G', coeff_value=0.062),
        'rs35596070': ModelData(effect_allele='C', coeff_value=0.06),
        'rs73043051': ModelData(effect_allele='C', coeff_value=0.041),
        'rs34310778': ModelData(effect_allele='C', coeff_value=0.036),
        'rs4836027': ModelData(effect_allele='T', coeff_value=0.05),
        'rs151283': ModelData(effect_allele='C', coeff_value=0.042),
        'rs2224202': ModelData(effect_allele='A', coeff_value=0.043),
        'rs2158712': ModelData(effect_allele='A', coeff_value=0.045),
        'rs180238': ModelData(effect_allele='T', coeff_value=0.043),
        'rs3757868': ModelData(effect_allele='G', coeff_value=0.077),
        'rs1997571': ModelData(effect_allele='A', coeff_value=0.042),
        'rs17168815': ModelData(effect_allele='G', coeff_value=0.062),
        'rs7072737': ModelData(effect_allele='A', coeff_value=0.079),
        'rs7130652': ModelData(effect_allele='T', coeff_value=0.076),
        'rs4963772': ModelData(effect_allele='A', coeff_value=0.09),
        'rs6488162': ModelData(effect_allele='C', coeff_value=0.103),
        'rs61928421': ModelData(effect_allele='C', coeff_value=0.09),
        'rs17180489': ModelData(effect_allele='C', coeff_value=0.055),
        'rs12906962': ModelData(effect_allele='T', coeff_value=0.048),
        'rs12974440': ModelData(effect_allele='G', coeff_value=0.067),
        'rs12986417': ModelData(effect_allele='G', coeff_value=0.037)
    },
    model_type='beta'
)
