from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-3.86, category_name='High risk'),
        QuantitativeCategory(from_=-3.86, to=47.59, category_name='Average risk'),
        QuantitativeCategory(from_=47.59, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs6968554': ModelData(effect_allele='A', coeff_value=-8.07),
        'rs10275488': ModelData(effect_allele='T', coeff_value=-8.44),
        'rs2892838': ModelData(effect_allele='A', coeff_value=-5.98),
        'rs11400459': ModelData(effect_allele='A', coeff_value=6.01),
        'rs10683220': ModelData(effect_allele='G', coeff_value=6.23),
        'rs12909047': ModelData(effect_allele='A', coeff_value=6.11),
        'rs62005807': ModelData(effect_allele='C', coeff_value=-7.58),
        'rs2470893': ModelData(effect_allele='T', coeff_value=6.65),
        'rs2472297': ModelData(effect_allele='T', coeff_value=7.85),
        'rs66500423': ModelData(effect_allele='T', coeff_value=9.58),
        'rs4803373': ModelData(effect_allele='C', coeff_value=5.83),
        'rs78011401': ModelData(effect_allele='T', coeff_value=6.9),
        'rs11668399': ModelData(effect_allele='C', coeff_value=-5.78),
        'rs56113850': ModelData(effect_allele='T', coeff_value=6.37),
        'rs56267346': ModelData(effect_allele='A', coeff_value=9.59),
        'rs28399442': ModelData(effect_allele='A', coeff_value=-6.13),
        'rs67210567': ModelData(effect_allele='T', coeff_value=6.48),
        'rs7260629': ModelData(effect_allele='T', coeff_value=5.94),
        'rs5828081': ModelData(effect_allele='AT', coeff_value=6.73),
        'rs28602288': ModelData(effect_allele='T', coeff_value=-5.71),
        'rs184589612': ModelData(effect_allele='T', coeff_value=-6.64),
        'rs72480748': ModelData(effect_allele='A', coeff_value=-5.92),
        'rs10425738': ModelData(effect_allele='A', coeff_value=-6.08),
        'rs56881024': ModelData(effect_allele='A', coeff_value=-5.65)
    },
    model_type='beta'
)
