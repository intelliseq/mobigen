from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-0.7001, category_name='High risk'),
        QuantitativeCategory(from_=-0.7001, to=-0.4219, category_name='Average risk'),
        QuantitativeCategory(from_=-0.4219, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs10936599': ModelData(effect_allele='T', coeff_value=-0.097),
        'rs2736100': ModelData(effect_allele='A', coeff_value=-0.078),
        'rs7675998': ModelData(effect_allele='A', coeff_value=-0.074),
        'rs9420907': ModelData(effect_allele='A', coeff_value=-0.069),
        'rs8105767': ModelData(effect_allele='A', coeff_value=-0.048),
        'rs755017': ModelData(effect_allele='A', coeff_value=-0.062),
        'rs11125529': ModelData(effect_allele='C', coeff_value=-0.056)
    },
    model_type='beta'
)
