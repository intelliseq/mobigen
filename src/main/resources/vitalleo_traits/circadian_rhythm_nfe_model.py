from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-0.3739, category_name='High risk'),
        QuantitativeCategory(from_=-0.3739, to=0.0583, category_name='Average risk'),
        QuantitativeCategory(from_=0.0583, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs909757': ModelData(effect_allele='T', coeff_value=0.0101154),
        'rs228726': ModelData(effect_allele='A', coeff_value=-0.0246902),
        'rs150812083': ModelData(effect_allele='C', coeff_value=-0.139101),
        'rs61773390': ModelData(effect_allele='T', coeff_value=0.0222605),
        'rs12065331': ModelData(effect_allele='T', coeff_value=-0.0121075),
        'rs17448682': ModelData(effect_allele='T', coeff_value=0.0164241),
        'rs10917513': ModelData(effect_allele='T', coeff_value=-0.0140683),
        'rs10916892': ModelData(effect_allele='T', coeff_value=-0.0154329),
        'rs2506089': ModelData(effect_allele='T', coeff_value=0.00954965),
        'rs12140153': ModelData(effect_allele='T', coeff_value=-0.0263311),
        'rs11208844': ModelData(effect_allele='A', coeff_value=-0.014163),
        'rs12040629': ModelData(effect_allele='A', coeff_value=0.0325023),
        'rs11588913': ModelData(effect_allele='A', coeff_value=-0.0105534),
        'rs5016898': ModelData(effect_allele='T', coeff_value=-0.0105916),
        'rs72720396': ModelData(effect_allele='A', coeff_value=-0.0189914),
        'rs481214': ModelData(effect_allele='A', coeff_value=0.0096222),
        'rs6702412': ModelData(effect_allele='T', coeff_value=0.010238),
        'rs2256459': ModelData(effect_allele='A', coeff_value=0.0130103),
        'rs1760217': ModelData(effect_allele='A', coeff_value=0.0131328),
        'rs41279738': ModelData(effect_allele='T', coeff_value=0.0308801),
        'rs17575798': ModelData(effect_allele='A', coeff_value=-0.0172218),
        'rs6537747': ModelData(effect_allele='T', coeff_value=-0.0106379),
        'rs11102807': ModelData(effect_allele='A', coeff_value=-0.00976878),
        'rs9436119': ModelData(effect_allele='A', coeff_value=0.0202979),
        'rs7524620': ModelData(effect_allele='A', coeff_value=0.0116737),
        'rs6665637': ModelData(effect_allele='A', coeff_value=-0.0112026),
        'rs115073088': ModelData(effect_allele='A', coeff_value=-0.0380572),
        'rs975025': ModelData(effect_allele='T', coeff_value=-0.0242884),
        'rs34335333': ModelData(effect_allele='T', coeff_value=-0.011129),
        'rs1144566': ModelData(effect_allele='T', coeff_value=0.10868),
        'rs2378824': ModelData(effect_allele='T', coeff_value=0.00989845),
        'rs34580810': ModelData(effect_allele='A', coeff_value=-0.0112003),
        'rs1221502': ModelData(effect_allele='A', coeff_value=0.011033),
        'rs4657983': ModelData(effect_allele='A', coeff_value=-0.0116315),
        'rs10754194': ModelData(effect_allele='C', coeff_value=-0.0164202),
        'rs6429233': ModelData(effect_allele='A', coeff_value=0.00968303),
        'rs13011556': ModelData(effect_allele='C', coeff_value=-0.0147395),
        'rs62124718': ModelData(effect_allele='A', coeff_value=-0.0201599),
        'rs72796401': ModelData(effect_allele='A', coeff_value=0.0162399),
        'rs11676272': ModelData(effect_allele='A', coeff_value=0.0103349),
        'rs11678584': ModelData(effect_allele='A', coeff_value=-0.0144417),
        'rs848552': ModelData(effect_allele='C', coeff_value=-0.0128053),
        'rs6725031': ModelData(effect_allele='T', coeff_value=-0.00971863),
        'rs75120545': ModelData(effect_allele='T', coeff_value=0.044999),
        'rs12998046': ModelData(effect_allele='A', coeff_value=0.0128244),
        'rs6544906': ModelData(effect_allele='A', coeff_value=0.010563),
        'rs66888174': ModelData(effect_allele='A', coeff_value=0.0100569),
        'rs972557': ModelData(effect_allele='T', coeff_value=0.0103256),
        'rs10495976': ModelData(effect_allele='A', coeff_value=-0.0126166),
        'rs12470914': ModelData(effect_allele='A', coeff_value=0.0236277),
        'rs4672458': ModelData(effect_allele='T', coeff_value=-0.0107536),
        'rs10175975': ModelData(effect_allele='T', coeff_value=0.0132679),
        'rs812925': ModelData(effect_allele='C', coeff_value=-0.0143375),
        'rs113851554': ModelData(effect_allele='T', coeff_value=-0.026945),
        'rs2706762': ModelData(effect_allele='T', coeff_value=-0.0179937),
        'rs12464387': ModelData(effect_allele='A', coeff_value=-0.0104612),
        'rs6727752': ModelData(effect_allele='A', coeff_value=0.0106282),
        'rs10520176': ModelData(effect_allele='T', coeff_value=0.0167843),
        'rs4254526': ModelData(effect_allele='T', coeff_value=-0.0112643),
        'rs11681299': ModelData(effect_allele='T', coeff_value=0.0109267),
        'rs34509802': ModelData(effect_allele='A', coeff_value=0.015674),
        'rs76064513': ModelData(effect_allele='T', coeff_value=0.0137471),
        'rs77248969': ModelData(effect_allele='A', coeff_value=-0.0156898),
        'rs28380327': ModelData(effect_allele='A', coeff_value=0.0180917),
        'rs2166559': ModelData(effect_allele='T', coeff_value=-0.0147327),
        'rs747003': ModelData(effect_allele='T', coeff_value=0.00963493),
        'rs13004345': ModelData(effect_allele='T', coeff_value=-0.0105315),
        'rs6433478': ModelData(effect_allele='T', coeff_value=-0.0114645),
        'rs1593205': ModelData(effect_allele='A', coeff_value=0.0131349),
        'rs11677484': ModelData(effect_allele='T', coeff_value=0.0118579),
        'rs1064213': ModelData(effect_allele='A', coeff_value=0.0196147),
        'rs184033703': ModelData(effect_allele='A', coeff_value=0.0266266),
        'rs57690685': ModelData(effect_allele='A', coeff_value=-0.0454184),
        'rs960783': ModelData(effect_allele='T', coeff_value=-0.0464552),
        'rs80261926': ModelData(effect_allele='T', coeff_value=0.0612319),
        'rs58574366': ModelData(effect_allele='A', coeff_value=-0.0234733),
        'rs80271258': ModelData(effect_allele='T', coeff_value=-0.0420601),
        'rs188004381': ModelData(effect_allele='A', coeff_value=-0.0282685),
        'rs62182135': ModelData(effect_allele='A', coeff_value=-0.0124195),
        'rs35346733': ModelData(effect_allele='A', coeff_value=-0.0133308),
        'rs111261826': ModelData(effect_allele='A', coeff_value=-0.0120069),
        'rs149611468': ModelData(effect_allele='T', coeff_value=0.062582),
        'rs6794796': ModelData(effect_allele='A', coeff_value=0.0117625),
        'rs9817910': ModelData(effect_allele='A', coeff_value=-0.0108386),
        'rs73050286': ModelData(effect_allele='T', coeff_value=0.0136068),
        'rs13059636': ModelData(effect_allele='A', coeff_value=-0.00998411),
        'rs114848860': ModelData(effect_allele='A', coeff_value=-0.0392365),
        'rs12636669': ModelData(effect_allele='T', coeff_value=0.0243861),
        'rs2236950': ModelData(effect_allele='A', coeff_value=-0.0155516),
        'rs56357549': ModelData(effect_allele='T', coeff_value=0.0139181),
        'rs17007397': ModelData(effect_allele='C', coeff_value=0.00948751),
        'rs17655548': ModelData(effect_allele='A', coeff_value=-0.0117959),
        'rs7626335': ModelData(effect_allele='A', coeff_value=-0.0127112),
        'rs7429614': ModelData(effect_allele='T', coeff_value=0.0165186),
        'rs116291712': ModelData(effect_allele='A', coeff_value=-0.0433646),
        'rs4324483': ModelData(effect_allele='T', coeff_value=-0.00948386),
        'rs56242353': ModelData(effect_allele='T', coeff_value=0.0161316),
        'rs1449403': ModelData(effect_allele='A', coeff_value=0.018559),
        'rs34967119': ModelData(effect_allele='A', coeff_value=0.0097007),
        'rs1800828': ModelData(effect_allele='C', coeff_value=0.0110459),
        'rs79402305': ModelData(effect_allele='A', coeff_value=0.0203378),
        'rs72966564': ModelData(effect_allele='T', coeff_value=-0.0112593),
        'rs13065394': ModelData(effect_allele='T', coeff_value=-0.012342),
        'rs4550782': ModelData(effect_allele='T', coeff_value=0.0123847),
        'rs7649164': ModelData(effect_allele='T', coeff_value=0.00995918),
        'rs6440833': ModelData(effect_allele='A', coeff_value=0.0104435),
        'rs2102506': ModelData(effect_allele='A', coeff_value=-0.00963073),
        'rs1599374': ModelData(effect_allele='A', coeff_value=0.0133638),
        'rs482204': ModelData(effect_allele='A', coeff_value=-0.0120556),
        'rs3850174': ModelData(effect_allele='A', coeff_value=-0.0148881),
        'rs301218': ModelData(effect_allele='A', coeff_value=-0.0110594),
        'rs4859215': ModelData(effect_allele='T', coeff_value=0.0124646),
        'rs4484214': ModelData(effect_allele='A', coeff_value=0.0113038),
        'rs1468945': ModelData(effect_allele='A', coeff_value=-0.0160509),
        'rs73182221': ModelData(effect_allele='A', coeff_value=0.0176745),
        'rs3796618': ModelData(effect_allele='A', coeff_value=-0.00967013),
        'rs4690085': ModelData(effect_allele='A', coeff_value=-0.00946361),
        'rs4698678': ModelData(effect_allele='C', coeff_value=0.0127609),
        'rs1502249': ModelData(effect_allele='A', coeff_value=0.00938648),
        'rs6838677': ModelData(effect_allele='A', coeff_value=-0.0103554),
        'rs4860734': ModelData(effect_allele='A', coeff_value=0.010745),
        'rs6846730': ModelData(effect_allele='T', coeff_value=-0.0152052),
        'rs2850979': ModelData(effect_allele='T', coeff_value=-0.01174),
        'rs4834311': ModelData(effect_allele='A', coeff_value=-0.00986469),
        'rs7700110': ModelData(effect_allele='A', coeff_value=0.0114822),
        'rs17455138': ModelData(effect_allele='T', coeff_value=0.0128276),
        'rs4241964': ModelData(effect_allele='T', coeff_value=-0.0135514),
        'rs938836': ModelData(effect_allele='A', coeff_value=-0.0103171),
        'rs72729847': ModelData(effect_allele='T', coeff_value=-0.0135853),
        'rs1458146': ModelData(effect_allele='A', coeff_value=-0.0114226),
        'rs10058356': ModelData(effect_allele='T', coeff_value=-0.0110316),
        'rs7701529': ModelData(effect_allele='A', coeff_value=-0.0136948),
        'rs32897': ModelData(effect_allele='T', coeff_value=0.0127838),
        'rs7721608': ModelData(effect_allele='T', coeff_value=0.0105143),
        'rs388340': ModelData(effect_allele='A', coeff_value=-0.0106416),
        'rs4269995': ModelData(effect_allele='T', coeff_value=-0.0164489),
        'rs79864120': ModelData(effect_allele='A', coeff_value=-0.01576),
        'rs77960': ModelData(effect_allele='A', coeff_value=0.0112845),
        'rs62378355': ModelData(effect_allele='A', coeff_value=-0.00995875),
        'rs1559253': ModelData(effect_allele='A', coeff_value=0.0102669),
        'rs17140201': ModelData(effect_allele='A', coeff_value=-0.0128042),
        'rs13172141': ModelData(effect_allele='A', coeff_value=0.0102482),
        'rs67988891': ModelData(effect_allele='C', coeff_value=-0.0159592),
        'rs2901796': ModelData(effect_allele='A', coeff_value=0.010664),
        'rs72834074': ModelData(effect_allele='A', coeff_value=0.0157017),
        'rs42210': ModelData(effect_allele='C', coeff_value=-0.0121263),
        'rs12518401': ModelData(effect_allele='A', coeff_value=-0.0111451),
        'rs7735794': ModelData(effect_allele='A', coeff_value=0.0143718),
        'rs465670': ModelData(effect_allele='T', coeff_value=0.0108088),
        'rs9394154': ModelData(effect_allele='C', coeff_value=-0.0104616),
        'rs9381812': ModelData(effect_allele='A', coeff_value=-0.0224408),
        'rs1811899': ModelData(effect_allele='T', coeff_value=-0.0126842),
        'rs2742986': ModelData(effect_allele='T', coeff_value=-0.0104988),
        'rs766406': ModelData(effect_allele='T', coeff_value=-0.0114101),
        'rs56402621': ModelData(effect_allele='T', coeff_value=0.0368966),
        'rs486416': ModelData(effect_allele='A', coeff_value=-0.0117019),
        'rs13203140': ModelData(effect_allele='T', coeff_value=-0.0106944),
        'rs12206814': ModelData(effect_allele='C', coeff_value=0.0097765),
        'rs2396004': ModelData(effect_allele='A', coeff_value=0.0102025),
        'rs3857599': ModelData(effect_allele='A', coeff_value=0.0135439),
        'rs2653349': ModelData(effect_allele='A', coeff_value=0.0305985),
        'rs1931814': ModelData(effect_allele='A', coeff_value=0.0125437),
        'rs2881955': ModelData(effect_allele='T', coeff_value=0.0123663),
        'rs12195792': ModelData(effect_allele='A', coeff_value=0.0145652),
        'rs11154718': ModelData(effect_allele='T', coeff_value=-0.00995368),
        'rs60616179': ModelData(effect_allele='A', coeff_value=0.0232065),
        'rs4535583': ModelData(effect_allele='T', coeff_value=0.0101527),
        'rs9496623': ModelData(effect_allele='A', coeff_value=-0.0106843),
        'rs2050185': ModelData(effect_allele='A', coeff_value=0.0101178),
        'rs827749': ModelData(effect_allele='T', coeff_value=-0.0140395),
        'rs9479402': ModelData(effect_allele='T', coeff_value=-0.0741237),
        'rs62436127': ModelData(effect_allele='C', coeff_value=0.0146695),
        'rs9348050': ModelData(effect_allele='T', coeff_value=0.0110808),
        'rs4027217': ModelData(effect_allele='A', coeff_value=-0.0112381),
        'rs10237162': ModelData(effect_allele='T', coeff_value=0.0166309),
        'rs10951325': ModelData(effect_allele='T', coeff_value=0.0164952),
        'rs12701263': ModelData(effect_allele='T', coeff_value=-0.0127421),
        'rs6967481': ModelData(effect_allele='T', coeff_value=0.0158365),
        'rs4236237': ModelData(effect_allele='A', coeff_value=-0.011223),
        'rs2944831': ModelData(effect_allele='A', coeff_value=0.0121783),
        'rs3807651': ModelData(effect_allele='A', coeff_value=0.0097622),
        'rs10254050': ModelData(effect_allele='C', coeff_value=-0.0242622),
        'rs202157': ModelData(effect_allele='T', coeff_value=-0.0111009),
        'rs6960360': ModelData(effect_allele='A', coeff_value=-0.0153433),
        'rs1131359': ModelData(effect_allele='T', coeff_value=0.0381967),
        'rs4729854': ModelData(effect_allele='A', coeff_value=-0.0178367),
        'rs73406288': ModelData(effect_allele='T', coeff_value=0.0231847),
        'rs76187115': ModelData(effect_allele='T', coeff_value=-0.0453712),
        'rs148266530': ModelData(effect_allele='T', coeff_value=0.0355364),
        'rs2396719': ModelData(effect_allele='A', coeff_value=0.0140264),
        'rs6968240': ModelData(effect_allele='A', coeff_value=0.0107296),
        'rs62465218': ModelData(effect_allele='A', coeff_value=-0.0133833),
        'rs6958557': ModelData(effect_allele='T', coeff_value=0.0117939),
        'rs113161209': ModelData(effect_allele='A', coeff_value=0.0175983),
        'rs2072413': ModelData(effect_allele='T', coeff_value=-0.0105492),
        'rs62479736': ModelData(effect_allele='T', coeff_value=0.0117295),
        'rs35524253': ModelData(effect_allele='A', coeff_value=0.0155012),
        'rs2979139': ModelData(effect_allele='A', coeff_value=-0.0128509),
        'rs2322605': ModelData(effect_allele='A', coeff_value=-0.0101057),
        'rs71523448': ModelData(effect_allele='C', coeff_value=-0.0227513),
        'rs6993892': ModelData(effect_allele='T', coeff_value=-0.0156925),
        'rs6468316': ModelData(effect_allele='T', coeff_value=-0.0094642),
        'rs7845620': ModelData(effect_allele='A', coeff_value=-0.0201794),
        'rs10109566': ModelData(effect_allele='A', coeff_value=-0.00977336),
        'rs34054660': ModelData(effect_allele='A', coeff_value=0.0094188),
        'rs187028': ModelData(effect_allele='A', coeff_value=-0.0114695),
        'rs16939162': ModelData(effect_allele='A', coeff_value=0.0172581),
        'rs6988733': ModelData(effect_allele='T', coeff_value=0.00977671),
        'rs7006885': ModelData(effect_allele='A', coeff_value=0.0126362),
        'rs3100052': ModelData(effect_allele='A', coeff_value=0.0115158),
        'rs2737245': ModelData(effect_allele='T', coeff_value=0.0160406),
        'rs4909296': ModelData(effect_allele='T', coeff_value=-0.0093573),
        'rs1871729': ModelData(effect_allele='A', coeff_value=-0.0104921),
        'rs6477309': ModelData(effect_allele='T', coeff_value=0.014064),
        'rs2844016': ModelData(effect_allele='T', coeff_value=0.0110916),
        'rs308521': ModelData(effect_allele='T', coeff_value=0.0145771),
        'rs6560218': ModelData(effect_allele='T', coeff_value=-0.009648),
        'rs62553781': ModelData(effect_allele='T', coeff_value=-0.0338118),
        'rs12378543': ModelData(effect_allele='T', coeff_value=-0.0102277),
        'rs555784': ModelData(effect_allele='A', coeff_value=-0.0108891),
        'rs295268': ModelData(effect_allele='T', coeff_value=-0.0123003),
        'rs3138490': ModelData(effect_allele='A', coeff_value=0.0104211),
        'rs10759208': ModelData(effect_allele='T', coeff_value=-0.0108421),
        'rs11788633': ModelData(effect_allele='C', coeff_value=0.0102734),
        'rs10818834': ModelData(effect_allele='T', coeff_value=0.0128065),
        'rs3780204': ModelData(effect_allele='C', coeff_value=-0.0105886),
        'rs4838192': ModelData(effect_allele='T', coeff_value=0.00946749),
        'rs10988239': ModelData(effect_allele='T', coeff_value=-0.0113038),
        'rs12380242': ModelData(effect_allele='T', coeff_value=-0.0106076),
        'rs28458909': ModelData(effect_allele='T', coeff_value=-0.031742),
        'rs497338': ModelData(effect_allele='T', coeff_value=0.011902),
        'rs1630848': ModelData(effect_allele='A', coeff_value=0.009267),
        'rs9416744': ModelData(effect_allele='A', coeff_value=0.0162008),
        'rs11597421': ModelData(effect_allele='A', coeff_value=-0.00949538),
        'rs12249410': ModelData(effect_allele='T', coeff_value=-0.0161244),
        'rs17712705': ModelData(effect_allele='A', coeff_value=-0.00995687),
        'rs2298117': ModelData(effect_allele='T', coeff_value=-0.00941342),
        'rs10999667': ModelData(effect_allele='A', coeff_value=0.0100024),
        'rs10762434': ModelData(effect_allele='C', coeff_value=0.0120324),
        'rs61875203': ModelData(effect_allele='T', coeff_value=0.0113467),
        'rs1163238': ModelData(effect_allele='A', coeff_value=-0.00958099),
        'rs7900191': ModelData(effect_allele='T', coeff_value=-0.00940837),
        'rs2173244': ModelData(effect_allele='T', coeff_value=-0.0104275),
        'rs3808964': ModelData(effect_allele='T', coeff_value=0.00992183),
        'rs9664044': ModelData(effect_allele='T', coeff_value=-0.012019),
        'rs10830107': ModelData(effect_allele='A', coeff_value=0.0122577),
        'rs76518095': ModelData(effect_allele='T', coeff_value=0.0183421),
        'rs12771973': ModelData(effect_allele='A', coeff_value=-0.011014),
        'rs7949336': ModelData(effect_allele='A', coeff_value=-0.0110576),
        'rs297343': ModelData(effect_allele='T', coeff_value=-0.010304),
        'rs10832648': ModelData(effect_allele='A', coeff_value=-0.0152297),
        'rs10742179': ModelData(effect_allele='A', coeff_value=0.0146756),
        'rs4923541': ModelData(effect_allele='T', coeff_value=0.0101765),
        'rs621421': ModelData(effect_allele='T', coeff_value=-0.0130099),
        'rs11032362': ModelData(effect_allele='A', coeff_value=0.0316312),
        'rs7111582': ModelData(effect_allele='A', coeff_value=-0.0210319),
        'rs35233100': ModelData(effect_allele='T', coeff_value=-0.0284884),
        'rs10838687': ModelData(effect_allele='T', coeff_value=0.0175979),
        'rs12808544': ModelData(effect_allele='A', coeff_value=-0.015624),
        'rs662094': ModelData(effect_allele='A', coeff_value=0.0122234),
        'rs1278402': ModelData(effect_allele='A', coeff_value=0.0114397),
        'rs271043': ModelData(effect_allele='A', coeff_value=0.0101268),
        'rs1508608': ModelData(effect_allele='A', coeff_value=0.0124923),
        'rs4121878': ModelData(effect_allele='C', coeff_value=0.00979473),
        'rs17577073': ModelData(effect_allele='A', coeff_value=0.0108108),
        'rs2514214': ModelData(effect_allele='A', coeff_value=0.0109572),
        'rs4936290': ModelData(effect_allele='A', coeff_value=-0.0110364),
        'rs7108229': ModelData(effect_allele='A', coeff_value=0.00932796),
        'rs3867239': ModelData(effect_allele='A', coeff_value=0.01152),
        'rs74357745': ModelData(effect_allele='A', coeff_value=0.0159952),
        'rs7943634': ModelData(effect_allele='T', coeff_value=-0.0111804),
        'rs3782860': ModelData(effect_allele='T', coeff_value=0.011116),
        'rs1799464': ModelData(effect_allele='A', coeff_value=-0.0103614),
        'rs12298405': ModelData(effect_allele='T', coeff_value=-0.0114625),
        'rs2433634': ModelData(effect_allele='A', coeff_value=-0.011749),
        'rs11611435': ModelData(effect_allele='T', coeff_value=0.0110995),
        'rs4436614': ModelData(effect_allele='A', coeff_value=0.0120274),
        'rs1843888': ModelData(effect_allele='A', coeff_value=0.0229432),
        'rs247929': ModelData(effect_allele='C', coeff_value=0.0142529),
        'rs7975791': ModelData(effect_allele='T', coeff_value=0.0258352),
        'rs10735836': ModelData(effect_allele='A', coeff_value=-0.0155518),
        'rs7299922': ModelData(effect_allele='A', coeff_value=0.0114368),
        'rs487722': ModelData(effect_allele='T', coeff_value=0.0126619),
        'rs11174782': ModelData(effect_allele='T', coeff_value=-0.0160855),
        'rs10877962': ModelData(effect_allele='T', coeff_value=0.0112827),
        'rs711098': ModelData(effect_allele='A', coeff_value=0.0100561),
        'rs7959983': ModelData(effect_allele='T', coeff_value=-0.014009),
        'rs7304278': ModelData(effect_allele='A', coeff_value=-0.0144347),
        'rs7298532': ModelData(effect_allele='T', coeff_value=0.0120263),
        'rs3955311': ModelData(effect_allele='T', coeff_value=0.0131154),
        'rs80097534': ModelData(effect_allele='T', coeff_value=-0.0172005),
        'rs7964138': ModelData(effect_allele='A', coeff_value=0.0105707),
        'rs9597241': ModelData(effect_allele='A', coeff_value=0.0157582),
        'rs2321993': ModelData(effect_allele='A', coeff_value=-0.0116257),
        'rs9571526': ModelData(effect_allele='T', coeff_value=-0.0115617),
        'rs2593487': ModelData(effect_allele='A', coeff_value=-0.0125354),
        'rs495593': ModelData(effect_allele='A', coeff_value=0.0112209),
        'rs45597035': ModelData(effect_allele='A', coeff_value=-0.00980824),
        'rs9573980': ModelData(effect_allele='A', coeff_value=0.0589561),
        'rs61963491': ModelData(effect_allele='A', coeff_value=0.102414),
        'rs9523817': ModelData(effect_allele='A', coeff_value=-0.0106609),
        'rs1886205': ModelData(effect_allele='A', coeff_value=0.0144381),
        'rs9558942': ModelData(effect_allele='T', coeff_value=-0.00984319),
        'rs3815983': ModelData(effect_allele='T', coeff_value=-0.0109785),
        'rs1163628': ModelData(effect_allele='A', coeff_value=-0.0142635),
        'rs61990287': ModelData(effect_allele='A', coeff_value=0.0110353),
        'rs2878172': ModelData(effect_allele='A', coeff_value=-0.00998016),
        'rs962961': ModelData(effect_allele='T', coeff_value=-0.0119951),
        'rs6573308': ModelData(effect_allele='T', coeff_value=0.00982424),
        'rs7143933': ModelData(effect_allele='T', coeff_value=0.0110793),
        'rs2978382': ModelData(effect_allele='T', coeff_value=0.00997671),
        'rs4903203': ModelData(effect_allele='A', coeff_value=0.0115603),
        'rs12436039': ModelData(effect_allele='T', coeff_value=0.014497),
        'rs4550384': ModelData(effect_allele='T', coeff_value=0.0119551),
        'rs710284': ModelData(effect_allele='T', coeff_value=0.00966848),
        'rs11845599': ModelData(effect_allele='A', coeff_value=-0.0127102),
        'rs59986227': ModelData(effect_allele='C', coeff_value=-0.0132245),
        'rs12442008': ModelData(effect_allele='T', coeff_value=0.0127329),
        'rs4775086': ModelData(effect_allele='A', coeff_value=-0.0113763),
        'rs12594634': ModelData(effect_allele='T', coeff_value=-0.0107548),
        'rs12442674': ModelData(effect_allele='A', coeff_value=0.0120939),
        'rs1873958': ModelData(effect_allele='A', coeff_value=0.0136386),
        'rs72773411': ModelData(effect_allele='A', coeff_value=0.0131039),
        'rs12445235': ModelData(effect_allele='C', coeff_value=-0.0097861),
        'rs2304467': ModelData(effect_allele='C', coeff_value=-0.0100462),
        'rs11641239': ModelData(effect_allele='T', coeff_value=0.0109354),
        'rs7203707': ModelData(effect_allele='A', coeff_value=-0.0104966),
        'rs35888008': ModelData(effect_allele='A', coeff_value=-0.0122264),
        'rs1344490': ModelData(effect_allele='T', coeff_value=-0.00984493),
        'rs12927162': ModelData(effect_allele='A', coeff_value=0.0248274),
        'rs1421085': ModelData(effect_allele='T', coeff_value=-0.0202621),
        'rs6499821': ModelData(effect_allele='A', coeff_value=0.017563),
        'rs2550298': ModelData(effect_allele='T', coeff_value=-0.0179894),
        'rs8063159': ModelData(effect_allele='A', coeff_value=0.0144827),
        'rs72790386': ModelData(effect_allele='T', coeff_value=0.0269846),
        'rs11641811': ModelData(effect_allele='A', coeff_value=-0.0106683),
        'rs17604349': ModelData(effect_allele='A', coeff_value=-0.016523),
        'rs1061032': ModelData(effect_allele='T', coeff_value=0.0288978),
        'rs11545787': ModelData(effect_allele='A', coeff_value=-0.0181401),
        'rs2232831': ModelData(effect_allele='A', coeff_value=0.0124124),
        'rs12938235': ModelData(effect_allele='T', coeff_value=0.00943034),
        'rs2011528': ModelData(effect_allele='T', coeff_value=-0.0147613),
        'rs3760381': ModelData(effect_allele='A', coeff_value=0.013339),
        'rs7225002': ModelData(effect_allele='A', coeff_value=-0.0112885),
        'rs12051': ModelData(effect_allele='A', coeff_value=-0.0127524),
        'rs612720': ModelData(effect_allele='A', coeff_value=0.0093955),
        'rs55846845': ModelData(effect_allele='A', coeff_value=-0.0110217),
        'rs72829706': ModelData(effect_allele='A', coeff_value=0.0274752),
        'rs8072058': ModelData(effect_allele='A', coeff_value=-0.0116783),
        'rs412000': ModelData(effect_allele='C', coeff_value=-0.0104066),
        'rs58681483': ModelData(effect_allele='A', coeff_value=0.0180656),
        'rs72841368': ModelData(effect_allele='A', coeff_value=-0.0144235),
        'rs2916148': ModelData(effect_allele='A', coeff_value=0.0128878),
        'rs2580160': ModelData(effect_allele='A', coeff_value=0.0112206),
        'rs62082402': ModelData(effect_allele='T', coeff_value=0.0211412),
        'rs1788784': ModelData(effect_allele='A', coeff_value=-0.0112061),
        'rs1013987': ModelData(effect_allele='T', coeff_value=-0.0124637),
        'rs4419127': ModelData(effect_allele='A', coeff_value=0.0205434),
        'rs17643207': ModelData(effect_allele='A', coeff_value=0.0193778),
        'rs9950528': ModelData(effect_allele='A', coeff_value=-0.0103637),
        'rs75457814': ModelData(effect_allele='A', coeff_value=-0.0117882),
        'rs12969848': ModelData(effect_allele='T', coeff_value=0.0160081),
        'rs9956387': ModelData(effect_allele='A', coeff_value=-0.00934139),
        'rs28602385': ModelData(effect_allele='A', coeff_value=0.0106989),
        'rs4800998': ModelData(effect_allele='A', coeff_value=0.01621),
        'rs9964420': ModelData(effect_allele='A', coeff_value=-0.0235868),
        'rs11152350': ModelData(effect_allele='A', coeff_value=-0.0125657),
        'rs34329963': ModelData(effect_allele='T', coeff_value=-0.0154058),
        'rs1025601': ModelData(effect_allele='T', coeff_value=-0.00966197),
        'rs10402849': ModelData(effect_allele='T', coeff_value=0.012545),
        'rs36055559': ModelData(effect_allele='A', coeff_value=-0.0155822),
        'rs7248205': ModelData(effect_allele='T', coeff_value=0.0122863),
        'rs9636202': ModelData(effect_allele='A', coeff_value=-0.0126398),
        'rs73026775': ModelData(effect_allele='A', coeff_value=-0.0146423),
        'rs4804951': ModelData(effect_allele='A', coeff_value=0.0107465),
        'rs56113850': ModelData(effect_allele='T', coeff_value=-0.0106669),
        'rs58876439': ModelData(effect_allele='A', coeff_value=0.0220732),
        'rs1007204': ModelData(effect_allele='A', coeff_value=-0.0101447),
        'rs11670534': ModelData(effect_allele='T', coeff_value=-0.0140938),
        'rs2760545': ModelData(effect_allele='T', coeff_value=0.0108296),
        'rs6131805': ModelData(effect_allele='T', coeff_value=0.0114936),
        'rs947088': ModelData(effect_allele='T', coeff_value=0.010528),
        'rs6131942': ModelData(effect_allele='A', coeff_value=-0.0140132),
        'rs1474754': ModelData(effect_allele='A', coeff_value=-0.0105825),
        'rs6047481': ModelData(effect_allele='A', coeff_value=0.0104565),
        'rs1737893': ModelData(effect_allele='T', coeff_value=-0.0112572),
        'rs2072727': ModelData(effect_allele='T', coeff_value=0.013632),
        'rs57236847': ModelData(effect_allele='C', coeff_value=0.0104442),
        'rs695459': ModelData(effect_allele='T', coeff_value=-0.00984758),
        'rs28459838': ModelData(effect_allele='T', coeff_value=0.0121112),
        'rs118047999': ModelData(effect_allele='C', coeff_value=0.0112614),
        'rs139911': ModelData(effect_allele='T', coeff_value=-0.0141776),
        'rs762995': ModelData(effect_allele='A', coeff_value=-0.0101045),
        'rs6007594': ModelData(effect_allele='A', coeff_value=-0.0119836)
    },
    model_type='beta'
)
