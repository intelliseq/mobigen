from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-1.071, category_name='High risk'),
        QuantitativeCategory(from_=-1.071, to=0.931, category_name='Average risk'),
        QuantitativeCategory(from_=0.931, category_name='Low risk')
    ],
    snips_and_coefficients={
        'rs2290758': ModelData(effect_allele='A', coeff_value=0.42),
        'rs2282326': ModelData(effect_allele='C', coeff_value=-0.91),
        'rs1695': ModelData(effect_allele='G', coeff_value=-0.18),
        'rs241775': ModelData(effect_allele='T', coeff_value=-0.18),
        'rs483082': ModelData(effect_allele='T', coeff_value=0.27),
        'rs62143206': ModelData(effect_allele='T', coeff_value=0.54)
    },
    model_type='beta'
)
