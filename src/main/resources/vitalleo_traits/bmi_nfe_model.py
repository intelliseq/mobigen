from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=1.3820, category_name='Low risk'),
        QuantitativeCategory(from_=1.3820, to=1.75891, category_name='Average risk'),
        QuantitativeCategory(from_=1.75891, category_name='High risk')

    ],
    snips_and_coefficients={
        'rs11209951': ModelData(effect_allele='T', coeff_value=0.032),
        'rs7551507': ModelData(effect_allele='C', coeff_value=0.023),
        'rs4130548': ModelData(effect_allele='C', coeff_value=0.025),
        'rs17024393': ModelData(effect_allele='C', coeff_value=0.084),
        'rs633715': ModelData(effect_allele='C', coeff_value=0.053),
        'rs2820315': ModelData(effect_allele='T', coeff_value=0.024),
        'rs2867125': ModelData(effect_allele='C', coeff_value=0.07),
        'rs6752378': ModelData(effect_allele='A', coeff_value=0.033),
        'rs2075171': ModelData(effect_allele='G', coeff_value=0.026),
        'rs759250': ModelData(effect_allele='A', coeff_value=0.03),
        'rs6804842': ModelData(effect_allele='G', coeff_value=0.023),
        'rs2365389': ModelData(effect_allele='C', coeff_value=0.028),
        'rs9852127': ModelData(effect_allele='A', coeff_value=0.03),
        'rs1720825': ModelData(effect_allele='A', coeff_value=0.026),
        'rs2035935': ModelData(effect_allele='G', coeff_value=0.044),
        'rs6809651': ModelData(effect_allele='G', coeff_value=0.048),
        'rs10938397': ModelData(effect_allele='G', coeff_value=0.035),
        'rs13107325': ModelData(effect_allele='T', coeff_value=0.051),
        'rs2112347': ModelData(effect_allele='T', coeff_value=0.023),
        'rs6870983': ModelData(effect_allele='C', coeff_value=0.025),
        'rs40067': ModelData(effect_allele='G', coeff_value=0.035),
        'rs6864049': ModelData(effect_allele='G', coeff_value=0.021),
        'rs943466': ModelData(effect_allele='G', coeff_value=0.024),
        'rs7757419': ModelData(effect_allele='T', coeff_value=0.028),
        'rs4715210': ModelData(effect_allele='T', coeff_value=0.039),
        'rs13191362': ModelData(effect_allele='A', coeff_value=0.036),
        'rs2430307': ModelData(effect_allele='T', coeff_value=0.043),
        'rs17405819': ModelData(effect_allele='T', coeff_value=0.024),
        'rs10968577': ModelData(effect_allele='T', coeff_value=0.03),
        'rs1928295': ModelData(effect_allele='T', coeff_value=0.022),
        'rs4929923': ModelData(effect_allele='C', coeff_value=0.025),
        'rs2049045': ModelData(effect_allele='G', coeff_value=0.043),
        'rs7124681': ModelData(effect_allele='A', coeff_value=0.026),
        'rs7138803': ModelData(effect_allele='A', coeff_value=0.03),
        'rs12429545': ModelData(effect_allele='A', coeff_value=0.031),
        'rs10132280': ModelData(effect_allele='C', coeff_value=0.023),
        'rs12885454': ModelData(effect_allele='C', coeff_value=0.022),
        'rs17522122': ModelData(effect_allele='T', coeff_value=0.021),
        'rs7141420': ModelData(effect_allele='T', coeff_value=0.024),
        'rs2241423': ModelData(effect_allele='G', coeff_value=0.033),
        'rs2531995': ModelData(effect_allele='T', coeff_value=0.031),
        'rs12446632': ModelData(effect_allele='G', coeff_value=0.036),
        'rs12325113': ModelData(effect_allele='C', coeff_value=0.03),
        'rs9930333': ModelData(effect_allele='G', coeff_value=0.084),
        'rs571312': ModelData(effect_allele='A', coeff_value=0.059),
        'rs757318': ModelData(effect_allele='C', coeff_value=0.021),
        'rs185350': ModelData(effect_allele='T', coeff_value=0.024),
        'rs11672660': ModelData(effect_allele='C', coeff_value=0.029),
        'rs2303108': ModelData(effect_allele='C', coeff_value=0.025)
    },
    model_type='beta'
)
