score_model:
    categories: 
        High risk: {to: 1.2039}
        Average risk: {from: 1.2039, to: 1.9501}
        Low risk: {from: 1.9501}
    variants:
        rs2336573: {effect_allele: T, coeff_value: 0.31}
        rs1131603: {effect_allele: C, coeff_value: 0.22}
        rs3742801: {effect_allele: T, coeff_value: 0.053}
        rs2270655: {effect_allele: G, coeff_value: 0.11}
        rs12272669: {effect_allele: A, coeff_value: 0.51}
        rs34324219: {effect_allele: C, coeff_value: 0.24}
        rs7788053: {effect_allele: A, coeff_value: 0.046}
        rs602662: {effect_allele: A, coeff_value: 0.17}
        rs1801222: {effect_allele: G, coeff_value: 0.12}
        rs41281112: {effect_allele: C, coeff_value: 0.19}
        rs1141321: {effect_allele: C, coeff_value: 0.071}
description:
  about: Vitamin B12 is found in several animal-based foods and supplements. Vitamin
    B12 is important for nerve cells, making DNA, methylation, and helping cells,
    like red blood cells, divide. Low vitamin B12 levels can be a risk factor for
    a form of anemia (a type of blood disease) and many other serious health issues.  People
    with a condition called pernicious anemia or certain digestive conditions don't
    absorb vitamin B12 as well. People who follow a strict or highly plant-based diet
    may be more likely to have lower vitamin B12 levels.
  category_on_the_left_side_of_plot: High risk
  genes:
  - AC005076.1
  - AC005519.1
  - AC093864.1
  - CD320
  - CLYBL
  - CUBN
  - FUT2
  - MMUT
  - SNRPCP14
  - TCN1
  - TCN2
  how_your_result_was_calculated: Your result is a risk based on 11 genetic variants,
    specifically called SNPs, found in genes associated with blood vitamin B12 level.
    Overall, this explains about 6% of the the genetic variation in vitamin B12 level
    in the human population, based on current scientific knowledge.
  other_factors:
  - content: Many animal based foods.  Along with a focus on red meat, poultry, fish,
      eggs, dairy products, and supplementation.
    title: Diet
  - content: People with pernicious anemia may not absorb vitamin B12 from foods and
      dietary supplements as well. People who have had stomach surgery to lose weight
      or who have digestive conditions (like celiac disease or Crohn's disease) may
      also not absorb vitamin B12 as well.
    title: Other Health Issues
  - content: Supplements often contain vitamin B12 as either methylocobalmin (the
      more effective, healthier version) or cyancocobalamin.  The "cyano" comes from
      cyanide and is the cheaper, less effective form
    title: Supplements
  - content: People who consume no or limited animal-based foods or drinks, such as
      vegans and some vegetarians, may have lower vitamin B12 levels.
    title: Lifestyle
  other_factors_statement: Many factors impact vitamin B12 levels, including diet,
    lifestyle, age, other health issues, and genetics. Genetics has a moderately high
    overall impact on vitamin B12 level when you consider all the other factors.
  references:
    article_links: []
    website_text_data:
    - accessed: Accessed June 21, 2019
      links: https://ods.od.nih.gov/factsheets/VitaminB12-Consumer/
      title: '"Vitamin B12 Fact Sheet for Consumers" from National Institutes of Health'
  result_statement_choice:
    Average risk: Based on the genetic factors tested, you are more likely to have
      an average blood vitamin B12 level.
    High risk: Based on the genetic factors tested, you are more likely to have a
      lower blood vitamin B12 level.
    Low risk: Based on the genetic factors tested, you are more likely to have a higher
      blood vitamin B12 level.
  science_behind_the_test: "Vitamin B12 blood level is moderately heritable, with\
    \ h\xB2 estimates of 27-59% [1]. Our test is based on results of the genome wide\
    \ association study (GWAS) [1], which included almost 50 000 individuals of European\
    \ ancestry. The study identified 11 variants significantly associated with cobalamin\
    \ blood concentration. Together these SNPs explain about 6% of between people\
    \ variance in this vitamin level.\nOur analyses use genetic data (genotypes) for\
    \ those SNPs to calculate the polygenic score."
  science_behind_your_result: Your results are based on genetic studies published
    in peer-revieved scientific and medical articles. Sometimes, the effects of many
    different genetic factors are combined into one risk estimate.
  test_type: Polygenic Risk Score
  trait: Vitamin B12
  trait_authors:
  - Katarzyna Tomala
  trait_copyright: Intelliseq all rights reserved
  trait_expiry: 10.03.2021
  trait_explained: 0.063
  trait_heritability: 0.59
  trait_pmids:
  - '23754956'
  trait_science: Blood level of vitamin B12 is partially controlled by the genetic
    factors. Our analyses included 11 genetic variants, which are know to influence
    concentration of this vitamin in the blood. Together, they explain about 6% of
    differences in cobalamin level observed among people.
  trait_short_description: Blood vitamin B12 level
  trait_snp_heritability: null
  trait_test_details: We determined your genotype at all the genomic positions, which
    are known to influence vitamin B12 blood level. Next, we summed up the effects
    of all the individual SNPs and in this way calculated your polygenic risk score.
    The estimated effect of the genetic factors may be enhanced or alleviated by the
    diet and lifestyle.
  trait_title: Blood_vitamin_B12
  trait_version: '1.0'
  variants: []
  what_you_can_do_choice:
    Average risk:
    - content: Other factors besides genetics may impact your risk for a low vitamin
        B12 level. Consider talking to your practitioner about tests or other ways
        to check your vitamin B12 level, and possible next steps you can take.
      title: Talk to Your Practitioner
    - content: Two foods, beef liver and clams, are naturally rich sources of vitamin
        B12. Other animal-based foods also naturally contain vitamin B12. These include
        fish, dark meats, chicken, turkey, eggs, milk, and other dairy products. If
        you consume no or limited animal-based foods or drinks, like vegans and some
        vegetarians, you may have lower vitamin B12 levels.
      title: Diet
    - content: (B12) supplements can help raise vitamin B12 to recommended daily levels,
        if needed. Requirements will vary by personal need.  These supplements should
        be used with care so you should discuss quality and dosage with your practitioner.
      title: Supplements
    High risk:
    - content: Your practitioner may be able to order tests or other ways to check
        your vitamin B12 level. Talk to them about this and possible next steps that
        you can take.
      title: Talk to Your Practitioner
    - content: Two foods, beef liver and clams, are naturally rich sources of vitamin
        B12. Other animal-based foods also naturally contain vitamin B12. These include
        fish, dark meats, chicken, turkey, eggs, milk, and other dairy products. If
        you consume no or limited animal-based foods or drinks, like vegans and some
        vegetarians, you may have lower vitamin B12 levels.
      title: Diet
    - content: (B12) supplements can help raise vitamin B12 to recommended daily levels,
        if needed. Requirements will vary by personal need.  These supplements should
        be used with care so you should discuss quality and dosage with your practitioner.
      title: Supplements
    - content: If you have pernicious anemia, you may not absorb vitamin B12 from
        foods and dietary supplements as well. If you have had stomach surgery to
        lose weight or have certain digestive conditions (like celiac disease or Crohn's
        disease), you may also not absorb vitamin B12 as well. Speak to your practitioner
        about how this factors into things, if your vitamin B12 levels are found to
        be low from other tests.
      title: Other Health Issues
    Low risk:
    - content: Other factors besides genetics may impact your risk for a low vitamin
        B12 level. Consider talking to your practitioner about tests to check your
        vitamin B12 level, and possible next steps you can take.
      title: Talk to Your Practitioner
    - content: Two foods, beef liver and clams, are naturally rich sources of vitamin
        B12. Other animal-based foods also naturally contain vitamin B12. These include
        fish, dark meats, chicken, turkey, eggs, milk, and other dairy products. If
        you consume no or limited animal-based foods or drinks, like vegans and some
        vegetarians, you may have lower vitamin B12 levels.
      title: Diet
  what_you_can_do_statement_choice:
    Average risk: There are things you can do to try to prevent low vitamin B12 levels
      and stay healthy. Talk to your practitioner about what is right for you.
    High risk: There are things you can do to try to prevent low vitamin B12 levels
      and stay healthy. Talk to your practitioner about what is right for you.
    Low risk: There are things you can do to try to prevent low vitamin B12 levels
      and stay healthy. Certain blood tests or other tests can also check your vitamin
      B12 level. Talk to your practitioner about what is right for you.
  what_your_result_means_choice:
    Average risk: Depending on your vitamin B12 level and other factors, you may have
      a higher risk for a form of anemia, issues with red blood cell formation, greater
      chance of birth defects, poor bone health, eye health problems, trouble with
      mood and depression, lower brain health, less energy, unhealthy skin and nails,
      and an increase homocysteine levels leading to other health issues.
    High risk: Depending on your vitamin B12 level and other factors, you may have
      a higher risk for a form of anemia, issues with red blood cell formation, greater
      chance of birth defects, poor bone health, eye health problems, trouble with
      mood and depression, lower brain health, less energy, unhealthy skin and nails,
      and an increase homocysteine levels leading to other health issues.
    Low risk: Depending on your vitamin B12 level and other factors, you may have
      a higher risk for a form of anemia, issues with red blood cell formation, greater
      chance of birth defects, poor bone health, eye health problems, trouble with
      mood and depression, lower brain health, less energy, unhealthy skin and nails,
      and an increase homocysteine levels leading to other health issues.

