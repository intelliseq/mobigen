from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-0.2546, category_name='Low risk'),
        QuantitativeCategory(from_=-0.2546, to=0.2471, category_name='Average risk'),
        QuantitativeCategory(from_=0.2471, category_name='High risk')
    ],
    snips_and_coefficients={
        'rs1801133': ModelData(effect_allele='A', coeff_value=0.1583),
        'rs2275565': ModelData(effect_allele='T', coeff_value=0.0542),
        'rs1047891': ModelData(effect_allele='A', coeff_value=0.0864),
        'rs9369898': ModelData(effect_allele='A', coeff_value=0.0449),
        'rs7130284': ModelData(effect_allele='T', coeff_value=-0.1242),
        'rs154657': ModelData(effect_allele='A', coeff_value=0.0963),
        'rs234709': ModelData(effect_allele='T', coeff_value=-0.0718),
        'rs4660306': ModelData(effect_allele='T', coeff_value=0.0435),
        'rs548987': ModelData(effect_allele='C', coeff_value=0.0597),
        'rs42648': ModelData(effect_allele='A', coeff_value=-0.0395),
        'rs1801222': ModelData(effect_allele='A', coeff_value=0.0453),
        'rs2251468': ModelData(effect_allele='A', coeff_value=-0.0512),
        'rs838133': ModelData(effect_allele='A', coeff_value=0.0422),
        'rs12134663': ModelData(effect_allele='A', coeff_value=-0.101),
        'rs12780845': ModelData(effect_allele='A', coeff_value=0.0529),
        'rs957140': ModelData(effect_allele='A', coeff_value=-0.045),
        'rs12921383': ModelData(effect_allele='T', coeff_value=-0.09),
        'rs2851391': ModelData(effect_allele='T', coeff_value=0.056)
    },
    model_type='beta'
)
