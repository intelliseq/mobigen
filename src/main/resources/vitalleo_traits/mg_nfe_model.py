from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=-0.0579, category_name='High risk'),
        QuantitativeCategory(from_=-0.0579, to=-0.0315, category_name='Average risk'),
        QuantitativeCategory(from_=-0.0315, category_name='Low risk')

    ],
    snips_and_coefficients={
        'rs4072037': ModelData(effect_allele='C', coeff_value=-0.01),
        'rs13146355': ModelData(effect_allele='G', coeff_value=-0.005),
        'rs11144134': ModelData(effect_allele='T', coeff_value=-0.011),
        'rs3925584': ModelData(effect_allele='C', coeff_value=-0.006),
        'rs448378': ModelData(effect_allele='G', coeff_value=-0.004)
    },
    model_type='beta'
)
