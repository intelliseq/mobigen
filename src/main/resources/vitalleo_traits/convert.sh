#!/bin/bash
IFS=''

# Read the input
while read -r line; do
  # Extract the category name and the range
  if [[ $line =~ "Average risk" ]]; then
    printf "$line" | sed -r 's/^([[:blank:]]*).*/\1/'
    category_name=$(echo "$line" | sed -r 's/.*category_name: ([^}]*).*/\1/')
    range=$(echo "$line" | sed -r 's/.*(from: [^,]*, to: [^,]*).*/\1/')
    echo "$category_name: {$range}"
  elif [[ $line =~ "Low risk" ]]; then
    printf "$line" | sed -r 's/^([[:blank:]]*).*/\1/'
    category_name=$(echo "$line" | sed -r 's/.*category_name: ([^}]*).*/\1/')
    range=$(echo "$line" | sed -r 's/.*(from: [^,]*, to: [^,]*|from: [^,]*|to: [^,]*).*/\1/')
    echo "$category_name: {$range}"
  else
    echo "$line"
  fi
done
