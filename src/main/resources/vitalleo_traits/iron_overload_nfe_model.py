from src.main.python.lib.model.seqql import PolygenicRiskScore
from src.main.python.lib.model.seqql import ModelData
from src.main.python.lib.model.category import QuantitativeCategory

trait_was_prepared_for_population = "nfe"

model = PolygenicRiskScore(
    categories=[
        QuantitativeCategory(to=1.1, category_name='Low risk'),
        QuantitativeCategory(from_=1.1, to=1.9, category_name='Average risk'),
        QuantitativeCategory(from_=1.9, category_name='High risk')
    ],
    snips_and_coefficients={
        'rs1799945': ModelData(effect_allele='G', coeff_value=0.6),
        'rs1800562': ModelData(effect_allele='A', coeff_value=1.0)
    },
    model_type='beta'
)
