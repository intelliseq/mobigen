#!/usr/bin/env bash

cd ../../data
if ! [[ -d "allele_freq_data" ]]; then
  mkdir allele_freq_data
fi
cd allele_freq_data

for chrom in {1..22} X Y; do
  fname=gnomad.genomes.r2.1.1.sites.${chrom}.vcf.bgz
  if ! [[ -f ${fname} ]]; then
    aria2c -x4 https://storage.googleapis.com/gnomad-public/release/2.1.1/vcf/genomes/${fname}
  fi
done