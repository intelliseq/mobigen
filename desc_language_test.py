import glob
import os
import importlib
import inspect
from typing import List

module_path = os.path.abspath(os.path.dirname(__file__))
dir_hierarhy = ['src', 'main', 'resources', 'vitalleo_traits'] # now tests are run tor vitalleo_traits only
traits_path = os.path.join(module_path, os.path.join(*dir_hierarhy))


class InconsistentCategoriesError(RuntimeError):
    pass

class FieldsNotPresentError(RuntimeError):
    pass


def test_importability(module_str: str):
    module = importlib.import_module(module_str)
    return module


def test_fields_present(module, beta_module_fields: List[str], or_module_fields: List[str]):
    """
    Test, whether a module has all fields it should have
    :param module:
    :param beta_module:
    :param or_module:
    :return:
    """
    if 'get_category' in module.__dict__ and 'beta' in module.__dict__:
        sub_test_fields_present(module, beta_module_fields)
    elif 'get_category' in module.__dict__ and 'odds_ratio' in module.__dict__:
        sub_test_fields_present(module, or_module_fields)
    elif 'get_value' in module.__dict__ and 'beta' in module.__dict__:
        sub_test_fields_present(module, beta_module_num_fields)
    elif 'get_value' in module.__dict__ and 'odds_ratio' in module.__dict__:
        sub_test_fields_present(module, or_module_num_fields)
    else:
        raise RuntimeError


def sub_test_fields_present(module, fields:List[str]):
    if not all([field in dir(module) for field in fields]):
        raise FieldsNotPresentError([field for field in fields if field not in dir(module)])


def test_classes_consistence(module):
    categories = []
    for priority_value in module.model.priority_conditions_action_dict.values():
        category_objects = inspect.getclosurevars(priority_value['action']).nonlocals['categories']
        categories.extend([category_object.name for category_object in category_objects])

    if not (set(module.__dict__['what_your_result_means'].keys()) == set(module.__dict__['result_statement'].keys()) ==
            set(module.__dict__['what_you_can_do_statement'].keys()) ==
            set(module.__dict__['what_you_can_do'].keys()) == set(categories)):
        raise InconsistentCategoriesError(set(module.__dict__['what_your_result_means'].keys()), set(module.__dict__['result_statement'].keys()),
            set(module.__dict__['what_you_can_do_statement'].keys()),
            set(module.__dict__['what_you_can_do'].keys()), set(categories))


if __name__ == '__main__':
    trait_file_endswith = '_model.py'
    trait_file_endswith_no_check = '_model_roboczy.py'
    beta_module = importlib.import_module('src.main.resources.traits_additional_files.pusty_beta_model')
    or_module = importlib.import_module('src.main.resources.traits_additional_files.pusty_or_model')
    beta_module_num = importlib.import_module('src.main.resources.traits_additional_files.pusty_beta_liczbowy_model')
    or_module_num = importlib.import_module('src.main.resources.traits_additional_files.pusty_or_liczbowy_model')
    beta_module_fields = dir(beta_module)
    or_module_fields = dir(or_module)
    beta_module_num_fields = dir(beta_module_num)
    or_module_num_fields = dir(or_module_num)
    for trait_file in glob.glob(os.path.join(traits_path, '*')):
        if os.path.isdir(trait_file):
            pass
        elif os.path.split(trait_file)[-1] == '__init__.py':
            pass
        elif trait_file.endswith(trait_file_endswith):
            f_name = os.path.basename(trait_file)
            print('Processing file {}'.format(f_name))
            module_str = '{}.{}'.format('.'.join(dir_hierarhy), f_name.split('.')[0])
            module = test_importability(module_str)
            print(module_str)
            test_fields_present(module, beta_module_fields, or_module_fields)
            if 'get_category' in module.__dict__:
                test_classes_consistence(module)
            elif 'get_value' in module.__dict__:
                pass
            else:
                raise RuntimeError
        else:
            raise RuntimeError('Improper file {} in {} directory'.format(trait_file, traits_path))
