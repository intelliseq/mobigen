#1 Mobigen

## How to buid the container
Run in directory containing Dockerfile:
```bash
docker build --no-cache -t intelliseqarray/desc_language:1.2 . -f src/main/docker/desc_lang/1.2/Dockerfile
```
## How to run the container
```bash
docker run -v path/to/the/data/directory:/app_data/ intelliseqarray/desc_language:1.2 --in_dir input_directory --log_dir log_directory --out_dir output_directory
```
for example:
```bash
docker run -v /home/wojtek/PycharmProjects/imputing/example_data/imputed_genotype:/app_data/ intelliseqarray/desc_language:1.2 --in_dir /input/imputing_out --out_dir /input/json_docker --log_dir /input/mobigen_logs_docker  --curated_initial_vcf /input/input_out/curated_input_vcf.vcf
```
or when you wanna use background allele frequency for a distinct population (in this case latino):
```bash
docker run -v /home/wojtek/PycharmProjects/imputing/example_data/imputed_genotype:/app_data/ intelliseqarray/desc_language:1.2 --in_dir /app_data --log_dir /app_data/log_directory --out_dir /app_data/output_directory --population amr
```

for help run:
```bash
docker run intelliseqarray/desc_language:1.a -h
```
### Models (description outdated)
odds_ratio and beta are twin functions that accept rsID, effect allele, as well as odds_ratio/beta value for the 
allele and returns another function. This function computes odds_ratio/beta values for given data and
allele frequency in given population. Data is dictionary similar to 
```python
{'rs12134456': 'G|G', 'rs1805007': 'C|C'}
```
Allele frequency is another dictionary looking as follows:
```python
{
    'rs9847240': {'A': 0.2, 'C': 0.3, 'G': 0.4, 'T': 0.1},
    'rs2025905': {'A': 0.25, 'C': 0.35, 'G': 0.3, 'T': 0.1},
}
```