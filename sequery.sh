#!/usr/bin/env bash
# explicitly activate virtualenv assuming it is placed in .venv directory
if [ ! -d venv ] ; then
    virtualenv venv -p/usr/bin/python3.6
    source venv/bin/activate
    pip install PyVCF numpy namedlist
    deactivate
fi

source venv/bin/activate
cat data/246_3.vcf | python src/main/python/hair_color.py
deactivate
